import sys

import boto3
import os

# Credentials & Region
access_key = os.environ['AWS_ACCESS_KEY_ID']
secret_key = os.environ['AWS_SECRET_ACCESS_KEY']
region = os.environ['AWS_REGION']
accountId = os.environ['AWS_ACCOUNT_ID']


def create_boto3_client(service_name):
    return boto3.client(
        service_name=service_name,
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
        region_name=region
    )


# AWS RDS client
rds_client = create_boto3_client('rds')

# AWS SQS client
sqs_client = boto3.client(
    'sqs',
    aws_access_key_id=access_key,
    aws_secret_access_key=secret_key,
    region_name=region,
    endpoint_url=f'https://sqs.{region}.amazonaws.com'
);

kafka_client = create_boto3_client('kafka')


def get_rds_instance_endpoint(rds_instance_name):
    instances = rds_client.describe_db_instances(
        DBInstanceIdentifier=rds_instance_name
    )['DBInstances']

    if len(instances) == 0:
        raise Exception(f"Missing RDS instance {rds_instance_name}")

    endpoint_data = instances[0]['Endpoint']

    return f"{endpoint_data['Address']}:{endpoint_data['Port']}"


def get_kafka_cluster(kafka_cluster_name):
    clusters = kafka_client.list_clusters(
        ClusterNameFilter=kafka_cluster_name
    )['ClusterInfoList']
    if len(clusters) == 0:
        raise Exception(f"Missing kafka cluster {kafka_cluster_name}")
    return clusters[0]


def get_kafka_brokers(kafka_cluster_name):
    cluster = get_kafka_cluster(kafka_cluster_name)
    brokers = kafka_client.get_bootstrap_brokers(
        ClusterArn=cluster['ClusterArn']
    )
    return brokers['BootstrapBrokerString']
    # return brokers['BootstrapBrokerStringTls']


def get_queue_url(queue_name):
    return sqs_client.get_queue_url(
        QueueName=queue_name
    )['QueueUrl']


def print_kafka_broker_string(cluster_name):
    print(get_kafka_brokers(cluster_name))


def print_queue_url(queue_name):
    queue_url = sqs_client.get_queue_url(
        QueueName=queue_name
    )['QueueUrl']
    print(queue_url)

def get_rds_instance_endpoint_from_stack(app_name):
    return get_rds_instance_endpoint(f"{app_name}-db-instance")

def print_rds_instance_endpoint(stack_name):
    rds_instance_endpoint = get_rds_instance_endpoint(f"{stack_name}-db-instance")
    print(rds_instance_endpoint)
