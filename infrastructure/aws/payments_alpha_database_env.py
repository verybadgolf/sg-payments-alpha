import os

from payments_alpha_stack_env import app_name, branch_name

# DB Details
branch_db_name = f"{branch_name}db".replace("-", "")
db_stack_name = f"{app_name}-db"
rds_instance_name = db_stack_name + "-instance"
rds_master_db_name = "masterdb"
rds_master_user = os.environ['RDS_MASTER_USER']
rds_master_user_password = os.environ['RDS_MASTER_USER_PASSWORD']




