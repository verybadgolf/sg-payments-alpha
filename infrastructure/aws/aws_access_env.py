import os

# AWS Access Credentials and Account Details
account_id = os.environ['AWS_ACCOUNT_ID']
region = os.environ['AWS_REGION']
access_key = os.environ['AWS_ACCESS_KEY_ID']
secret_key = os.environ['AWS_SECRET_ACCESS_KEY']
