import sys

sys.path.insert(0, "../..")

from payments_alpha_stack_env import file_upload_bucket_name
from payments_alpha_common import empty_s3_bucket


# Teardown any changes that would prevent the stack being destroyed
print("Emptying file upload S3 bucket")
empty_s3_bucket(file_upload_bucket_name)
print("Emptied file upload S3 bucket")
