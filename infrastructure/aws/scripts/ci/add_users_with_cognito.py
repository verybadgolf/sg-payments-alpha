import yaml
import sys

from yaml import Loader

sys.path.insert(0, "../..")

from payments_alpha_common import *
from payments_alpha_stack_env import app_name, branch_name

user_pool_stack_name = f"{app_name}-{branch_name}-user-pool"
cognito_user_pool_id = get_resource_id_from_stack_output(user_pool_stack_name, f"{branch_name}-UserPoolId")
cognito_client_id = get_resource_id_from_stack_output(user_pool_stack_name, f"{branch_name}-UserPoolUiClientId")

## This file allows the creation of users directly with cognito - this bypasses the standard password authentication flow
## for the creation of users

def create_cognito_group_if_not_exists(group_name):
    groups = cognito_idp_client.list_groups(
        UserPoolId=cognito_user_pool_id,
        Limit=60
    )['Groups']
    filtered_groups = list(filter(lambda g: g['GroupName'] == group_name, groups))
    if len(filtered_groups) > 0:
        print(f"Group {group_name} already exists")
    else:
        print(f"Creating group {group_name}")
        cognito_idp_client.create_group(
            GroupName=group_name,
            UserPoolId=cognito_user_pool_id
        )
    return group_name

def add_users_to_group(user_name, group_name):
    cognito_idp_client.admin_add_user_to_group(
        UserPoolId=cognito_user_pool_id,
        Username=user_name,
        GroupName=group_name
    )
    print(f"User {user_name} added to group {group_name}")

def create_user_in_cognito(user, password):
    user_name = user["name"]
    users = cognito_idp_client.list_users(
        UserPoolId=cognito_user_pool_id,
        Limit=60
    )['Users']
    filtered_groups = list(filter(lambda g: g['Username'] == user_name, users))
    if len(filtered_groups) > 0:
        print(f"User {user_name} already exists")
    else:
        cognito_idp_client.admin_create_user(
            UserPoolId=cognito_user_pool_id,
            Username=user_name,
            UserAttributes=[
                {
                    'Name': 'email',
                    'Value': user["email"]
                },
                {
                    'Name': 'custom:permitted_actions',
                    'Value': user["actions"]

                }],
            TemporaryPassword=password,
            MessageAction='SUPPRESS',
            DesiredDeliveryMediums=['EMAIL'],
        )
        cognito_idp_client.admin_set_user_password(
            UserPoolId=cognito_user_pool_id,
            Username= user["name"],
            Password=password,
            Permanent=True
        )

def __add_user(userconfig, email, password):

    rolesconfig = userconfig["roles"]
    groups = []
    actionsSet = set()
    for role in rolesconfig:
        parsedRole = role.split('\\')
        groups.append(parsedRole[0])
        actionsSet.add(parsedRole[1])

    actionsStr = ";".join(actionsSet)
    user = {
        "name" : userconfig['name'],
        "email" : email,
        "actions" : actionsStr
    }
    print(f"**** creating user = {user}  **** ")
    create_user_in_cognito(user, password)
    for group in groups:
        if(group):
            create_cognito_group_if_not_exists(group)
            add_users_to_group(user["name"], group)

def read_users_file(user, email, password):
    with open(user, 'r') as outfile:
        values = yaml.load(outfile, Loader=Loader)
        users = values['users']
        [__add_user(user, email, password) for user in users]


## pass in YAML file with user info, e-mail address for all users and password for all users
filename = sys.argv[1]
email = sys.argv[2]
password = sys.argv[3]

read_users_file(filename, email, password)