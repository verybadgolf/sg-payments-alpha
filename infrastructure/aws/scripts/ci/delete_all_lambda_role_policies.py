import sys

sys.path.insert(0, "../..")

from payments_alpha_stack_env import policy_name, lambda_name
from payments_alpha_common import iam_client
from scripts.ci.add_sqs_policy_to_lambda_role import get_lambda_by_name, get_role_name_by_arn
from scripts.ci.delete_lambda_policy import delete_policy

def delete_all_role_policies(role_name):
    response = iam_client.list_role_policies(
        RoleName=role_name
    )
    for policy in response['PolicyNames']:
        delete_policy(policy, role_name)


lambda_function = get_lambda_by_name(lambda_name)
if lambda_function is not None:
    lambda_role_arn = lambda_function['Role']
    lambda_role_name = get_role_name_by_arn(lambda_role_arn)
    delete_all_role_policies(lambda_role_name)
