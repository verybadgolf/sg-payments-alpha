import sys
import json

sys.path.insert(0, "../..")

from aws_access_env import account_id
from payments_alpha_common import sqs_client, lambda_client, iam_client
from payments_alpha_stack_env import lambda_name, sqs_queue_name, policy_name


def get_sqs_url_by_name(sqs_queue_name):
    response = sqs_client.get_queue_url(
        QueueName=sqs_queue_name,
        QueueOwnerAWSAccountId=account_id
    )
    return response['QueueUrl']


def get_arn_from_queue_url(queue_url):
    response = sqs_client.get_queue_attributes(
        QueueUrl=queue_url,
        AttributeNames=['QueueArn']
    )
    return response['Attributes']['QueueArn']


def get_lambda_by_name(lambda_name):
    response = lambda_client.list_functions()
    for lambda_function in response['Functions']:
        if lambda_function['FunctionName'] == lambda_name:
            return lambda_function


sqs_permissions = [
    "sqs:GetQueueUrl",
    "sqs:SendMessage"
]


def create_policy(sqs_arn):
    return {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "VisualEditor0",
                "Effect": "Allow",
                "Action": "sqs:ListQueues",
                "Resource": "*"
            },
            {
                "Sid": "VisualEditor1",
                "Effect": "Allow",
                "Action": sqs_permissions,
                "Resource": sqs_arn
            }
        ]
    }


def submit_policy(policy, policy_name, role_name):
    return iam_client.put_role_policy(
        RoleName=role_name,
        PolicyName=policy_name,
        PolicyDocument=json.dumps(policy)
    )


def get_role_name_by_arn(role_arn):
    roles = iam_client.list_roles()
    for role in roles['Roles']:
        if role['Arn'] == role_arn:
            return role['RoleName']


def main():
    queue_url = get_sqs_url_by_name(sqs_queue_name)
    queue_arn = get_arn_from_queue_url(queue_url)
    policy = create_policy(queue_arn)
    lambda_function = get_lambda_by_name(lambda_name)
    lambda_role_arn = lambda_function['Role']
    lambda_role_name = get_role_name_by_arn(lambda_role_arn)
    submit_policy(policy, policy_name, lambda_role_name)


if __name__ == "__main__":
    main()
