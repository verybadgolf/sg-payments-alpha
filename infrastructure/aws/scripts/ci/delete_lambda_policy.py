import sys
sys.path.insert(0, "../..")

from payments_alpha_stack_env import policy_name, lambda_name
from payments_alpha_common import iam_client
from scripts.ci.add_sqs_policy_to_lambda_role import get_lambda_by_name, get_role_name_by_arn


def delete_policy(policy_name, role_name):
    iam_client.delete_role_policy(
        RoleName=role_name,
        PolicyName=policy_name
    )


def does_role_have_policy(policy_name, role_name):
    response = iam_client.list_role_policies(
        RoleName=role_name
    )
    for policy in response['PolicyNames']:
        if policy == policy_name:
            return True
    return False


def main():
    lambda_function = get_lambda_by_name(lambda_name)
    if lambda_function is not None:
        lambda_role_arn = lambda_function['Role']
        lambda_role_name = get_role_name_by_arn(lambda_role_arn)
        if does_role_have_policy(policy_name, lambda_role_name):
            delete_policy(policy_name, lambda_role_name)


if __name__ == "__main__":
    main()
