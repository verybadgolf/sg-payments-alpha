import boto3
import sys


sys.path.insert(0, "../../..")

from aws_access_env import account_id, access_key, secret_key
from payments_alpha_stack_env import environment_name

datasourcename=f"{environment_name}_datasource"
quicksight_group_name = 'sg-payments-reports'

quicksight_client_us = boto3.client(
    service_name='quicksight',
    aws_access_key_id=access_key,
    aws_secret_access_key=secret_key,
    region_name='us-east-1'
)

def create_group():
    response = quicksight_client_us.create_group(
        GroupName=quicksight_group_name,
        Description='A group for sg-payments reporting',
        AwsAccountId=account_id,
        Namespace='default'
    )
    print(response)

def add_user_to_group(username):
    response = quicksight_client_us.create_group_membership(
        GroupName=quicksight_group_name,
        MemberName=username,
        AwsAccountId=account_id,
        Namespace='default'
    )
    print(response)


user_name = sys.argv[1]
add_user_to_group(user_name)
