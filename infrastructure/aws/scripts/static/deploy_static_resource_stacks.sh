#!/usr/bin/env bash

echo -e "Deploying AWS resources..."

cd ../../cdk/static
cdk bootstrap aws://${AWS_ACCOUNT_ID}/${AWS_REGION}
cdk deploy ${app_name}-eks
cdk deploy ${app_name}-db
cd ../../scripts/static
