#bin/sh

echo "Deleting NGINX..."

helm init --upgrade --force-upgrade
if [[ $(helm ls -q) == *${app_name}-nginx* ]]; then
  helm delete ${app_name}-nginx
else
  echo "NGINX Helm release not found"
fi