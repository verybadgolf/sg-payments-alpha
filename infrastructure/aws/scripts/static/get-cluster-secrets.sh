#!/usr/bin/env bash

echo "Retrieving Kubernetes cluster details..."

ca_certificate_secret=$(kubectl get secret | grep default-token | awk '{print $1}')
ca_certificate=$(kubectl get secret $ca_certificate_secret -o jsonpath="{['data']['ca\.crt']}" | base64 --decode)

service_token_secret=$(kubectl -n kube-system get secret | grep k8s-admin-token | awk '{print $1}')
service_token=$(kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep $service_token_secret | awk '{print $1}'))

# Open file descriptor (fd) 3 for writing to a file
exec 3<> eks_cluster_secrets.txt

  echo -e "\nCLUSTER NAME:\n" >&3
  echo ${app_name}-eks-cluster >&3

  echo -e "\nMASTER API:\n" >&3
  kubectl cluster-info | grep master >&3

  echo -e "\nCA CERTIFICATE:\n" >&3
  echo -e "${ca_certificate}" >&3

  echo -e "\nSERVICE TOKEN:\n" >&3
  echo -e "${service_token}" >&3

# Close fd 3
exec 3<> eks_cluster_secrets.txt
cat eks_cluster_secrets.txt

echo -e "\nKubernetes cluster details retrieved successfully.\n"