# Params: $1 = k8s deployment name
delete_kubernetes_deployment() {

  echo "Deleting Kubernetes deployment $1..."

  if [[ $(kubectl get deployment --namespace=kube-system) == *$1 ]]; then
    kubectl delete deployment $1 --namespace=kube-system
  else
    echo "Kubernetes deployment $1 not found"
  fi
}