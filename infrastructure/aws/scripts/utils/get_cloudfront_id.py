import sys
sys.path.insert(0, "../..")

from payments_alpha_common import get_resource_id_from_stack_output
from payments_alpha_stack_env import frontend_stack_name

def get_stack_id(stack_name, cloudfront_logical_id):
    logical_id = get_resource_id_from_stack_output(stack_name, cloudfront_logical_id)
    print(logical_id)


get_stack_id(frontend_stack_name, f"{frontend_stack_name}-CloudFrontDistributionId")
