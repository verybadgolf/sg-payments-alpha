#!/usr/bin/env python3
import sys

from aws_cdk import core
from file_upload_stack import FileUploadStack

app = core.App()
FileUploadStack(app, "file-resources")
app.synth()
