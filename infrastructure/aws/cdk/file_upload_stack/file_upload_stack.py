import sys
import os

from aws_cdk.core import CfnOutput, CfnTag, Construct, Environment, Stack
from aws_cdk.aws_sns import Topic
from aws_cdk.aws_transfer import CfnServer

sys.path.insert(0, "../..")

from aws_access_env import account_id, region
from payments_alpha_stack_env import app_name, base_name, branch_name

file_upload_topic_name = f"{app_name}-file-event-topic"
ftp_server_name = f"{app_name}-ftp-server"

class FileUploadStack(Stack):

    def __init__(self, scope: Construct, stack_name: str) -> None:
        super().__init__(
            scope=scope,
            name=stack_name,
            env=Environment(region=region, account=account_id),
            tags={'Project': app_name, 'Branch': branch_name}
        )

        file_event_topic = Topic(
            scope=self,
            id=file_upload_topic_name,
            display_name=file_upload_topic_name,
            topic_name=file_upload_topic_name
        )

        sftp_server = CfnServer(
            scope=self,
            id=f"{app_name}-sftp-server",
            identity_provider_type='SERVICE_MANAGED',
            tags=[
                CfnTag(
                    key='Name',
                    value=ftp_server_name
                )
            ]
        )

        CfnOutput(
            scope=self,
            id=f"{base_name}-sns-topic-arn-output",
            export_name="FileEventTopicArn",
            value=file_event_topic.topic_arn,
            description=f"File Event Topic ARN for the {stack_name} stack"
        )

        CfnOutput(
            scope=self,
            id=f"{base_name}-sftp_server-output",
            export_name="SFTPServerId",
            value=sftp_server.attr_server_id,
            description=f"SFTP Server id for the {stack_name} stack"
        )
