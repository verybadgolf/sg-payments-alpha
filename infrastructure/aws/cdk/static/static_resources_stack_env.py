import os

app_name=os.environ['app_name']
aws_account_id=os.environ['AWS_ACCOUNT_ID']
region=os.environ['AWS_REGION']

# EKS Stack Details
eks_stack_name=f"{app_name}-eks"
eks_cluster_name=f"{app_name}-eks-cluster"
eks_node_group_name=f"{app_name}-node-group"
eks_node_type="t3.large"
eks_default_port=8080
eks_masters_role_arn="arn:aws:iam::" + aws_account_id + ":role/EKSMastersRole"

# QuickSight DB Stack Details
db_stack_name = f"{app_name}-db"
rds_instance_name = db_stack_name + "-instance"
rds_master_db_name = "masterdb"
rds_master_user = os.environ['RDS_MASTER_USER']
rds_master_user_password = os.environ['RDS_MASTER_USER_PASSWORD']