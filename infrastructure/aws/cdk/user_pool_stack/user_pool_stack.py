import sys
from aws_cdk.aws_cognito import CfnUserPool, CfnUserPoolDomain, CfnUserPoolClient
from aws_cdk.core import Stack, Construct, Environment, CfnOutput

sys.path.insert(0, "../..")
from aws_access_env import account_id, region
from payments_alpha_stack_env import app_name, branch_name, domain_name, user_pool_name

service_client_name = "internal-communication"

class UserPoolStack(Stack):
    def __init__(self, scope: Construct, stack_name: str) -> None:
        super().__init__(
            scope=scope,
            name=stack_name,
            env=Environment(region=region, account=account_id),
            tags={'Project': app_name, 'Branch': branch_name}
        )

        action_schema = CfnUserPool.SchemaAttributeProperty(
            attribute_data_type= 'String',
            name='permitted_actions',
            mutable = True,
            required= False
        )
        email_schema = CfnUserPool.SchemaAttributeProperty(
            attribute_data_type= 'String',
            name='email',
            mutable = True,
            required= True
        )

        adminConfig = CfnUserPool.AdminCreateUserConfigProperty(
            allow_admin_create_user_only=True
        )

        user_pool = CfnUserPool(
            scope=self,
            id=f"{app_name}-user-pool",
            user_pool_name=user_pool_name,
            auto_verified_attributes=["email"],
            admin_create_user_config=adminConfig,
            schema=[email_schema, action_schema]
        )

        user_pool_ui_client =  CfnUserPoolClient(
            scope=self,
            client_name=f"{app_name}-ui",
            id=f"{branch_name}-user-pool-client-ui",
            user_pool_id = user_pool.ref,
            allowed_o_auth_flows_user_pool_client=True,
            allowed_o_auth_flows=["implicit", "code"],
            allowed_o_auth_scopes=["openid", "profile"],
            explicit_auth_flows=["ADMIN_NO_SRP_AUTH"],
            callback_ur_ls=[f"https://{branch_name}.{domain_name}/alpha", f"https://{branch_name}.{domain_name}/alpha/index.html"],
            supported_identity_providers=["COGNITO"]
        );

        internal_communication_client =  CfnUserPoolClient(
            scope=self,
            client_name=service_client_name,
            id=f"{branch_name}-user-pool-internal-client",
            explicit_auth_flows=["ADMIN_NO_SRP_AUTH"],
            user_pool_id = user_pool.ref,
            generate_secret=True
        );


        cognito_auth_domain = f"sg-{app_name}-{branch_name}"
        if branch_name == "master":
            cognito_auth_domain = f"sg-{app_name}-mstr"

        # TODO - store internal_communication_client.secret in secrets manager?
        domain_name_cognito = CfnUserPoolDomain(
            scope = self,
            id=f"{branch_name}-user-pool-domain",
            domain=cognito_auth_domain,
            user_pool_id = user_pool.ref,
        )

        CfnOutput(
            scope=self,
            id=f"{branch_name}-user-pool-id-output",
            export_name=f"{branch_name}-UserPoolId",
            value=user_pool.ref,
            description=f"User Pool Id for the {branch_name} {stack_name} stack"
        )

        CfnOutput(
            scope=self,
            id=f"{branch_name}-user-pool-ui-client-id-output",
            export_name=f"{branch_name}-UserPoolUiClientId",
            value=user_pool_ui_client.ref,
            description=f"User Pool Ui Client Id for the {branch_name} {stack_name} stack"
        )

        CfnOutput(
            scope=self,
            id=f"{branch_name}-service-to-service-ui-client-id-output",
            export_name=f"{branch_name}-UserPoolServiceToServiceClientId",
            value=internal_communication_client.ref,
            description=f"User Pool Ui Client Id for the {branch_name} {stack_name} stack"
        )
