#!/usr/bin/env python3
import sys

from aws_cdk import core
from lambda_stack import LambdaStack

sys.path.insert(0, "../..")
from aws_access_env import account_id, region
from payments_alpha_stack_env import app_name

app = core.App()
LambdaStack(app, f"{app_name}-lambda")
app.synth()
