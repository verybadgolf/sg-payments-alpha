import sys
import os

from aws_cdk.core import Construct, Environment, RemovalPolicy, Stack
from aws_cdk.aws_lambda import Function, Runtime, Code
from aws_cdk.aws_s3 import BlockPublicAccess, Bucket, BucketEncryption, EventType
from aws_cdk.aws_iam import ServicePrincipal
from aws_cdk import aws_s3_notifications

sys.path.insert(0, "../..")

from aws_access_env import account_id, region
from payments_alpha_stack_env import app_name, branch_name, ci_project_directory, file_upload_bucket_id, file_upload_bucket_name, lambda_name

class LambdaStack(Stack):

    def __init__(self, scope: Construct, stack_name: str) -> None:
        super().__init__(
            scope,
            stack_name,
            env=Environment(region=region, account=account_id),
            tags={'Project': app_name, 'Branch': branch_name}
        )

        bucket = Bucket(
            scope=self,
            id=file_upload_bucket_id,
            bucket_name=file_upload_bucket_name,
            encryption=BucketEncryption.KMS_MANAGED,
            block_public_access=BlockPublicAccess(
                block_public_acls=True,
                block_public_policy=True,
                ignore_public_acls=True,
                restrict_public_buckets=True
            ),
            versioned=True,
            removal_policy=RemovalPolicy.DESTROY  # or RETAIN?
        )

        lambda_function = Function(
            scope=self,
            id=f"{app_name}-file-upload-bucket-lambda",
            function_name=lambda_name,
            code=Code.asset(f'{ci_project_directory}/infrastructure/aws/scripts/ci'),
            handler='s3_notification_lambda.lambda_handler',
            runtime=Runtime.PYTHON_3_7,
        )

        lambda_function.add_permission(
            id=f"{app_name}-lambda-s3-permission",
            principal=ServicePrincipal("s3.amazonaws.com"),
            source_arn=bucket.bucket_arn,
            action='lambda:*',
            source_account=account_id
        )

        bucket.add_event_notification(
            event=EventType.OBJECT_CREATED,
            dest=aws_s3_notifications.LambdaDestination(lambda_function),
        )

        bucket.add_event_notification(
            event=EventType.OBJECT_REMOVED,
            dest=aws_s3_notifications.LambdaDestination(lambda_function),
        )
