import boto3

from aws_access_env import *

# Create AWS boto3 client / resource

def create_boto3_client(service_name):
    return boto3.client(
        service_name=service_name,
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
        region_name=region
    )

def create_boto3_resource(service_name):
    return boto3.resource(
        service_name=service_name,
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
        region_name=region
    )

cf_client = create_boto3_client('cloudformation')
cf_resource = create_boto3_resource('cloudformation')
cognito_idp_client = create_boto3_client('cognito-idp')
ec2_client = create_boto3_client('ec2')
iam_client = create_boto3_client('iam')
lambda_client= create_boto3_client('lambda')
rds_client = create_boto3_client('rds')
s3_client = create_boto3_client('s3')
s3_resource = create_boto3_resource('s3')
sqs_client = create_boto3_client('sqs')
transfer_client = create_boto3_client('transfer')
quicksight_client = create_boto3_client('quicksight')

# Common methods

def stack_is_in_state(stack_name, state_list):
    stacks = cf_client.list_stacks(
        StackStatusFilter=state_list
    )["StackSummaries"]
    filtered_stacks = list(filter(lambda s: s['StackName'] == stack_name, stacks))
    return len(filtered_stacks) > 0


def stack_exists(stack_name):
    return stack_is_in_state(
        stack_name,
        ['CREATE_IN_PROGRESS', 'CREATE_FAILED', 'CREATE_COMPLETE', 'ROLLBACK_IN_PROGRESS',
         'ROLLBACK_FAILED', 'ROLLBACK_COMPLETE', 'DELETE_IN_PROGRESS', 'DELETE_FAILED',
         'UPDATE_IN_PROGRESS', 'UPDATE_COMPLETE_CLEANUP_IN_PROGRESS',
         'UPDATE_COMPLETE', 'UPDATE_ROLLBACK_IN_PROGRESS', 'UPDATE_ROLLBACK_FAILED',
         'UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS', 'UPDATE_ROLLBACK_COMPLETE',
         'REVIEW_IN_PROGRESS']
    )


def get_resource_id_from_stack(stack_name, logical_id):
    stack_resource = cf_resource.StackResource(stack_name, logical_id)
    return stack_resource.physical_resource_id


def get_resource_id_from_stack_output(stack_name, export_name):
    stack = cf_resource.Stack(stack_name)
    output = list(filter(lambda c: ('ExportName' in c) and (c['ExportName'] == export_name), stack.outputs))
    if len(output) > 0:
        return output[0]['OutputValue']
    else:
        return 'Not found'


def get_kafka_cluster(kafka_cluster_name):
    clusters = kafka_client.list_clusters(
        ClusterNameFilter=kafka_cluster_name
    )['ClusterInfoList']
    if len(clusters) == 0:
        raise Exception(f"Missing kafka cluster {kafka_cluster_name}")
    return clusters[0]


def get_kafka_brokers(kafka_cluster_name):

    cluster = get_kafka_cluster(kafka_cluster_name)
    brokers = kafka_client.get_bootstrap_brokers(
        ClusterArn=cluster['ClusterArn']
    )
    return brokers['BootstrapBrokerString']


def cognito_user_exists(user_name, cognito_user_pool_id):
    users = cognito_idp_client.list_users(
        UserPoolId=cognito_user_pool_id
    )['Users']
    filtered_users = list(filter(lambda u: u['Username'] == user_name, users))
    return len(filtered_users) > 0


def get_cognito_oauth_scopes(resource_server_id):
    scopes = cognito_idp_client.describe_resource_server(
        UserPoolId=cognito_user_pool_id,
        Identifier=resource_server_id
    )['ResourceServer']['Scopes']
    return list(map(lambda s: s['ScopeName'], scopes))



def s3_bucket_exists(bucket_name):
    buckets = s3_client.list_buckets()['Buckets']
    filtered_buckets = list(filter(lambda u: u['Name'] == bucket_name, buckets))
    return len(filtered_buckets) > 0


def empty_s3_bucket(bucket_name):

    if not s3_bucket_exists(bucket_name):
        return

    bucket = s3_resource.Bucket(bucket_name)
    bucket.objects.all().delete()
    bucket.object_versions.delete()
