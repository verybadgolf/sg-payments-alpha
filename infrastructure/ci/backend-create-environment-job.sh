#!/usr/bin/env bash

set -eo pipefail

echo "K8s Namespace: $KUBE_NAMESPACE"
echo "proj path slug: $CI_PROJECT_PATH_SLUG"
echo "env slug: $CI_ENVIRONMENT_SLUG"

# Deploy the environment stacks
. ${CI_PROJECT_DIR}/infrastructure/aws/cdk/activate_cdk.sh

cd ${CI_PROJECT_DIR}/infrastructure/aws/cdk/user_pool_stack
cdk deploy payments-${CI_BRANCH_STR}-user-pool

cd ${CI_PROJECT_DIR}/infrastructure/aws/cdk/sqs_stack
cdk deploy payments-${CI_BRANCH_STR}-sqs

# Deactive the CDK venv
deactivate

cd ${CI_PROJECT_DIR}/infrastructure/aws/scripts/ci
python3 add_branch_folder_to_bucket.py
python3 add_sqs_policy_to_lambda_role.py
python3 create_database.py
python3 create_quicksight_datasource.py acl-config.yml
python3 add_users_with_cognito.py ci_user.yml ${CI_USER_EMAIL} ${CI_USER_PASSWORD}
## Add this line back in to create default users
#python3 add_default_users_cognito.py default_users.yml ${DEFAULT_USER_EMAIL} ${DEFAULT_USER_PASSWORD}

API_PATH=$CI_BRANCH_STR-api.$DOMAIN_NAME;
echo "Adding DNS Records: $API_PATH"
python3 -c "from add_route_53_ingress import create_nginx_dns_record; create_nginx_dns_record('$API_PATH')"

# Create kubernetes resources
cd ${CI_PROJECT_DIR}/infrastructure/kubernetes/scripts
# Configure Kubectl for EKS cluster
./configure-kubectl.sh
helm init --upgrade

## Create Kubernetes namespace if not exists
./create-kubernetes-namespace.sh $KUBE_NAMESPACE $CI_BRANCH_STR

## Set up Istio
# Label namespace for Istio injection"
# Temporarily disable istio-injection
kubectl label --overwrite namespace ${KUBE_NAMESPACE} istio-injection=disabled
helm upgrade --install --namespace=${KUBE_NAMESPACE}  ${CI_BRANCH_STR}-latest ../helm/environment --debug

#Create all required secrets
./create-kubernetes-secrets.sh

##populate kafka details
cd ${CI_PROJECT_DIR}/infrastructure/confluent
./ccloud_login.sh
./create_broker_configmap.sh