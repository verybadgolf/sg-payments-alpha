#!/usr/bin/env bash

set -eo pipefail

# Discover all active environments
cd ${CI_PROJECT_DIR}/infrastructure/aws/scripts/ci

eval ACTIVE_ENVIRONMENTS=($(python3 list_active_environments.py ${APP_NAME} | tr -d [],))

if [ -z "$ACTIVE_ENVIRONMENTS" ]
then
  echo "No active environments found."
  return 0
fi

echo Found ${#ACTIVE_ENVIRONMENTS[@]} active environments:
printf '%s\n' "${ACTIVE_ENVIRONMENTS[@]}"

cd ${CI_PROJECT_DIR}/infrastructure/kubernetes/scripts
# Configure Kubectl for EKS cluster
./configure-kubectl.sh

# Teardown all active environments
for ENVIRONMENT in ${ACTIVE_ENVIRONMENTS[@]}; do
  echo -e "\nTearing down ${ENVIRONMENT}"
  export CI_BRANCH_STR=${ENVIRONMENT}
  ${CI_PROJECT_DIR}/infrastructure/ci/delete-environment-job.sh || true
done

echo "Finished tearing down active environments."