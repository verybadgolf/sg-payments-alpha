#!/usr/bin/env bash

set -eo pipefail

# Deploy the environment stacks
. ${CI_PROJECT_DIR}/infrastructure/aws/cdk/activate_cdk.sh

cd ${CI_PROJECT_DIR}/infrastructure/aws/cdk/frontend_stack
cdk deploy payments-${CI_BRANCH_STR}-frontend

# Deactive the CDK venv
deactivate
