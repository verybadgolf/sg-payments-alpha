import argparse
import hashlib
import os
import yaml
from subprocess import check_output
from yaml import Loader


## SERVICE ACCOUNT ##

def create_service_account_if_not_exists(prefix, service_account):
    sa_name = get_hashed_service_account_name(service_account['name'])
    print(f"Checking if service account exists for {sa_name}")
    service_id = get_service_account_id(sa_name)
    if(service_id == None):
        sa_description = prefix+'-'+service_account['name']
        print(f"Creating service account {sa_name}")
        return create_service_account(sa_name, sa_description)
    return service_id.strip()

def get_service_account_ids(service_accounts):
    service_accounts_ids = {}
    for service_account in service_accounts:
        sa_name = get_hashed_service_account_name(service_account['name'])
        service_account_id = get_service_account_id(sa_name)
        service_accounts_ids[sa_name] = service_account_id
    return service_accounts_ids

def get_service_account_id(name):
    output = check_output("ccloud service-account list",shell=True).decode("utf-8")
    accounts = output.splitlines()[2:]
    for account in accounts:
        service_account = [x.strip() for x in account.split("|")]
        if(service_account[1] == name):
            return service_account[0]
    return None

def get_api_key(service_account_id):
    output = check_output("ccloud api-key list",shell=True).decode("utf-8")
    api_keys = output.splitlines()[2:]
    for api_key_row in api_keys:
        api_key_array = [x.strip() for x in api_key_row.split("|")]
        if(api_key_array[1] == service_account_id):
            return api_key_array[0]
    return None

## TOPIC ##

def does_topic_exist(topic_name):
    output = check_output("ccloud kafka topic list",shell=True).decode("utf-8")
    topic_list = [x.strip() for x in output.splitlines()[2:]]
    for topic in topic_list:
        if topic == topic_name:
            return True
    return False

def get_topic_list():
    output = check_output("ccloud kafka topic list",shell=True).decode("utf-8")
    accounts = list(map(lambda it: it.strip(), output.splitlines()))
    ## the first two element of our topic list are a header
    return accounts[2:]

## CLUSTER

def get_cluster_id_from_name(cluster_name):
    output = check_output("ccloud kafka cluster list",shell=True).decode("utf-8")
    # remove the header rows
    cluster_list = output.splitlines()[2:]
    for cluster in cluster_list:
        # split by columns
        cluster_info = [x.strip() for x in cluster.split("|")]
        if(cluster_info[1] == cluster_name):
            # remove indicating '*'
            return cluster_info[0].replace("*", "").strip()

## CREATION / DELETION

## We have to limit the service account to 32 characters so create a hash of the branch
## name at the start
def get_hashed_service_account_name(name):
    hash_prefix = hashlib.sha1(prefix.encode("utf-8")).hexdigest()[:9]
    hashed_name = hash_prefix +"-"+hashlib.sha1(name.encode("utf-8")).hexdigest()[:21]
    return hashed_name

def create_service_account(name, description):
    print(f"ccloud service-account create {name} --description {description}")
    output = check_output(f"ccloud service-account create {name} --description {description}",shell=True).decode("utf-8")
    for line in output.splitlines()[1:-1]:
        # remove first and list "|" and then split into columns
        service_info = [x.strip() for x in line[1:-1].split("|")]
        if(service_info[0] == "Id"):
            return service_info[1]
    return None

def create_acl_for_topic(service_account_id, operation, topic, is_prefix):
    if is_prefix:
        print(f"ccloud kafka acl create --allow --service-account-id {service_account_id} --operation {operation} --prefix --topic {topic}")
        check_output(f"ccloud kafka acl create --allow --service-account-id {service_account_id} --operation {operation} --prefix --topic {topic}",shell=True)
    else:
        print(f"ccloud kafka acl create --allow --service-account-id {service_account_id} --operation {operation} --topic {topic}")
        check_output(f"ccloud kafka acl create --allow --service-account-id {service_account_id} --operation {operation} --topic {topic}",shell=True)

def create_acl_for_group(service_account_id, operation, group, is_prefix):
    if is_prefix:
        print(f"ccloud kafka acl create --allow --service-account-id {service_account_id} --operation {operation} --prefix --consumer-group {group}")
        check_output(f"ccloud kafka acl create --allow --service-account-id {service_account_id} --operation {operation} --prefix --consumer-group {group}",shell=True)
    else:
        print(f"ccloud kafka acl create --allow --service-account-id {service_account_id} --operation {operation} --consumer-group {group}")
        check_output(f"ccloud kafka acl create --allow --service-account-id {service_account_id} --operation {operation} --consumer-group {group}",shell=True)

def create_topic(topic, partitions, config):
    if config is not None:
        config_entries = config.items()
        config_list = [i[0] + '=' + i[1] for i in config_entries]
        config_params = ','.join(config_list)
        print(f"ccloud kafka topic create {topic} --partitions {partitions} --config {config_params}")
        check_output(f"ccloud kafka topic create {topic} --partitions {partitions} --config {config_params}",shell=True)
    else:
        print(f"ccloud kafka topic create {topic} --partitions {partitions}")
        check_output(f"ccloud kafka topic create {topic} --partitions {partitions}",shell=True)

def delete_service_account(service_account_id):
    print(f"ccloud service-account delete {service_account_id} ")
    os.system(f"ccloud service-account delete {service_account_id}")

def delete_acl_for_topic(service_account_id, operation, topic, is_prefix):
    if is_prefix:
        print(f"ccloud kafka acl delete --allow --service-account-id {service_account_id} --operation {operation} --prefix --topic {topic}")
        os.system(f"ccloud kafka acl delete --allow --service-account-id {service_account_id} --operation {operation} --prefix --topic {topic}")
    else:
        print(f"ccloud kafka acl delete --allow --service-account-id {service_account_id} --operation {operation} --topic {topic}")
        os.system(f"ccloud kafka acl delete --allow --service-account-id {service_account_id} --operation {operation} --topic {topic}")

def delete_acl_for_group(service_account_id, operation, group, is_prefix):
    if is_prefix:
        print(f"ccloud kafka acl delete --allow --service-account-id {service_account_id} --operation {operation} --prefix --consumer-group {group}")
        os.system(f"ccloud kafka acl delete --allow --service-account-id {service_account_id} --operation {operation} --prefix --consumer-group {group}")
    else:
        print(f"ccloud kafka acl delete --allow --service-account-id {service_account_id} --operation {operation} --consumer-group {group}")
        os.system(f"ccloud kafka acl delete --allow --service-account-id {service_account_id} --operation {operation} --consumer-group {group}")

def delete_topic(topic):
    print(f"ccloud kafka topic delete {topic}")
    os.system(f"ccloud kafka topic delete {topic} ")

def create_topic_if_not_exist(prefix, name, num_partitions, config):
    topic_name = prefix + "-" + name
    if(not does_topic_exist(topic_name)):
        create_topic(topic_name, num_partitions, config)

def create_topic_acls(service_account_ids, prefix, topic):
    acl = topic['access_list']
    topic_name = prefix + "-" + topic['name']
    is_prefix = 'prefix' in topic
    for service_access in acl:
        permissions, service_account_id = __get_sa_id_and_permissions(service_access, service_account_ids)
        [create_acl_for_topic(service_account_id, access, topic_name, is_prefix) for access in permissions]

def delete_topic_if_exists(prefix, topic):
    topic_name = prefix + "-" + topic
    if(does_topic_exist(topic_name)):
        delete_topic(topic_name)

def delete_topic_acls(service_account_ids, prefix, topic):
    topic_name = prefix + "-" + topic['name']
    acl = topic['access_list']
    is_prefix = 'prefix' in group
    for service_access in acl:
        permissions, service_account_id = __get_sa_id_and_permissions(service_access, service_account_ids)
        [delete_acl_for_topic(service_account_id, access, topic_name, is_prefix) for access in permissions]

def __get_sa_id_and_permissions(service_access, service_account_ids):
    hashed_sa_name = get_hashed_service_account_name(service_access['service_account'])
    service_account_id = service_account_ids[hashed_sa_name]
    permissions = service_access['permissions']
    return permissions, service_account_id

def create_group_acls(service_account_ids, prefix, group):
    group_name = prefix + "-" + group['name']
    acl = group['access_list']
    is_prefix = 'prefix' in group
    for service_access in acl:
        permissions, service_account_id = __get_sa_id_and_permissions(service_access, service_account_ids)
        [create_acl_for_group(service_account_id, access, group_name, is_prefix) for access in permissions]

def delete_group_acls(service_account_ids, prefix, group):
    group_name = prefix + "-" + group['name']
    acl = group['access_list']
    is_prefix = 'prefix' in group
    for service_access in acl:
        permissions, service_account_id = __get_sa_id_and_permissions(service_access, service_account_ids)
        [delete_acl_for_group(service_account_id, access, group_name, is_prefix) for access in permissions]

def create_access_control(service_account_ids, prefix, access_control):
    topics = access_control['topics']
    print(f"\n** Creating Topic Access Control Lists ** \n")
    for topic in topics:
        create_topic_acls(service_account_ids, prefix, topic)
    groups = access_control.get('groups')
    if groups is not None:
        print(f"\n** Creating Group Access Control Lists ** \n")
        for group in groups:
            create_group_acls(service_account_ids, prefix, group)

def delete_access_control(service_account_ids, prefix, access_control):
    print(f"\n** Deleting Topic Access Control Lists ** \n")
    topics = access_control['topics']
    for topic in topics:
        delete_topic_acls(service_account_ids, prefix, topic)

    print(f"\n** Deleting Group Access Control Lists ** \n")
    groups = access_control['groups']
    for group in groups:
        delete_group_acls(service_account_ids, prefix, group)

def create_kubernetes_secret(api_info, kafka_key_name, namespace_name):

    print("\n** Creating Kubernetes Secret **\n")
    k8command = f"""kubectl create secret generic -n {namespace_name} {kafka_key_name} \
        --from-literal=kafka_key={api_info['key']} --from-literal=kafka_password={api_info['secret']} \
        --from-literal=schema_registry_key={os.environ['CCLOUD_SCHEMA_KEY']} --from-literal=schema_registry_password={os.environ['CCLOUD_SCHEMA_SECRET']} \
        -o yaml --dry-run | kubectl apply -f -"""
    check_output(k8command,shell=True)

def create_api_key_and_kafka_secret(service_account_id, cluster_id, namespace_name, kafka_key_name):
    existing_key = get_api_key(service_account_id)
    if existing_key is None:
        print(f"Creating API Key for {service_account_id}")
        print(f"ccloud api-key create --service-account-id {service_account_id} --resource {cluster_id}")
        output = check_output(f"ccloud api-key create --description {prefix} --service-account-id {service_account_id} --resource {cluster_id}",shell=True).decode("utf-8")
        key_output = output.splitlines()
        api_info = {}
        for line in key_output:
            # remove first and list "|" and then split into columns
            line_info = [x.strip() for x in line[1:-1].split("|")]
            if(len(line_info) == 1):
                continue
            if(line_info[0] == 'API Key'):
                api_info['key'] = line_info[1]
            if(line_info[0] == 'Secret'):
                api_info['secret'] = line_info[1]

        create_kubernetes_secret(api_info, kafka_key_name, namespace_name)

def delete_api_key(api_key):
    print(f"ccloud api-key delete {api_key}")
    os.system(f"ccloud api-key delete {api_key}")

def create_sa_and_get_ids(prefix, cluster_id, k8_namespace, service_accounts):
    print(f"\n** Creating Service Accounts ** \n")
    service_accounts_ids = {}
    for service_account in service_accounts:
        service_account_id = create_service_account_if_not_exists(prefix, service_account)
        service_account_name = get_hashed_service_account_name(service_account['name'])
        service_accounts_ids[service_account_name] = service_account_id
        kafka_key_name = service_account['kafka_key_name']
        create_api_key_and_kafka_secret(service_account_id, cluster_id, k8_namespace, kafka_key_name)
    return service_accounts_ids

def delete_api_keys_for_service_account(service_account_id):
    api_key_output = check_output("ccloud api-key list",shell=True).decode("utf-8")
    api_key_list = api_key_output.splitlines()[2:]
    for line in api_key_list:
        line_info = [x.strip() for x in line.split("|")]
        if(line_info[1] == service_account_id):
            delete_api_key(line_info[0])

def delete_service_accounts(service_accounts):
    service_accounts_ids = {}
    for service_account in service_accounts:
        sa_name = get_hashed_service_account_name(service_account['name'])
        service_account_id = get_service_account_id(sa_name)
        service_accounts_ids[sa_name] = service_account_id
        delete_api_keys_for_service_account(service_account_id)
        delete_service_account(service_account_id)
    return service_accounts_ids

def create_confluent_resources_from_file(config_file):
    cluster_id = get_cluster_id_from_name(cluster)
    print(f"\n** Looking at cluster_id {cluster_id} ** \n")
    check_output(f"ccloud kafka cluster use \"{cluster_id}\"",shell=True).decode("utf-8")
    with open(config_file, 'r', encoding= "utf-8") as outfile:
        values = yaml.load(outfile, Loader=Loader)
        context = values['config']
        service_accounts = context['service_accounts']
        topics = context.get('topics')
        access_control = context['access_control']

        service_account_ids = create_sa_and_get_ids(prefix, cluster_id, kube_namespace, service_accounts)

        print(f"\n** Creating Topics ** \n")
        if topics is not None:
            for topic in topics:
                create_topic_if_not_exist(prefix, topic['name'], topic['partitions'], topic.get('config'))

        print(f"\n** Creating ACLs ** \n")
        create_access_control(service_account_ids, prefix, access_control)

def delete_confluent_resources_from_file(config_file):
    with open(config_file, 'r') as outfile:
        values = yaml.load(outfile, Loader=Loader)
        context = values['config']
        service_accounts = context['service_accounts']
        access_control = context['access_control']
        topics = context.get('topics')
        service_account_ids = get_service_account_ids(service_accounts)

        print(f"\n** Deleting ACLs ** \n")
        delete_access_control(service_account_ids, prefix, access_control)

        print(f"\n** Deleting Service Accounts ** \n")
        delete_service_accounts(service_account_ids)

        print(f"\n** Deleting Topics ** \n")
        if topics is not None:
            for topic in topics:
                delete_topic_if_exists(prefix, topic['name'])


parser = argparse.ArgumentParser(description='Create or Delete Confluent infrastructure.')

parser.add_argument("-prefix", help='used for prefixing topics and creating service-account')
parser.add_argument("-namespace", help='namespace to put kubernetes secrets in')
parser.add_argument("-file", help='a yaml config file with SA/ACL set up information')
parser.add_argument("-cluster", help=" the name of the cluster in confluent setup")
group = parser.add_mutually_exclusive_group()
group.add_argument("--create", help=' this will create the confluent infrastructure', action="store_true")
group.add_argument("--delete", help=' this will delete the confluent infrastructure', action="store_true")
args = parser.parse_args()

prefix = args.prefix
kube_namespace = args.namespace
file_name = args.file
cluster = args.cluster
if(args.create):
    create_confluent_resources_from_file(file_name)
if(args.delete):
    delete_confluent_resources_from_file(file_name)




