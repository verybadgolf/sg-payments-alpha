ENVIRONMENT=$1

echo DELETING CONFLUENT INFRASTRUCTURE for $ENVIRONMENT

echo DELETING ACLs
ccloud kafka acl list | grep $ENVIRONMENT | grep GROUP | grep PREFIX | awk '{print "ccloud kafka acl delete --service-account-id " substr($1, 6) " --" tolower($3) " --operation " tolower($5) " --consumer-group " tolower($9) " --prefix" }' > acl.sh
ccloud kafka acl list | grep $ENVIRONMENT | grep GROUP | grep LITERAL | awk '{print "ccloud kafka acl delete --service-account-id " substr($1, 6) " --" tolower($3) " --operation " tolower($5) " --consumer-group " tolower($9) }' >> acl.sh
ccloud kafka acl list | grep $ENVIRONMENT | grep TOPIC | grep LITERAL | awk '{print "ccloud kafka acl delete --service-account-id " substr($1, 6) " --" tolower($3) " --operation " tolower($5) " --topic " tolower($9) }'  >> acl.sh
ccloud kafka acl list | grep $ENVIRONMENT | grep TOPIC | grep PREFIX | awk '{print "ccloud kafka acl delete --service-account-id " substr($1, 6) " --" tolower($3) " --operation " tolower($5) " --topic " tolower($9) " --prefix" }' >> acl.sh
./acl.sh

echo DELETING TOPICS
ccloud kafka topic list | grep $ENVIRONMENT | awk '{print $1}' | while read line; do ccloud kafka topic delete $line; done
ccloud service-account list | grep $ENVIRONMENT | awk '{print $1}'  > sa-grep-file.txt

echo DELETING API KEYS
ccloud api-key list | grep -f sa-grep-file.txt | awk '{print $1}' | while read line; do ccloud api-key delete $line; done

echo DELETING SERVICE ACCOUNTS
ccloud service-account list | grep $ENVIRONMENT | awk '{print $1}' | while read line; do ccloud service-account delete $line; done

rm sa-grep-file.txt

