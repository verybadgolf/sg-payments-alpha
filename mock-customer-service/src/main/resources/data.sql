create table users(
	username varchar_ignorecase(50) not null primary key,
	password varchar_ignorecase(50) not null,
	enabled boolean not null
);

create table authorities (
	username varchar_ignorecase(50) not null,
	authority varchar_ignorecase(50) not null,
	constraint fk_authorities_users foreign key(username) references users(username)
);
create unique index ix_auth_username on authorities (username,authority);

--- Customer users

insert into users (username, password, enabled) values ('jon.snow', 'Ghost', TRUE);
insert into users (username, password, enabled) values ('arya.stark', 'Nymeria', TRUE);
insert into users (username, password, enabled) values ('sansa.stark', 'Lady', TRUE);

insert into authorities (username, authority) values ('jon.snow', 'user');
insert into authorities (username, authority) values ('arya.stark', 'user');
insert into authorities (username, authority) values ('sansa.stark', 'user');

--- Parking tickets
insert into parking_tickets (id, amount, date, description, username) values ('74547a08-39e8-4c56-82fa-f23dd3bfd741',
    50, {ts '2012-09-17 18:47:52.69'}, 'Blocked the Winterfell gate.', 'jon.snow');

insert into parking_tickets (id, amount, date, description, username) values ('5238dd49-fd53-48e4-b137-4185ad8acd94',
    100, {ts '2012-11-23 22:23:11.23'}, 'Stopped under the Weirwood tree.', 'jon.snow');

insert into parking_tickets (id, amount, date, description, username) values ('bdb1f6e7-4515-4583-9fb2-f3d3b1bad1c2',
    20, {ts '2019-05-16 16:37:42.19'}, 'Blocked Westeros west port.', 'arya.stark');

insert into parking_tickets (id, amount, date, description, username) values ('d487f944-eb4f-4c38-b52b-e0b3fc9c6372',
    40, {ts '2019-05-19 12:29:12.23'}, 'Stopped in front of water hydrant in King''s Landing.', 'arya.stark');

--- Payments
insert into payments (id, amount, reference, status, username)
values ('67c07419-5ef6-4289-84ce-9269618491e3', 10, 'FishingPermit:OneYear', 0, 'jon.snow')
