package gov.scott.payment.sgmockservice.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Wither;

@Data
@RequiredArgsConstructor(staticName = "of")
public class GetPaymentResult {

    @JsonProperty("id")
    private @NonNull String paymentId;

    private @NonNull MoneyDto amount;

    private @NonNull @Wither String status;

    private @NonNull String reference;

    private @NonNull String returnUrl;

    public GetPaymentResult() {

    }
}
