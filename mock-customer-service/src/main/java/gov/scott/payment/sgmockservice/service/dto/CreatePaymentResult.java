package gov.scott.payment.sgmockservice.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Wither;

@Data
@RequiredArgsConstructor(staticName = "of")
public class CreatePaymentResult {

    @JsonProperty("id")
    private @NonNull String paymentId;

    private @NonNull MoneyDto amount;

    private @NonNull String status;

    private @NonNull String reference;

    @JsonIgnore
    private @NonNull @Wither String nextUrl;

    public CreatePaymentResult() {

    }
}
