package gov.scott.payment.sgmockservice.service;

import gov.scott.payment.sgmockservice.type.Payment;

public interface PaymentObserver {

    void update(Payment payment);
}
