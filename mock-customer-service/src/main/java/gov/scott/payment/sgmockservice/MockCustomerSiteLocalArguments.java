package gov.scott.payment.sgmockservice;

import com.beust.jcommander.Parameter;

import java.util.*;

public class MockCustomerSiteLocalArguments extends MockCustomerSiteArguments{

    @Parameter(names = "-service.port",required = true
        ,description = "The port on which the core payments service is running locally")
    private Integer servicePort;

    @Parameter(names = "-cognito"
            , description = "Specify if you wish to use cognito for authentication. If not specified then dummy authentication will be used")
    private boolean cognito;

    @Parameter(names = "-cognito.tokenUrl"
            , description = "Only applicable if -cognito is specified. The url to obtain a Oauth2 client token from")
    private String cognitoTokenUrl;

    @Parameter(names = "-fishing.clientId"
            , description = "Only applicable if -cognito is specified. The cognito app client Id with permission to make payments on the fishing permit service")
    private String fishingClientId = "fishing";

    @Parameter(names = "-fishing.clientSecret"
            , description = "Only applicable if -cognito is specified. The cognito app client secret with permission to make payments on the fishing permit service")
    private String fishingClientSecret = "fishingSecret";

    @Parameter(names = "-parking.clientId"
            , description = "Only applicable if -cognito is specified. The cognito app client Id with permission to make payments on the parking fines service")
    private String parkingClientId = "parking";

    @Parameter(names = "-parking.clientSecret"
            , description = "Only applicable if -cognito is specified. The cognito app client secret with permission to make payments on the parking fines service")
    private String parkingClientSecret = "parkingSecret";

    @Override
    public Set<String> getProfiles() {
        Set<String> profiles = new HashSet<>();
        profiles.add("live");
        return profiles;
    }

    @Override
    public Set<String> validate() {
        Set<String> violations = new HashSet<>();
        if(cognito){
            if(fishingClientId == null){
                violations.add("-fishing.clientId must be provided if using cognito");
            }
            if(fishingClientSecret == null){
                violations.add("-fishing.clientSecret must be provided if using cognito");
            }
            if(parkingClientId == null){
                violations.add("-parking.clientId must be provided if using cognito");
            }
            if(parkingClientSecret == null){
                violations.add("-parking.clientSecret must be provided if using cognito");
            }
            if(cognitoTokenUrl == null){
                violations.add("-cognito.tokenUrl must be provided if using cognito");
            }
        }
        return violations;
    }

    @Override
    public Map<String, Object> getProperties() {
        Map<String,Object> properties = new HashMap<>(super.getProperties());
        String baseUrl = "http://localhost:" + servicePort;
        properties.put("sg.service.baseUrl", baseUrl);
        properties.put("sg.service.url.payment.create", baseUrl+"/cardpayments");
        properties.put("sg.service.url.payment.get", baseUrl+"/cardpayments/{paymentId}");
        properties.put("sg.config.fishing.client.id",fishingClientId);
        properties.put("sg.config.fishing.client.secret",fishingClientSecret);
        properties.put("sg.config.parking.client.id",parkingClientId);
        properties.put("sg.config.parking.client.secret",parkingClientSecret);
        properties.put("sg.service.url.access.token",cognitoTokenUrl);
        if(!cognito){
            properties.put("sg.service.url.access.token",baseUrl+"/auth/oauth_token");
        }
        return properties;
    }
}
