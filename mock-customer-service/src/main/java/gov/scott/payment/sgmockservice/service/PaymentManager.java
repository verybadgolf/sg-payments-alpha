package gov.scott.payment.sgmockservice.service;

import gov.scott.payment.sgmockservice.type.*;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.control.Either;

import java.net.URI;

public interface PaymentManager {

    Either<PaymentError, Tuple2<Payment, URI>> newPayment(User user, List<? extends Item> items, URI returnUrl);

    Either<PaymentError, Payment> proceedPayment(PaymentId paymentId);
}
