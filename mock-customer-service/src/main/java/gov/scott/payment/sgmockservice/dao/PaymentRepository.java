package gov.scott.payment.sgmockservice.dao;

import gov.scott.payment.sgmockservice.dao.model.PaymentEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PaymentRepository extends CrudRepository<PaymentEntity, String> {

    List<PaymentEntity> findByUsername(String username);
}
