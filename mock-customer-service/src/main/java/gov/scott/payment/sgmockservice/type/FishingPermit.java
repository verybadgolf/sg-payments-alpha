package gov.scott.payment.sgmockservice.type;

import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

import java.time.LocalDate;
import java.time.Period;

@Value
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Accessors(fluent = true)
public class FishingPermit implements Item {

    private @NonNull ItemId id;

    private @NonNull String description;

    private @NonNull Money amount;

    private @NonNull Period period;

    public LocalDate validUntil(LocalDate startTimeStamp) {
        return startTimeStamp.plus(period);
    }

    public LocalDate validUntilStaringFromNow() {
        return validUntil(LocalDate.now());
    }

    public static FishingPermit OneDayPermit = new FishingPermit(
            ItemId.of("FishingPermit:OneDay"), "One day fishing license",
                Money.ofMajor(CurrencyUnit.GBP, 1), Period.ofDays(1)
    );

    public static FishingPermit OneWeekPermit = new FishingPermit(
            ItemId.of("FishingPermit:OneWeek"), "One week fishing license",
            Money.ofMajor(CurrencyUnit.GBP, 5), Period.ofWeeks(1)
    );

    public static FishingPermit OneYearPermit = new FishingPermit(
            ItemId.of("FishingPermit:OneYear"), "One year fishing license",
            Money.of(CurrencyUnit.GBP, 100), Period.ofYears(1)
    );

    public static List<FishingPermit> AllPermits = List.of(OneDayPermit, OneWeekPermit, OneYearPermit);

    public static Option<FishingPermit> findById(ItemId id) {
        return AllPermits.find(p -> p.id.equals(id));
    }
}
