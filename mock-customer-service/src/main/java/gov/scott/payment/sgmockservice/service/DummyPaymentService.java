package gov.scott.payment.sgmockservice.service;

import gov.scott.payment.sgmockservice.service.dto.CreatePaymentRequest;
import gov.scott.payment.sgmockservice.service.dto.CreatePaymentResult;
import gov.scott.payment.sgmockservice.service.dto.GetPaymentResult;
import gov.scott.payment.sgmockservice.type.PaymentError;
import gov.scott.payment.sgmockservice.type.PaymentStatus;
import io.vavr.control.Either;
import io.vavr.control.Option;
import lombok.val;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import static io.vavr.control.Either.right;

@Service
@Profile("dev")
public class DummyPaymentService implements PaymentService {

    private final ConcurrentHashMap<String, GetPaymentResult> paymentMap =
            new ConcurrentHashMap<>();

    @Override
    public Either<PaymentError, CreatePaymentResult> newPayment(CreatePaymentRequest request) {

        val paymentId = UUID.randomUUID().toString();
        val payment = GetPaymentResult.of(
                paymentId, request.getAmount(), "Created",
                request.getReference(), request.getReturnUrl()
        );

        paymentMap.put(paymentId, payment);

        val nextUrl = "/payment/" + paymentId;
        val result = CreatePaymentResult.of(
                paymentId, request.getAmount(), payment.getStatus(),
                payment.getReference(), nextUrl
        );

        return right(result);
    }

    @Override
    public Either<PaymentError, GetPaymentResult> getPayment(String paymentId) {
        return Option.of(paymentMap.get(paymentId))
                .toEither(PaymentError.NotFound);
    }

    public Option<GetPaymentResult> updatePayment(String paymentId, PaymentStatus status) {
        return Option.of(
                paymentMap.computeIfPresent(paymentId, (id, res) -> res.withStatus(status.name()))
        );
    }
}
