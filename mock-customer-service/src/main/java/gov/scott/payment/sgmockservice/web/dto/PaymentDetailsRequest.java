package gov.scott.payment.sgmockservice.web.dto;

import lombok.Data;

@Data
public class PaymentDetailsRequest {

    private String cardNumber;

    private String expiryMonth;

    private String expiryYear;

    private String cvCode;
}
