package gov.scott.payment.sgmockservice.service;

import gov.scott.payment.sgmockservice.dao.PaymentRepository;
import gov.scott.payment.sgmockservice.dao.model.PaymentEntity;
import gov.scott.payment.sgmockservice.service.dto.CreatePaymentRequest;
import gov.scott.payment.sgmockservice.service.dto.CreatePaymentResult;
import gov.scott.payment.sgmockservice.service.dto.GetPaymentResult;
import gov.scott.payment.sgmockservice.service.dto.MoneyDto;
import gov.scott.payment.sgmockservice.type.*;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.control.Either;
import io.vavr.control.Option;
import io.vavr.control.Try;
import lombok.val;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

import java.math.BigDecimal;
import java.net.URI;

import static gov.scott.payment.sgmockservice.service.Utils.itemIdsToReference;
import static gov.scott.payment.sgmockservice.service.Utils.referenceToItemIds;


public class DefaultPaymentManager implements PaymentManager, PaymentSubject {

    private final PaymentRepository repository;

    private final PaymentService paymentService;


    private List<PaymentObserver> observers = List.empty();

    public DefaultPaymentManager(PaymentRepository repository, PaymentService paymentService) {
        this.repository = repository;
        this.paymentService = paymentService;
    }

    @Override
    public Either<PaymentError, Tuple2<Payment, URI>> newPayment(User user, List<? extends Item> items, URI returnUrl) {
        val request = createRequest(items, returnUrl);

        val response = request.flatMap(paymentService::newPayment);

        val result = response.map(r -> Tuple.of(resultToPayment(user, r), URI.create(r.getNextUrl())));

        return result.map(r -> {
            storePayment(r._1);
            notifyObservers(r._1);

            return r;
        });
    }

    @Override
    public Either<PaymentError, Payment> proceedPayment(PaymentId paymentId) {
        val response = paymentService.getPayment(paymentId.value());

        val result = response.flatMap(
                r -> loadPayment(PaymentId.of(r.getPaymentId())).map(p -> Tuple.of(p, r))
        );

        return result.map(tpl -> {
            Payment p = tpl._1;
            GetPaymentResult r = tpl._2;

            Payment updatedPayment = !p.finished()
                    ? p.withStatus(PaymentStatus.of(r.getStatus()))
                    : p;

            if (!updatedPayment.status().equals(p.status())) {
                storePayment(updatedPayment);
                notifyObservers(updatedPayment);
            }

            return updatedPayment;
        });
    }

    @Override
    public void addObserver(PaymentObserver observer) {
        observers = observers.append(observer);
    }

    private Either<PaymentError, Payment> loadPayment(PaymentId paymentId) {
        return Option.ofOptional(repository.findById(paymentId.value()))
                .map(e -> Payment.builder()
                        .id(paymentId)
                        .amount(Money.ofMinor(CurrencyUnit.GBP, e.getAmount()))
                        .status(e.getStatus())
                        .items(referenceToItemIds(e.getReference()))
                        .user(User.of(e.getUsername()))
                        .build()
                )
                .toEither(PaymentError.NotFound);
    }

    private void storePayment(Payment payment) {
        val entity = PaymentEntity.builder()
                .id(payment.id().value())
                .amount(payment.amount().getAmountMinor().intValue())
                .status(payment.status())
                .reference(itemIdsToReference(payment.items()))
                .username(payment.user().username())
                .build();

        repository.save(entity);
    }

    private void notifyObservers(Payment payment) {
        observers.forEach(o -> o.update(payment));
    }

    private static Either<PaymentError, CreatePaymentRequest> createRequest(List<? extends Item> items, URI returnUrl) {
        val amount = totalAmount(items.map(Item::amount)).map(DefaultPaymentManager::moneyToDto);
        val reference = itemIdsToReference(items.map(Item::id));
        val description = String.join(";", items.map(Item::description));

        return amount.map(a -> CreatePaymentRequest.of(a, reference, returnUrl.toString(), description, ""));
    }

    private static Payment resultToPayment(User user, CreatePaymentResult result) {
        return Payment.builder()
                .id(PaymentId.of(result.getPaymentId()))
                .amount(dtoToMoney(result.getAmount()))
                .status(PaymentStatus.of(result.getStatus()))
                .items(referenceToItemIds(result.getReference()))
                .user(user)
                .build();
    }

    private static Either<PaymentError, Money> totalAmount(List<Money> amounts) {
        return Try.of(() -> Money.total(amounts)).toEither()
                .mapLeft(ex -> PaymentError.invalidInput(ex.getMessage()));
    }

    private static MoneyDto moneyToDto(Money amount) {
        return MoneyDto.of(amount.getAmount().toPlainString(), amount.getCurrencyUnit().getCode());
    }

    private static Money dtoToMoney(MoneyDto dto) {
        return Money.of(CurrencyUnit.of(dto.getCurrency()), new BigDecimal(dto.getAmount()));
    }
}
