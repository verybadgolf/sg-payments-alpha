package gov.scott.payment.sgmockservice.type;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import lombok.experimental.Wither;
import org.joda.money.Money;

@Value
@Accessors(fluent = true)
@RequiredArgsConstructor(staticName = "of")
@Builder
public class Payment {

    private @NonNull PaymentId id;

    private @NonNull Money amount;

    private @NonNull @Wither PaymentStatus status;

    private @NonNull User user;

    private @NonNull List<ItemId> items;

    public boolean finished() {
        return status == PaymentStatus.Success
                || status == PaymentStatus.Failed
                || status == PaymentStatus.Cancelled
                || status == PaymentStatus.Error;
    }

    public boolean succeeded() {
        return status == PaymentStatus.Success;
    }

    public boolean failed() {
        return status == PaymentStatus.Failed
                || status == PaymentStatus.Cancelled
                || status == PaymentStatus.Error;
    }
}
