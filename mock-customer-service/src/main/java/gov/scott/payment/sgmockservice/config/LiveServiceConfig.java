package gov.scott.payment.sgmockservice.config;

import gov.scott.payment.sgmockservice.dao.PaymentRepository;
import gov.scott.payment.sgmockservice.service.DefaultPaymentManager;
import gov.scott.payment.sgmockservice.service.PaymentService;
import gov.scott.payment.sgmockservice.service.SGPaymentService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("live")
public class LiveServiceConfig {

    @Value("${sg.service.url.access.token}")
    private String accessTokenUrl;

    @Value("${sg.service.url.payment.create}")
    private String createPaymentUrl;

    @Value("${sg.service.url.payment.get}")
    private String getPaymentUrl;

    @Value("${sg.service.url.payment.next}")
    private String nextPaymentUrl;


    @Bean("FishingPaymentManager")
    public DefaultPaymentManager fishingPaymentManager(
            PaymentRepository repository,
            @Qualifier("FishingPaymentService") PaymentService paymentService) {
        return new DefaultPaymentManager(repository, paymentService);

    }

    @Bean({"ParkingPaymentManager", "ParkingPaymentSubject"})
    public DefaultPaymentManager parkingPaymentManager(
            PaymentRepository repository,
            @Qualifier("ParkingPaymentService") PaymentService paymentService) {
        return new DefaultPaymentManager(repository, paymentService);
    }

    @Bean("FishingPaymentService")
    public PaymentService fishingPaymentService(
            @Value("${sg.config.fishing.client.id}") String clientId,
            @Value("${sg.config.fishing.client.secret}") String clientSecret,
            @Value("${sg.config.fishing.service.id}") String serviceId) {
        return new SGPaymentService(
                clientId, clientSecret, serviceId,
                accessTokenUrl, createPaymentUrl, getPaymentUrl, nextPaymentUrl
        );
    }

    @Bean("ParkingPaymentService")
    public PaymentService parkingPaymentService(
            @Value("${sg.config.parking.client.id}") String clientId,
            @Value("${sg.config.parking.client.secret}") String clientSecret,
            @Value("${sg.config.parking.service.id}") String serviceId) {
        return new SGPaymentService(
                clientId, clientSecret, serviceId,
                accessTokenUrl, createPaymentUrl, getPaymentUrl, nextPaymentUrl
        );
    }
}
