package gov.scott.payment.sgmockservice.service;

import gov.scott.payment.sgmockservice.service.dto.CreatePaymentRequest;
import gov.scott.payment.sgmockservice.service.dto.CreatePaymentResult;
import gov.scott.payment.sgmockservice.service.dto.GetPaymentResult;
import gov.scott.payment.sgmockservice.type.PaymentError;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.java.Log;
import lombok.val;
import org.apache.logging.log4j.util.Strings;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsAccessTokenProvider;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.web.util.UriComponentsBuilder;

@Log
public class SGPaymentService implements PaymentService {

    private final OAuth2RestTemplate restTemplate;

    private final String serviceId;

    private final String createPaymentUrl;
    private final String getPaymentUrl;

    private final UriComponentsBuilder nextPaymentUrlBuilder;

    public SGPaymentService(
            String clientId, String clientSecret, String serviceId,
            String accessTokenUrl, String createPaymentUrl, String getPaymentUrl, String nextPaymentUrl) {

        validateArgument("clientId", clientId);
        validateArgument("clientSecret", clientSecret);
        validateArgument("serviceId", serviceId);
        validateArgument("accessTokenUrl", accessTokenUrl);
        validateArgument("createPaymentUrl", createPaymentUrl);
        validateArgument("getPaymentUrl", getPaymentUrl);
        validateArgument("nextPaymentUrl", nextPaymentUrl);

        val credentialResourceDetails = new ClientCredentialsResourceDetails();
        credentialResourceDetails.setAccessTokenUri(accessTokenUrl);
        credentialResourceDetails.setClientId(clientId);
        credentialResourceDetails.setClientSecret(clientSecret);

        restTemplate = new OAuth2RestTemplate(credentialResourceDetails);
        restTemplate.setAccessTokenProvider(new ClientCredentialsAccessTokenProvider());

        this.serviceId = serviceId;

        this.createPaymentUrl = createPaymentUrl;
        this.getPaymentUrl = getPaymentUrl;

        this.nextPaymentUrlBuilder = UriComponentsBuilder.fromHttpUrl(nextPaymentUrl);
    }

    @Override
    public Either<PaymentError, CreatePaymentResult> newPayment(CreatePaymentRequest request) {
        val result = Try.of(
                () -> restTemplate.postForObject(
                        createPaymentUrl,
                        request.withServiceId(serviceId),
                        CreatePaymentResult.class)
        ).map(r -> r.withNextUrl(buildNextPaymentUrl(r.getPaymentId())));

        return result.toEither()
                .mapLeft(this::logError)
                .mapLeft(t -> PaymentError.serverError(t.getMessage()));
    }

    @Override
    public Either<PaymentError, GetPaymentResult> getPayment(String paymentId) {
        val result = Try.of(
                () -> restTemplate.getForObject(getPaymentUrl, GetPaymentResult.class, paymentId)
        );

        return result.toEither()
                .mapLeft(this::logError)
                .mapLeft(t -> PaymentError.serverError(t.getMessage()));
    }

    private String buildNextPaymentUrl(String paymentId) {
        return nextPaymentUrlBuilder.buildAndExpand(paymentId).toUriString();
    }

    private <T extends Throwable> T logError(T value) {
        log.warning(value.toString());
        return value;
    }

    private static void validateArgument(String name, String value) {
        log.info(() -> String.format("Configuration - name: %s, value: %s", name, value));
        if (Strings.isBlank(name)) {
            throw new IllegalArgumentException(value);
        }
    }
}
