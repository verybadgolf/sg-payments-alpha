package gov.scott.payment.sgmockservice.dao.model;

import gov.scott.payment.sgmockservice.type.PaymentStatus;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Payments")
@Getter @Setter
@NoArgsConstructor
@Builder
public class PaymentEntity {

    @Id
    private @NotNull String id;

    private @NonNull PaymentStatus status;

    private @NotNull String reference;

    private @NotNull Integer amount;

    private @NotNull String username;

    private PaymentEntity(String id, PaymentStatus status, String reference, Integer amount, String username)
    {
        this.id = id;
        this.status = status;
        this.reference = reference;
        this.amount = amount;
        this.username = username;
    }
}
