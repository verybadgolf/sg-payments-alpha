package gov.scott.payment.sgmockservice;

import com.beust.jcommander.Parameter;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MockCustomerSiteAwsArguments extends MockCustomerSiteArguments{

    @Parameter(names = "-fishing.clientId"
            ,required = true
            , description = "The cognito app client Id with permission to make payments on the fishing permit service")
    private String fishingClientId;

    @Parameter(names = "-fishing.clientSecret"
            ,required = true
            , description = "The cognito app client secret with permission to make payments on the fishing permit service")
    private String fishingClientSecret;

    @Parameter(names = "-parking.clientId"
            ,required = true
            , description = "The cognito app client Id with permission to make payments on the parking fine service")
    private String parkingClientId;

    @Parameter(names = "-parking.clientSecret"
            ,required = true
            , description = "The cognito app client secret with permission to make payments on the parking fine service")
    private String parkingClientSecret;

    @Parameter(names = "-service.url"
            , description = "The url the core service to use is deployed at")
    private String serviceUrl = "https://payments-poc.sgpaypoc.org.uk/payments-poc/payments-poc-develop-core";

    @Parameter(names = "-cognito.tokenUrl"
            , description = "The cognito url to obtain a Oauth2 client token from")
    private String tokenUrl = "https://sgpaypoc.auth.eu-west-1.amazoncognito.com/oauth2/token";

    @Override
    public Set<String> getProfiles() {
        return Collections.singleton("live");
    }

    @Override
    public Map<String, Object> getProperties() {
        Map<String,Object> properties = new HashMap<>(super.getProperties());
        properties.put("sg.config.fishing.client.id",fishingClientId);
        properties.put("sg.config.fishing.client.secret",fishingClientSecret);
        properties.put("sg.config.parking.client.id",parkingClientId);
        properties.put("sg.config.parking.client.secret",parkingClientSecret);
        properties.put("sg.service.baseUrl",serviceUrl);
        properties.put("sg.service.url.access.token",tokenUrl);
        return properties;
    }

    @Override
    public Set<String> validate() {
        return Collections.emptySet();
    }
}
