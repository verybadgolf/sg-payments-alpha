package gov.scot.payments.adapters;

import gov.scot.payments.adapters.ilf.ILFFileParser;
import gov.scot.payments.adapters.ilf_new.ILFNewFileParser;
import gov.scot.payments.adapters.sppa.SPPAFileParser;
import gov.scot.payments.adapters.ssa.SSAFileParser;
import gov.scot.payments.customers.model.api.CustomerPaymentBatch;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.api.CreatePaymentRequest;
import io.vavr.collection.List;
import lombok.AllArgsConstructor;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@AllArgsConstructor
@RequestMapping({"/files/parse"})
public class AdaptersService {

    private ILFFileParser ilfFileParser;
    private ILFNewFileParser ilfNewFileParser;
    private SSAFileParser ssaFileParser;
    private SPPAFileParser sppaFileParser;

    @RequestMapping(method = RequestMethod.POST, value = "/ILF", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public Mono<ResponseEntity<List<CustomerPaymentBatch>>> parseILFFile (
            @RequestParam("product") CompositeReference product,
            @RequestParam("fileName") String fileName,
            @RequestPart("file") FilePart uploadingFiles) {
        return DataBufferUtils.join(uploadingFiles.content())
            .map(DataBuffer::asInputStream)
            .map(is ->  ilfFileParser.generatePaymentList(is)
                    .map(p -> p.toPaymentInstruction(product, fileName)))
            .map(l -> List.of(new CustomerPaymentBatch(fileName, l)))
            .map(ResponseEntity::ok)
            .onErrorResume(e ->  (Mono)Mono.just(ResponseEntity
                    .badRequest()
                    .body("Error encountered reading ILF CSV input stream: " + e.getMessage()))
            );
    }

    @RequestMapping(method = RequestMethod.POST, value="/ILF_NEW", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public Mono<ResponseEntity<List<CustomerPaymentBatch>>> parseILFNewFile (
            @RequestParam("product") CompositeReference product,
            @RequestParam("fileName") String fileName,
            @RequestPart("file") FilePart uploadingFiles) {
        return DataBufferUtils.join(uploadingFiles.content())
                .map(DataBuffer::asInputStream)
                .map(is -> ilfNewFileParser.generatePaymentList(is)
                        .map(p -> p.toPaymentInstruction(product, fileName))
                        .groupBy(CreatePaymentRequest::getBatchId)
                        .map(e -> new CustomerPaymentBatch(e._1,e._2))
                        .toList())
                .map(ResponseEntity::ok)
                .onErrorResume(e ->  (Mono)Mono.just(ResponseEntity
                        .badRequest()
                        .body("Error encountered reading ILF new CSV input stream: " + e.getMessage()))
                );
    }

    @RequestMapping (method = RequestMethod.POST, value = "/SSA", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public Mono<ResponseEntity<List<CustomerPaymentBatch>>> parseSSAFile (
            @RequestParam("product") CompositeReference product,
            @RequestParam("fileName") String fileName,
            @RequestPart("file") FilePart uploadingFiles) {
        return DataBufferUtils.join(uploadingFiles.content())
            .map(DataBuffer::asInputStream)
            .map(inputStream -> ssaFileParser.readRecords(inputStream))
            .map(records -> records.clientPaymentRecords
                    .values()
                    .map(cpr -> cpr.toPaymentInstruction(product, fileName, records.controlRecords.get(0)))
                    .toList())
                .map(l -> List.of(new CustomerPaymentBatch(fileName, l)))
            .map(ResponseEntity::ok)
            .onErrorResume(e ->  (Mono)Mono.just(ResponseEntity
                .badRequest()
                .body("Error encountered reading SSA CSV input stream: " + e.getMessage()))
            );
    }

    @RequestMapping (method = RequestMethod.POST, value = "/SPPA", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public Mono<ResponseEntity<List<CustomerPaymentBatch>>> parseSPPAFile (
            @RequestParam("product") CompositeReference product,
            @RequestParam("fileName") String fileName,
            @RequestPart("file") FilePart uploadingFiles
    ) {
        return DataBufferUtils.join(uploadingFiles.content())
                .map(DataBuffer::asInputStream)
                .map(inputStream -> sppaFileParser.generatePaymentBatches(inputStream))
                .map(batch -> batch.payments
                        .map(p -> p.toPaymentInstruction(product, fileName, batch.header,sppaFileParser::findAccountDetails))
                        .toList())
                .map(l -> List.of(new CustomerPaymentBatch(fileName, l)))
                .map(ResponseEntity::ok)
        .onErrorResume(e ->  (Mono)Mono.just(ResponseEntity
                .badRequest()
                .body("Error encountered reading SPPA input stream: " + e.getMessage()))
        );
    }

}
