package gov.scot.payments.adapters.ilf_new;

import gov.scot.payments.adapters.InvalidCSVFieldException;
import gov.scot.payments.adapters.RollNumber;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.payments.model.api.CreatePaymentRequest;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import org.apache.avro.reflect.Nullable;
import org.apache.commons.csv.CSVRecord;
import org.javamoney.moneta.Money;

import javax.money.Monetary;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.AbstractMap;
import java.util.Map;

@Value
@Builder(toBuilder = true)
public class ILFNewBACSPayment {

    @NonNull private final String clientRef;
    @NonNull private final String paymentRef;
    @NonNull private final String payrunRef;
    @NonNull private final BigDecimal amount;
    @NonNull private final LocalDate paymentDate;
    @NonNull private final String accountHolderName;
    @Nullable final RollNumber buildingSocietyNumber;
    @NonNull private final SortCode sortCode;
    @NonNull private final UKAccountNumber accountNumber;
    @NonNull private final String fund;
    @NonNull private final String country;

    private final static String ACCOUNT_NAME ="account";
    private final static int CLIENT_REF_INDEX = 0;
    private final static int PAYMENT_REF_INDEX = 1;
    private final static int PAYRUN_REF_INDEX = 2;
    private final static int AMOUNT_INDEX = 3;
    private final static int PAYMENT_DATE_INDEX = 4;
    private final static int ACCOUNT_HOLDER_NAME_INDEX = 5;
    private final static int BUILDING_SOCIETY_NUMBER_INDEX = 6;
    private final static int SORT_CODE_INDEX = 7;
    private final static int ACCOUNT_NUMBER_INDEX = 8;
    private final static int FUND_INDEX = 9;
    private final static int COUNTRY_INDEX = 10;

    private static final Map<Integer, String> COLUMN_INDEX_NAME_MAP = Map.ofEntries(
        new AbstractMap.SimpleEntry<Integer, String>(CLIENT_REF_INDEX, "Client Reference"),
        new AbstractMap.SimpleEntry<Integer, String>(PAYMENT_REF_INDEX, "Payment Reference"),
        new AbstractMap.SimpleEntry<Integer, String>(PAYRUN_REF_INDEX, "Payrun Reference"),
        new AbstractMap.SimpleEntry<Integer, String>(AMOUNT_INDEX, "Amount"),
        new AbstractMap.SimpleEntry<Integer, String>(PAYMENT_DATE_INDEX, "Payment Date"),
        new AbstractMap.SimpleEntry<Integer, String>(ACCOUNT_HOLDER_NAME_INDEX, "Account Holder Name"),
        new AbstractMap.SimpleEntry<Integer, String>(BUILDING_SOCIETY_NUMBER_INDEX, "Building Society Number"),
        new AbstractMap.SimpleEntry<Integer, String>(SORT_CODE_INDEX, "Sort Code"),
        new AbstractMap.SimpleEntry<Integer, String>(ACCOUNT_NUMBER_INDEX, "Account Number"),
        new AbstractMap.SimpleEntry<Integer, String>(FUND_INDEX, "Fund"),
        new AbstractMap.SimpleEntry<Integer, String>(COUNTRY_INDEX, "Country")
    );

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    private static String getColumnNameFromIndex(int index){
        if(COLUMN_INDEX_NAME_MAP.containsKey(index)){
            return COLUMN_INDEX_NAME_MAP.get(index);
        }
        return "";
    }

    static ILFNewBACSPayment fromCSVRecord(CSVRecord record) throws InvalidCSVFieldException {

        String clientRef = validateIndexValueAndGetFromCSV(record, CLIENT_REF_INDEX);
        String paymentRef = validateIndexValueAndGetFromCSV(record, PAYMENT_REF_INDEX);
        String payrunRef = validateIndexValueAndGetFromCSV(record, PAYRUN_REF_INDEX);
        BigDecimal amount = getAmountFromCSV(record);
        LocalDate paymentDate = createPaymentDatefromCSV(record);
        String accountHolderName = validateIndexValueAndGetFromCSV(record, ACCOUNT_HOLDER_NAME_INDEX);
        RollNumber buildingSocietyNumber = createBuildingSocietyNumberFromCSV(record);
        UKAccountNumber accountNumber = createAccountNumberFromCSV(record);
        SortCode sortCode = createSortCodeFromCSV(record);
        String fund = validateIndexValueAndGetFromCSV(record, FUND_INDEX);
        String country = validateIndexValueAndGetFromCSV(record, COUNTRY_INDEX);

        return ILFNewBACSPayment.builder()
                .clientRef(clientRef)
                .paymentRef(paymentRef)
                .payrunRef(payrunRef)
                .amount(amount)
                .paymentDate(paymentDate)
                .accountHolderName(accountHolderName)
                .buildingSocietyNumber(buildingSocietyNumber)
                .sortCode(sortCode)
                .accountNumber(accountNumber)
                .fund(fund)
                .country(country)
                .build();
    }

    private static void checkFieldPresent(CSVRecord record, int index) throws InvalidCSVFieldException {
        String fieldValue = record.get(index);
        if(fieldValue == null){
            throw new InvalidCSVFieldException(record.getRecordNumber(), getColumnNameFromIndex(index), "Field not found");
        }
    }

    private static BigDecimal getAmountFromCSV(CSVRecord record) throws InvalidCSVFieldException{
        checkFieldPresent(record, AMOUNT_INDEX);
        String amountStr = record.get(AMOUNT_INDEX);
        if(!checkNumericAndPositive(amountStr)){
            String errorMessage = "Value "+ amountStr + " not a valid positive number";
            throw new InvalidCSVFieldException(record.getRecordNumber(), getColumnNameFromIndex(AMOUNT_INDEX), errorMessage);
        }
        return new BigDecimal(amountStr);
    }

    private static LocalDate createPaymentDatefromCSV(CSVRecord record) throws InvalidCSVFieldException {
        String fieldValue = record.get(PAYMENT_DATE_INDEX);

        if (fieldValue == null) {
            throw new InvalidCSVFieldException(record.getRecordNumber(), getColumnNameFromIndex(PAYMENT_DATE_INDEX), "Field not found");
        }

        try {
            return LocalDate.parse(fieldValue, DATE_FORMATTER);
        } catch (DateTimeParseException cause) {
            throw new InvalidCSVFieldException(record.getRecordNumber(), getColumnNameFromIndex(PAYMENT_DATE_INDEX), cause);
        }
    }

    private static RollNumber createBuildingSocietyNumberFromCSV(CSVRecord record) {
        String fieldValue = record.get(BUILDING_SOCIETY_NUMBER_INDEX);
        return fieldValue == null || fieldValue.equals("NULL") ? null : new RollNumber(fieldValue);
    }

    private static UKAccountNumber createAccountNumberFromCSV(CSVRecord record) throws InvalidCSVFieldException {
        checkFieldPresent(record, ACCOUNT_NUMBER_INDEX);
        try {
            return UKAccountNumber.fromString(record.get(ACCOUNT_NUMBER_INDEX));
        } catch (IllegalArgumentException e) {
            throw new InvalidCSVFieldException(record.getRecordNumber(), getColumnNameFromIndex(ACCOUNT_NUMBER_INDEX), e);
        }
    }

    private static SortCode createSortCodeFromCSV(CSVRecord record) throws InvalidCSVFieldException {
        checkFieldPresent(record, SORT_CODE_INDEX);
        try {
            return SortCode.fromString(record.get(SORT_CODE_INDEX));
        } catch(InvalidPaymentFieldException e){
            throw new InvalidCSVFieldException(record.getRecordNumber(), getColumnNameFromIndex(SORT_CODE_INDEX), e);
        }
    }

    private static String validateIndexValueAndGetFromCSV(CSVRecord record, int index) throws InvalidCSVFieldException {
        checkFieldPresent(record, index);
        return record.get(index);
    }

    private static boolean checkNumericAndPositive(String amountStr) {
        try{
            var bd = new BigDecimal(amountStr);
            if(bd.compareTo(BigDecimal.ZERO) < 0){
                return false;
            }
        } catch (NumberFormatException e){
            return false;
        }
        return true;
    }

    public CreatePaymentRequest toPaymentInstruction(CompositeReference product, String fileName) {
        CashAccount account;
        if (buildingSocietyNumber != null) {
            account = UKBuildingSocietyAccount.builder()
                                              .name(ACCOUNT_NAME)
                                              .sortCode(sortCode)
                                              .rollNumber(buildingSocietyNumber.getValue())
                                              .accountNumber(accountNumber)
                                              .currencyUnit(Monetary.getCurrency("GBP"))
                                              .build();
        } else {
            account = UKBankAccount.builder()
                                   .name(ACCOUNT_NAME)
                                   .sortCode(sortCode)
                                   .accountNumber(accountNumber)
                                   .currencyUnit(Monetary.getCurrency("GBP"))
                                   .build();
        }

        var productReference = new CompositeReference(product.getComponent0(), String.format("%s-%s", fund, country));

        var creditor = PartyIdentification.builder()
                                          .name(accountHolderName)
                                          .build();
        return CreatePaymentRequest.builder()
                                   .amount(Money.of(amount, Monetary.getCurrency("GBP")))
                                   .latestExecutionDate(paymentDate.atStartOfDay().toInstant(ZoneOffset.UTC))
                                   .batchId(payrunRef)
                                   .product(productReference)
                                   .creditorAccount(account)
                                   .creditor(creditor)
                                   .metadata(io.vavr.collection.List.of(new MetadataField("PaymentRef", paymentRef), new MetadataField("PayrunRef", payrunRef)))
                                   .creditorMetadata(io.vavr.collection.List.of(MetadataField.clientRef(clientRef)))
                                   .build();
    }
}
