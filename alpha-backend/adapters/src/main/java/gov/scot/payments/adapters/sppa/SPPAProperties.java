package gov.scot.payments.adapters.sppa;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.Map;

@Data
@ConfigurationProperties(prefix = "sppa.international")
public class SPPAProperties {

    private String sortCode;
    private String accountNumber;

    private Map<String,String> employeeAccounts = new HashMap<>();
}
