package gov.scot.payments.adapters.ilf;

import gov.scot.payments.adapters.InvalidCSVFieldException;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.payments.model.api.CreatePaymentRequest;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import org.apache.commons.csv.CSVRecord;
import org.javamoney.moneta.Money;

import javax.money.Monetary;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Map;

@Value
@Builder(toBuilder = true)
public class ILFBACSPayment {

    @NonNull private final String recipient;
    @NonNull private final String reference;
    @NonNull private final SortCode sortCode;
    @NonNull private final UKAccountNumber accountNumber;
    @NonNull private final BigDecimal amount;

    private final static String ACCOUNT_NAME = "account";
    private final static int RECIPIENT_INDEX = 0;
    private final static int REF_INDEX = 1;
    private final static int SORT_CODE_INDEX = 2;
    private final static int ACCOUNT_NUM_INDEX = 3;
    private final static int AMOUNT_INDEX = 4;

    private static final Map<Integer, String> COLUMN_INDEX_NAME_MAP =
            Map.of(RECIPIENT_INDEX, "Recipient",
                    REF_INDEX, "Reference",
                    SORT_CODE_INDEX, "Sort Code",
                    ACCOUNT_NUM_INDEX, "Account Number",
                    AMOUNT_INDEX, "Amount");

    private static String getColumnNameFromIndex(int index){
        if(COLUMN_INDEX_NAME_MAP.containsKey(index)){
            return COLUMN_INDEX_NAME_MAP.get(index);
        }
        return "";
    }

    static ILFBACSPayment fromCSVRecord(CSVRecord record) throws InvalidCSVFieldException {

        SortCode sortCode = createSortCodeFromCSV(record);
        UKAccountNumber accountNumber = createAccountNumberFromCSV(record);
        BigDecimal targetAmount = getTargetAmountFromCSV(record);
        String recipient = validateIndexValueAndGetFromCSV(record, RECIPIENT_INDEX);
        String reference = validateIndexValueAndGetFromCSV(record, REF_INDEX);

        return ILFBACSPayment.builder()
                .recipient(recipient)
                .reference(reference)
                .sortCode(sortCode)
                .accountNumber(accountNumber)
                .amount(targetAmount)
                .build();
    }

    private static void checkFieldPresent(CSVRecord record, int index) throws InvalidCSVFieldException {
        String fieldValue = record.get(index);
        if(fieldValue == null){
            throw new InvalidCSVFieldException(record.getRecordNumber(), getColumnNameFromIndex(index), "Field not found");
        }
    }

    private static UKAccountNumber createAccountNumberFromCSV(CSVRecord record) throws InvalidCSVFieldException {
        checkFieldPresent(record, ACCOUNT_NUM_INDEX);
        return UKAccountNumber.fromString(record.get(ACCOUNT_NUM_INDEX));
    }

    private static SortCode createSortCodeFromCSV(CSVRecord record) throws InvalidCSVFieldException {
        checkFieldPresent(record, SORT_CODE_INDEX);
        try {
            return SortCode.fromString(record.get(SORT_CODE_INDEX));
        } catch(InvalidPaymentFieldException e){
            throw new InvalidCSVFieldException(record.getRecordNumber(), getColumnNameFromIndex(SORT_CODE_INDEX), e);
        }
    }

    private static String validateIndexValueAndGetFromCSV(CSVRecord record, int index) throws InvalidCSVFieldException {
        checkFieldPresent(record, index);
        return record.get(index);
    }

    private static BigDecimal getTargetAmountFromCSV(CSVRecord record) throws InvalidCSVFieldException{
        checkFieldPresent(record, AMOUNT_INDEX);
        String amountStr = record.get(AMOUNT_INDEX);
        if(!checkNumericAndPositive(amountStr)){
            String errorMessage = "Value "+ amountStr + " not a valid positive number";
            throw new InvalidCSVFieldException(record.getRecordNumber(), getColumnNameFromIndex(AMOUNT_INDEX), errorMessage);
        }
        return new BigDecimal(amountStr);
    }

    private static boolean checkNumericAndPositive(String amountStr) {
        try{
            var bd = new BigDecimal(amountStr);
            if(bd.compareTo(BigDecimal.ZERO) < 0){
                return false;
            }
        } catch (NumberFormatException e){
            return false;
        }
        return true;
    }

    public CreatePaymentRequest toPaymentInstruction(CompositeReference productReference, String fileName){
        var account = UKBankAccount.builder()
                .name(ACCOUNT_NAME)
                .sortCode(sortCode)
                .accountNumber(accountNumber)
                .currencyUnit(Monetary.getCurrency("GBP"))
                .build();
        var creditor = PartyIdentification.builder()
                .name(recipient)
                .build();
        return CreatePaymentRequest.builder()
                .amount(Money.of(amount, Monetary.getCurrency("GBP")))
                .latestExecutionDate(LocalDate.now().plusDays(7).atStartOfDay().toInstant(ZoneOffset.UTC))
                .batchId(fileName)
                .product(productReference)
                .creditorAccount(account)
                .creditor(creditor)
                .creditorMetadata(io.vavr.collection.List.of(MetadataField.clientRef(reference)))
                .build();
    }

}
