package gov.scot.payments.adapters;

import gov.scot.payments.customers.model.api.CustomerPaymentBatch;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.MetadataField;
import gov.scot.payments.payments.model.aggregate.UKBuildingSocietyAccount;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ILFNewAdapterIntegrationTest {
    
    @Autowired
    private WebTestClient webTestClient;

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    @Test
    @DisplayName("ILF new file parser generates correct payment instructions from payment file")
    public void testILFNewEndpoint() throws IOException {

        String fileName = "someFileName";
        CompositeReference product = CompositeReference.parse("ilf.someProductRef");

        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("ilf_new_examples/ILF_new_example.csv");
        var response = webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/ILF_NEW")
                        .queryParam("product", product.toString())
                        .queryParam("fileName", fileName)
                        .build())
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(CustomerPaymentBatch.class)
                .returnResult();

        final List<CustomerPaymentBatch> responseBody = response.getResponseBody();
        responseBody.sort(Comparator.comparing(CustomerPaymentBatch::getId));
        assertEquals(3, responseBody.size());

        var customerPaymentBatch = responseBody.get(2);
        assertEquals("2467", customerPaymentBatch.getId());
        assertEquals(3, customerPaymentBatch.getPayments().size());

        var paymentInstruction = customerPaymentBatch.getPayments().get(0);
        assertEquals("00266735", paymentInstruction.getCreditorAccountAs(UKBuildingSocietyAccount.class).getAccountNumber().getValue());
        assertEquals("110836", paymentInstruction.getCreditorAccountAs(UKBuildingSocietyAccount.class).getSortCode().getValue());
        assertEquals("Rodney the hamster", paymentInstruction.getCreditor().getName());
        assertEquals("107998", paymentInstruction.getCreditorMetadata(MetadataField.CLIENT_REF).get().getValue());
        assertEquals("2/55994511-2", paymentInstruction.getCreditorAccountAs(UKBuildingSocietyAccount.class).getRollNumber());
        assertEquals("2467", paymentInstruction.getBatchId());
        assertEquals("TRAN-S", paymentInstruction.getProduct().getComponent1());
        assertEquals(Money.of(3500, "GBP"), paymentInstruction.getAmount());
        assertEquals(LocalDate.parse("04/12/2019", DATE_FORMATTER), LocalDate.ofInstant(paymentInstruction.getLatestExecutionDate(), ZoneId.of("UTC")));

        customerPaymentBatch = responseBody.get(1);
        assertEquals("2466", customerPaymentBatch.getId());
        assertEquals(3, customerPaymentBatch.getPayments().size());

        customerPaymentBatch = responseBody.get(0);
        assertEquals("2465", customerPaymentBatch.getId());
        assertEquals(5, customerPaymentBatch.getPayments().size());
    }

    @Test
    @DisplayName("ILF new file parser returns an error when given a file with invalid rows")
    public void testILFNewEndpointWithInvalidRows() throws IOException {
        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("ilf_new_examples/ILF_new_error_row.csv");
        webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/ILF_NEW")
                        .queryParam("product", "ilf.someProductRef")
                        .queryParam("fileName", "someFileName")
                        .build())
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @Test
    @DisplayName("ILF new file parser does not parse empty rows")
    public void testILFNewEndpointWithEmptyRows() throws IOException {
        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("ilf_new_examples/ILF_new_example_with_empty.csv");
        var response = webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/ILF_NEW")
                        .queryParam("product", "ilf.someProductRef")
                        .queryParam("fileName", "someFileName")
                        .build())
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(CustomerPaymentBatch.class)
                .returnResult();

        final List<CustomerPaymentBatch> responseBody = response.getResponseBody();
        responseBody.sort(Comparator.comparing(CustomerPaymentBatch::getId));
        assertEquals(3, responseBody.size());

        var customerPaymentBatch = responseBody.get(2);
        assertEquals("2467", customerPaymentBatch.getId());
        assertEquals(2, customerPaymentBatch.getPayments().size());
    }

    @Test
    @DisplayName("ILF new file parser returns an error when file name parameter is not provided")
    public void testILFNewEndpointWithoutFileName() throws IOException {
        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("ilf_new_examples/ILF_new_example.csv");
        webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/ILF_NEW")
                        .queryParam("product", "someProductRef")
                        .build())
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @Test
    @DisplayName("ILF new file parser returns an error when product reference parameter is not provided")
    public void testILFNewEndpointWithoutProductRef() throws IOException {
        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("ilf_new_examples/ILF_new_example.csv");
        webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/ILF_NEW")
                        .queryParam("fileName", "someFileName")
                        .build())
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @Test
    @DisplayName("ILF new file parser returns an error when file is not provided")
    public void testILFNewEndpointWithoutBody() throws IOException {
        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("ilf_new_examples/ILF_new_example.csv");
        webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/ILF_NEW")
                        .queryParam("product", "someProductRef")
                        .queryParam("fileName", "someFileName")
                        .build())
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @Test
    @DisplayName("ILF new file parser endpoint returns an error when request method is not POST")
    public void testILFNewEndpointWithInvalidRequestMethod() {
        webTestClient
                .get()
                .uri("/files/parse/ILF_NEW")
                .exchange()
                .expectStatus()
                .isEqualTo(405);
    }
}

