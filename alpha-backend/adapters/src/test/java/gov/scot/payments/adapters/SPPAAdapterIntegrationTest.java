package gov.scot.payments.adapters;

import gov.scot.payments.customers.model.api.CustomerPaymentBatch;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.UKBankAccount;
import gov.scot.payments.payments.model.aggregate.UKBuildingSocietyAccount;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import javax.money.Monetary;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SPPAAdapterIntegrationTest {

    @Autowired
    private WebTestClient webTestClient;

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    @Test
    @DisplayName("SPPA adapter generates correct payment instructions from file payments")
    public void testSPPAEndpoint() throws IOException {

        CompositeReference product = CompositeReference.parse("sppa.someProductRef");
        String fileName = "someFileName";

        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("sppa_examples/SPPA_single_batch.txt");
        var response = webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/SPPA")
                        .queryParam("product", product.toString())
                        .queryParam("fileName", fileName)
                        .build())
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(CustomerPaymentBatch.class)
                .returnResult();

        assertEquals(1, response.getResponseBody().size());

        var customerPaymentBatch = response.getResponseBody().get(0);

        assertEquals(fileName, customerPaymentBatch.getId());
        assertEquals(39, customerPaymentBatch.getPayments().size());

        var instruction = customerPaymentBatch.getPayments().get(0);
        assertEquals("99709 AAAMONTHLY 97TH BQCS", instruction.getBatchId());
        assertNull(instruction.getAllowedMethods());
        assertEquals(Money.of(new BigDecimal("97.99"), Monetary.getCurrency("GBP")), instruction.getAmount());
        assertEquals(LocalDate.parse("02/02/2020", DATE_FORMATTER).atStartOfDay().toInstant(ZoneOffset.UTC), instruction.getLatestExecutionDate());
        assertEquals("someProductRef", instruction.getProduct().getComponent1());
        assertEquals("sppa", instruction.getProduct().getComponent0());

        assertEquals(UKBuildingSocietyAccount.class, instruction.getCreditorAccount().getClass());
        UKBuildingSocietyAccount account = (UKBuildingSocietyAccount)instruction.getCreditorAccount();
        assertEquals("D/99009099-9", account.getRollNumber());
        assertEquals("999799", account.getSortCode().getValue());
        assertEquals("99979988", account.getAccountNumber().getValue());

        assertEquals("AAAAAAAA", instruction.getCreditor().getName());
        assertEquals("999999", instruction.getCreditorMetadata().get(0).getValue());

        instruction = customerPaymentBatch.getPayments().get(1);
        assertEquals(UKBankAccount.class, instruction.getCreditorAccount().getClass());
        UKBankAccount accountUK = (UKBankAccount)instruction.getCreditorAccount();
        assertEquals("000000", accountUK.getSortCode().getValue());
        assertEquals("00000000", accountUK.getAccountNumber().getValue());

        assertEquals("AAAAAAA2", instruction.getCreditor().getName());
        assertEquals("999991", instruction.getCreditorMetadata().get(0).getValue());
    }

    @Test
    @DisplayName("SPPA adapter returns an error when given a file with invalid rows")
    public void testSPPAEndpointWithInvalidRows() throws IOException {
        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("sppa_examples/SPPA_single_batch_with_error_row.txt");
        webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/SPPA")
                        .queryParam("product", "sppa.someProductRef")
                        .queryParam("fileName", "someFileName")
                        .build())
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @Test
    @DisplayName("SPPA adapter returns an error when file name parameter is not provided")
    public void testSPPAEndpointWithoutFileName() throws IOException {
        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("sppa_examples/SPPA_single_batch.txt");
        webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/SPPA")
                        .queryParam("product", "sppa.someProductRef")
                        .build())
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @Test
    @DisplayName("SPPA adapter returns an error when product reference parameter is not provided")
    public void testSPPAEndpointWithoutProductRef() throws IOException {
        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("sppa_examples/SPPA_single_batch.txt");
        webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/SPPA")
                        .queryParam("fileName", "someFileName")
                        .build())
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @Test
    @DisplayName("SPPA adapter returns an error when file is not provided")
    public void testSPPAEndpointWithoutBody() throws IOException {
        webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/SPPA")
                        .queryParam("product", "sppa.someProductRef")
                        .queryParam("fileName", "someFileName")
                        .build())
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @Test
    public void testSPPAEndpointWithInvalidRequestMethod() {
        webTestClient
                .get()
                .uri("/files/parse/SPPA")
                .exchange()
                .expectStatus()
                .isEqualTo(405);
    }
}
