package gov.scot.payments.psps.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasAdditionalHeaders;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.util.Map;
import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "pspcore", type = "paymentCancelled")
@NoArgsConstructor
@RequiredArgsConstructor
public class PaymentCancelledEvent extends EventWithCauseImpl implements HasKey<String>, HasAdditionalHeaders {

    @NonNull private UUID paymentId;
    @NonNull private PaymentMethod method;

    @Override
    @JsonIgnore
    public String getKey() {
        return paymentId.toString();
    }

    @Override
    @JsonIgnore
    public Map<String, String> additionalHeaders() {
        return Map.of(Payment.METHOD_HEADER,method.name());
    }
}
