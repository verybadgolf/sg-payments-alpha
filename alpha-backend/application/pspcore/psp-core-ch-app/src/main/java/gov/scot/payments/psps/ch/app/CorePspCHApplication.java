package gov.scot.payments.psps.ch.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.application.component.commandhandler.stateful.EventGeneratorFunction;
import gov.scot.payments.application.component.commandhandler.stateful.StateUpdateFunction;
import gov.scot.payments.application.component.commandhandler.stateful.StatefulCommandHandlerApplication;
import gov.scot.payments.model.CommandFailureInfo;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.user.Scope;
import gov.scot.payments.psps.model.PspPayment;
import org.springframework.context.annotation.Bean;

@ApplicationComponent
public class CorePspCHApplication extends StatefulCommandHandlerApplication<PspPayment,PspPaymentState, EventWithCauseImpl> {

    @Override
    protected Class<PspPayment> stateEntityClass() {
        return PspPayment.class;
    }

    @Override
    protected Class<PspPaymentState> stateClass() {
        return PspPaymentState.class;
    }

    @Override
    protected PspPaymentState createNewAggregateInstance(SerializedMessage command, CommandFailureInfo error, PspPayment previous, PspPayment current, Long currentVersion) {
        return new PspPaymentState(command, error, previous, current, currentVersion);
    }

    @Override
    protected Scope extractScope(PspPaymentState state) {
        return state.getCurrent().getPayment().getProduct().toScope();
    }

    @Bean
    public StateUpdateFunction<PspPaymentState,PspPayment> stateUpdateFunction() {
        return new PspStateUpdater();
    }

    @Bean
    public EventGeneratorFunction<PspPaymentState, EventWithCauseImpl> eventGenerationFunction() {
        return new PspEventGenerator();
    }

    public static void main(String[] args){
        BaseApplication.run(CorePspCHApplication.class,args);
    }
}