package gov.scot.payments.paymentbatches.ch.app;

import gov.scot.payments.application.component.commandhandler.stateful.AggregateState;
import gov.scot.payments.application.kafka.MessageInfo;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.CommandFailureInfo;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.GenericErrorEvent;
import gov.scot.payments.paymentbatches.model.aggregate.PaymentBatch;
import gov.scot.payments.paymentbatches.model.command.*;
import gov.scot.payments.paymentbatches.model.event.*;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.vavr.Function3;
import io.vavr.Function5;
import io.vavr.collection.List;
import lombok.Value;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.state.KeyValueStore;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;
import org.springframework.messaging.MessageHeaders;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;

public class PaymentBatchTestHarness {



    @Value
    public static class PbTestResult {
        private final EventWithCauseImpl event;
        private final AggregateState<PaymentBatch> state;
    }

    private static final String KEY = "999";


    private static final List<Class<? extends Command>> commandClasses = List.of(
            AddPaymentToBatchCommand.class,
            ClosePaymentBatchCommand.class,
            CreatePaymentBatchCommand.class,
            FailPaymentBatchCommand.class,
            OpenPaymentBatchCommand.class
    );

    private static final List<Class<? extends EventWithCauseImpl>> eventClasses = List.of(
            PaymentBatchClosedEvent.class,
            PaymentBatchCreatedEvent.class,
            PaymentBatchFailedEvent.class,
            PaymentBatchOpenedEvent.class,
            PaymentBatchUpdatedEvent.class
    );

    private final PaymentBatchStateUpdateFunc stateUpdateHandler = spy(new PaymentBatchStateUpdateFunc());
    private final PaymentBatchEventGenerator paymentBatchEventGenerator = spy(new PaymentBatchEventGenerator());
    private final KafkaStreamsTestHarness testHarness;

    public PaymentBatchTestHarness(){
        testHarness = createPaymentBatchTestHarness();
    }


    public KafkaStreamsTestHarness createPaymentBatchTestHarness(){

        Function5<SerializedMessage, CommandFailureInfo, PaymentBatch, PaymentBatch, Long, PaymentBatchState> aggregateStateSupplier = PaymentBatchState::new;
        Function3<Command, PaymentBatch, Throwable, List<EventWithCauseImpl>> errorHandler = spy(new TestErrorHandler<>());

        var configurator = CHTestHarnessConfigurator.<PaymentBatch, PaymentBatchState>
                builder()
                .aggregateStateClass(PaymentBatchState.class)
                .stateClass(PaymentBatch.class)
                .aggregateStateSupplier(aggregateStateSupplier)
                .errorHandler(errorHandler)
                .commandClasses(commandClasses)
                .eventClasses(eventClasses)
                .eventGeneratorFunction(paymentBatchEventGenerator)
                .stateUpdateFunction(stateUpdateHandler)
                .build();

        return configurator.createAndSetUpTestHarness();
    }


    public <C extends Command> PbTestResult setStateAndSendCommand(PaymentBatch currentCustomerState, Long currentStateVersion, C command) {
        try (final TopologyTestDriver topology = testHarness.toTopology()) {

            KeyValueStore<byte[], AggregateState<PaymentBatch>> stateStore = topology.getKeyValueStore(KafkaStreamsTestHarness.STATE_STORE);

            if (currentCustomerState != null) {
                stateStore.put(KEY.getBytes(), PaymentBatchState.builder()
                        .current(currentCustomerState)
                        .currentVersion(currentStateVersion)
                        .message(new SerializedMessage(new byte[0], new MessageInfo()))
                        .build()
                );
            }
            KeyValueWithHeaders<String, C> commandMessage =
                    KeyValueWithHeaders.msg(KEY,
                            command,
                            new RecordHeader(MessageHeaders.CONTENT_TYPE, command.getClass().getName().getBytes()));

            testHarness.sendKeyValues(topology, commandMessage);

            var entities = testHarness.drainKeyValues(topology);
            Assertions.assertEquals(1, entities.size());
            return new PbTestResult((EventWithCauseImpl) entities.get(0).value, stateStore.get(KEY.getBytes()));
        }
    }

    public <C extends Command> PbTestResult setNullStateAndSendCommand( C command) {
        try (final TopologyTestDriver topology = testHarness.toTopology()) {
            KeyValueStore<byte[], AggregateState<PaymentBatch>> stateStore = topology.getKeyValueStore(KafkaStreamsTestHarness.STATE_STORE);
            KeyValueWithHeaders<String, C> commandMessage =
                    KeyValueWithHeaders.msg(KEY,
                            command,
                            new RecordHeader(MessageHeaders.CONTENT_TYPE, command.getClass().getName().getBytes()));
            testHarness.sendKeyValues(topology, commandMessage);
            var entities = testHarness.drainKeyValues(topology);
            assertEquals(1, entities.size());
            return new PbTestResult((EventWithCauseImpl) entities.get(0).value, stateStore.get(KEY.getBytes()));
        }
    }

    private void setStateInStateStore(PaymentBatch currentPaymentBatchState, Long currentStateVersion, TopologyTestDriver topology) {
        KeyValueStore<byte[], AggregateState<PaymentBatch>> stateStore = topology.getKeyValueStore(KafkaStreamsTestHarness.STATE_STORE);
        if (currentPaymentBatchState != null) {
            stateStore.put(KEY.getBytes(), PaymentBatchState.builder()
                    .current(currentPaymentBatchState)
                    .currentVersion(currentStateVersion)
                    .message(new SerializedMessage(new byte[0], new MessageInfo()))
                    .build()
            );
        }
    }



    public int getNumberOfStateUpdateCalls(){
        return Mockito.mockingDetails(stateUpdateHandler).getInvocations().size();
    }

    public int getPaymentBatchEventGeneratorCalls(){
        return Mockito.mockingDetails(paymentBatchEventGenerator).getInvocations().size();
    }

    private static class TestErrorHandler<E> implements Function3<Command, E, Throwable, List<EventWithCauseImpl>> {
        @Override
        public List<EventWithCauseImpl> apply(Command command, E entity, Throwable throwable) {
            return List.of(GenericErrorEvent.from(command, throwable));
        }
    }



}
