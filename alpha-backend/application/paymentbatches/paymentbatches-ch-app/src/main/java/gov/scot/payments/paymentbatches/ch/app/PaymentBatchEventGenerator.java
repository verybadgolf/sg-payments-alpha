package gov.scot.payments.paymentbatches.ch.app;

import gov.scot.payments.application.component.commandhandler.stateful.EventGeneratorFunction;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.paymentbatches.model.command.*;
import gov.scot.payments.paymentbatches.model.event.*;
import io.vavr.collection.List;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PaymentBatchEventGenerator extends EventGeneratorFunction.PatternMatching<PaymentBatchState, EventWithCauseImpl> {

    @Override
    protected void handlers(final PatternMatcher.Builder2<Command, PaymentBatchState, List<EventWithCauseImpl>> builder) {

        builder.match2(CreatePaymentBatchCommand.class, (c, s) -> List.of(mapToCreateEvent(c,s)))
                .match2(FailPaymentBatchCommand.class, (c, s) -> List.of(PaymentBatchFailedEvent.builder()
                                                                                .carriedState(s.getCurrent())
                                                                                .stateVersion(s.getCurrentVersion()).build()))
                .match2(OpenPaymentBatchCommand.class, (c, s) -> List.of(PaymentBatchOpenedEvent.builder()
                                                                                .carriedState(s.getCurrent())
                                                                                .stateVersion(s.getCurrentVersion()).build()))
                .match2(AddPaymentToBatchCommand.class, (c, s) -> List.of(mapToUpdateEvent(c,s)))
                .match2(ClosePaymentBatchCommand.class, (c, s) -> List.of(PaymentBatchClosedEvent.builder()
                                                                                .carriedState(s.getCurrent())
                                                                                .stateVersion(s.getCurrentVersion()).build()));

    }

    public PaymentBatchCreatedEvent mapToCreateEvent(Command command, PaymentBatchState s){
        var pme=  PaymentBatchCreatedEvent.builder()
                .carriedState(s.getCurrent())
                .stateVersion(s.getCurrentVersion()).build();
        log.debug("Created event {}", pme.toString());
        return pme;

    }

    public PaymentBatchUpdatedEvent mapToUpdateEvent(AddPaymentToBatchCommand command, PaymentBatchState s){
        var pme=  PaymentBatchUpdatedEvent.builder()
                .payment(command.getPayment())
                .carriedState(s.getCurrent())
                .user(command.getUser())
                .stateVersion(s.getCurrentVersion()).build();
        log.debug("Updated event {}", pme.toString());
        return pme;

    }


    }
