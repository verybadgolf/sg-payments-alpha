package gov.scot.payments.paymentbatches.proj.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.component.projector.springdata.RepositoryMutatingStorageService;
import gov.scot.payments.application.component.projector.springdata.jpa.JpaMutatingProjectorApplication;
import gov.scot.payments.application.component.projector.springdata.jpa.SubjectSpecification;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.user.Subject;
import gov.scot.payments.paymentbatches.proj.model.PaymentBatchProjection;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableAutoConfiguration(exclude = {HypermediaAutoConfiguration.class})
@EnableJpaRepositories(basePackageClasses = PaymentBatchesRepository.class)
@EntityScan(basePackageClasses = PaymentBatchProjection.class)
@ApplicationComponent
public class PaymentBatchesProjApplication extends JpaMutatingProjectorApplication<String, PaymentBatchProjection> {

        /*
    @Bean
    public StreamJoiner<Event,Event,Event> joiner(SchemaRegistryClient confluentSchemaRegistryClient){
        HeaderPreservingCompositeNonNativeSerde<Event> eventSerde = valueSerde(Event.class);
        return StreamJoiner.<Event, Event, Event>builder()
            .repartitionTopicName()
            .joinWindow()
            .joiner()
            .rekeyFunction()
            .leftValueSerde(eventSerde)
            .rightValueSerde(eventSerde)
            .stateSerde(stateSerde(confluentSchemaRegistryClient,SerializedMessage.class))
            .build();
    }
     */

    @Override
    protected Class<PaymentBatchProjection> projectionClass() {
        return PaymentBatchProjection.class;
    }

    @Override
    protected Specification<PaymentBatchProjection> aclCheck(Subject subject) {
        return SubjectSpecification.of(subject, (c,r,s) -> {
            if(s.hasGlobalAccess()){
                return c.and();
            }
            var productMatch = c.equal(r.get("product"), subject.getScopesAsString());
            var customerValues = c.in(r.get("customer"));
            subject.getScopes().filter(scope -> scope.getParent() == null)
                    .forEach(scope -> customerValues.value(scope.getQualifiedName()));
            return c.or(customerValues, productMatch);
        });
    }

        @Bean
        public RepositoryMutatingStorageService<String, PaymentBatchProjection> handler(Metrics metrics, PaymentBatchesRepository repository){
            return new PaymentBatchesStorageService(metrics,repository);
        }

        public static void main(String[] args){
            BaseApplication.run(PaymentBatchesProjApplication.class,args);
        }

}