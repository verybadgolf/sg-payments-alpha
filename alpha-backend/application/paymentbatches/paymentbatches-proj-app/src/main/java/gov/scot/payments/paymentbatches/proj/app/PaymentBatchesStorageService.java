package gov.scot.payments.paymentbatches.proj.app;

import gov.scot.payments.application.component.projector.springdata.ProjectionRepository;
import gov.scot.payments.application.component.projector.springdata.RepositoryMutatingStorageService;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Event;
import gov.scot.payments.paymentbatches.model.aggregate.PaymentBatch;
import gov.scot.payments.paymentbatches.model.event.*;
import gov.scot.payments.paymentbatches.proj.model.PaymentBatchProjection;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PaymentBatchesStorageService extends RepositoryMutatingStorageService<String, PaymentBatchProjection>{

    public PaymentBatchesStorageService(final Metrics metrics, final ProjectionRepository<String, PaymentBatchProjection> repository) {
        super(metrics, repository);
    }

    @Override
    protected boolean shouldDelete(Event event) {
        return false;
    }

    @Override
    protected void idExtractors(PatternMatcher.Builder2<Event,String,String> builder) {
        builder.match2(BasePaymentBatchEventWithCause.class, (e, k) -> e.getKey());
    }

    @Override
    protected void createHandlers(PatternMatcher.Builder<Event, PaymentBatchProjection> builder) {
        builder.match(PaymentBatchCreatedEvent.class, e -> PaymentBatchProjection.fromCreateEvent(e));
        // TODO error handling?
        //  .match(TestErrorEvent.class, e -> {throw new RuntimeException();});
    }

    @Override
    protected void updateHandlers(PatternMatcher.Builder2<Event, PaymentBatchProjection, PaymentBatchProjection> builder) {
        builder.match2(PaymentBatchUpdatedEvent.class, (e, s) -> s.updateProjectionFromNewPayment(e))
                .match2(PaymentBatchClosedEvent.class, (e, s) -> s.toBuilder().status(PaymentBatch.Status.Closed).build())
                .match2(PaymentBatchFailedEvent.class, (e, s) -> s.updateProjectionFromFailedPayment(e));
    }





}
