package gov.scot.payments.paymentbatches.proj.app;

import gov.scot.payments.application.component.projector.springdata.ProjectionRepository;
import gov.scot.payments.application.component.projector.springdata.RepositoryMutatingStorageService;
import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.paymentbatches.model.aggregate.PaymentBatch;
import gov.scot.payments.paymentbatches.model.event.PaymentBatchCreatedEvent;
import gov.scot.payments.paymentbatches.model.event.PaymentBatchUpdatedEvent;
import gov.scot.payments.paymentbatches.proj.model.PaymentBatchProjection;
import gov.scot.payments.payments.model.aggregate.PaymentStatus;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.Tuple2;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.time.Instant;
import java.util.HashMap;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

@AutoConfigurationPackage
@EntityScan("gov.scot.payments.paymentbatches.proj.model")
@DataJpaTest(properties = {"spring.datasource.url=jdbc:h2:mem:PaymentBatchesStorageServiceTest"})
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=validate",
        "spring.flyway.locations=classpath:db/migration/h2",
        "spring.flyway.schemas=",
        "spring.jpa.properties.hibernate.default_schema="
})
@ContextConfiguration(classes = PaymentBatchesStorageServiceTest.TestConfiguration.class)
public class PaymentBatchesStorageServiceTest {

    @Autowired
    private RepositoryMutatingStorageService<String, PaymentBatchProjection> service;

    @Autowired
    private ProjectionRepository<String,PaymentBatchProjection> jpaRepository;

    @Autowired
    private JpaRepository<PaymentBatchProjection, String> repository;

    @AfterEach
    public void tearDown(@Autowired JpaRepository repository) {
        repository.deleteAll();
    }

    @Test
    public void testCreateProjection() throws MalformedURLException {

        var exampleBatch = PaymentBatchExampleData.createExampleBatch(PaymentBatch.Status.New);
        var event = PaymentBatchCreatedEvent.builder()
                .carriedState(exampleBatch)
                .build();

        var currencyTotals = new HashMap<String, BigDecimal>();
        currencyTotals.put("GBP", new BigDecimal(12.00));

        var prodcuts = new HashSet<String>();
        prodcuts.add("product1");

        PaymentBatchProjection projection = PaymentBatchProjection.builder()
                .id(exampleBatch.getId().toString())
                .status(PaymentBatch.Status.Closed)
                .totalNumberOfPayments(10)
                .currencyTotals(currencyTotals)
                .products(prodcuts)
                .processingTime(Instant.now())
                .customer("customer")
                .product("customer.product1")
                .name("name")
                .build();

        Tuple2<String,PaymentBatchProjection> entity = service.apply(event.getKey(),event);
        assertEquals(1,repository.count());
        var existing = repository.getOne(event.getKey());
        assertEquals(existing.getCurrencyTotals().get("GBP").intValue(), 0);
        assertEquals(existing.getName(), "mybatch");

    }

    @Test
    public void testUpdateEventProjection() throws MalformedURLException {

        var exampleBatch = PaymentBatchExampleData.createExampleBatch(PaymentBatch.Status.Closed);
        setupExampleProjection(exampleBatch);

        var payment = PaymentBatchExampleData.createExamplePayment(exampleBatch.getId());
        var updated = PaymentBatchUpdatedEvent.builder()
                .payment(payment)
                .carriedState(exampleBatch)
                .build();
        Tuple2<String,PaymentBatchProjection> entity2 = service.apply(updated.getKey(),updated);
        assertEquals(1,repository.count());
        var existing = repository.getOne(exampleBatch.getId().toString());
        assertEquals(new BigDecimal(24.00), existing.getCurrencyTotals().get("GBP"));
        assertEquals(11, existing.getTotalNumberOfPayments());
        assertEquals(2, existing.getTotalErrorPayments());
        assertEquals(3, existing.getTotalPendingPayments());

    }

    @Test
    public void testUpdateEventForInvalidPayment() throws MalformedURLException {

        var exampleBatch = PaymentBatchExampleData.createExampleBatch(PaymentBatch.Status.Closed);
        setupExampleProjection(exampleBatch);

        var payment = PaymentBatchExampleData.createExamplePayment(exampleBatch.getId());
        payment = payment.toBuilder().status(PaymentStatus.Invalid).build();

        var updated = PaymentBatchUpdatedEvent.builder()
                .payment(payment)
                .carriedState(exampleBatch)
                .build();
        Tuple2<String,PaymentBatchProjection> entity2 = service.apply(updated.getKey(),updated);
        assertEquals(1,repository.count());
        var existing = repository.getOne(exampleBatch.getId().toString());
        assertEquals(new BigDecimal(24), existing.getCurrencyTotals().get("GBP"));
        assertEquals(11, existing.getTotalNumberOfPayments());
        assertEquals(3, existing.getTotalErrorPayments());
        assertEquals(3, existing.getTotalPendingPayments());

    }

    @Test
    public void testUpdateEventForInternationalPayment() throws MalformedURLException {

        var exampleBatch = PaymentBatchExampleData.createExampleBatch(PaymentBatch.Status.Closed);
        setupExampleProjection(exampleBatch);

        var payment = PaymentBatchExampleData.createExamplePayment(exampleBatch.getId());
        payment = payment.toBuilder().amount(Money.of(10.0, "EUR")).build();

        var updated = PaymentBatchUpdatedEvent.builder()
                .payment(payment)
                .carriedState(exampleBatch)
                .build();
        Tuple2<String,PaymentBatchProjection> entity2 = service.apply(updated.getKey(),updated);
        assertEquals(1,repository.count());
        var existing = repository.getOne(exampleBatch.getId().toString());
        assertEquals(new BigDecimal(12), existing.getCurrencyTotals().get("GBP"));
        assertEquals(10, existing.getCurrencyTotals().get("EUR").intValue());
        assertEquals(11, existing.getTotalNumberOfPayments());
        assertEquals(2, existing.getTotalErrorPayments());
        assertEquals(3, existing.getTotalPendingPayments());

    }

    private void setupExampleProjection(PaymentBatch exampleBatch) {
        var currencyTotals = new HashMap<String, BigDecimal>();
        currencyTotals.put("GBP", new BigDecimal(12.00));
        var products = new HashSet<String>();
        products.add("product1");

        PaymentBatchProjection projection = PaymentBatchProjection.builder()
                .id(exampleBatch.getId().toString())
                .name("test-name")
                .status(PaymentBatch.Status.Closed)
                .totalNumberOfPayments(10)
                .currencyTotals(currencyTotals)
                .totalErrorPayments(2)
                .totalPendingPayments(3)
                .products(products)
                .processingTime(Instant.now())
                .customer("customer")
                .product("customer.product1")
                .build();

        jpaRepository.save(projection);
    }


    @Configuration
    public static class TestConfiguration {

        @Bean
        public RepositoryMutatingStorageService<String, PaymentBatchProjection> service(PaymentBatchesRepository repository){
            return new PaymentBatchesStorageService(new MicrometerMetrics(new SimpleMeterRegistry()), repository);
        }

    }

}
