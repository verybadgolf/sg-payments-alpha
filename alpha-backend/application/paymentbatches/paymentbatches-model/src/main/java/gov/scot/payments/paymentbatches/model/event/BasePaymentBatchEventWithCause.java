package gov.scot.payments.paymentbatches.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.HasState;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.paymentbatches.model.aggregate.PaymentBatch;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@NoArgsConstructor
@MessageType(context = "paymentbatches", type = "basePaymentBatchEventWithCause")
public class BasePaymentBatchEventWithCause extends EventWithCauseImpl implements HasState<PaymentBatch>, HasKey<String> {

    private PaymentBatch carriedState;
    private Long stateVersion;

    @Override
    @JsonIgnore
    public String getKey() {
        return carriedState.getKey();
    }

    @Override
    @JsonIgnore
    public void setCarriedState(PaymentBatch state, Long version) {
        this.carriedState = state;
        this.stateVersion = version;
    }
}
