package gov.scot.payments.paymentbatches.proj.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.Projection;
import gov.scot.payments.paymentbatches.model.aggregate.PaymentBatch;
import gov.scot.payments.paymentbatches.model.event.PaymentBatchCreatedEvent;
import gov.scot.payments.paymentbatches.model.event.PaymentBatchFailedEvent;
import gov.scot.payments.paymentbatches.model.event.PaymentBatchUpdatedEvent;
import gov.scot.payments.payments.model.aggregate.Payment;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.reflect.AvroIgnore;
import org.apache.avro.reflect.Nullable;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.MathContext;
import java.time.Instant;
import java.util.*;

import static gov.scot.payments.paymentbatches.proj.model.PaymentStatusMapper.isErrorStatus;
import static gov.scot.payments.paymentbatches.proj.model.PaymentStatusMapper.isPendingStatus;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode(of = "id")
@Slf4j
public class PaymentBatchProjection implements Projection<String>, HasKey<String>{

    @NonNull @Id private String id;

    @NonNull @Builder.Default private Integer totalNumberOfPayments = 0;

    // change to total per currency
    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(name="PAYMENT_BATCH_PROJECTION_CURRENCY_TOTALS", joinColumns=@JoinColumn(name="payment_batch_projection_id"))
    @MapKeyColumn(name="currency_str")
    @Column(name="total_amount")
    @NonNull private Map<String, BigDecimal> currencyTotals;
    @NonNull @Builder.Default private Integer totalErrorPayments = 0;
    @NonNull @Builder.Default private Integer totalPendingPayments = 0;


    @NonNull @Enumerated(EnumType.STRING) @Column(length = 20) private PaymentBatch.Status status;
    @Nullable @Builder.Default private String errorMessage = "";
    @NonNull private String name;

    @ElementCollection(fetch = FetchType.EAGER)
    @NonNull private Set<String> products;
    @NonNull private String customer;

    @JsonIgnore @AvroIgnore
    // this is a comma separated list for acls in quicksight and not for frontend
    private String product;

    public static String GBP_CURRENCY_CODE = "GBP";

    @NonNull @Builder.Default private Instant processingTime = Instant.now();

    public static PaymentBatchProjection fromCreateEvent(PaymentBatchCreatedEvent event){

        log.info("Creating projection from  event {}", event.toString());

        HashMap<String, BigDecimal> currencyTotals = createDefaultTotalsMap();

        return PaymentBatchProjection.builder()
                .id(event.getKey())
                .status(event.getCarriedState().getStatus())
                .currencyTotals(currencyTotals)
                .name(event.getCarriedState().getName())
                .processingTime(Instant.now())
                .products(Collections.emptySet())
                .customer(event.getCarriedState().getCustomerId())
                .name(event.getCarriedState().getName())
                .build();
    }

    private static HashMap<String, BigDecimal> createDefaultTotalsMap() {
        var currencyTotals = new HashMap<String, BigDecimal>();
        currencyTotals.put(GBP_CURRENCY_CODE, new BigDecimal("0.00"));
        return currencyTotals;
    }

    public PaymentBatchProjection updateProjectionFromNewPayment(PaymentBatchUpdatedEvent event){

        var payment = event.getPayment();

        var currency = payment.getAmount().getCurrency().getCurrencyCode();
        var updatedCurrencyTotals = getUpdatedCurrencyTotalsMap(payment, currency);

        var newNumPayments = totalNumberOfPayments + 1;
        var newTotalError = totalErrorPayments;
        if(isErrorStatus(payment.getStatus())){
            newTotalError = totalErrorPayments + 1;
        }
        var newTotalPending = totalPendingPayments;
        if(isPendingStatus(payment.getStatus())){
            newTotalPending = totalPendingPayments + 1;
        }

        var newProducts = new HashSet<String>();
        newProducts.addAll(getProducts());
        newProducts.add(payment.getProduct().getComponent1());

        var newAclProducts = getNewAclCustomerProductString(payment);

        return toBuilder()
                .currencyTotals(updatedCurrencyTotals)
                .totalNumberOfPayments(newNumPayments)
                .totalPendingPayments(newTotalPending)
                .totalErrorPayments(newTotalError)
                .totalPendingPayments(newTotalPending)
                .status(event.getCarriedState().getStatus())
                .products(newProducts)
                .product(newAclProducts)
                .build();
    }

    public PaymentBatchProjection updateProjectionFromFailedPayment(PaymentBatchFailedEvent event){
        return toBuilder()
                .status(PaymentBatch.Status.Failed)
                .errorMessage(event.getCarriedState().getMessage())
                .build();
    }

    private String getNewAclCustomerProductString(Payment payment){

        var customerProductSet = new HashSet<String>();

        if(product != null && !product.isEmpty()) {
            var currentCustomerProductList = Arrays.asList(product.split(","));
            customerProductSet.addAll(currentCustomerProductList);
        }
        customerProductSet.add(payment.getProduct().toString());
        return String.join(",", customerProductSet);
    }


    private HashMap<String, BigDecimal> getUpdatedCurrencyTotalsMap(Payment payment, String currency) {

        var amount = payment.getAmount().getNumberStripped();

        var updatedCurrencyTotals = new HashMap<String, BigDecimal>();
        updatedCurrencyTotals.putAll(currencyTotals);
        var existing = updatedCurrencyTotals.get(currency);
        if(existing == null){
            updatedCurrencyTotals.put(currency, amount);
        } else {
            var total = existing.add( amount);
            updatedCurrencyTotals.put(currency, total);
        }
        return updatedCurrencyTotals;
    }

    @Override
    @JsonIgnore
    public String getKey(){
        return id;
    }
}