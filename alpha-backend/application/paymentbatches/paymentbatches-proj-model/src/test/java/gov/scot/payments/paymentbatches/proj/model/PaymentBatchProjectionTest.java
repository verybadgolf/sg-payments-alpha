package gov.scot.payments.paymentbatches.proj.model;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.paymentbatches.model.aggregate.PaymentBatch;
import gov.scot.payments.paymentbatches.model.event.PaymentBatchCreatedEvent;
import gov.scot.payments.paymentbatches.model.event.PaymentBatchUpdatedEvent;
import gov.scot.payments.payments.model.aggregate.*;
import io.vavr.collection.List;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;

import static gov.scot.payments.payments.model.aggregate.PaymentMethod.Bacs;
import static org.junit.jupiter.api.Assertions.assertEquals;

class PaymentBatchProjectionTest {

    @Test
    void fromCreateEvent() throws MalformedURLException {

        var exampleBatch = createExampleBatch(PaymentBatch.Status.New);

        var event = PaymentBatchCreatedEvent.builder()
                .carriedState(exampleBatch)
                .build();
        var newState = PaymentBatchProjection.fromCreateEvent(event);

        var currency = new HashMap<String, BigDecimal>();
        currency.put("GBP", new BigDecimal("0.00"));

        assertEquals(currency, newState.getCurrencyTotals());
        assertEquals(0, newState.getTotalNumberOfPayments());
        assertEquals(0, newState.getTotalErrorPayments());
        assertEquals(0, newState.getTotalPendingPayments());
        assertEquals(exampleBatch.getId(), newState.getId());
        assertEquals(exampleBatch.getName(), newState.getName());
    }

    @Test
    void updateProjectionFromSuccessfulPayment() throws MalformedURLException {

        UUID id = UUID.randomUUID();

        PaymentBatchProjection current = createPaymentBatchProjection(id);
        var payment = createPayment(id);

        var exampleBatch = createExampleBatch(PaymentBatch.Status.New);

        var event = PaymentBatchUpdatedEvent.builder()
                .payment(payment)
                .carriedState(exampleBatch)
                .build();

        var updated = current.updateProjectionFromNewPayment(event);

        assertEquals(new BigDecimal("17.42"), updated.getCurrencyTotals().get("GBP"));
        assertEquals(14, updated.getTotalNumberOfPayments());
        assertEquals(2, updated.getTotalErrorPayments());
        assertEquals(1, updated.getTotalPendingPayments());
        assertEquals(Set.of("product2", "product1"), updated.getProducts());
        assertEquals("customer", updated.getCustomer());
        assertEquals("customer.product2,customer.product1", updated.getProduct());
    }

    @Test
    void updateProjectionFromSuccessfulPaymentWithDifferentCurrency() throws MalformedURLException {

        UUID id = UUID.randomUUID();

        PaymentBatchProjection current = createPaymentBatchProjection(id);
        var payment = createPayment(id);

        payment = payment.toBuilder().amount(Money.of(4.34, "EUR")).build();

        var exampleBatch = createExampleBatch(PaymentBatch.Status.New);

        var event = PaymentBatchUpdatedEvent.builder()
                .payment(payment)
                .carriedState(exampleBatch)
                .build();

        var updated = current.updateProjectionFromNewPayment(event);

        assertEquals(new BigDecimal("10.00"), updated.getCurrencyTotals().get("GBP"));
        assertEquals(new BigDecimal("8.34"), updated.getCurrencyTotals().get("EUR"));
        assertEquals(14, updated.getTotalNumberOfPayments());
        assertEquals(2, updated.getTotalErrorPayments());
        assertEquals(1, updated.getTotalPendingPayments());
        assertEquals(Set.of("product2", "product1"), updated.getProducts());
        assertEquals("customer", updated.getCustomer());
        assertEquals("customer.product2,customer.product1", updated.getProduct());
    }

    @Test
    void updateProjectionFromPendingPayment() throws MalformedURLException {

        UUID id = UUID.randomUUID();

        PaymentBatchProjection current = createPaymentBatchProjection(id);
        var payment = createPayment(id).toBuilder().status(PaymentStatus.ApprovalRequired).build();
        var exampleBatch = createExampleBatch(PaymentBatch.Status.Open);

        var event = PaymentBatchUpdatedEvent.builder()
                .payment(payment)
                .carriedState(exampleBatch)
                .build();

        var updated = current.updateProjectionFromNewPayment(event);

        assertEquals(new BigDecimal("17.42"), updated.getCurrencyTotals().get("GBP"));
        assertEquals(14, updated.getTotalNumberOfPayments());
        assertEquals(2, updated.getTotalErrorPayments());
        assertEquals(2, updated.getTotalPendingPayments());
        assertEquals(Set.of("product2", "product1"), updated.getProducts());
        assertEquals("customer", updated.getCustomer());
        assertEquals("customer.product2,customer.product1", updated.getProduct());
    }

    @Test
    void updateProjectionFromErrorPayment() throws MalformedURLException {

        UUID id = UUID.randomUUID();

        PaymentBatchProjection current = createPaymentBatchProjection(id);
        var payment = createPayment(id).toBuilder().status(PaymentStatus.Invalid).build();
        var exampleBatch = createExampleBatch(PaymentBatch.Status.Open);

        var event = PaymentBatchUpdatedEvent.builder()
                .payment(payment)
                .carriedState(exampleBatch)
                .build();

        var updated = current.updateProjectionFromNewPayment(event);

        assertEquals(new BigDecimal("17.42"), updated.getCurrencyTotals().get("GBP"));
        assertEquals(14 , updated.getTotalNumberOfPayments());
        assertEquals(3, updated.getTotalErrorPayments());
        assertEquals(1, updated.getTotalPendingPayments());
        assertEquals(Set.of("product2", "product1"), updated.getProducts());
    }

    private Payment createPayment(UUID id) {
        return Payment.builder()
                .createdBy("Test")
                .allowedMethods(List.of(Bacs))
                .amount(Money.of(7.42, "GBP"))
                .latestExecutionDate(Instant.now())
                .product(CompositeReference.parse("customer.product2"))
                .batchId(id.toString())
                .status(PaymentStatus.Paid)
                .creditor(PartyIdentification.builder().name("person").build())
                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                      .debtor(PartyIdentification.builder().name("person").build())
                      .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                      .build();
    }

    private PaymentBatchProjection createPaymentBatchProjection(UUID id) {

        var map = new HashMap<String, BigDecimal>();
        map.put("GBP", new BigDecimal("10.00"));
        map.put("EUR", new BigDecimal("4.00"));

        return PaymentBatchProjection.builder()
                    .id(id.toString())
                    .status(PaymentBatch.Status.New)
                    .currencyTotals(map)
                    .totalErrorPayments(2)
                    .totalNumberOfPayments(13)
                    .totalPendingPayments(1)
                    .customer("customer")
                    .product("customer.product1")
                    .products(Set.of("product1"))
                    .name("mybatch")
                    .build();
    }

    public static PaymentBatch createExampleBatch(PaymentBatch.Status status) throws MalformedURLException {
        String id = "id";
        var file = new URL("http://myfile.csv");
        // Request to create a customer
        return PaymentBatch.builder()
                .payments(List.empty())
                .id(id)
                .file(file)
                .customerId("customer")
                .createdBy("test-user")
                .createdAt(Instant.now())
                .message("this is a test message")
                .name("mybatch")
                .status(status)
                .build();
    }
}