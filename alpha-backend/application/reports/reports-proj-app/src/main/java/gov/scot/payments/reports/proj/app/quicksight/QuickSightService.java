package gov.scot.payments.reports.proj.app.quicksight;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import gov.scot.payments.reports.proj.app.quicksight.config.QuickSightConfigFile;
import gov.scot.payments.reports.proj.app.quicksight.config.QuickSightDataSetConfig;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import software.amazon.awssdk.auth.credentials.EnvironmentVariableCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.quicksight.QuickSightClient;

import java.io.File;
import java.io.IOException;

@Builder
@Slf4j
public class QuickSightService {

    @NonNull private final String environmentName;
    @NonNull private final String datasourceName;
    @NonNull private final String aclDatasetName;
    @NonNull private final QuickSightWrapper quickSightWrapper;
    @NonNull private final String quickSightRoleArn;


    private String getDatasetNameForEnv(String datasetName){
        return environmentName+"_"+datasetName;
    }

    public static QuickSightConfigFile readConfigFile(File file) throws IOException {
        var om = new ObjectMapper(new YAMLFactory());
        return om.readValue(file, QuickSightConfigFile.class);
    }

    public List<String> createDataSetsFromConfigFile(QuickSightConfigFile file){

        var dataSourceArn = quickSightWrapper.getDatasourceArnFromName(datasourceName);
        var aclDataSetArn = quickSightWrapper.getDatasetArnFromName(aclDatasetName);
        var dataSetConfigs = List.ofAll(file.getDatasets());
        return dataSetConfigs.map( dataset -> createDataSetFromConfigIfNotExists(dataSourceArn, aclDataSetArn, dataset));
    }

    private String createDataSetFromConfigIfNotExists(String datasourceArn, String aclDatasetArn, QuickSightDataSetConfig config){

        var datasetName = getDatasetNameForEnv(config.getName());
        var datasetSummary = quickSightWrapper.getDataSetByName(datasetName);
        if(!datasetSummary.isEmpty()){
            var id = datasetSummary.get().dataSetId();
            log.info("Found existing dataset {} with name {}", id, datasetName);
            return id;
        }

        var columns = List.ofAll(config.getColumns()).map(col -> col.toInputColumn());

        var datasetInfo = QuickSightDataSetInfo.builder()
                .datasetName(datasetName)
                .columns(columns)
                .principalArn(quickSightRoleArn)
                .aclDatasetArn(aclDatasetArn)
                .sqlQuery(config.getQuery())
                .dataSourceArn(datasourceArn)
                .build();

        log.info("Creating new quicksight dataset {}", datasetName);
        return quickSightWrapper.createDataSetFromSqlQueryTable(datasetInfo);
    }

    public static void main(String[] args) {

        var accountId = System.getenv("AWS_ACCOUNT_ID");
        var quicksightArn = System.getenv("QUICKSIGHT_GROUP_NAME");


        var provider = EnvironmentVariableCredentialsProvider.create();
        var quickSightClient = QuickSightClient.builder()
                .credentialsProvider(provider)
                .region(Region.EU_WEST_2)
                .build();

        var quickSightWrapper = QuickSightWrapper.builder()
                .awsAccountId(accountId)
                .quickSightClient(quickSightClient)
                .build();

        var service = QuickSightService.builder()
                .quickSightWrapper(quickSightWrapper)
                .environmentName("test")
                .datasourceName("developds")
                .quickSightRoleArn(quicksightArn)
                .build();



    }

}
