package gov.scot.payments.reports.proj.app.quicksight;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.ToString;
import lombok.Value;
import software.amazon.awssdk.services.quicksight.model.InputColumn;

@Value
@Builder
@ToString
public class QuickSightDataSetInfo {

    private String datasetName;
    private String dataSourceArn;
    private String sqlQuery;
    private List<InputColumn> columns;
    private String principalArn;
    private String aclDatasetArn;


}
