package gov.scot.payments.payments.ch.app;

import gov.scot.payments.customers.model.aggregate.PaymentMethodConfiguration;
import gov.scot.payments.payments.model.aggregate.*;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.objectlab.kit.datecalc.common.DateCalculator;
import net.objectlab.kit.datecalc.common.HolidayCalendar;
import net.objectlab.kit.datecalc.common.PeriodCountCalculator;
import net.objectlab.kit.datecalc.jdk8.LocalDateKitCalculatorsFactory;
import org.javamoney.moneta.Money;
import org.springframework.cglib.core.Local;
import org.springframework.security.access.method.P;
import org.springframework.util.StringUtils;
import org.threeten.extra.Temporals;

import javax.money.Monetary;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

public class PaymentValidator {

    private static final Pattern BACS_SERVICE_USER_NUMBER_PATTERN = Pattern.compile("[0-9]{6}");
    private static final Pattern BACS_SERVICE_USER_NAME_PATTERN = Pattern.compile("[0-9]*");
    private static final Money BACS_PAYMENT_LIMIT = Money.parse("GBP 250000");
    private static final LocalTime BACS_CUT_OFF = LocalTime.of(16,0);

    public PaymentValidator(HolidayCalendar<LocalDate> holidays){
        LocalDateKitCalculatorsFactory.getDefaultInstance().registerHolidays("UK", holidays);
    }

    public Payment validate(Payment payment){
        LocalDateTime now = LocalDateTime.now();
        return PaymentValidation.of(payment)
                .and(p -> PaymentValidationResult.of(!p.getAmount().isNegativeOrZero(),() -> "Amount must be > 0"))
                .and(p -> PaymentValidationResult.of(!p.getAllowedMethods().isEmpty(),() -> "At least one payment method must be provided"))
                .and(p -> PaymentValidationResult.of(p.getAllowedMethods().forAll(Objects::nonNull),() -> "All payment methods must not be null"))
                .and(p -> PaymentValidationResult.of(!p.getEarliestExecutionDate().isAfter(p.getLatestExecutionDate()),() -> "Earliest payment date must not be after latest payment date"))
                .and(p -> PaymentValidationResult.of(p.getAllowedMethods().toSet().size() == p.getAllowedMethods().size(),() -> "Payment methods must be unique"))
                .and(p -> PaymentValidationResult.of(!p.getLatestExecutionDate().isBefore(Instant.now()),() -> "Latest payment date must be in the future"))
                .andIfSuccess( p -> p.getAllowedMethods()
                        .map(method -> new Tuple2<>(List.of(method),validatePaymentMethod(p,method,now)))
                        .foldLeft(new Tuple2<>(List.<PaymentMethod>empty(),PaymentValidationResult.success())
                                , (t1, t2) -> new Tuple2<>(t2._2.isSuccess() ? t1._1.appendAll(t2._1): t1._1,t1._2.merge(t2._2)))
                        .apply( (m,r) -> new PaymentValidation(p
                                .toBuilder()
                                .allowedMethods(m)
                                .build(),r)))
                .getPaymentWithResult();

    }

    private PaymentValidationResult validatePaymentMethod(Payment payment, PaymentMethod method, LocalDateTime now) {
        PaymentValidation result = new PaymentValidation(payment,PaymentValidationResult.failure("Payment method not recognized"));
        switch (method) {
            case Chaps:
                result = validateChaps(payment,now);
                break;
            case Bacs:
                result = validateBacs(payment,now);
                break;
            case FasterPayments:
                result = validateFasterPayments(payment,now);
                break;
            case Sepa_DC:
                result = validateSepa(payment,now);
                break;
            case iMovo:
                result = validateIMovo(payment,now);
                break;
        }
        return result.getResult();
    }

    private PaymentValidation validateIMovo(final Payment payment, LocalDateTime now) {
        return PaymentValidation.of(payment)
                .and(p -> validatePaymentDate(p,now,3,"i-movo"))
                .and(p -> PaymentValidationResult.of(!StringUtils.isEmpty(p.getCreditor().getName()),() -> "Creditor name must be provided"))
                .and(p -> PaymentValidationResult.of(p.getCreditor().getContactDetails() != null && !StringUtils.isEmpty(p.getCreditor().getContactDetails().getMobileNumber()),() -> "Creditor mobile number must be provided"));
    }

    private PaymentValidation validateSepa(final Payment payment, LocalDateTime now) {
        return PaymentValidation.of(payment)
                .and(p -> validatePaymentDate(p,now,1,"SEPA"))
                .and(p -> PaymentValidationResult.of(!StringUtils.isEmpty(p.getCreditor().getName()),() -> "Creditor name must be provided"))
                .and(p -> PaymentValidationResult.of(p.getCreditorAccountAs(SepaBankAccount.class).isDefined(),() -> "Sepa bank account must be provided"))
                .and(p -> PaymentValidationResult.of(p.getCreditorAccountAs(SepaBankAccount.class).filter(a -> a.getCurrency().equals("EUR")).isDefined(),() -> "Sepa bank account must be in EUR"));
    }

    private PaymentValidation validateFasterPayments(final Payment payment, LocalDateTime now) {
        return PaymentValidation.of(payment)
                .and(p -> PaymentValidationResult.of(p.getAmount().isLessThan(BACS_PAYMENT_LIMIT),() -> String.format("amount must be < %s",BACS_PAYMENT_LIMIT)))
                .and(p -> PaymentValidationResult.of(!StringUtils.isEmpty(p.getCreditor().getName()),() -> "Creditor name must be provided"))
                .and(p -> PaymentValidationResult.of(p.getCreditorAccountAs(UKBankAccount.class).isDefined(),() -> "UK bank account must be provided"));
    }

    private PaymentValidation validateBacs(final Payment payment, LocalDateTime now) {
        return PaymentValidation.of(payment)
                .and(p -> validatePaymentDate(p,now,3,"BACS"))
                .and(p -> PaymentValidationResult.of(p.getAmount().isLessThan(BACS_PAYMENT_LIMIT),() -> String.format("amount must be < %s",BACS_PAYMENT_LIMIT)))
                .and(p -> PaymentValidationResult.of(!StringUtils.isEmpty(p.getCreditor().getName()),() -> "Creditor name must be provided"))
                .and(p -> PaymentValidationResult.of(p.getCreditorAccountAs(UKBankAccount.class).isDefined(),() -> "UK bank account must be provided"))
                .and(p -> PaymentValidationResult.of(p.getMethodProperty(PaymentMethod.Bacs,PaymentMethodConfiguration.BACS_SERVICE_USER_NUMBER).filter(s -> BACS_SERVICE_USER_NUMBER_PATTERN.matcher(s).matches()).isDefined(),() -> "BACS SUN must be provided"))
                .and(p -> PaymentValidationResult.of(p.getMethodProperty(PaymentMethod.Bacs,PaymentMethodConfiguration.BACS_SERVICE_ACCOUNT_NAME).filter(s -> BACS_SERVICE_USER_NAME_PATTERN.matcher(s).matches()).isDefined(),() -> "BACS SAN must be provided"));
    }

    private PaymentValidation validateChaps(final Payment payment, LocalDateTime now) {
        return PaymentValidation.of(payment)
                .and(p -> validatePaymentDate(p,now,0,"CHAPS"))
                .and(p -> PaymentValidationResult.of(p.getCreditorAccountAs(UKBankAccount.class).isDefined(),() -> "UK bank account must be provided"));
    }

    PaymentValidationResult validatePaymentDate(Payment p, LocalDateTime now, int settlementCycle, String method) {
        LocalDate adjustedLatestDate = adjustPaymentDateForWeekendsAndHolidays(p.getLatestExecutionDate());
        long availableWorkingDays = getWorkingDaysBetween(now.toLocalDate(),adjustedLatestDate);
        if(availableWorkingDays < settlementCycle || availableWorkingDays == settlementCycle && now.toLocalTime().isAfter(BACS_CUT_OFF)){
            return PaymentValidationResult.failure(String.format("Insufficient time to make %s payment by: %s",method,adjustedLatestDate));
        }
        return PaymentValidationResult.success();
    }

    private LocalDate adjustPaymentDateForWeekendsAndHolidays(Instant date) {
        DateCalculator<LocalDate> cal = LocalDateKitCalculatorsFactory.backwardCalculator("UK");
        cal.setStartDate(LocalDate.ofInstant(date, ZoneId.of("UTC")));
        return cal.getCurrentBusinessDate();
    }

    private long getWorkingDaysBetween(LocalDate start, LocalDate end) {
        DateCalculator<LocalDate> cal = LocalDateKitCalculatorsFactory.backwardCalculator("UK");
        return start.datesUntil( end )
                .filter( d -> !cal.isNonWorkingDay(d) )
                .count();
    }

}
