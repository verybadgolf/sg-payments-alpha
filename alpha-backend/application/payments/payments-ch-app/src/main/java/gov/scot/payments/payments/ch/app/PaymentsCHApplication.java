package gov.scot.payments.payments.ch.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.application.component.commandhandler.stateful.EventGeneratorFunction;
import gov.scot.payments.application.component.commandhandler.stateful.StateUpdateFunction;
import gov.scot.payments.application.component.commandhandler.stateful.StatefulCommandHandlerApplication;
import gov.scot.payments.model.CommandFailureInfo;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.user.Scope;
import gov.scot.payments.model.user.Subject;
import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import net.objectlab.kit.datecalc.common.DefaultHolidayCalendar;
import net.objectlab.kit.datecalc.common.HolidayCalendar;
import org.springframework.context.annotation.Bean;
import gov.scot.payments.payments.model.aggregate.Payment;

import java.time.LocalDate;

@ApplicationComponent
public class PaymentsCHApplication extends StatefulCommandHandlerApplication<Payment, PaymentsState, EventWithCauseImpl> {

    @Override
    protected Set<String> registeredVerbs() {
        return HashSet.of("Read","Create","ApprovePayment","CancelPayment");
    }

    @Override
    protected Class<Payment> stateEntityClass() {
        return Payment.class;
    }

    @Override
    protected Class<PaymentsState> stateClass() {
        return PaymentsState.class;
    }

    @Override
    protected PaymentsState createNewAggregateInstance(SerializedMessage command, CommandFailureInfo error, Payment previous, Payment current, Long currentVersion) {
        return new PaymentsState(command, error, previous, current, currentVersion);
    }

    @Override
    protected Scope extractScope(PaymentsState state) {
        return state.getCurrent().getProduct().toScope();
    }


    //TODO: actually wire this data in from somewhere
    @Bean
    public HolidayCalendar<LocalDate> holidays(){
        return new DefaultHolidayCalendar<>();
    }

    @Bean
    public PaymentValidator paymentValidator(HolidayCalendar<LocalDate> holidays){
        return new PaymentValidator(holidays);
    }

    @Bean
    public StateUpdateFunction<PaymentsState,Payment> stateUpdateFunction(PaymentValidator validator) {
        return new PaymentsStateUpdateFunc(validator);
    }

    @Bean
    public EventGeneratorFunction<PaymentsState, EventWithCauseImpl> eventGenerationFunction() {
        return new PaymentsEventGenerator();
    }

    public static void main(String[] args){
        BaseApplication.run(PaymentsCHApplication.class,args);
    }
}