package gov.scot.payments.payments.ch.app;

import gov.scot.payments.application.component.commandhandler.stateful.EventGeneratorFunction;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.GenericErrorEvent;
import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.payments.model.aggregate.PaymentStatus;
import gov.scot.payments.payments.model.command.*;
import gov.scot.payments.payments.model.event.*;
import io.vavr.collection.List;
import lombok.extern.slf4j.Slf4j;

import static gov.scot.payments.application.component.commandhandler.stateful.AggregateState.failure;

@Slf4j
public class PaymentsEventGenerator extends EventGeneratorFunction.PatternMatching<PaymentsState, EventWithCauseImpl> {

    @Override
    protected void handlers(final PatternMatcher.Builder2<Command, PaymentsState, List<EventWithCauseImpl>> builder) {
        //registrations
        builder
                .match2(command -> true, failure(), (command, paymentsState) -> List.of(GenericErrorEvent.from(command, paymentsState.getError())))
                .match2(RegisterPaymentCommand.class, (c, s) -> List.of(new PaymentRegisteredEvent(s.getCurrent())))
                .match2(ReleasePaymentCommand.class, (c, s) -> handleReleasePayment(c, s.getCurrent()))
                .match2(RejectPaymentCommand.class, (c, s) -> List.of(new PaymentRejectedEvent(s.getCurrent())))

                //approvals
                .match2(RequestPaymentApprovalCommand.class, (c, s) -> List.of(new PaymentApprovalRequestedEvent(s.getCurrent())))
                .match2(ApprovePaymentCommand.class, (c, s) -> handleApprovePayment(c,s.getCurrent()))
                .match2(RejectPaymentApprovalCommand.class, (c, s) -> List.of(new PaymentRejectedEvent(s.getCurrent())))

                //submissions
                .match2(SubmitPaymentCommand.class, (c, s) -> List.of(new PaymentReadyForSubmissionEvent(s.getCurrent())))
                .match2(CompletePaymentSubmissionCommand.class, (c, s) -> List.of(new PaymentSubmissionSuccessEvent(s.getCurrent())))
                .match2(FailPaymentSubmissionCommand.class, (c, s) -> List.of(new PaymentSubmissionFailedEvent(s.getCurrent())))
                .match2(CompletePaymentCommand.class, (c, s) -> List.of(new PaymentCompleteEvent(s.getCurrent())))
                .match2(ReturnPaymentCommand.class, (c, s) -> List.of(new PaymentReturnedEvent(s.getCurrent())))

                //cancel post submissions
                .match2(FailCancelPaymentCommand.class, (c, s) -> List.of(new PaymentCancellationFailedEvent(s.getCurrent())))
                .match2(CompleteCancelPaymentCommand.class, (c, s) -> List.of(new PaymentCancellationCompleteEvent(s.getCurrent())))

                //cancel approvals
                .match2(CancelPaymentCommand.class, (c, s) -> List.of(new PaymentCancellationRequestedEvent(s.getCurrent())))
                .match2(ApproveCancelPaymentCommand.class, (c, s) -> List.of(new PaymentCancelApprovedEvent(s.getCurrent())))
                .match2(RejectCancelPaymentCommand.class, (c, s) -> List.of(new PaymentCancelRejectedEvent(s.getCurrent())));

    }

    private List<EventWithCauseImpl> handleApprovePayment(ApprovePaymentCommand c, Payment current) {
        if(current.getStatus() == PaymentStatus.ReadyForSubmission){
            return List.of(new PaymentReadyForSubmissionEvent(current));
        } else {
            return List.of(new PaymentInvalidEvent(current));
        }
    }

    private List<EventWithCauseImpl> handleReleasePayment(final ReleasePaymentCommand c, final Payment current) {
        if(current.getStatus() == PaymentStatus.Invalid){
            return List.of(new PaymentInvalidEvent(current));
        } else {
            return List.of(new PaymentValidEvent(current));
        }
    }
}
