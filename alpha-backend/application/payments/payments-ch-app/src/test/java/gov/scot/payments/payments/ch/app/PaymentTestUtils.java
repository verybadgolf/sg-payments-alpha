package gov.scot.payments.payments.ch.app;

import gov.scot.payments.customers.model.aggregate.PaymentMethodConfiguration;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import io.vavr.collection.List;
import org.javamoney.moneta.Money;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

public class PaymentTestUtils {

    static final String AMOUNT_VALUE = "100";
    static final String BATCH_ID = "111";
    static final String CREDITOR_NAME = "someName";
    static final String CURRENCY_CODE = "GBP";
    static final String CLIENT_REFERENCE = "ref1";
    static final String PRODUCT_REFERENCE = "ref2";
    static final String ACCOUNT_NAME = "acct";
    static final String ACCOUNT_NUMBER = "12345678";
    static final String SORT_CODE = "123456";
    static final String USER = "testUser";
    static final String REASON = "Some reason";
    static final UUID PAYMENT_ID = UUID.randomUUID();

    static final List<PaymentMethod> ALLOWED_METHODS = List.of(PaymentMethod.Bacs);
    static final Money AMOUNT = Money.of(new BigDecimal(AMOUNT_VALUE), CURRENCY_CODE);
    static final CompositeReference PRODUCT = new CompositeReference(CLIENT_REFERENCE, PRODUCT_REFERENCE);

    static Payment getValidPayment(PaymentStatus paymentStatus) {
        return getValidPayment().toBuilder()
                .status(paymentStatus)
                .build();
    }

    static Payment getValidPayment(UUID paymentId, PaymentStatus paymentStatus) {
        return getValidPayment().toBuilder()
                .id(paymentId)
                .status(paymentStatus)
                .build();
    }

    static Payment getValidPayment(UUID paymentId) {
        return getValidPayment().toBuilder()
                .id(paymentId)
                .build();
    }

    static Payment getValidPayment() {
        Map<String, Map<String, String>> methodProperties = new HashMap<>();
        Map<String, String> bacsProperties = new HashMap<>();
        bacsProperties.put(PaymentMethodConfiguration.BACS_SERVICE_USER_NUMBER, "123456");
        bacsProperties.put(PaymentMethodConfiguration.BACS_SERVICE_ACCOUNT_NAME, "");
         methodProperties.put(PaymentMethod.Bacs.name(), bacsProperties);
        return Payment.builder()
                .batchId(BATCH_ID)
                .allowedMethods(ALLOWED_METHODS)
                .amount(AMOUNT)
                .latestExecutionDate(LocalDate.now().plusDays(7).atStartOfDay().toInstant(ZoneOffset.UTC))
                .product(PRODUCT)
                .methodProperties(methodProperties)
                .creditorAccount(
                        UKBankAccount.builder()
                                .name(ACCOUNT_NAME)
                                .currency(CURRENCY_CODE)
                                .sortCode(new SortCode(SORT_CODE))
                                .accountNumber(new UKAccountNumber(ACCOUNT_NUMBER))
                                .build())
                .creditor(
                        PartyIdentification.builder()
                                .name(CREDITOR_NAME)
                                .build())
                  .debtorAccount(
                          UKBankAccount.builder()
                                       .name(ACCOUNT_NAME)
                                       .currency(CURRENCY_CODE)
                                       .sortCode(new SortCode(SORT_CODE))
                                       .accountNumber(new UKAccountNumber(ACCOUNT_NUMBER))
                                       .build())
                  .debtor(
                          PartyIdentification.builder()
                                             .name(CREDITOR_NAME)
                                             .build())
                .createdBy(USER)
                .build();
    }

    static void checkExpectedPaymentValues(Payment payment, PaymentStatus expectedStatus) {

        UKBankAccount expectedCreditorAccount = UKBankAccount.builder()
                .name(ACCOUNT_NAME)
                .currency(CURRENCY_CODE)
                .sortCode(new SortCode(SORT_CODE))
                .accountNumber(new UKAccountNumber(ACCOUNT_NUMBER))
                .build();

        PartyIdentification expectedCreditor = PartyIdentification.builder()
                .name(CREDITOR_NAME)
                .build();

        assertThat(payment.getStatus(), is(expectedStatus));
        assertThat(payment.getAmount(), is(AMOUNT));
        assertThat(payment.getAmount().getCurrency().getCurrencyCode(), is(CURRENCY_CODE));
        assertThat(payment.getAllowedMethods(), is(ALLOWED_METHODS));
        assertThat(payment.getProduct(), is(PRODUCT));
        assertThat(payment.getCreditorAccount().getCurrency(), is(CURRENCY_CODE));
        assertThat(payment.getCreditorAccount(), instanceOf(UKBankAccount.class));
        assertThat(payment.getCreditorAccount(), is(expectedCreditorAccount));
        assertThat(payment.getCreditor(), is(expectedCreditor));
        assertThat(payment.getCreditor().getName(), is(CREDITOR_NAME));
        assertThat(payment.getCreatedBy(), is(USER));
    }
}
