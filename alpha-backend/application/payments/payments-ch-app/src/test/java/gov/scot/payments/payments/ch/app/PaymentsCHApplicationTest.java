package gov.scot.payments.payments.ch.app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
import gov.scot.payments.application.component.commandhandler.stateful.*;
import gov.scot.payments.application.kafka.MessageHeaderEnricher;
import gov.scot.payments.application.kafka.MessageInfo;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.model.*;
import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.payments.model.command.RegisterPaymentCommand;
import gov.scot.payments.payments.model.event.PaymentRegisteredEvent;
import gov.scot.payments.payments.model.event.PaymentInvalidEvent;
import gov.scot.payments.payments.model.event.PaymentReadyForSubmissionEvent;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.Function3;
import io.vavr.Function5;
import io.vavr.collection.*;
import io.vavr.jackson.datatype.VavrModule;
import lombok.Value;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.Stores;
import gov.scot.payments.model.Command;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.payments.model.command.*;
import gov.scot.payments.payments.model.event.*;
import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static gov.scot.payments.payments.ch.app.PaymentTestUtils.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

public class PaymentsCHApplicationTest {

    private static final long STATE_VERSION = 99L;
    private static final String KEY = "999";

    private PaymentsTestHarness harness;

    @BeforeEach
    private void setUp() {
        harness = new PaymentsTestHarness();
    }

    @Test
    @DisplayName("When a CreatePaymentCommand is received then a payment is created in ApprovalRequired state and a PaymentCreatedEvent is emitted")
    void receivingCreatePaymentCommandCreatesAPaymentAndEmitsPaymentCreatedEvent() {

        var payment = getValidPayment(PAYMENT_ID);

        var command = RegisterPaymentCommand.builder()
                .payment(payment)
                .reply(true)
                .build();

        var result = harness.setNullStateAndSendCommand(command);

        var expectedEvent = PaymentRegisteredEvent.builder()
                .stateVersion(1L)
                .reply(true)
                .executionCount(1)
                .build();

        var expectedCurrentState = payment.toBuilder()
                .status(PaymentStatus.Registered)
                .build();

        checkStateAndEvent(result, expectedCurrentState, expectedEvent, 1L);
    }

    @Test
    @DisplayName("When a ApprovePaymentCommand is received the payment is moved to ReadyForSubmission state and a PaymentReadyForSubmissionEvent is emitted")
    void receivingApprovePaymentCommandUpdatesPaymentAndEmitsPaymentReadyForSubmissionEvent() {

        setupStateSendCommandThenCheckStateAndEvent(

                // Initial payment status
                PaymentStatus.ApprovalRequired,

                // initial events
                List.of(Approval.builder().requiredBy("r").build()),

                // The command to send
                ApprovePaymentCommand.builder()
                        .paymentId(PAYMENT_ID)
                        .reply(true)
                        .user(USER+"1")
                        .build(),

                // Expected payment status after command is sent
                PaymentStatus.ReadyForSubmission,

                // Expected event emitted after command is sent
                PaymentReadyForSubmissionEvent.builder()
                        .stateVersion(STATE_VERSION + 1L)
                        .reply(true)
                        .executionCount(1)
                        .build(),

                // Expected PaymentEvents added to the payment
                List.of(
                        Approval.builder()
                                .status(Approval.Status.Approved)
                                .user(USER+"1")
                                .requiredBy("r")
                                .build()
                )
        );
    }

    @Test
    @DisplayName("When a RejectPaymentCommand is received the payment is moved to Rejected state and a PaymentRejectedEvent is emitted")
    void receivingRejectPaymentCommandUpdatesPaymentAndEmitsPaymentRejectedEvent() {

        setupStateSendCommandThenCheckStateAndEvent(

                // Initial payment status
                PaymentStatus.ApprovalRequired,

                // initial events
                List.of(Approval.builder().requiredBy("r").build()),

                // The command to send
                RejectPaymentApprovalCommand.builder()
                        .paymentId(PAYMENT_ID)
                        .reason(REASON)
                        .user(USER+"1")
                        .build(),

                // Expected payment status after command is sent
                PaymentStatus.Rejected,

                // Expected event emitted after command is sent
                PaymentRejectedEvent.builder()
                        .stateVersion(STATE_VERSION + 1L)
                        .reply(true)
                        .executionCount(1)
                        .build(),

                // Expected PaymentEvents added to the payment
                List.of(
                        Approval.builder()
                                .status(Approval.Status.Rejected)
                                .requiredBy("r")
                                .user(USER+"1")
                                .reason(REASON)
                                .build()
                )
        );
    }

    @Test
    @DisplayName("When a CompletePaymentSubmissionCommand is received the payment is moved to Submitted state and a PaymentSubmissionSuccessEvent is emitted")
    void receivingCompletePaymentSubmissionCommandUpdatesPaymentAndEmitsPaymentSubmissionSuccessEvent() {

        setupStateSendCommandThenCheckStateAndEvent(

                // Initial payment status
                PaymentStatus.ReadyForSubmission,

                // The command to send
                CompletePaymentSubmissionCommand.builder()
                        .paymentId(PAYMENT_ID)
                        .method(PaymentMethod.Bacs)
                        .psp("psp")
                        .reply(true)
                        .build(),

                // Expected payment status after command is sent
                PaymentStatus.Submitted,

                // Expected event emitted after command is sent
                PaymentSubmissionSuccessEvent.builder()
                        .stateVersion(STATE_VERSION + 1L)
                        .reply(true)
                        .executionCount(1)
                        .build(),

                // Expected PaymentEvents added to the payment
                List.of(
                        Submission.builder()
                                .status(Submission.Status.Success)
                                .build()
                )
        );
    }

    @Test
    @DisplayName("When a FailPaymentSubmissionCommand is received the payment is moved to SubmissionFailed state and a PaymentSubmissionFailedEvent is emitted")
    void receivingFailPaymentSubmissionCommandUpdatesPaymentAndEmitsPaymentSubmissionFailedEvent() {

        setupStateSendCommandThenCheckStateAndEvent(

                // Initial payment status
                PaymentStatus.ReadyForSubmission,

                // The command to send
                FailPaymentSubmissionCommand.builder()
                        .paymentId(PAYMENT_ID)
                        .reply(true)
                        .failureDetails(List.of())
                        .build(),

                // Expected payment status after command is sent
                PaymentStatus.SubmissionFailed,

                // Expected event emitted after command is sent
                PaymentSubmissionFailedEvent.builder()
                        .stateVersion(STATE_VERSION + 1L)
                        .reply(true)
                        .executionCount(1)
                        .build(),

                // Expected PaymentEvents added to the payment
                List.of(
                        Submission.builder()
                                .status(Submission.Status.Failure)
                                .build()
                )
        );
    }

    @Test
    @DisplayName("When a CompletePaymentCommand is received the payment is moved to Paid state and a PaymentCompleteEvent is emitted")
    void receivingCompletePaymentCommandUpdatesPaymentAndEmitsPaymentSubmissionFailedEvent() {

        setupStateSendCommandThenCheckStateAndEvent(

                // Initial payment status
                PaymentStatus.Submitted,

                // The command to send
                CompletePaymentCommand.builder()
                        .paymentId(PAYMENT_ID)
                        .method(PaymentMethod.Bacs)
                        .psp("psp")
                        .reply(true)
                        .build(),

                // Expected payment status after command is sent
                PaymentStatus.Paid,

                // Expected event emitted after command is sent
                PaymentCompleteEvent.builder()
                        .stateVersion(STATE_VERSION + 1L)
                        .reply(true)
                        .executionCount(1)
                        .build(),

                // Expected PaymentEvents added to the payment
                List.of(
                        Settlement.builder()
                                .paymentMethod(PaymentMethod.Bacs)
                                .psp("psp")
                                .build()
                )
        );
    }

    @Test
    @DisplayName("When a ReturnPaymentCommand is received the payment is moved to Returned state and a PaymentReturnedEvent is emitted")
    void receivingReturnPaymentCommandUpdatesPaymentAndEmitsPaymentReturnedEvent() {

        setupStateSendCommandThenCheckStateAndEvent(

                // Initial payment status
                PaymentStatus.Submitted,

                // The command to send
                ReturnPaymentCommand.builder()
                        .paymentId(PAYMENT_ID)     .method(PaymentMethod.Bacs)
                        .psp("psp")
                        .message("m")
                        .code("c")
                        .amount(Money.parse("GBP 1"))
                        .reply(true)
                        .build(),

                // Expected payment status after command is sent
                PaymentStatus.Returned,

                // Expected event emitted after command is sent
                PaymentReturnedEvent.builder()
                        .stateVersion(STATE_VERSION + 1L)
                        .reply(true)
                        .executionCount(1)
                        .build(),

                // Expected PaymentEvents added to the payment
                List.of(
                        Return.builder()
                                .paymentMethod(PaymentMethod.Bacs)
                                .psp("psp")
                                .reason("m")
                                .code("c")
                                .build()
                )
        );
    }

    private void setupStateSendCommandThenCheckStateAndEvent(PaymentStatus initialPaymentStatus, Command command, PaymentStatus expectedPaymentStatus, BasePaymentEventWithCause expectedEvent, List<Object> expectedPaymentEvents) {
        setupStateSendCommandThenCheckStateAndEvent(initialPaymentStatus, List.empty(), command, expectedPaymentStatus, expectedEvent, expectedPaymentEvents);
    }

    private void setupStateSendCommandThenCheckStateAndEvent(PaymentStatus initialPaymentStatus, List<Object> initialPaymentEvents, Command command, PaymentStatus expectedPaymentStatus, BasePaymentEventWithCause expectedEvent, List<Object> expectedPaymentEvents) {

        var initialCurrentState = getValidPayment(PAYMENT_ID, initialPaymentStatus)
                .toBuilder()
                .approval((Approval)initialPaymentEvents.filter(e -> e instanceof Approval).getOrNull())
                .submissions(initialPaymentEvents.filter(e -> e instanceof Submission).map(e -> (Submission)e))
                .settlement((Settlement)initialPaymentEvents.filter(e -> e instanceof Settlement).getOrNull())
                .returns((Return) initialPaymentEvents.filter(e -> e instanceof Return).getOrNull())
                .build();

        var result = harness.setStateAndSendCommand(initialCurrentState, STATE_VERSION, command);

        var expectedCurrentState = initialCurrentState.toBuilder()
                .status(expectedPaymentStatus)
                .approval((Approval)expectedPaymentEvents.filter(e -> e instanceof Approval).getOrNull())
                .submissions(initialCurrentState.getSubmissions().appendAll(expectedPaymentEvents.filter(e -> e instanceof Submission).map(e -> (Submission)e)))
                .settlement((Settlement)expectedPaymentEvents.filter(e -> e instanceof Settlement).getOrNull())
                .returns((Return) expectedPaymentEvents.filter(e -> e instanceof Return).getOrNull())
                .build();

        checkStateAndEvent(result, expectedCurrentState, expectedEvent, STATE_VERSION + 1);
    }

    private void checkStateAndEvent(PaymentsTestHarness.Result result, Payment expectedCurrentState, BasePaymentEventWithCause expectedEvent, Long expectedStateVersion) {
        assertThat(result.getState().getCurrentVersion(), is(expectedStateVersion));
        assertThat(result.getState().getCurrent(), samePropertyValuesAs(expectedCurrentState, "updatedAt","approval","settlement","submissions","validation","cancellationApproval","cancellation","returns"));

        var event = (BasePaymentEventWithCause) result.getEvent();
        assertThat(event, samePropertyValuesAs(expectedEvent, "key", "carriedState", "timestamp", "correlationId", "messageId", "causeDetails"));
        assertThat(event.getCarriedState(), samePropertyValuesAs(expectedCurrentState, "updatedAt","approval","settlement","submissions","validation","cancellationApproval","cancellation","returns"));
        checkPaymentEvent(expectedCurrentState.getApproval(), event.getCarriedState().getApproval());
        checkPaymentEvent(expectedCurrentState.getSettlement(), event.getCarriedState().getSettlement());
        checkPaymentEvent(expectedCurrentState.getValidation(), event.getCarriedState().getValidation());
        checkPaymentEvent(expectedCurrentState.getCancellationApproval(), event.getCarriedState().getCancellationApproval());
        checkPaymentEvent(expectedCurrentState.getCancellation(), event.getCarriedState().getCancellation());
        Assertions.assertThat(expectedCurrentState.getSubmissions())
                .usingElementComparatorOnFields("status")
                .containsExactlyElementsOf(event.getCarriedState().getSubmissions());
    }

    private void checkPaymentEvent(PaymentEvent expected, PaymentEvent actual) {
        if(actual != null || expected != null){
            Assertions.assertThat(expected)
                    .isEqualToIgnoringGivenFields(actual, "timestamp");
        }

    }

}
