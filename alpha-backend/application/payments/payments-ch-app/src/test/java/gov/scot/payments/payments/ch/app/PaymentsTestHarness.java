package gov.scot.payments.payments.ch.app;

import gov.scot.payments.application.component.commandhandler.stateful.AggregateState;
import gov.scot.payments.application.kafka.MessageInfo;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.CommandFailureInfo;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.GenericErrorEvent;
import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.payments.model.command.*;
import gov.scot.payments.payments.model.event.*;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.vavr.Function3;
import io.vavr.Function5;
import io.vavr.collection.List;
import lombok.Value;
import net.objectlab.kit.datecalc.common.DefaultHolidayCalendar;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.state.KeyValueStore;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;
import org.springframework.messaging.MessageHeaders;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;

public class PaymentsTestHarness {

    @Value
    public static class Result {
        private final EventWithCauseImpl event;
        private final AggregateState<Payment> state;
    }

    private static final String KEY = "999";

    private static final List<Class<? extends Command>> commandClasses = List.of(
            ApproveCancelPaymentCommand.class,
            ApprovePaymentCommand.class,
            CancelPaymentCommand.class,
            CompleteCancelPaymentCommand.class,
            CompletePaymentCommand.class,
            CompletePaymentSubmissionCommand.class,
            FailCancelPaymentCommand.class,
            FailPaymentSubmissionCommand.class,
            RegisterPaymentCommand.class,
            RejectCancelPaymentCommand.class,
            RejectPaymentApprovalCommand.class,
            RejectPaymentCommand.class,
            ReleasePaymentCommand.class,
            RequestPaymentApprovalCommand.class,
            ReturnPaymentCommand.class,
            SubmitPaymentCommand.class
    );

    private static final List<Class<? extends EventWithCauseImpl>> eventClasses = List.of(
            PaymentApprovalRequestedEvent.class,
            PaymentCancelApprovedEvent.class,
            PaymentCancellationCompleteEvent.class,
            PaymentCancellationFailedEvent.class,
            PaymentCancellationRequestedEvent.class,
            PaymentCancelRejectedEvent.class,
            PaymentCompleteEvent.class,
            PaymentInvalidEvent.class,
            PaymentReadyForSubmissionEvent.class,
            PaymentRegisteredEvent.class,
            PaymentRejectedEvent.class,
            PaymentReturnedEvent.class,
            PaymentSubmissionFailedEvent.class,
            PaymentSubmissionSuccessEvent.class,
            PaymentValidEvent.class
    );

    private final PaymentValidator validator = new PaymentValidator(new DefaultHolidayCalendar<>());
    private final PaymentsStateUpdateFunc stateUpdateHandler = spy(new PaymentsStateUpdateFunc(validator));
    private final PaymentsEventGenerator paymentsEventGenerator = spy(new PaymentsEventGenerator());
    private final KafkaStreamsTestHarness testHarness;

    public PaymentsTestHarness(){
        testHarness = createPaymentsTestHarness();
    }


    public KafkaStreamsTestHarness createPaymentsTestHarness() {

        Function5<SerializedMessage, CommandFailureInfo, Payment, Payment, Long, PaymentsState> aggregateStateSupplier = PaymentsState::new;
        Function3<Command, Payment, Throwable, List<EventWithCauseImpl>> errorHandler = spy(new TestErrorHandler<>());

        var configurator = CHTestHarnessConfigurator.<Payment, PaymentsState>
                builder()
                .aggregateStateClass(PaymentsState.class)
                .stateClass(Payment.class)
                .aggregateStateSupplier(aggregateStateSupplier)
                .errorHandler(errorHandler)
                .commandClasses(commandClasses)
                .eventClasses(eventClasses)
                .eventGeneratorFunction(paymentsEventGenerator)
                .stateUpdateFunction(stateUpdateHandler)
                .build();

        return configurator.createAndSetUpTestHarness();
    }

    public <C extends Command> Result setStateAndSendCommand(Payment currentState, Long currentStateVersion, C command) {
        try (final TopologyTestDriver topology = testHarness.toTopology()) {

            KeyValueStore<byte[], AggregateState<Payment>> stateStore = topology.getKeyValueStore(KafkaStreamsTestHarness.STATE_STORE);

            if (currentState != null) {
                stateStore.put(KEY.getBytes(), PaymentsState.builder()
                        .current(currentState)
                        .currentVersion(currentStateVersion)
                        .message(new SerializedMessage(new byte[0], new MessageInfo()))
                        .build()
                );
            }
            KeyValueWithHeaders<String, C> commandMessage =
                    KeyValueWithHeaders.msg(KEY,
                            command,
                            new RecordHeader(MessageHeaders.CONTENT_TYPE, command.getClass().getName().getBytes()));

            testHarness.sendKeyValues(topology, commandMessage);

            var entities = testHarness.drainKeyValues(topology);
            Assertions.assertEquals(1, entities.size());
            return new Result((EventWithCauseImpl) entities.get(0).value, stateStore.get(KEY.getBytes()));
        }
    }

    public <C extends Command> Result setNullStateAndSendCommand(C command) {
        try (final TopologyTestDriver topology = testHarness.toTopology()) {
            KeyValueStore<byte[], AggregateState<Payment>> stateStore = topology.getKeyValueStore(KafkaStreamsTestHarness.STATE_STORE);
            KeyValueWithHeaders<String, C> commandMessage =
                    KeyValueWithHeaders.msg(KEY,
                            command,
                            new RecordHeader(MessageHeaders.CONTENT_TYPE, command.getClass().getName().getBytes()));
            testHarness.sendKeyValues(topology, commandMessage);
            var entities = testHarness.drainKeyValues(topology);
            assertEquals(1, entities.size());
            return new Result((EventWithCauseImpl) entities.get(0).value, stateStore.get(KEY.getBytes()));
        }
    }

    private void setStateInStateStore(Payment currentPaymentState, Long currentStateVersion, TopologyTestDriver topology) {
        KeyValueStore<String, AggregateState<Payment>> stateStore = topology.getKeyValueStore(KafkaStreamsTestHarness.STATE_STORE);
        if (currentPaymentState != null) {
            stateStore.put(KEY, PaymentsState.builder()
                    .current(currentPaymentState)
                    .currentVersion(currentStateVersion)
                    .message(new SerializedMessage(new byte[0], new MessageInfo()))
                    .build()
            );
        }
    }

    public int getNumberOfStateUpdateCalls(){
        return Mockito.mockingDetails(stateUpdateHandler).getInvocations().size();
    }

    public int getPaymentsEventGeneratorCalls(){
        return Mockito.mockingDetails(paymentsEventGenerator).getInvocations().size();
    }

    private static class TestErrorHandler<E> implements Function3<Command, E, Throwable, List<EventWithCauseImpl>> {
        @Override
        public List<EventWithCauseImpl> apply(Command command, E entity, Throwable throwable) {
            return List.of(GenericErrorEvent.from(command, throwable));
        }
    }



}
