package gov.scot.payments.payments.proj.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.component.projector.springdata.ProjectionRepository;
import gov.scot.payments.application.component.projector.springdata.QueryByIdResource;
import gov.scot.payments.application.component.projector.springdata.RepositoryMutatingStorageService;
import gov.scot.payments.application.component.projector.springdata.jpa.JpaConsumingProjectorApplication;
import gov.scot.payments.application.component.projector.springdata.jpa.SearchResource;
import gov.scot.payments.application.component.projector.springdata.jpa.SubjectSpecification;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.application.security.SecurityCustomizer;
import gov.scot.payments.model.user.Subject;
import gov.scot.payments.payments.proj.model.PaymentDetails;
import gov.scot.payments.payments.proj.model.PaymentSummary;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.HttpMethod;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.UUID;
import java.util.function.Function;

@EnableAutoConfiguration(exclude = {HypermediaAutoConfiguration.class})
@EnableJpaRepositories(basePackageClasses = PaymentsSummaryRepository.class)
@EntityScan(basePackageClasses = PaymentSummary.class)
@ApplicationComponent
public class PaymentsProjApplication extends JpaConsumingProjectorApplication {

    @Bean
    public RepositoryMutatingStorageService<UUID, PaymentSummary> summaryHandler(Metrics metrics, PaymentsSummaryRepository repository){
        return new PaymentsSummaryStorageService(metrics,repository);
    }

    @Bean
    public RepositoryMutatingStorageService<UUID, PaymentDetails> detailsHandler(Metrics metrics, PaymentDetailsRepository repository){
        return new PaymentDetailsStorageService(metrics,repository);
    }

    @Bean
    public DetailsQueryById detailsQueryById(ProjectionRepository<UUID, PaymentDetails> repository, PagedResourcesAssembler<PaymentDetails> assembler){
        return new DetailsQueryById(repository,assembler);
    }

    @Bean
    public SummaryQueryById summaryQueryById(ProjectionRepository<UUID, PaymentSummary> repository, PagedResourcesAssembler<PaymentSummary> assembler){
        return new SummaryQueryById(repository,assembler);
    }

    @Bean
    public SummarySearch summarySearch(JpaSpecificationExecutor<PaymentSummary> repository, PagedResourcesAssembler<PaymentSummary> assembler){
        return new SummarySearch(repository,assembler,this::aclSummaryCheck);
    }

    @Bean
    public SecurityCustomizer projectionServiceSecurity(@Value("${projection.query.authority:}") String authority){
        return r -> {
            if(!StringUtils.isEmpty(authority)){
                r.pathMatchers(HttpMethod.GET,"/**").hasAuthority(authority);
            }
        };
    }

    private Specification<PaymentSummary> aclSummaryCheck(final Subject subject) {

        return SubjectSpecification.of(subject, (c,r,s) -> {

            if(s.hasGlobalAccess()){
                return c.and();
            }
            var productValues = c.in(r.get("product"));
            subject.getScopes().forEach(scope -> productValues.value(scope.getQualifiedName()));
            var customerValues = c.in(r.get("customer"));
            subject.getScopes().filter(scope -> scope.getParent() == null)
                   .forEach(scope -> customerValues.value(scope.getQualifiedName()));
            return c.or(customerValues, productValues);
        });

    }

    @RequestMapping("/details")
    public class DetailsQueryById extends QueryByIdResource<UUID,PaymentDetails> {

        public DetailsQueryById(ProjectionRepository<UUID, PaymentDetails> repository, PagedResourcesAssembler<PaymentDetails> assembler) {
            super(repository,assembler);
        }
    }

    @RequestMapping
    public class SummaryQueryById extends QueryByIdResource<UUID,PaymentSummary> {

        public SummaryQueryById(ProjectionRepository<UUID, PaymentSummary> repository, PagedResourcesAssembler<PaymentSummary> assembler) {
            super(repository,assembler);
        }
    }

    @RequestMapping
    public class SummarySearch extends SearchResource<PaymentSummary> {

        public SummarySearch(JpaSpecificationExecutor<PaymentSummary> repository, PagedResourcesAssembler<PaymentSummary> assembler, Function<Subject, Specification<PaymentSummary>> aclCheck) {
            super(repository,assembler,aclCheck);
        }
    }

    public static void main(String[] args){
        BaseApplication.run(PaymentsProjApplication.class,args);
    }


}