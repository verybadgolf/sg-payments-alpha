package gov.scot.payments.payments.proj.app;

import gov.scot.payments.application.component.projector.springdata.jpa.JpaProjectionRepository;
import gov.scot.payments.model.user.Subject;
import gov.scot.payments.payments.proj.model.PaymentSummary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface PaymentsSummaryRepository extends JpaProjectionRepository<UUID, PaymentSummary>{

    @Query("from PaymentSummary p where " +
            "( p.product in (?#{#principal.getScopesAsString()}) " +
            "or  p.customer in (?#{#principal.getRootScopesAsString()}) " +
            "or ?#{#principal.hasGlobalAccess()} = true )")
    Page<PaymentSummary> findAll(Pageable pageable, @Param("principal") Subject subject);

    @Query("from PaymentSummary p where p.id = ?#{#id} and " +
            "( p.product in (?#{#principal.getScopesAsString()}) " +
            "or p.customer in (?#{#principal.getRootScopesAsString()}) " +
            "or ?#{#principal.hasGlobalAccess()} = true )")
    Optional<PaymentSummary> findById(@Param("id") UUID id, @Param("principal") Subject subject);
}
