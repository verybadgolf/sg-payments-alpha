package gov.scot.payments.payments.proj.app;


import gov.scot.payments.application.component.projector.springdata.ProjectionRepository;
import gov.scot.payments.application.component.projector.springdata.RepositoryMutatingStorageService;
import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.payments.model.event.PaymentRegisteredEvent;
import gov.scot.payments.payments.proj.model.PaymentDetails;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.collection.List;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

import static gov.scot.payments.testing.matchers.ISODateMatcher.isWithin;
import static java.time.Instant.now;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;

@AutoConfigurationPackage
@EntityScan("gov.scot.payments.payments.proj.model")
@DataJpaTest(properties = {"spring.datasource.url=jdbc:h2:mem:PaymentsStorageServiceTest"})
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=validate",
        "spring.flyway.locations=classpath:db/migration/h2"
})
@ContextConfiguration(classes = PaymentDetailsStorageServiceTest.TestConfiguration.class)
public class PaymentDetailsStorageServiceTest {

    public static final UUID TEST_ID = UUID.randomUUID();
    private static final String BATCH_ID = "batch_id";
    public static final String CURRENCY_CODE = "GBP";
    public static final long AMOUNT = 123L;
    private static final String ACCOUNT_NUMBER = "12345678";
    private static final String SORT_CODE = "123456";
    private static final String CREDITOR_NAME = "creditorName";
    private static final String CREATED_BY = "Test User";
    private static final Instant CREATED_AT = Instant.parse("2020-01-03T12:00:00.00Z");
    private static final CompositeReference PRODUCT = CompositeReference.parse("Customer.Product");

    // These two dates should be the same
    private static final Instant LATEST_EXECUTION_DATE = Instant.parse("2020-01-06T00:00:00.00Z");
    private static final LocalDate PAYMENT_DATE = LocalDate.of(2020, 1, 6);


    @Autowired
    private RepositoryMutatingStorageService<UUID, PaymentDetails> service;

    @Autowired
    private ProjectionRepository<UUID, PaymentDetails> jpaRepository;

    @Autowired
    private JpaRepository<PaymentDetails, UUID> repository;


    @Test
    @DisplayName("Give a PaymentCreatedEvent, when applying to storage service then a Payment Summary is persisted")
    public void givenPaymentCreatedEventWhenApplyThenPaymentDetailsPersisted() {

        var event = PaymentRegisteredEvent.builder()
                .carriedState(Payment.builder()
                        .id(TEST_ID)
                        .batchId(BATCH_ID)
                        .createdBy(CREATED_BY)
                        .createdAt(CREATED_AT)
                        .status(PaymentStatus.Registered)
                        .amount(Money.of(AMOUNT, CURRENCY_CODE))
                        .earliestExecutionDate(CREATED_AT)
                        .latestExecutionDate(LATEST_EXECUTION_DATE)
                        .product(PRODUCT)
                        .debtorAccount(getCreditorAccount())
                        .debtor(getCreditor())
                        .creditorAccount(getCreditorAccount())
                        .creditor(getCreditor())
                        .allowedMethods(List.empty())
                        .build())
                .build();

        var result = service.apply(event.getKey(), event);

        assertThat(result._1, is(TEST_ID.toString()));
        assertThat(repository.count(), is(1L));

        var paymentDetails = repository.getOne(TEST_ID);

        assertEquals(1, paymentDetails.getEvents().size());
        var transactionEvent = paymentDetails.getEvents().get(0);
        assertThat(transactionEvent.getStatus(), is("Registered"));

        assertThat(paymentDetails.getId(), is(TEST_ID));
        assertThat(paymentDetails.getBatchId(), is(BATCH_ID));
        assertThat(paymentDetails.getCreatedAt(), is(CREATED_AT));
        assertThat(paymentDetails.getStatus(), is(PaymentStatus.Registered));
        assertThat(paymentDetails.getMessage(), is(nullValue()));
        assertThat(paymentDetails.getAmount(), is(BigDecimal.valueOf(AMOUNT)));
        assertThat(paymentDetails.getCurrency(), is(CURRENCY_CODE));
        assertThat(paymentDetails.getPaymentDate(), is(PAYMENT_DATE));
        assertThat(paymentDetails.getProcessingTime().toString(), isWithin(1).secondsOf(now()));
        assertThat(result._2, is(paymentDetails));
    }

    private static UKBankAccount getCreditorAccount() {
        return UKBankAccount.builder()
                .name("acct")
                .currency(CURRENCY_CODE)
                .sortCode(new SortCode(SORT_CODE))
                .accountNumber(new UKAccountNumber(ACCOUNT_NUMBER))
                .build();
    }

    private static PartyIdentification getCreditor() {
        return PartyIdentification.builder()
                .name(CREDITOR_NAME)
                .build();
    }

    @Configuration
    public static class TestConfiguration {
        @Bean
        public RepositoryMutatingStorageService<UUID, PaymentDetails> service(PaymentDetailsRepository repository){
            return new PaymentDetailsStorageService(new MicrometerMetrics(new SimpleMeterRegistry()), repository);
        }
    }


}
