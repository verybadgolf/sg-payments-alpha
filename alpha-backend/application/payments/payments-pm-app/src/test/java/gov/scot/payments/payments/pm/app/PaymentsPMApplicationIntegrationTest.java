package gov.scot.payments.payments.pm.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.customers.model.event.CustomerPaymentApprovalRequiredEvent;
import gov.scot.payments.customers.model.event.CustomerPaymentRejectedEvent;
import gov.scot.payments.customers.model.event.CustomerPaymentVerifiedEvent;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.paymentbatches.model.aggregate.PaymentBatch;
import gov.scot.payments.paymentbatches.model.event.PaymentBatchClosedEvent;
import gov.scot.payments.paymentbatches.model.event.PaymentBatchUpdatedEvent;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.payments.model.command.*;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.spring.TestConfiguration;
import io.vavr.collection.List;
import org.apache.kafka.streams.KafkaStreams;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@ApplicationIntegrationTest(classes = {PaymentsPMApplicationIntegrationTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.PROCESS_MANAGER
        , properties = {"events.destinations=test"})
public class PaymentsPMApplicationIntegrationTest {

    private static final String TEST_USER = "testUser";
    private static final String AMOUNT_VALUE = "100";
    private static final String BATCH_ID = "111";
    private static final String CREDITOR_NAME = "someName";
    private static final String CURRENCY_CODE = "GBP";
    private static final String CLIENT_REFERENCE = "ref1";
    private static final String PRODUCT_REFERENCE = "ref2";
    private static final String ACCOUNT_NUMBER = "12345678";
    private static final String SORT_CODE = "123456";
    private static final String USER = "testUser";

    private static final List<PaymentMethod> ALLOWED_METHODS = List.of(PaymentMethod.Sepa_DC);
    private static final Money AMOUNT = Money.of(new BigDecimal(AMOUNT_VALUE), CURRENCY_CODE);
    private static final CompositeReference PRODUCT = new CompositeReference(CLIENT_REFERENCE, PRODUCT_REFERENCE);

    @BeforeEach
    public void setUp(EmbeddedBrokerClient brokerClient
            , @Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter
            , @Value("${commands.destination}") String commandTopic){
            brokerClient.getBroker().addTopicsIfNotExists(commandTopic);
            kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING,Duration.ofSeconds(10));
     }

    @Test
    public void testRegisterPaymentCommandIsEmitted(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws MalformedURLException {

        Payment payment = getValidPayment();
        PaymentBatch batch = getPaymentBatch();
        batch = batch.toBuilder().status(PaymentBatch.Status.Open).build();

        brokerClient.sendToDestination("events-external", List.of(getPaymentBatchUpdatedEvent(payment, batch)));

        var createdCommands = brokerClient.readAllFromTopic(commandTopic, RegisterPaymentCommand.class);

        assertEquals(1, createdCommands.size());
        assertEquals(createdCommands.get(0).getClass(), RegisterPaymentCommand.class);

        var command = (RegisterPaymentCommand) createdCommands.get(0);
        assertEquals(payment.getKey(), command.getKey());
        assertEquals(payment, command.getPayment());
        assertFalse(command.isReply());
    }

    @Test
    public void testMultipleRegisterPaymentCommandsAreEmitted(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws MalformedURLException {

        Payment payment1 = getValidPayment();
        Payment payment2 = getValidPayment();
        Payment payment3 = getValidPayment();
        Payment payment4 = getValidPayment();
        PaymentBatch batch = getPaymentBatch();

        PaymentBatchUpdatedEvent event1 = getPaymentBatchUpdatedEvent(payment1, batch);
        PaymentBatchUpdatedEvent event2 = getPaymentBatchUpdatedEvent(payment2, batch);
        PaymentBatchUpdatedEvent event3 = getPaymentBatchUpdatedEvent(payment3, batch);
        PaymentBatchUpdatedEvent event4 = getPaymentBatchUpdatedEvent(payment4, batch);

        brokerClient.sendToDestination("events-external", List.of(event1, event2, event3, event4));

        var createdCommands = brokerClient.readAllFromTopic(commandTopic, RegisterPaymentCommand.class);

        assertEquals(4, createdCommands.size());
        assertEquals(createdCommands.get(0).getClass(), RegisterPaymentCommand.class);

        var command = (RegisterPaymentCommand) createdCommands.get(0);
        assertEquals(payment1.getKey(), command.getKey());
        assertEquals(payment1, command.getPayment());
        assertFalse(command.isReply());

        var command2 = (RegisterPaymentCommand) createdCommands.get(1);
        assertEquals(payment2.getKey(), command2.getKey());
        assertEquals(payment2, command2.getPayment());
        assertFalse(command2.isReply());

        var command3 = (RegisterPaymentCommand) createdCommands.get(2);
        assertEquals(payment3.getKey(), command3.getKey());
        assertEquals(payment3, command3.getPayment());
        assertFalse(command3.isReply());

        var command4 = (RegisterPaymentCommand) createdCommands.get(3);
        assertEquals(payment4.getKey(), command4.getKey());
        assertEquals(payment4, command4.getPayment());
        assertFalse(command4.isReply());

    }

    @Test
    public void testPaymentBatchClosed(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws MalformedURLException {
        Payment payment = getValidPayment();
        Payment payment1 = getValidPayment();
        PaymentBatch batch = getPaymentBatch().toBuilder().payments(List.of(payment.getId(),payment1.getId())).build();
        PaymentBatchClosedEvent event = PaymentBatchClosedEvent.builder()
                .carriedState(batch)
                .stateVersion(1L)
                .correlationId(UUID.randomUUID())
                .executionCount(1)
                .build();
        brokerClient.sendToDestination("events-external", List.of(event));
        var releasedCommands = brokerClient.readAllFromTopic(commandTopic, ReleasePaymentCommand.class);

        assertEquals(2, releasedCommands.size());
        assertEquals(releasedCommands.get(0).getClass(), ReleasePaymentCommand.class);
        assertEquals(releasedCommands.get(0).getPaymentId(), payment.getId());
        assertEquals(releasedCommands.get(1).getClass(), ReleasePaymentCommand.class);
        assertEquals(releasedCommands.get(1).getPaymentId(), payment1.getId());
    }

    @Test
    public void testPaymentVerified(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws MalformedURLException {
        Payment payment = getValidPayment();
        CustomerPaymentVerifiedEvent event = CustomerPaymentVerifiedEvent.builder()
                .payment(payment.getId())
                .product(CompositeReference.parse("cust.prod"))
                .correlationId(UUID.randomUUID())
                .executionCount(1)
                .build();
        brokerClient.sendToDestination("events-external", List.of(event));
        var commands = brokerClient.readAllFromTopic(commandTopic, SubmitPaymentCommand.class);

        assertEquals(1, commands.size());
        assertEquals(commands.get(0).getClass(), SubmitPaymentCommand.class);
        assertEquals(commands.get(0).getPaymentId(), payment.getId());
    }

    @Test
    public void testPaymentRejected(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws MalformedURLException {
        Payment payment = getValidPayment();
        CustomerPaymentRejectedEvent event = CustomerPaymentRejectedEvent.builder()
                .payment(payment.getId())
                .product(CompositeReference.parse("cust.prod"))
                .reason("a")
                .correlationId(UUID.randomUUID())
                .executionCount(1)
                .build();
        brokerClient.sendToDestination("events-external", List.of(event));
        var commands = brokerClient.readAllFromTopic(commandTopic, RejectPaymentCommand.class);

        assertEquals(1, commands.size());
        assertEquals(commands.get(0).getClass(), RejectPaymentCommand.class);
        assertEquals(commands.get(0).getPaymentId(), payment.getId());
    }

    @Test
    public void testPaymentRequiresApproval(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws MalformedURLException {
        Payment payment = getValidPayment();
        CustomerPaymentApprovalRequiredEvent event = CustomerPaymentApprovalRequiredEvent.builder()
                .payment(payment.getId())
                .product(CompositeReference.parse("cust.prod"))
                .reason("a")
                .correlationId(UUID.randomUUID())
                .executionCount(1)
                .build();
        brokerClient.sendToDestination("events-external", List.of(event));
        var commands = brokerClient.readAllFromTopic(commandTopic, RequestPaymentApprovalCommand.class);

        assertEquals(1, commands.size());
        assertEquals(commands.get(0).getClass(), RequestPaymentApprovalCommand.class);
        assertEquals(commands.get(0).getPaymentId(), payment.getId());
    }


    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends PaymentsPMApplication{

    }

    private Payment getValidPayment() {

        return Payment.builder()
                .batchId(BATCH_ID)
                .allowedMethods(ALLOWED_METHODS)
                .amount(AMOUNT)
                .latestExecutionDate(Instant.now())
                .product(PRODUCT)
                      .debtorAccount(
                              UKBankAccount.builder()
                                           .name("acct")
                                           .currency(CURRENCY_CODE)
                                           .sortCode(new SortCode(SORT_CODE))
                                           .accountNumber(new UKAccountNumber(ACCOUNT_NUMBER))
                                           .build())
                      .debtor(
                              PartyIdentification.builder()
                                                 .name(CREDITOR_NAME)
                                                 .build())
                .creditorAccount(
                        UKBankAccount.builder()
                                .name("acct")
                                .currency(CURRENCY_CODE)
                                .sortCode(new SortCode(SORT_CODE))
                                .accountNumber(new UKAccountNumber(ACCOUNT_NUMBER))
                                .build())
                .creditor(
                        PartyIdentification.builder()
                                .name(CREDITOR_NAME)
                                .build())
                .createdBy(USER)
                .build();
    }

    private PaymentBatch getPaymentBatch() throws MalformedURLException {
        String id = "id";
        var file = new URL("http://myfile.csv");
        return PaymentBatch.builder()
                .payments(List.empty())
                .id(id)
                .file(file)
                .customerId(CompositeReference.parse("customer.product").getComponent0())
                .createdBy(TEST_USER)
                .createdAt(Instant.now())
                .message("this is a test message")
                .name("mybatch")
                .status(PaymentBatch.Status.Open)
                .build();
    }

    private PaymentBatchUpdatedEvent getPaymentBatchUpdatedEvent(Payment payment, PaymentBatch batch) {
        return PaymentBatchUpdatedEvent.builder()
                .payment(payment)
                .carriedState(batch)
                .user(TEST_USER)
                .stateVersion(1L)
                .correlationId(UUID.randomUUID())
                .executionCount(1)
                .build();
    }
}