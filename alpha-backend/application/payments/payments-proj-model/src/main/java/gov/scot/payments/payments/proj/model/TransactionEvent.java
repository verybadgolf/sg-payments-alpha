package gov.scot.payments.payments.proj.model;

import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.payments.model.aggregate.PaymentStatus;
import lombok.*;
import org.apache.avro.reflect.Nullable;

import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
public class TransactionEvent {

    @NonNull private Instant date;
    @NonNull private String status;
    @Nullable private String details;
    @Nullable @Column(name = "user_val") private String user;


    public static TransactionEvent fromPayment(Payment payment, Instant eventTime){

        var status = payment.getStatus();

        var statusString = getPaymentStatusDisplayString(status);
        var user = getUserIfApplicapble(payment);

        return TransactionEvent.builder()
                .date(eventTime)
                .status(statusString)
                .user(user)
                .details(getDetails(payment))
                .build();
    }

    public static String getDetails(Payment payment){
        switch(payment.getStatus()){
            case Registered:
                return "Payment Registered";
            case Rejected:
                return payment.getApproval().getReason();
            case ApprovalRequired:
                return "Approval Required";
            case Returned:
                return payment.getReturns().getReason();
            case Paid:
                return String.format("Payment paid via %s by %s",payment.getSettlement().getPaymentMethod(), payment.getSettlement().getPsp());
            case ReadyForSubmission:
                return "Payment Ready for Submission";
            case Valid:
            case Invalid:
                return payment.getValidation().getMessage();
            case Submitted:
            case SubmissionFailed:
                return payment.getSubmissions().size() > 0 ? payment.getSubmissions().last().getMessage() : "Submission Failed";
            default:
                throw new IllegalStateException("Unhandled PaymentStatus: " + payment.getStatus());

        }
    }


    public static String getUserIfApplicapble(Payment payment){

        switch(payment.getStatus()){
            case Registered:
                return payment.getCreatedBy();
            case ReadyForSubmission:
                if (payment.getApproval() != null) {
                    return payment.getApproval().getUser();
                }
                return null;
            case Rejected:
                return payment.getApproval().getUser();
            case Invalid:
            case Submitted:
            case Returned:
            case ApprovalRequired:
            case Paid:
            case Valid:
            case SubmissionFailed:
                return null;
            default:
                throw new IllegalStateException("Unhandled PaymentStatus: " + payment.getStatus());

        }
    }


    public static String getPaymentStatusDisplayString(PaymentStatus paymentStatus) {

        switch (paymentStatus) {
            case Registered:
            case Invalid:
            case Rejected:
            case Submitted:
            case Returned:
            case Paid:
                return paymentStatus.toString();
            case Valid:
                return "Validated";
            case ApprovalRequired:
                return "Approval Required";
            case ReadyForSubmission:
                return "Ready for Submission";
            case SubmissionFailed:
                return "Submission Failed";
            default:
                throw new IllegalStateException("Unhandled PaymentStatus: " + paymentStatus);
        }
    }

}
