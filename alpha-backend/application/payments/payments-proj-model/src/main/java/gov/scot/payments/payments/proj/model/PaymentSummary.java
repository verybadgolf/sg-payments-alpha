package gov.scot.payments.payments.proj.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.Projection;
import gov.scot.payments.payments.model.event.BasePaymentEventWithCause;
import gov.scot.payments.payments.model.event.PaymentRegisteredEvent;
import lombok.*;
import org.apache.avro.reflect.Nullable;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.UUID;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode(of = "id")
public class PaymentSummary implements Projection<UUID>, HasKey<String>{

    @NonNull @Id private UUID id;
    @Nullable private String batchId;
    @NonNull private Instant createdAt;
    @NonNull private PaymentSummaryStatus status;
    @Nullable private String message;
    @NonNull private BigDecimal amount;
    @NonNull private String currency;
    @NonNull private LocalDate paymentDate;
    @NonNull private Instant processingTime;
    @NonNull private String customer;
    @NonNull private String product;
    @Nullable private Long stateVersion;

    @Override
    @JsonIgnore
    public String getKey(){
        return id.toString();
    }


    public static PaymentSummary fromRegisterEvent(PaymentRegisteredEvent event) {

        var payment = event.getCarriedState();

        return PaymentSummary.builder()
                .id(payment.getId())
                .batchId(payment.getBatchId())
                .createdAt(payment.getCreatedAt())
                .status(PaymentSummaryStatus.fromPaymentStatus(payment.getStatus()))
                .amount(new BigDecimal(payment.getAmount().getNumber().toString()))
                .currency(payment.getAmount().getCurrency().getCurrencyCode())
                .paymentDate(LocalDate.ofInstant(payment.getLatestExecutionDate(), ZoneOffset.UTC))
                .processingTime(event.getTimestamp())
                .customer(payment.getProduct().getComponent0())
                .product(payment.getProduct().toString())
                .stateVersion(event.getStateVersion())
                .build();
    }

    public PaymentSummary updateStatusAndMessage(BasePaymentEventWithCause event, String message) {
        var payment = event.getCarriedState();
        return toBuilder()
                .status(PaymentSummaryStatus.fromPaymentStatus(payment.getStatus()))
                .processingTime(event.getTimestamp())
                .message(message)
                .build();
    }

    public PaymentSummary updateStatusAndResetMessage(BasePaymentEventWithCause event) {
        return updateStatusAndMessage(event, null);
    }

    public PaymentSummary updateStatus(BasePaymentEventWithCause event) {
        var payment = event.getCarriedState();
        return toBuilder()
                .status(PaymentSummaryStatus.fromPaymentStatus(payment.getStatus()))
                .processingTime(event.getTimestamp())
                .build();
    }
}