package gov.scot.payments.payments.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.*;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Subject;
import gov.scot.payments.payments.model.api.RejectPaymentRequest;
import io.vavr.collection.Set;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "payments", type = "rejectCancelPayment")
@NoArgsConstructor
@AllArgsConstructor
public class RejectCancelPaymentCommand extends CommandImpl implements HasPaymentId, HasUser, HasStateVersion, HasKey<String> {

    @NonNull private UUID paymentId;
    @NonNull private String user;
    @Nullable private Set<Role> roles;
    @Nullable private Long stateVersion;
    @NonNull private String reason;

    public RejectCancelPaymentCommand(UUID paymentId, String user, Set<Role> roles, String reason, Long stateVersion, boolean reply) {
        super(reply);
        this.paymentId = paymentId;
        this.user = user;
        this.roles = roles;
        this.reason = reason;
        this.stateVersion = stateVersion;
    }

    @MessageConstructor(role = "payments:ApprovePayment")
    public static RejectCancelPaymentCommand fromRequest(RejectPaymentRequest request, boolean reply, Long stateVersion, Subject principal){
        return new RejectCancelPaymentCommand(request.getPaymentId(), principal.getName(), principal.getRoles(), request.getReason(), stateVersion, reply);
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return paymentId.toString();
    }
}
