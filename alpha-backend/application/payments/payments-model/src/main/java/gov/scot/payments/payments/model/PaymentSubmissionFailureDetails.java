package gov.scot.payments.payments.model;

import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.payments.model.aggregate.Submission;
import lombok.*;
import org.apache.avro.reflect.Nullable;

import java.util.Map;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@Builder(toBuilder = true)
public class PaymentSubmissionFailureDetails {

    @Nullable private String psp;
    @NonNull private String message;
    @Nullable private String code;
    @Nullable private Map<String,String> pspMetadata;

    public Submission toSubmission(PaymentMethod method) {
        return Submission.builder()
                         .pspMetadata(pspMetadata)
                         .psp(psp)
                         .paymentMethod(method)
                         .code(code)
                         .message(message)
                         .status(Submission.Status.Failure)
                         .build();
    }
}
