package gov.scot.payments.payments.model.aggregate;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.apache.avro.reflect.Stringable;

import java.io.IOException;
import java.util.regex.Pattern;

@Value
@EqualsAndHashCode
@Stringable
public class SortCode {

    private static int SORT_CODE_LENGTH = 6;

    private static final Pattern regexNumeric = Pattern.compile("^[0-9]*$");

    private String value;

    public SortCode(String value) {
        if(value.length() != SORT_CODE_LENGTH){
            throw new InvalidPaymentFieldException("Invalid sort-code number "+ value + " - must be 6 digits");
        }
        boolean isNumeric = regexNumeric.matcher(value).matches();
        if(!isNumeric){
            throw new InvalidPaymentFieldException("Invalid sort-code number "+ value + " - must be numeric");
        }

        this.value = value;
    }

    public static SortCode fromString(String sortCodeStr) {
        return new SortCode(sortCodeStr);
    }

    @Override
    public String toString() {
        return value;
    }

    public String toFormattedString(){
        return String.format("%s-%s-%s",value.substring(0,2),value.substring(2,4),value.substring(4,6));
    }

    public static class SortCodeSerializer extends JsonSerializer<SortCode> {


        @Override
        public void serialize(SortCode value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeString(value.toString());
        }
    }

    public static class SortCodeDeserializer extends JsonDeserializer<SortCode> {
        @Override
        public SortCode deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            try {
                return SortCode.fromString(p.getText());
            } catch (InvalidPaymentFieldException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }
}
