package gov.scot.payments.payments.model.aggregate;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;
import org.javamoney.moneta.Money;

import javax.validation.constraints.NotNull;
import java.util.Map;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@SuperBuilder(toBuilder = true)
public class Settlement extends PaymentEvent {

    @NonNull private PaymentMethod paymentMethod;
    @NonNull private String psp;
    @NonNull @Builder.Default private Map<String,String> pspMetadata = Map.of();
}
