package gov.scot.payments.payments.model.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.*;
import org.apache.avro.reflect.AvroIgnore;
import org.apache.avro.reflect.Nullable;
import org.javamoney.moneta.Money;

import java.time.Instant;
import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@Builder(toBuilder = true)
public class CreatePaymentRequest {

    private String batchId;
    @NonNull @Builder.Default @EqualsAndHashCode.Include private UUID id = UUID.randomUUID();
    @Nullable private List<PaymentMethod> allowedMethods;

    //payment fields
    @JsonDeserialize(using = MoneyDeserializer.class)
    @JsonSerialize(using = MoneySerializer.class)
    @NonNull private Money amount;
    @NonNull @Builder.Default private Instant earliestExecutionDate = Instant.now();
    @NonNull private Instant latestExecutionDate;

    //payee / payer fields
    @NonNull private CompositeReference product;
    @NonNull private CashAccount creditorAccount;
    @NonNull private PartyIdentification creditor;

    //supplementary information
    @NonNull @Builder.Default private List<MetadataField> metadata = List.empty();
    @NonNull @Builder.Default private List<MetadataField> creditorMetadata = List.empty();

    @JsonIgnore
    public <T extends CashAccount> T getCreditorAccountAs(Class<T> clazz){
        return (T)creditorAccount;
    }

    @JsonIgnore
    public Option<MetadataField> getMetadata(String name){
        return metadata.find(m -> m.getName().equals(name));
    }

    @JsonIgnore
    public Option<MetadataField> getCreditorMetadata(String name){
        return creditorMetadata.find(m -> m.getName().equals(name));
    }
}
