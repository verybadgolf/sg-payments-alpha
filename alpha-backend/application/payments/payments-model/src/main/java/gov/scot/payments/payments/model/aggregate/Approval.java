package gov.scot.payments.payments.model.aggregate;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

import java.time.Instant;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@SuperBuilder(toBuilder = true)
public class Approval extends PaymentEvent {

    @NonNull private String requiredBy;
    @NonNull @Builder.Default private Status status = Status.InProgress;
    @Nullable private String user;
    @Nullable private String reason;

    public enum Status {
        InProgress,
        Approved,
        Rejected
    }

}
