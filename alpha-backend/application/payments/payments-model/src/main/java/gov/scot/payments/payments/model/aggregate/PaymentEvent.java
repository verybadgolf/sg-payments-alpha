package gov.scot.payments.payments.model.aggregate;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.Instant;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@SuperBuilder(toBuilder = true)
public abstract class PaymentEvent {

    @NonNull @Builder.Default
    private Instant timestamp = Instant.now();
}
