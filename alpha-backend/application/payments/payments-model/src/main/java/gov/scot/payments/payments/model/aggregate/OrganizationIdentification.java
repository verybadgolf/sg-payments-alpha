package gov.scot.payments.payments.model.aggregate;

import io.vavr.collection.List;
import lombok.*;
import org.apache.avro.reflect.Nullable;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder(toBuilder = true)
public class OrganizationIdentification {

    //largely based on OrganisationIdentification29 from ISO20022 PAIN

    @Nullable @Pattern(regexp = "[A-Z0-9]{4,4}[A-Z]{2,2}[A-Z0-9]{2,2}([A-Z0-9]{3,3}){0,1}") private String bic;
    @Nullable @Pattern(regexp = "[A-Z0-9]{18,18}[0-9]{2,2}") private String lei;
    @Nullable private List<OtherIdentifiers> otherIdentifiers;

    @Getter
    @AllArgsConstructor
    @NoArgsConstructor
    @EqualsAndHashCode
    @ToString
    @Builder(toBuilder = true)
    public static class OtherIdentifiers {
        @NonNull @Size(max = 35) private String id;
        @Size(max = 35) private String issuer;
    }
}
