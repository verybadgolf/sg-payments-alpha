package gov.scot.payments.payments.model.aggregate;

import lombok.*;
import org.apache.avro.reflect.Nullable;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder(toBuilder = true)
public class PostalAddress {

    //largely based on PostalAddress24 from ISO20022 PAIN

    public enum AddressType {ADDR, PBOX, HOME, BIZZ, MLTO, DLVY};

    @Nullable private AddressType addressType;
    @Nullable @Size(max = 70) private String department;
    @Nullable @Size(max = 70) private String subDepartment;
    @Nullable @Size(max = 70) private String streetName;
    @Nullable @Size(max = 16) private String buildingNumber;
    @Nullable @Size(max = 35) private String buildingName;
    @Nullable @Size(max = 70) private String floor;
    @Nullable @Size(max = 16) private String postBox;
    @Nullable @Size(max = 70) private String room;
    @Nullable @Size(max = 16) private String postCode;
    @Nullable @Size(max = 35) private String townName;
    @Nullable @Size(max = 35) private String townLocationName;
    @Nullable @Size(max = 35) private String districtName;
    @Nullable @Size(max = 35) private String countrySubDivision;
    @Nullable @Pattern(regexp = "\\+[0-9]{1,3}-[0-9()+\\-]{1,30}") private String country;
    @Nullable @Size(max = 7) private List<String> addressLines;

}
