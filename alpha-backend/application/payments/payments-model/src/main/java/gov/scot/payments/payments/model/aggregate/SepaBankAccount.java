package gov.scot.payments.payments.model.aggregate;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Pattern;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
@SuperBuilder(toBuilder = true)
public class SepaBankAccount extends CashAccount {

    //TODO: isoify
    @NonNull @Pattern(regexp = " [A-Z]{2,2}[0-9]{2,2}[a-zA-Z0-9]{1,30}") private String iban;
}
