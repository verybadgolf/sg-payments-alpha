package gov.scot.payments.payments.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.HasStateVersion;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.MoneyDeserializer;
import gov.scot.payments.payments.model.aggregate.MoneySerializer;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.payments.model.command.HasPaymentId;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;
import org.javamoney.moneta.Money;

import java.util.Map;
import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "payments", type = "returnPayment")
@NoArgsConstructor
@AllArgsConstructor
public class ReturnPaymentCommand extends CommandImpl implements HasPaymentId, HasKey<String> {

    @NonNull private UUID paymentId;
    @NonNull private PaymentMethod method;
    @NonNull private String psp;
    @NonNull private String message;
    @NonNull private String code;
    @JsonDeserialize(using = MoneyDeserializer.class)
    @JsonSerialize(using = MoneySerializer.class)
    @NonNull private Money amount;
    @NonNull @Builder.Default private Map<String,String> pspMetadata = Map.of();

    @Override
    @JsonIgnore
    public String getKey() {
        return paymentId.toString();
    }
}
