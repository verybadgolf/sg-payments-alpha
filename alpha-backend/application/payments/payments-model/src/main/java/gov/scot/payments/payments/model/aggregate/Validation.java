package gov.scot.payments.payments.model.aggregate;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@SuperBuilder(toBuilder = true)
public class Validation extends PaymentEvent {

    @NonNull @Builder.Default private Status status = Status.Success;
    @Nullable private String message;

    public enum Status {
        Failure,
        Success
    }


}
