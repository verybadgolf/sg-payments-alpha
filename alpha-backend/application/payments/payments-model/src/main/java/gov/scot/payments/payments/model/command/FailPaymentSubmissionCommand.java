package gov.scot.payments.payments.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.HasStateVersion;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.PaymentSubmissionFailureDetails;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.payments.model.command.HasPaymentId;
import io.vavr.collection.List;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "payments", type = "failPaymentSubmission")
@NoArgsConstructor
@AllArgsConstructor
public class FailPaymentSubmissionCommand extends CommandImpl implements HasPaymentId, HasKey<String> {

    @NonNull private UUID paymentId;
    @Nullable private PaymentMethod method;
    @NonNull private List<PaymentSubmissionFailureDetails> failureDetails;

    @Override
    @JsonIgnore
    public String getKey() {
        return paymentId.toString();
    }
}
