package gov.scot.payments.payments.model.aggregate;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@SuperBuilder(toBuilder = true)
public class UKBankAccount extends CashAccount {

    @JsonDeserialize(using = SortCode.SortCodeDeserializer.class)
    @JsonSerialize(using = SortCode.SortCodeSerializer.class)
    @NonNull private SortCode sortCode;

    @JsonDeserialize(using = UKAccountNumber.UKAccountNumberDeserializer.class)
    @JsonSerialize(using = UKAccountNumber.UKAccountNumberSerializer.class)
    @NonNull private UKAccountNumber accountNumber;

}
