package gov.scot.payments.payments.model.aggregate;

import io.vavr.Tuple2;
import lombok.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder(toBuilder = true)
public class MetadataField {

    public static final String CLIENT_REF = "ClientRef";

    @NonNull private String name;
    @NonNull private String value;
    private boolean personalInformation;
    private boolean financiallyRelevant;

    public MetadataField(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public static MetadataField clientRef(String value){
        return new MetadataField(CLIENT_REF,value);
    }
}
