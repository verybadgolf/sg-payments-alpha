package gov.scot.payments.customers.pm.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.customers.model.aggregate.ProductStatus;
import gov.scot.payments.customers.model.command.ApproveDeleteProductCommand;
import gov.scot.payments.customers.model.command.FinalizeCreateProductCommand;
import gov.scot.payments.customers.model.command.FinalizeDeleteCustomerCommand;
import gov.scot.payments.customers.model.command.ParseFileCommand;
import gov.scot.payments.customers.model.event.DeleteCustomerApprovedEvent;
import gov.scot.payments.customers.model.event.ProductDeletedEvent;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.user.Scope;
import gov.scot.payments.paymentfiles.model.event.FileUploadedEvent;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.spring.TestConfiguration;
import gov.scot.payments.users.model.UserFileResourcesUpdatedEvent;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import org.apache.kafka.streams.KafkaStreams;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;

import java.net.URI;
import java.time.Duration;
import java.time.Instant;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@ApplicationIntegrationTest(classes = {CustomersPMApplicationIntegrationTest.TestApplication.class}
        , componentType = ApplicationIntegrationTest.ComponentType.PROCESS_MANAGER)
public class CustomersPMApplicationIntegrationTest extends CustomersPMTestBase{

    private static final UUID CORRELATION_ID = UUID.randomUUID();

    @BeforeEach
    public void setUp(EmbeddedBrokerClient brokerClient, @Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter, @Value("${commands.destination}") String commandTopic) {
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(10));
        brokerClient.getBroker().addTopicsIfNotExists(commandTopic);
    }

    @Test
    @DisplayName("When UserFileResourcesUpdatedEvent received then a FinalizeCreateProductCommand is emitted")
    void testFinalizeProductCreation(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) {

        var triggerEvent = UserFileResourcesUpdatedEvent.builder()
                .scope(new Scope(PRODUCT_1, new Scope(CUSTOMER)))
                .correlationId(CORRELATION_ID)
                .build();

        brokerClient.sendToDestination("events-external", List.of(triggerEvent));
        var createdCommands = brokerClient.readAllFromTopic(commandTopic, FinalizeCreateProductCommand.class);

        assertThat(createdCommands.size(), is(1));
        assertThat(createdCommands.get(0).getClass(), is(FinalizeCreateProductCommand.class));
        var command = (FinalizeCreateProductCommand) createdCommands.get(0);
        assertThat(command.getProductId(), is(new CompositeReference(CUSTOMER, PRODUCT_1)));
        assertThat(command.isReply(), is(false));
    }

    @Test
    @DisplayName("When DeleteCustomerApprovedEvent received with some products not in final status then some commands are emitted")
    void testBeginCustomerDeletionWithSomeNonFinalizedProducts(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) {

        var products = HashSet.of(
                testProduct(PRODUCT_1, ProductStatus.Live),
                testProduct(PRODUCT_2, ProductStatus.Rejected),
                testProduct(PRODUCT_3, ProductStatus.Deleting)
        );

        var triggerEvent = DeleteCustomerApprovedEvent.builder()
                .carriedState(testCustomerBuilder().products(products).build())
                .user(USER)
                .stateVersion(99L)
                .correlationId(CORRELATION_ID)
                .build();

        brokerClient.sendToDestination("events-external", List.of(triggerEvent));
        var createdCommands = brokerClient.readAllFromTopic(commandTopic, ApproveDeleteProductCommand.class);

        assertThat(createdCommands.size(), is(2));
        assertThat(createdCommands.get(0).getClass(), is(ApproveDeleteProductCommand.class));
        assertThat(createdCommands.get(1).getClass(), is(ApproveDeleteProductCommand.class));
        ApproveDeleteProductCommand product_1_command;
        ApproveDeleteProductCommand product_3_command;
        if (createdCommands.get(0).getProductId().getComponent1().equals(PRODUCT_1)) {
            product_1_command = createdCommands.get(0);
            product_3_command = createdCommands.get(1);
        } else {
            product_1_command = createdCommands.get(1);
            product_3_command = createdCommands.get(0);
        }
        assertThat(product_1_command.getProductId().getComponent0(), is(CUSTOMER));
        assertThat(product_1_command.getProductId().getComponent1(), is(PRODUCT_1));
        assertThat(product_1_command.isForceDelete(), is(true));
        assertThat(product_1_command.getUser(), is(USER));
        assertThat(product_1_command.getRoles().size(), is(0));
        assertThat(product_3_command.getProductId().getComponent0(), is(CUSTOMER));
        assertThat(product_3_command.getProductId().getComponent1(), is(PRODUCT_3));
        assertThat(product_3_command.isForceDelete(), is(true));
        assertThat(product_3_command.getUser(), is(USER));
        assertThat(product_3_command.getRoles().size(), is(0));
    }

    @Test
    @DisplayName("When ProductDeletedEvent is received with only finalized products then a FinalizeDeleteCustomerCommand is emitted")
    void testFinalizeCustomerDeletionWithOnlyFinalizedProducts(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) {

        var products = HashSet.of(
                testProduct(PRODUCT_1, ProductStatus.Deleted),
                testProduct(PRODUCT_2, ProductStatus.Rejected),
                testProduct(PRODUCT_3, ProductStatus.Deleted)
        );

        var triggerEvent = ProductDeletedEvent.builder()
                .productId(PRODUCT_1)
                .carriedState(testCustomerBuilder().products(products).build())
                .user(USER)
                .stateVersion(99L)
                .correlationId(CORRELATION_ID)
                .build();

        brokerClient.sendToDestination("events-external", List.of(triggerEvent));
        var createdCommands = brokerClient.readAllFromTopic(commandTopic, FinalizeDeleteCustomerCommand.class);

        assertThat(createdCommands.size(), is(1));
        assertThat(createdCommands.get(0).getClass(), is(FinalizeDeleteCustomerCommand.class));
        var command = (FinalizeDeleteCustomerCommand)createdCommands.get(0);
        assertThat(command.getCustomerId(), is(CUSTOMER));
    }

    @Test
    @DisplayName("When FileUploadedEvent is received then a ParseFileCommand is emitted")
    void testBeginFileParsingCommandEmission(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) {

        CompositeReference product = CompositeReference.parse("client.product");
        URI path = URI.create("s3://123/abc/def.csv");

        var triggerEvent = FileUploadedEvent.builder()
                .path(path)
                .createdAt(Instant.now())
                .product(product)
                .user(USER)
                .build();

        brokerClient.sendToDestination("events-external", List.of(triggerEvent));

        var createdCommands = brokerClient.readAllFromTopic(commandTopic, ParseFileCommand.class);
        assertThat(createdCommands.size(), is(1));
        assertThat(createdCommands.get(0).getClass(), is(ParseFileCommand.class));

        var command = (ParseFileCommand) createdCommands.get(0);

        assertThat(command.getProduct(), is(product));
        assertThat(command.getFile(), is(path));
        assertThat(command.getUser(), is(USER));
    }


    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends CustomersPMApplication {

    }
}