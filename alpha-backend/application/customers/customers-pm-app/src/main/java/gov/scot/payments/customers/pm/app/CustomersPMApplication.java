package gov.scot.payments.customers.pm.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.MessageTypeRegistry;
import gov.scot.payments.application.component.processmanager.EventTransformer;
import gov.scot.payments.application.component.processmanager.ProcessManagerApplication;
import gov.scot.payments.customers.model.event.*;
import gov.scot.payments.paymentfiles.model.event.FileUploadedEvent;
import gov.scot.payments.payments.model.event.PaymentValidEvent;
import gov.scot.payments.users.model.UserFileResourcesDeletedEvent;
import gov.scot.payments.users.model.UserFileResourcesUpdatedEvent;
import org.apache.kafka.common.header.Headers;
import org.springframework.context.annotation.Bean;

import java.util.function.Predicate;

@ApplicationComponent
public class CustomersPMApplication extends ProcessManagerApplication {

    /*
    @Bean
    public StreamJoiner<Event,Event,Event> joiner(SchemaRegistryClient confluentSchemaRegistryClient){
        HeaderPreservingCompositeNonNativeSerde<Event> eventSerde = valueSerde(Event.class);
        return StreamJoiner.<Event, Event, Event>builder()
            .repartitionTopicName()
            .joinWindow()
            .joiner()
            .rekeyFunction()
            .leftValueSerde(eventSerde)
            .rightValueSerde(eventSerde)
            .stateSerde(stateSerde(confluentSchemaRegistryClient,SerializedMessage.class))
            .build();
    }
     */

    @Bean
    public EventTransformer eventTransformFunction() {
        return new CustomersEventTransformer();
    }

    @Bean
    public Predicate<Headers> externalEventHeaderFilter(MessageTypeRegistry messageTypeRegistry){
        return h -> messageTypeRegistry.matchesType(h
                , UserFileResourcesUpdatedEvent.class
                ,UserFileResourcesDeletedEvent.class
                ,DeleteCustomerApprovedEvent.class
                ,ProductDeletedEvent.class
                ,FileUploadedEvent.class
                ,PaymentValidEvent.class);
    }

    public static void main(String[] args){
        BaseApplication.run(CustomersPMApplication.class,args);
    }
}