package gov.scot.payments.customers.ch.app.verification;

import gov.scot.payments.customers.model.aggregate.Product;
import gov.scot.payments.customers.model.aggregate.Validation;
import gov.scot.payments.customers.model.aggregate.ValidationAction;
import gov.scot.payments.payments.model.aggregate.Payment;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class StatelessPaymentVerifier {

    private final Map<String,ValidationRuleEvaluator> evaluators;

    public PaymentVerificationResult verifyPayment(Payment payment, List<Validation> validationRules) {
        return validationRules
                .filter(r -> r.getType() == Validation.Type.Stateless)
                .map(r -> new Tuple2<>(r,evaluators.get(r.getEvaluator())))
                .filter(r -> r._2.isDefined())
                .map(r -> r.map2(Option::get))
                .map(r -> r.map2(rule -> rule.evaluate(r._1.getRule(),payment)))
                .map(r -> {
                    if(r._2.isSuccess()){
                        return PaymentVerificationResult.verified();
                    } else if(r._1.getAction() == ValidationAction.RejectAction) {
                        return PaymentVerificationResult.of(PaymentVerificationResult.Status.Rejected, r._1::getErrorMessage);
                    } else {
                        return PaymentVerificationResult.of(PaymentVerificationResult.Status.ApprovalRequired, r._1::getErrorMessage);
                    }
                })
                .fold(PaymentVerificationResult.verified(),PaymentVerificationResult::merge);

    }
}
