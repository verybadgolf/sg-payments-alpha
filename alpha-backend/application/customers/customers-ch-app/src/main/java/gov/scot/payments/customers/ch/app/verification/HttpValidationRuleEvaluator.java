package gov.scot.payments.customers.ch.app.verification;

import gov.scot.payments.payments.model.aggregate.Payment;
import lombok.RequiredArgsConstructor;
import org.springframework.web.reactive.function.client.WebClient;

@RequiredArgsConstructor
public class HttpValidationRuleEvaluator implements ValidationRuleEvaluator {

    private final WebClient client;

    @Override
    public ValidationRuleEvaluationResult evaluate(String rule, Payment payment) {
        try{
            return client.post()
                    .uri(rule)
                    .bodyValue(payment)
                    .retrieve()
                    .bodyToMono(ValidationRuleEvaluationResult.class)
                    .block();
        } catch (Exception e) {
            return ValidationRuleEvaluationResult.failure(e.getMessage());
        }
    }

    @Override
    public String getName() {
        return "http";
    }
}
