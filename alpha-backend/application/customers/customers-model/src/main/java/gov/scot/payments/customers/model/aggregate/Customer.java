package gov.scot.payments.customers.model.aggregate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import lombok.*;
import org.apache.avro.reflect.Nullable;

import java.time.Instant;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@Builder(toBuilder = true)
@MessageType(context = "customers", type = "customer")
public class Customer implements HasKey<String> {

    @EqualsAndHashCode.Include @NonNull private String name;
    @NonNull private Instant createdAt;
    @NonNull private CustomerStatus status;
    @Nullable private Set<Product> products;
    @NonNull private String createdBy;
    @NonNull private String lastModifiedBy;
    @Nullable private CustomerApproval deleteApproval;
    @Nullable private CustomerApproval approval;

    @Override
    @JsonIgnore
    public String getKey() {
        return getName();
    }

    @JsonIgnore
    public Option<Product> getProduct(String productName) {
        if (products == null) {
            return Option.none();
        }

        var product = products.find(p -> p.getName().equals(productName));
        return product;
    }
}
