package gov.scot.payments.customers.model.aggregate;

import lombok.*;
import org.apache.avro.reflect.Nullable;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder(toBuilder = true)
public class ProductConfiguration {
    @Nullable private ProductAdapter adapter;
    @Nullable private OutboundConfiguration outboundConfiguration;
}
