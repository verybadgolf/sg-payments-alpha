package gov.scot.payments.customers.model.api;

import gov.scot.payments.customers.model.aggregate.OutboundConfiguration;
import gov.scot.payments.customers.model.aggregate.ProductAdapter;
import lombok.*;
import org.apache.avro.reflect.Nullable;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@Builder
public class ProductRequest {
    @EqualsAndHashCode.Include @NonNull private String name;
    @EqualsAndHashCode.Include @NonNull private String customerId;
    @NonNull private ProductAdapter adapter;
    @Nullable private OutboundConfiguration outboundConfiguration;
}
