package gov.scot.payments.customers.model.event;

import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.customers.model.aggregate.Product;
import gov.scot.payments.model.MessageType;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@NoArgsConstructor
@RequiredArgsConstructor
@MessageType(context = "customers", type = "baseProductEvent")
public class BaseProductEvent extends BaseCustomerEvent {

    /*
     * Defines @MessageType and is not abstract in order to pass the archunit tests.
     */

    @NonNull private String productId;

    BaseProductEvent(String productId, Customer customer, String user, Long stateVersion) {
        super(customer, user, stateVersion);
        this.productId = productId;
    }
}
