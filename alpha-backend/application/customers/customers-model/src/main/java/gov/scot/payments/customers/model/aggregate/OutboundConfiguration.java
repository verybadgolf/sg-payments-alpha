package gov.scot.payments.customers.model.aggregate;

import gov.scot.payments.payments.model.aggregate.MetadataField;
import gov.scot.payments.payments.model.aggregate.PartyIdentification;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.payments.model.aggregate.UKBankAccount;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import lombok.*;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder(toBuilder = true)
public class OutboundConfiguration {

    /*
     * We are ignoring metadata, notifications and reports for the time being.
     * Serialization needs to be made to work for them.
     */

    private boolean fileImports;
    private boolean apiAccess;
    @NonNull private List<PaymentMethod> defaultPaymentMethods;
    @NonNull @Builder.Default private Map<String, PaymentMethodConfiguration> paymentMethodConfiguration = new HashMap<>();
    @NonNull private UKBankAccount source;
    @NonNull private UKBankAccount returns;
    @NonNull private PartyIdentification payer;
    @NonNull @Builder.Default private List<MetadataField> metadata = List.empty();
    @NonNull @Builder.Default private Set<Validation> validationRules = HashSet.empty();
//    @NonNull private Set<Notification> notifications;
//    @NonNull private Set<Report> reports;
}
