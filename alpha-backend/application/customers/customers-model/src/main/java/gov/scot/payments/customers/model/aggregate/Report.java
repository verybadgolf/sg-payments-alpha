package gov.scot.payments.customers.model.aggregate;

import lombok.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
public class Report {

    @NonNull private String schedule;
    @NonNull private String query;
    @NonNull private DeliveryConfiguration deliveryConfiguration;
}
