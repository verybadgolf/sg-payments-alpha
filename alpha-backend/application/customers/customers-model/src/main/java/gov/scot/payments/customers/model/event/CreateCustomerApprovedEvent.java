package gov.scot.payments.customers.model.event;

import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.model.MessageType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "createCustomerApproved")
@NoArgsConstructor
@ToString
public class CreateCustomerApprovedEvent extends BaseCustomerEventWithCause {

    public CreateCustomerApprovedEvent(Customer customer, String user, Long stateVersion) {
        super(customer, user, stateVersion);
    }
}
