package gov.scot.payments.customers.model.aggregate;

public enum ProductStatus {
    New,
    Creating,
    Live,
    Rejected,
    ModifyRequested,
    DeleteRequested,
    Deleting,
    Deleted
}
