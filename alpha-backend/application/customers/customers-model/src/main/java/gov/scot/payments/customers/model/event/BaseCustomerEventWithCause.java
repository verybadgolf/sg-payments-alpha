package gov.scot.payments.customers.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.model.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@NoArgsConstructor
@RequiredArgsConstructor
@MessageType(context = "customers", type = "baseCustomerEventWithCause")
public class BaseCustomerEventWithCause extends EventWithCauseImpl implements HasState<Customer>,  HasKey<String> {

    /*
     * Defines @MessageType and is not abstract in order to pass the archunit tests.
     */

    @Getter @NonNull private String user;

    @Getter private Customer carriedState;
    @Getter private Long stateVersion;

    public BaseCustomerEventWithCause(final Customer customer, final String user, final Long stateVersion,
                                      final UUID correlationId, final int executionCount, final boolean reply) {
        super(correlationId, executionCount, reply);
        this.setCarriedState(customer, stateVersion);
        this.user = user;
    }

    public BaseCustomerEventWithCause(final Customer customer, final String user, final Long stateVersion) {
        super();
        this.setCarriedState(customer, stateVersion);
        this.user = user;
    }

    @JsonIgnore
    public void setCarriedState(Customer state, Long version) {
        this.carriedState = state;
        this.stateVersion = version;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return getCarriedState().getKey();
    }
}
