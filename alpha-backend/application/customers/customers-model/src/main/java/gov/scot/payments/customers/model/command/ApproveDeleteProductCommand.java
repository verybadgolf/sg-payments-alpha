package gov.scot.payments.customers.model.command;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.MessageConstructor;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Subject;
import io.vavr.collection.Set;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "approveDeleteProduct")
@NoArgsConstructor
public class ApproveDeleteProductCommand extends BaseProductCommand {

    @Builder.Default private boolean forceDelete = false;

    public ApproveDeleteProductCommand(CompositeReference productId, boolean forceDelete, String user, Set<Role> roles, Long stateVersion, boolean reply) {
        super(productId, user, roles, stateVersion, reply);
        this.forceDelete = forceDelete;
    }

    @MessageConstructor(role = "customers:ApproveProductModification")
    public static ApproveDeleteProductCommand fromRequest(CompositeReference productId, boolean reply, Long stateVersion, Subject principal){
        return new ApproveDeleteProductCommand(productId, false, principal.getName(), principal.getRoles(), stateVersion, reply);
    }
}
