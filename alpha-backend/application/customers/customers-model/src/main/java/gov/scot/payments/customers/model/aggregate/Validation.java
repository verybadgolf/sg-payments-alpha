package gov.scot.payments.customers.model.aggregate;

import lombok.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
public class Validation {

    public enum Type {Stateless, Batch, Window}

    @NonNull @Builder.Default private String evaluator = "spel";
    @NonNull @Builder.Default private Type type = Type.Stateless;
    @NonNull private String rule;
    @NonNull private String errorMessage;
    @NonNull @Builder.Default private ValidationAction action = ValidationAction.RejectAction;
}
