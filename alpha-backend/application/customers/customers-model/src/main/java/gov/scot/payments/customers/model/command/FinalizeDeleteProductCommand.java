package gov.scot.payments.customers.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.customers.model.aggregate.HasProduct;
import gov.scot.payments.model.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "finalizeDeleteProduct")
@NoArgsConstructor
public class FinalizeDeleteProductCommand extends CommandImpl implements HasProduct, HasKey<String> {

    @NonNull private CompositeReference productId;

    public FinalizeDeleteProductCommand(CompositeReference productId, boolean reply) {
        super(reply);
        this.productId = productId;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return productId.getComponent0();
    }
}
