package gov.scot.payments.customers.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.model.EventImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.HasState;
import gov.scot.payments.model.MessageType;
import lombok.*;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@NoArgsConstructor
@RequiredArgsConstructor
@MessageType(context = "customers", type = "baseCustomerEvent")
public class BaseCustomerEvent extends EventImpl implements HasState<Customer>, HasKey<String> {

    /*
     * Defines @MessageType and is not abstract in order to pass the archunit tests.
     */

    @Getter @NonNull private Customer carriedState;
    @Getter @NonNull private String user;
    @Getter @NonNull private Long stateVersion;

    @Override
    @JsonIgnore
    public void setCarriedState(Customer state, Long version) {
        this.carriedState = state;
        stateVersion = version;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return getCarriedState().getKey();
    }
}
