package gov.scot.payments.customers.model.aggregate;

import gov.scot.payments.model.CompositeReference;

public interface HasProduct {

    CompositeReference getProductId();
}
