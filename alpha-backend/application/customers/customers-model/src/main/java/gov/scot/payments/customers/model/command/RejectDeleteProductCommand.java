package gov.scot.payments.customers.model.command;

import gov.scot.payments.model.MessageConstructor;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Subject;
import io.vavr.collection.Set;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "rejectDeleteProduct")
@NoArgsConstructor
public class RejectDeleteProductCommand extends BaseProductCommand {

    @NonNull private String reason;

    public RejectDeleteProductCommand(CompositeReference productId, String reason, String user, Set<Role> roles, Long stateVersion, boolean reply) {
        super(productId, user, roles, stateVersion, reply);
        this.reason = reason;
    }

    @MessageConstructor(role = "customers:ApproveProductModification")
    public static RejectDeleteProductCommand fromRequest(CompositeReference productId, String reason, boolean reply, Long stateVersion, Subject principal){
        return new RejectDeleteProductCommand(productId, reason, principal.getName(), principal.getRoles(), stateVersion, reply);
    }
}
