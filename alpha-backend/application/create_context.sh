echo Enter a context name
read CONTEXT_NAME

SCRIPT_PATH=$(dirname "$0")
CONTEXT_PATH="$SCRIPT_PATH"/"$CONTEXT_NAME"
GENERATED_PATH="$SCRIPT_PATH"/../core/context/generated

if [ -d "$CONTEXT_PATH" ]; then
    echo Context already exists, exiting
    exit
fi

../gradlew :core:context:cleanArch :core:context:generate -Dgroup=gov.scot.payments."$CONTEXT_NAME" -Dname="$CONTEXT_NAME" -Dversion=1.0-SNAPSHOT -Dcom.orctom.gradle.archetype.binding.contextName="$CONTEXT_NAME"
cp -R "$GENERATED_PATH"/* "$SCRIPT_PATH"