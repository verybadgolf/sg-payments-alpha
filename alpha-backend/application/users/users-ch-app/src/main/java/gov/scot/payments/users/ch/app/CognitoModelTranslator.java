package gov.scot.payments.users.ch.app;

import gov.scot.payments.model.user.*;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AttributeType;
import software.amazon.awssdk.services.cognitoidentityprovider.model.GroupType;
import software.amazon.awssdk.services.cognitoidentityprovider.model.UserPoolClientType;
import software.amazon.awssdk.services.cognitoidentityprovider.model.UserType;

public class CognitoModelTranslator {

    private static List<String> OAUTH_IGNORE = List.of("openid", "profile");

    public static User mapUserPoolTypeToUser(UserType cognitoUser, Set<GroupType> groups)  {
        return mapUserPoolTypeToUser(cognitoUser.username(),List.ofAll(cognitoUser.attributes()),groups);
    }

    public static User mapUserPoolTypeToUser(String username, List<AttributeType> attributes, Set<GroupType> groups)  {

        var emailAttr = attributes.filter(att -> att.name().equals("email")).getOrNull();

        if(emailAttr == null){
            throw new CognitoUserException(String.format("Invalid user %s - missing e-mail field", username));
        }

        var actions = getActionsFromCognitoUser(attributes);
        var scopes = groups.map(group -> group.groupName());
        var roles = mapGroupsAndActionsToRoles(scopes, actions);

        return User.builder()
                   .name(username)
                   .groups(scopes.map(Group::new))
                   .roles(roles)
                   .email(emailAttr.value())
                   .build();

    }

    private static Set<String> getActionsFromCognitoUser(List<AttributeType> attributes) {

        var permittedActionsAttribute = attributes
                .filter(att -> att.name().equals("custom:permitted_actions"))
                .getOrNull();
        if(permittedActionsAttribute == null || permittedActionsAttribute.value().isEmpty()){
            return HashSet.of();
        }
        return HashSet.of(permittedActionsAttribute.value().split(";"));
    }

    public static Set<Role> mapGroupsAndActionsToRoles(Set<String> groups, Set<String> permittedActions){
        // this creates a role for every scope/action pair
        return groups.map(groupName -> {
            var scope = Scope.parse(groupName);
            var actions = permittedActions.map(actionStr -> Action.parse(actionStr));
            return new Role(actions,scope);
        });
    }

    public static String mapRolesToPermittedActionsString(Set<Role> roles){
        // get all actions across all roles.
        var actions = roles.flatMap(role -> role.getActions()).map(action -> action.toString());
        return String.join(";", actions);
    }

    public static Application mapUserPoolClientToApplication(UserPoolClientType appClient) {

        var scopes = List.ofAll(appClient.allowedOAuthScopes());
        var roles = scopes.filter(scope -> !OAUTH_IGNORE.contains(scope))
                .map(oauthScope -> Role.parse(oauthScope));

        return Application.builder()
                .name(appClient.clientName())
                .clientId(appClient.clientId())
                .clientSecret(appClient.clientSecret())
                .roles(HashSet.ofAll(roles))
                .build();
    }

}
