package gov.scot.payments.users.ch.app;

public class SftpUserException extends RuntimeException {
    public SftpUserException(String s) {
        super(s);
    }
}
