package gov.scot.payments.users.ch.app;

public class CognitoUserException extends RuntimeException {

    public CognitoUserException(String errMessage) {
        super(errMessage);
    }

    public CognitoUserException(String appErrMsg, Exception e) {
        super(appErrMsg, e);
    }

}
