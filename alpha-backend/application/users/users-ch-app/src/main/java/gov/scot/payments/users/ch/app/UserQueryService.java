package gov.scot.payments.users.ch.app;

import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Subject;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@RequestMapping
public class UserQueryService {

    private final CognitoService service;

    @GetMapping("/details")
    @ResponseBody
    public Mono<Subject> getUserDetails(@AuthenticationPrincipal Subject subject){
        return Mono.just(subject);
    }

}
