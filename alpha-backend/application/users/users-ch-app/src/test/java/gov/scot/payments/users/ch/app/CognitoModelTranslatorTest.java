package gov.scot.payments.users.ch.app;

import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import org.junit.jupiter.api.Test;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AttributeType;
import software.amazon.awssdk.services.cognitoidentityprovider.model.GroupType;
import software.amazon.awssdk.services.cognitoidentityprovider.model.UserType;

import static gov.scot.payments.users.ch.app.CognitoModelTranslator.mapUserPoolTypeToUser;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CognitoModelTranslatorTest {

    @Test
    void test_mapUserPoolTypeToUser() {

        var userType = UserType.builder()
                .username("testusername")
                .attributes(List.of(
                        AttributeType.builder().name("email").value("testemail").build(),
                        AttributeType.builder().name("custom:permitted_actions").value("payment:read,payment:create").build()
                ).toJavaList())
                .build();

        var group = GroupType.builder()
                .groupName("testproduct")
                .build();
        var group2 = GroupType.builder()
                .groupName("testproduct2")
                .build();

        var modelUser = mapUserPoolTypeToUser(userType, HashSet.of(group, group2));

        assertEquals("testusername", modelUser.getName());
        assertEquals("testemail", modelUser.getEmail());

        // TODO - verify roles created
        /*
        assertEquals(4, modelUser.getRoles().size());
        var roles = modelUser.getRoles();
        var expectedRoles = List.of(
                Role.parse("testproduct/payment:read"),
                Role.parse("testproduct/payment:create"),
                Role.parse("testproduct2/payment:read"),
                Role.parse("testproduct2/payment:create"));
        var exampleRole = Role.parse("testproduct/payment:read");
        assertTrue(roles.contains(exampleRole));
        */
    }

    @Test
    void mapUserPoolClientToApplication() {

        // TODO
    }
}