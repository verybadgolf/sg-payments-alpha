package gov.scot.payments.users.ch.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.model.user.*;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.security.MockSubjectAuthentication;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.spring.TestConfiguration;
import gov.scot.payments.users.model.RemoveUserRequest;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import org.apache.kafka.streams.KafkaStreams;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import software.amazon.awssdk.services.cognitoidentityprovider.CognitoIdentityProviderClient;
import software.amazon.awssdk.services.cognitoidentityprovider.model.*;
import software.amazon.awssdk.services.cognitoidentityprovider.paginators.ListResourceServersIterable;
import software.amazon.awssdk.services.transfer.TransferClient;

import java.time.Duration;

import static gov.scot.payments.testing.security.MockSubjectAuthentication.user;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockAuthentication;

@ApplicationIntegrationTest(classes = {UsersCHApplicationIntegrationTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.COMMAND_HANDLER
        ,properties = {"cloud.aws.credentials.accessKey=test"
            ,"cloud.aws.credentials.secretKey=test"
            ,"cloud.aws.region.static=eu-west-2"
            ,"COGNITO_USER_POOL_ID=local"
            , "FILE_BUCKET_NAME=filebucket"
            , "SFTP_SERVER_ID=localserver"
            , "SFTP_USER_ROLE=userrolearn"
})
public class UsersCHApplicationIntegrationTest {

    @MockBean CognitoIdentityProviderClient cognitoClient;
    @MockBean TransferClient transferClient;

    @BeforeEach
    public void setUp(@Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter, EmbeddedBrokerClient brokerClient, @Value("${namespace}") String namespace){
        brokerClient.getBroker().addTopicsIfNotExists(namespace+"-users-command-errors");
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(10));
    }

    @Test
    public void testAddUserForbidden(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){
        User entity = User.builder()
                          .name("hello")
                          .email("email")
                          .groups(HashSet.of(new Group("group")))
                          .roles(HashSet.of(Role.parse("scope/resource:Write")))
                          .build();

        client
                .mutateWith(mockAuthentication(user("test", Role.parse("users/users:Read"))))
                .put()
                .uri(uri -> uri.path("/command/addUser")
                               .queryParam("reply",true)
                               .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(entity)
                .exchange()
                .expectStatus()
                .isForbidden();

    }

    @Test
    public void testAddUserExists(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){

        User entity = User.builder()
                          .name("hello")
                          .email("email")
                          .sshPublicKey("test")
                          .groups(HashSet.of(new Group("scope")))
                          .roles(HashSet.of(Role.parse("scope/resource:Write")))
                          .build();

        var group = GroupType.builder()
                                .groupName("scope")
                                .description("scope")
                                .build();

        when(cognitoClient.listGroups(any(ListGroupsRequest.class)))
                .thenReturn(ListGroupsResponse.builder()
                        .groups(List.of(group).toJavaList()).build());

        when(cognitoClient.adminCreateUser(any(AdminCreateUserRequest.class)))
                .thenThrow(UsernameExistsException.builder().build());

        client
                .mutateWith(mockAuthentication(user("test", Role.parse("users/users:Write"))))
                .put()
                .uri(uri -> uri.path("/command/addUser")
                               .queryParam("reply",true)
                               .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(entity)
                .exchange()
                .expectStatus()
                .isBadRequest()
                .expectBody()
                .jsonPath("$.error.errorType").isEqualTo("CognitoUserException");
    }

    @Test
    public void testAddUserValid(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){

        User entity = User.builder()
                          .name("hello")
                          .email("email")
                          .groups(HashSet.of(new Group("scope")))
                          .roles(HashSet.of(Role.parse("scope/resource:Write")))
                          .build();

        var group = GroupType.builder()
                .groupName("scope")
                .description("scope")
                .build();

        when(cognitoClient.listGroups(any(ListGroupsRequest.class)))
                .thenReturn(ListGroupsResponse.builder()
                        .groups(List.of(group).toJavaList()).build());

        when(cognitoClient.adminCreateUser(any(AdminCreateUserRequest.class)))
                .thenReturn(AdminCreateUserResponse.builder()
                                                   .user(UserType.builder()
                                                                 .username("hello")
                                                                 .attributes(AttributeType.builder()
                                                                                          .name("email")
                                                                                          .value("email")
                                                                                          .build()
                                                                         ,AttributeType.builder()
                                                                                                                .name("custom:permitted_actions")
                                                                                                                .value("resource:Write")
                                                                                                                .build())
                                                                 .build())
                                                   .build());
        when(cognitoClient.adminListGroupsForUser(any(AdminListGroupsForUserRequest.class)))
                .thenReturn(AdminListGroupsForUserResponse.builder()
                                                          .groups(GroupType.builder()
                                                                           .groupName("scope")
                                                                           .build())
                                                          .build());
        when(cognitoClient.adminAddUserToGroup(any(AdminAddUserToGroupRequest.class))).thenReturn(AdminAddUserToGroupResponse.builder().build());

        client
                .mutateWith(mockAuthentication(user("test", Role.parse("users/users:Write"))))
                .put()
                .uri(uri -> uri.path("/command/addUser")
                               .queryParam("reply",true)
                               .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(entity)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.user.name").isEqualTo("hello")
                .jsonPath("$.user.groups[0].name").isEqualTo("scope")
                .jsonPath("$.user.roles[0].scope.name").isEqualTo("scope")
                .jsonPath("$.user.roles[0].actions[0].resource").isEqualTo("resource")
                .jsonPath("$.user.roles[0].actions[0].verb").isEqualTo("Write");
    }

    @Test
    public void testRemoveUserNotExists(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){

        when(cognitoClient.adminDeleteUser(any(AdminDeleteUserRequest.class))).thenThrow(UserNotFoundException.builder().build());

        var removeReq = RemoveUserRequest.builder()
                .email("test-email")
                .userName("test")
                .build();

        client
                .mutateWith(mockAuthentication(user("test", Role.parse("users/users:Write"))))
                .put()
                .uri(uri -> uri.path("/command/removeUser")
                               .queryParam("reply",true)
                               .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(removeReq)
                .exchange()
                .expectStatus()
                .isBadRequest()
                .expectBody()
                .jsonPath("$.error.errorType").isEqualTo("CognitoUserException");
    }

    @Test
    public void testRemoveUserValid(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){
        when(cognitoClient.adminDeleteUser(any(AdminDeleteUserRequest.class)))
                .thenReturn(AdminDeleteUserResponse.builder().build());

        var removeReq = RemoveUserRequest.builder()
                .email("test-email")
                .userName("test")
                .build();

        client
                .mutateWith(mockAuthentication(user("test", Role.parse("users/users:Write"))))
                .put()
                .uri(uri -> uri.path("/command/removeUser")
                               .queryParam("reply",true)
                               .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(removeReq)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.user").isEqualTo("test");
    }

    @Test
    public void testAddAppExists(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){
        when(cognitoClient.listUserPoolClients(any(ListUserPoolClientsRequest.class)))
                .thenReturn(ListUserPoolClientsResponse.builder()
                                                       .userPoolClients(UserPoolClientDescription.builder()
                                                                                                 .clientName("hello")
                                                                                                 .build())
                                                       .build());
        client
                .mutateWith(mockAuthentication(user("test", Role.parse("users/users:Write"))))
                .put()
                .uri(uri -> uri.path("/command/addApplication")
                               .queryParam("reply",true)
                               .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"hello\"")
                .exchange()
                .expectStatus()
                .isBadRequest()
                .expectBody()
                .jsonPath("$.error.errorType").isEqualTo("CognitoApplicationException");
    }

    @Test
    public void testAddAppValid(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){
        when(cognitoClient.listUserPoolClients(any(ListUserPoolClientsRequest.class)))
                .thenReturn(ListUserPoolClientsResponse.builder().build());

        setupCognitoClientToReturnEmptyResourceServerList();

        when(cognitoClient.createUserPoolClient(any(CreateUserPoolClientRequest.class)))
                .thenReturn(CreateUserPoolClientResponse.builder()
                                                        .userPoolClient(UserPoolClientType.builder()
                                                                                          .clientId("123")
                                                                                          .clientName("hello")
                                                                                          .clientSecret("456")
                                                                                          .allowedOAuthScopes("scope/resource:Write")
                                                                                          .build())
                                                        .build());

        when(cognitoClient.updateUserPoolClient(any(UpdateUserPoolClientRequest.class)))
                .thenReturn(UpdateUserPoolClientResponse.builder()
                                                        .userPoolClient(UserPoolClientType.builder()
                                                                                          .clientId("123")
                                                                                          .clientName("hello")
                                                                                          .clientSecret("456")
                                                                                          .allowedOAuthScopes("scope/resource:Write")
                                                                                          .build())
                                                        .build());


        client
                .mutateWith(mockAuthentication(user("test", Role.parse("users/users:Write"))))
                .put()
                .uri(uri -> uri.path("/command/addApplication")
                               .queryParam("reply",true)
                               .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"hello\"")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.application.name").isEqualTo("hello")
                .jsonPath("$.application.roles[0].scope.name").isEqualTo("scope")
                .jsonPath("$.application.roles[0].actions[0].resource").isEqualTo("resource")
                .jsonPath("$.application.roles[0].actions[0].verb").isEqualTo("Write");
    }


    @Test
    public void testRemoveAppNotExists(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){

        when(cognitoClient.listUserPoolClients(any(ListUserPoolClientsRequest.class))).thenReturn(ListUserPoolClientsResponse.builder()
                .build());
        when(cognitoClient.deleteUserPoolClient(any(DeleteUserPoolClientRequest.class))).thenThrow(ResourceNotFoundException.builder().build());

        client
                .mutateWith(mockAuthentication(user("test", Role.parse("users/users:Write"))))
                .put()
                .uri(uri -> uri.path("/command/removeApplication")
                               .queryParam("reply",true)
                               .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"clientId\"")
                .exchange()
                .expectStatus()
                .isBadRequest()
                .expectBody()
                .jsonPath("$.error.errorType").isEqualTo("CognitoApplicationException");
    }

    @Test
    public void testRemoveAppValid(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){
        when(cognitoClient.deleteUserPoolClient(any(DeleteUserPoolClientRequest.class)))
                .thenReturn(DeleteUserPoolClientResponse.builder().build());
        when(cognitoClient.listUserPoolClients(any(ListUserPoolClientsRequest.class)))
                .thenReturn(ListUserPoolClientsResponse.builder()
                        .userPoolClients(UserPoolClientDescription.builder()
                                .clientName("test")
                                .build())
                        .build());

        client
                .mutateWith(mockAuthentication(user("test", Role.parse("users/users:Write"))))
                .put()
                .uri(uri -> uri.path("/command/removeApplication")
                               .queryParam("reply",true)
                               .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"test\"")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.application").isEqualTo("test");
    }

    @Test
    public void testAddScope(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){
        when(cognitoClient.describeResourceServer(any(DescribeResourceServerRequest.class)))
                .thenThrow(ResourceNotFoundException.builder().build());
        when(cognitoClient.getGroup(any(GetGroupRequest.class)))
                .thenThrow(ResourceNotFoundException.builder().build());
        when(cognitoClient.createResourceServer(any(CreateResourceServerRequest.class)))
                .thenReturn(CreateResourceServerResponse.builder()
                                                        .resourceServer(ResourceServerType.builder().build())
                                                        .build());
        when(cognitoClient.createGroup(any(CreateGroupRequest.class)))
                .thenReturn(CreateGroupResponse.builder()
                                               .group(GroupType.builder().build())
                                               .build());

        client
                .mutateWith(mockAuthentication(user("test", Role.parse("users/users:Write"))))
                .put()
                .uri(uri -> uri.path("/command/addScope")
                               .queryParam("reply",true)
                               .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(new Scope("test"))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.scope.name").isEqualTo("test");
    }

    @Test
    public void testRemoveScope(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){

        when(cognitoClient.listUserPoolClients(any(ListUserPoolClientsRequest.class))).thenReturn(ListUserPoolClientsResponse.builder().build());
        when(cognitoClient.listUsers(any(ListUsersRequest.class))).thenReturn(ListUsersResponse.builder().build());
        when(cognitoClient.deleteGroup(any(DeleteGroupRequest.class))).thenReturn(DeleteGroupResponse.builder().build());
        when(cognitoClient.deleteResourceServer(any(DeleteResourceServerRequest.class))).thenReturn(DeleteResourceServerResponse.builder().build());

        client
                .mutateWith(mockAuthentication(user("test", Role.parse("users/users:Write"))))
                .put()
                .uri(uri -> uri.path("/command/removeScope")
                               .queryParam("reply",true)
                               .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(new Scope("test"))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.scope.name").isEqualTo("test");
    }



    @Test
    public void testAddResource(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){
        when(cognitoClient.describeResourceServer(any(DescribeResourceServerRequest.class)))
                .thenReturn(DescribeResourceServerResponse.builder()
                                                          .resourceServer(ResourceServerType.builder()
                                                                                            .name("test")
                                                                                            .build())
                                                          .build());
        when(cognitoClient.updateResourceServer(any(UpdateResourceServerRequest.class)))
                .thenReturn(UpdateResourceServerResponse.builder()
                                                        .resourceServer(ResourceServerType.builder()
                                                                                          .name("test")
                                                                                          .scopes(ResourceServerScopeType.builder()
                                                                                                                         .scopeName("resource:Write")
                                                                                                                         .build())
                                                                                          .build())
                                                        .build());

        setupCognitoClientToReturnEmptyResourceServerList();

        ResourceWithVerbs entity = ResourceWithVerbs.builder()
                                                    .resource("resourcetest")
                                                    .verbs(HashSet.of("Write"))
                                                    .build();

        client
                .mutateWith(mockAuthentication(user("test", Role.parse("users/users:Write"))))
                .put()
                .uri(uri -> uri.path("/command/addResource")
                               .queryParam("reply",true)
                               .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(entity)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.resource.resource").isEqualTo("resourcetest");
    }

    private void setupCognitoClientToReturnEmptyResourceServerList() {
        var mockIterator = mock(ListResourceServersIterable.class);
        var resourceList = List.of(ListResourceServersResponse.builder()
                .resourceServers(ResourceServerType.builder().build())
                .build());
        when(mockIterator.iterator()).thenReturn(resourceList.iterator());
        when(cognitoClient.listResourceServersPaginator(any(ListResourceServersRequest.class)))
                .thenReturn(mockIterator);
    }

    @Test
    public void testRemoveResource(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){
        when(cognitoClient.describeResourceServer(any(DescribeResourceServerRequest.class)))
                .thenReturn(DescribeResourceServerResponse.builder()
                                                          .resourceServer(ResourceServerType.builder()
                                                                                            .name("resourcetest")
                                                                                            .scopes(ResourceServerScopeType.builder()
                                                                                                                           .scopeName("resourcetest:Write")
                                                                                                                           .build())
                                                                                            .build())
                                                          .build());
        when(cognitoClient.updateResourceServer(any(UpdateResourceServerRequest.class)))
                .thenReturn(UpdateResourceServerResponse.builder()
                                                        .resourceServer(ResourceServerType.builder()
                                                                                          .name("resourcetest")
                                                                                          .build())
                                                        .build());

        var mockIterator = mock(ListResourceServersIterable.class);
        var resourceList = List.of(ListResourceServersResponse.builder()
                .resourceServers(ResourceServerType.builder()
                                .name("resourcetest")
                                .build())
                .build());
        when(mockIterator.iterator()).thenReturn(resourceList.iterator());
        when(cognitoClient.listResourceServersPaginator(any(ListResourceServersRequest.class)))
                .thenReturn(mockIterator);

        client
                .mutateWith(mockAuthentication(user("test", Role.parse("users/users:Write"))))
                .put()
                .uri(uri -> uri.path("/command/removeResource")
                               .queryParam("reply",true)
                               .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"resourcetest\"")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.resource").isEqualTo("resourcetest");
    }

    @Test
    public void testGetDetails(@Autowired WebTestClient client){

        final MockSubjectAuthentication user = user("test", Role.parse("users/users:Write"));
        client
                .mutateWith(mockAuthentication(user))
                .get()
                .uri(uri -> uri.path("/details")
                               .queryParam("reply",true)
                               .build())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.name").isEqualTo("test");
    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends UsersCHApplication {

    }
}