package gov.scot.payments.users.pm.app;

import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.MessageTypeRegistry;
import gov.scot.payments.application.component.processmanager.EventTransformer;
import gov.scot.payments.application.component.processmanager.ProcessManagerApplication;
import gov.scot.payments.customers.model.event.CreateCustomerApprovedEvent;
import gov.scot.payments.customers.model.event.CreateProductApprovedEvent;
import gov.scot.payments.customers.model.event.DeleteCustomerApprovedEvent;
import gov.scot.payments.customers.model.event.DeleteProductApprovedEvent;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.user.ResourceRegisteredEvent;
import gov.scot.payments.model.user.Scope;
import gov.scot.payments.paymentfiles.model.event.FileResourceCreatedEvent;
import gov.scot.payments.paymentfiles.model.event.FileResourceDeletedEvent;
import gov.scot.payments.users.model.*;
import io.vavr.collection.List;
import org.apache.kafka.common.header.Headers;
import org.springframework.context.annotation.Bean;

import java.util.function.Predicate;

@ApplicationComponent
public class UsersPMApplication extends ProcessManagerApplication {

    @Bean
    public EventTransformer eventTransformFunction() {
        return EventTransformer.patternMatching(b ->
                b.match(CreateProductApprovedEvent.class,e -> List.of(new AddScopeCommand(new Scope(e.getProductId(), new Scope(e.getCarriedState().getName())),false)))
                 .match(DeleteProductApprovedEvent.class,e -> List.of(new RemoveScopeCommand(new Scope(e.getProductId(), new Scope(e.getCarriedState().getName())),false)))
                 .match(CreateCustomerApprovedEvent.class,e -> List.of(new AddScopeCommand(Scope.parse(e.getCarriedState().getName()),false)))
                 .match(DeleteCustomerApprovedEvent.class, e -> List.of(new RemoveScopeCommand(Scope.parse(e.getCarriedState().getName()),false)))
                 .match(ResourceRegisteredEvent.class,e -> List.of(new AddResourceCommand(e.getResource(),false)))
                 .match(FileResourceCreatedEvent.class, e -> List.of(new UpdateUserFilePermissionsCommand(e.getProduct(), e.getPath())))
                 .match(FileResourceDeletedEvent.class, e -> List.of(new DeleteUserFilePermissionsCommand(e.getProduct(), e.getPath())))
                .orElse(List.empty())
        );
    }

    @Bean
    public Predicate<Headers> externalEventHeaderFilter(MessageTypeRegistry messageTypeRegistry){
        return h -> messageTypeRegistry.matchesType(h
                , CreateProductApprovedEvent.class
                ,DeleteProductApprovedEvent.class
                ,CreateCustomerApprovedEvent.class
                ,DeleteCustomerApprovedEvent.class
                ,FileResourceCreatedEvent.class
                ,FileResourceDeletedEvent.class
                ,ResourceRegisteredEvent.class);
    }

    public static void main(String[] args){
        BaseApplication.run(UsersPMApplication.class,args);
    }
}