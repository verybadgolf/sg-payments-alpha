package gov.scot.payments.users.pm.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.customers.model.aggregate.CustomerStatus;
import gov.scot.payments.customers.model.event.CreateCustomerApprovedEvent;
import gov.scot.payments.customers.model.event.CreateProductApprovedEvent;
import gov.scot.payments.customers.model.event.DeleteCustomerApprovedEvent;
import gov.scot.payments.customers.model.event.DeleteProductApprovedEvent;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.user.ResourceRegisteredEvent;
import gov.scot.payments.model.user.ResourceWithVerbs;
import gov.scot.payments.paymentfiles.model.event.FileResourceCreatedEvent;
import gov.scot.payments.paymentfiles.model.event.FileResourceDeletedEvent;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.spring.TestConfiguration;
import gov.scot.payments.users.model.*;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import org.apache.kafka.streams.KafkaStreams;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;

import static java.time.Instant.now;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ApplicationIntegrationTest(classes = {UsersPMApplicationIntegrationTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.PROCESS_MANAGER
        , properties = {"events.destinations=test"})
public class UsersPMApplicationIntegrationTest {

    @BeforeEach
    public void setUp(@Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter,  EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic){
            brokerClient.getBroker().addTopicsIfNotExists(commandTopic);
            kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(20));
     }

    @Test
    public void testFileResourceCreatedEvent(@Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter, EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws MalformedURLException {
        String productName = "product";
        String customerName = "customer";
        var compRef = new CompositeReference( customerName, productName);
        URL url = new URL("https://test/url.com");

        var fileResourceCreatedEvent = new FileResourceDeletedEvent(compRef,  url);
        brokerClient.sendToDestination("events-external", List.of(fileResourceCreatedEvent));

        var createdEvents = brokerClient.readAllFromTopic(commandTopic, DeleteUserFilePermissionsCommand.class);
        assertEquals(1, createdEvents.size());
        assertEquals(compRef, createdEvents.head().getProduct());
        assertEquals(url, createdEvents.head().getPath());
    }

    @Test
    public void testFileResourceDeletedEvent(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws MalformedURLException {

        String productName = "product";
        String customerName = "customer";
        var compRef = new CompositeReference( customerName, productName);
        URL url = new URL("https://test/url.com");

        var fileResourceCreatedEvent = new FileResourceCreatedEvent(compRef,  url);
        brokerClient.sendToDestination("events-external", List.of(fileResourceCreatedEvent));

        var createdEvents = brokerClient.readAllFromTopic(commandTopic, UpdateUserFilePermissionsCommand.class);
        assertEquals(1, createdEvents.size());
        assertEquals(compRef, createdEvents.head().getProduct());
        assertEquals(url, createdEvents.head().getPath());
    }


    @Test
    public void testCreateCustomerApprovedEvent(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws MalformedURLException {

        String customerName = "customername";
        var createCustomerEvent = new CreateCustomerApprovedEvent(testCustomer(customerName, "testuser", CustomerStatus.Live), "testuser", 1L);
        brokerClient.sendToDestination("events-internal", List.of(createCustomerEvent));

        var createdEvents = brokerClient.readAllFromTopic(commandTopic, AddScopeCommand.class);
        assertEquals(1, createdEvents.size());
        assertEquals("customername", createdEvents.head().getScope().getName());
    }

    @Test
    public void testDeleteCustomerApprovedEvent(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws MalformedURLException {

        var customerObj = Customer.builder()
                .name("customername")
                .status(CustomerStatus.Live)
                .createdAt(Instant.now())
                .createdBy("creatorname")
                .lastModifiedBy("modifiername")
                .products(HashSet.empty())
                .build();
        var deleteCustomerApprovedEvent = new DeleteCustomerApprovedEvent(customerObj, "testuser", 1L);
        brokerClient.sendToDestination("events-internal", List.of(deleteCustomerApprovedEvent));

        var createdEvents = brokerClient.readAllFromTopic(commandTopic, RemoveScopeCommand.class);
        assertEquals(1, createdEvents.size());
        assertEquals("customername", createdEvents.head().getScope().getName());
    }

    @Test
    public void testCreateProductApprovedEvent(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws MalformedURLException {

        String customerName = "customername";
        var createProductApprovedEvent = new CreateProductApprovedEvent("productname", testCustomer(customerName, "testuser", CustomerStatus.Live), "testuser", 1L);
        brokerClient.sendToDestination("events-internal", List.of(createProductApprovedEvent));

        var createdEvents = brokerClient.readAllFromTopic(commandTopic, AddScopeCommand.class);
        assertEquals(1, createdEvents.size());
        assertEquals("productname", createdEvents.head().getScope().getName());
        assertEquals("customername", createdEvents.head().getScope().getParent().getName());
    }

    @Test
    public void testDeleteProductApprovedEvent(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws MalformedURLException {

        String customerName = "customername";
        var deleteProductApprovedEvent = new DeleteProductApprovedEvent("productname", testCustomer(customerName, "testuser", CustomerStatus.Live), "testuser", 1L);
        brokerClient.sendToDestination("events-internal", List.of(deleteProductApprovedEvent));

        var createdEvents = brokerClient.readAllFromTopic(commandTopic, RemoveScopeCommand.class);
        assertEquals(1, createdEvents.size());
        assertEquals("productname", createdEvents.head().getScope().getName());
        assertEquals("customername", createdEvents.head().getScope().getParent().getName());
    }


    @Test
    public void testRegisterResourceEvent(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws MalformedURLException {

        var resource = ResourceWithVerbs.builder().resource("resourceName").verbs(HashSet.of("write", "read")).build();

        var resourceRegisteredEvent = new ResourceRegisteredEvent(resource);
        brokerClient.sendToDestination("events-internal", List.of(resourceRegisteredEvent));

        var createdEvents = brokerClient.readAllFromTopic(commandTopic, AddResourceCommand.class);
        assertEquals(1, createdEvents.size());
        assertEquals(resource.getResource(), createdEvents.head().getResource().getResource());
        assertEquals(resource.getVerbs(), createdEvents.head().getResource().getVerbs());
    }

    private Customer testCustomer(String customerName, String lastChangeUser, CustomerStatus customerStatus) {
        return Customer.builder()
                .name(customerName)
                .createdAt(now())
                .createdBy(lastChangeUser)
                .lastModifiedBy(lastChangeUser)
                .status(customerStatus)
                .build();
    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends UsersPMApplication {

    }
}