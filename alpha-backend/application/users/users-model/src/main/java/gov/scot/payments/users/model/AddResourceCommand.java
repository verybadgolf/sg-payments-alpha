package gov.scot.payments.users.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageConstructor;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.user.ResourceWithVerbs;
import gov.scot.payments.model.user.Subject;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "users", type = "addResource")
@NoArgsConstructor
public class AddResourceCommand extends CommandImpl implements HasKey<String> {

    @NonNull private ResourceWithVerbs resource;

    public AddResourceCommand(ResourceWithVerbs resource, boolean reply){
        this.resource = resource;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return resource.getResource();
    }
}
