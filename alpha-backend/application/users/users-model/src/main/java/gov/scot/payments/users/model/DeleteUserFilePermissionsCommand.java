package gov.scot.payments.users.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.customers.model.aggregate.Product;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

import java.net.URL;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "users", type = "deleteUserFileResources")
@NoArgsConstructor
public class DeleteUserFilePermissionsCommand extends CommandImpl implements HasKey<String> {

    @NonNull private CompositeReference product;
    @NonNull private URL path;

    public DeleteUserFilePermissionsCommand(CompositeReference product, URL path){
        super(false);
        this.product = product;
        this.path = path;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return product.toString();
    }
}