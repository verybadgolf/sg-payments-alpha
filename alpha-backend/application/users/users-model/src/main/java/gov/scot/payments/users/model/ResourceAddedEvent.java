package gov.scot.payments.users.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.user.ResourceWithVerbs;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "users", type = "resourceAdded")
@NoArgsConstructor
public class ResourceAddedEvent extends EventWithCauseImpl implements HasKey<String> {

    @NonNull private ResourceWithVerbs resource;

    public ResourceAddedEvent(ResourceWithVerbs resource){
        this.resource = resource;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return resource.getResource();
    }
}
