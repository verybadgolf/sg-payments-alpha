SCRIPT_PATH=$(dirname "$0")

echo Enter a payment method "(Bacs, FassterPayments, SEPA_DC, iMovo etc..)"
read PAYMENT_METHOD

echo Enter a component name
read COMPONENT_NAME

COMPONENT_PATH="$SCRIPT_PATH"/"$COMPONENT_NAME"-app
if [ -d "$COMPONENT_PATH" ]; then
    echo Component already exists, exiting
    exit
fi

../../gradlew :application:pspadapter:psp-adapter-lib:cleanArch :application:pspadapter:psp-adapter-lib::generate -Dgroup=gov.scot.payments.psps.adapter."$COMPONENT_NAME" -Dname="$COMPONENT_NAME" -Dversion=1.0-SNAPSHOT -Dcom.orctom.gradle.archetype.binding.contextName="$COMPONENT_NAME" -Dcom.orctom.gradle.archetype.binding.paymentMethod="$PAYMENT_METHOD"
GENERATED_PATH="$SCRIPT_PATH"/psp-adapter-lib/generated
cp -R "$GENERATED_PATH"/* "$SCRIPT_PATH"
exit