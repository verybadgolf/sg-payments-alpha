package gov.scot.payments.psps.adapter.bacs;

import gov.scot.payments.bacs.standard18.input.*;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.payments.model.aggregate.SortCode;
import io.vavr.collection.List;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class BacsSubmissionFileBuilderTest {

    private final static String TIME_ZONE_ID = "UTC";

    @Test
    public void testBacsSubmissionFileCreatedFromListOfPayments() {

        BacsSubmissionFileBuilder bacsSubmissionFileBuilder = new BacsSubmissionFileBuilder();
        ServiceUserNumber bureauNumber = ServiceUserNumber.fromString("123456");
        SubmissionSerialNumber submissionSerialNumber = SubmissionSerialNumber.fromString("0123456");

        Instant now = Instant.now();
        
        List<MetadataField> metadataFields = List.of(MetadataField.clientRef("fieldValue1"));

        String SUN = "654321";
        String SAN = "0123456789";

        List<Payment> payments = List.of(
                makePayment("GBP 1.00", SUN, SAN, makeBankAccount("111111", "12345678"), now, metadataFields),
                makePayment("GBP 2.00", SUN, SAN, makeBankAccount("111112", "12345678"), now, metadataFields),
                makePayment("GBP 3.00", SUN, SAN, makeBankAccount("111113", "12345678"), now, metadataFields),
                makePayment("GBP 4.00", SUN, SAN, makeBankAccount("111114", "12345678"), now, metadataFields),
                makePayment("GBP 5.00", SUN, SAN, makeBankAccount("111115", "12345678"), now, metadataFields),
                makePayment("GBP 6.00", SUN, SAN, makeBankAccount("111116", "12345678"), now, metadataFields),
                makePayment("GBP 7.00", SUN, SAN, makeBankAccount("111117", "12345678"), now, metadataFields),
                makePayment("GBP 8.00", SUN, SAN, makeBuildingSocietyAccount("111118", "12345678", "D/99009099-9"), now, metadataFields)
        );

        var bacsSubmissionFile = bacsSubmissionFileBuilder.buildBacsPaymentFile(bureauNumber, submissionSerialNumber, payments);

        assertEquals(BacsSubmissionFile.class, bacsSubmissionFile.getClass());
        assertNotNull(bacsSubmissionFile.getPaymentFiles());

        VolumeHeaderLabelOne volumeHeaderLabelOne = bacsSubmissionFile.getVolumeHeaderLabelOne();
        assertEquals(submissionSerialNumber, volumeHeaderLabelOne.getSubmissionSerialNumber());
        assertEquals(bureauNumber, volumeHeaderLabelOne.getServiceUserNumber());

        List<PaymentFile> bacsPaymentFiles = List.ofAll(bacsSubmissionFile.getPaymentFiles());
        assertEquals(1, bacsPaymentFiles.size());

        PaymentFile paymentFile = bacsPaymentFiles.get(0);
        assertNotNull(paymentFile.getDataRecords());
        assertEquals(payments.size(), paymentFile.getDataRecords().size());

        Headers headers = paymentFile.getHeaders();

        HeaderLabelOne headerLabelOne = headers.getHeaderLabelOne();
        assertEquals(String.valueOf(1), headerLabelOne.getFileIdentifier().getNextFileSerialNumber());
        assertEquals(submissionSerialNumber, headerLabelOne.getSetIdentification());
        assertEquals("654321", headerLabelOne.getFileIdentifier().getServiceUserNumber().getValue());
        assertEquals(bureauNumber, headerLabelOne.getFileIdentifier().getServiceUserNumberTwo());
        assertEquals(LocalDate.ofInstant(now, ZoneId.of(TIME_ZONE_ID)), headerLabelOne.getExpirationDate().getValue());

        assertNotNull(headers.getHeaderLabelTwo());

        UserHeaderLabelOne userHeaderLabelOne = headers.getUserHeaderLabelOne();
        assertEquals(LocalDate.ofInstant(now, ZoneId.of(TIME_ZONE_ID)), userHeaderLabelOne.getProcessingDate().getValue());
        assertEquals("000", userHeaderLabelOne.getFileNumber());

        assertEquals(MultiPayDayDataRecord.class, paymentFile.getDataRecords().get(0).getClass());
        MultiPayDayDataRecord ukBankAccountDataRecord = (MultiPayDayDataRecord)paymentFile.getDataRecords().get(0);
        assertEquals("111111", ukBankAccountDataRecord.getDestinationSortCode().getValue());
        assertEquals("12345678", ukBankAccountDataRecord.getDestinationAccountNumber().getValue());
        assertEquals(new BigInteger("100"), ukBankAccountDataRecord.getAmount());
        assertEquals(String.format("%18s", "fieldValue1"), ukBankAccountDataRecord.getReference());
        assertEquals(LocalDate.ofInstant(now, ZoneId.of(TIME_ZONE_ID)), ukBankAccountDataRecord.getProcessingDate().getValue());

        assertEquals(MultiPayDayDataRecord.class, paymentFile.getDataRecords().get(7).getClass());
        MultiPayDayDataRecord ukBuildingSocietyDataRecord = (MultiPayDayDataRecord)paymentFile.getDataRecords().get(7);
        assertEquals("111118", ukBuildingSocietyDataRecord.getDestinationSortCode().getValue());
        assertEquals("12345678", ukBuildingSocietyDataRecord.getDestinationAccountNumber().getValue());
        assertEquals(new BigInteger("800"), ukBuildingSocietyDataRecord.getAmount());
        assertEquals(String.format("%18s", "D/99009099-9"), ukBuildingSocietyDataRecord.getReference());
        assertEquals(LocalDate.ofInstant(now, ZoneId.of(TIME_ZONE_ID)), ukBuildingSocietyDataRecord.getProcessingDate().getValue());

        Trailers trailers = paymentFile.getTrailers();

        EndOfFileLabelOne endOfFileLabelOne = trailers.getEndOfFileLabelOne();
        assertEquals(String.valueOf(1), endOfFileLabelOne.getNextFileSerialNumber());
        assertEquals(submissionSerialNumber, endOfFileLabelOne.getSetIdentification());
        assertEquals(LocalDate.ofInstant(now, ZoneId.of(TIME_ZONE_ID)), endOfFileLabelOne.getExpirationDate().getValue());
        assertEquals("654321", endOfFileLabelOne.getServiceUserNumber().getValue());
        assertEquals(bureauNumber, endOfFileLabelOne.getServiceUserNumberTwo());

        assertNotNull(trailers.getEndOfFileLabelTwo());

        UserTrailerLabelOne userTrailerLabelOne = trailers.getUserTrailerLabelOne();
        assertEquals(payments.size(), userTrailerLabelOne.getCreditItemCount());
        assertEquals(getTotalSubmittedCreditValue(payments), userTrailerLabelOne.getCreditValueTotal());

    }

    @Test
    public void testBacsSubmissionFileWithSeveralPaymentFilesCreatedFromListOfPayments() {

        BacsSubmissionFileBuilder bacsSubmissionFileBuilder = new BacsSubmissionFileBuilder();

        ServiceUserNumber bureauNumber = ServiceUserNumber.fromString("123456");
        SubmissionSerialNumber submissionSerialNumber = SubmissionSerialNumber.fromString("0123456");

        Instant now = Instant.now();
        Instant nowPlus1Day = now.plusSeconds(90000);
        Instant nowPlus2Days = nowPlus1Day.plusSeconds(90000);
        Instant nowPlus3Days = nowPlus2Days.plusSeconds(90000);
        Instant nowPlus4Days = nowPlus3Days.plusSeconds(90000);
        Instant nowPlus5Days = nowPlus4Days.plusSeconds(90000);
        Instant nowPlus6Days = nowPlus5Days.plusSeconds(90000);
        Instant nowPlus7Days = nowPlus6Days.plusSeconds(90000);
        Instant nowPlus8Days = nowPlus7Days.plusSeconds(90000);
        Instant nowPlus9Days = nowPlus8Days.plusSeconds(90000);

        List<MetadataField> metadataFields = List.of(new MetadataField("fieldName1", "fieldValue1"), new MetadataField("fieldName2", "fieldValue2"));

        String SUN1 =  "654321";
        String SUN2 =  "333333";

        String SAN1 = "0123456789";
        String SAN2 = "9876543210";

        List<Payment> payments = List.of(
                makePayment("GBP 1.00", SUN1, SAN1, makeBankAccount("111111", "12345678"), now, metadataFields),
                makePayment("GBP 2.00", SUN1, SAN1, makeBankAccount("111112", "12345678"), now, metadataFields),
                makePayment("GBP 3.00", SUN2, SAN2, makeBankAccount("111113", "12345678"), nowPlus1Day, metadataFields),
                makePayment("GBP 4.00", SUN1, SAN1, makeBankAccount("111114", "12345678"), nowPlus2Days, metadataFields),
                makePayment("GBP 5.00", SUN1, SAN1, makeBankAccount("111115", "12345678"), now, metadataFields),
                makePayment("GBP 6.00", SUN1, SAN1, makeBankAccount("111116", "12345678"), nowPlus1Day, metadataFields),
                makePayment("GBP 7.00", SUN1, SAN1, makeBankAccount("111117", "12345678"), now, metadataFields),
                makePayment("GBP 8.00", SUN1, SAN1, makeBuildingSocietyAccount("111118", "12345678", "D/99009099-9"), now, metadataFields),
                makePayment("GBP 9.00", SUN1, SAN1, makeBankAccount("111119", "12345678"), nowPlus3Days, metadataFields),
                makePayment("GBP 10.00", SUN1, SAN1, makeBankAccount("111110", "12345678"), nowPlus4Days, metadataFields),
                makePayment("GBP 11.00", SUN1, SAN1, makeBankAccount("111111", "12345678"), nowPlus5Days, metadataFields),
                makePayment("GBP 12.00", SUN1, SAN1, makeBankAccount("111112", "12345678"), nowPlus6Days, metadataFields),
                makePayment("GBP 13.00", SUN1, SAN1, makeBankAccount("111113", "12345678"), nowPlus7Days, metadataFields),
                makePayment("GBP 14.00", SUN1, SAN1, makeBankAccount("111114", "12345678"), nowPlus8Days, metadataFields),
                makePayment("GBP 15.00", SUN1, SAN1, makeBankAccount("111115", "12345678"), nowPlus9Days, metadataFields),
                makePayment("GBP 16.00", SUN1, SAN1, makeBankAccount("111116", "12345678"), nowPlus9Days, metadataFields)
        );

        var bacsSubmissionFile = bacsSubmissionFileBuilder.buildBacsPaymentFile(bureauNumber, submissionSerialNumber, payments);

        assertEquals(BacsSubmissionFile.class, bacsSubmissionFile.getClass());
        assertNotNull(bacsSubmissionFile.getPaymentFiles());

        List<PaymentFile> bacsPaymentFiles = List.ofAll(bacsSubmissionFile.getPaymentFiles());
        assertEquals(11, bacsPaymentFiles.size());

        PaymentFile paymentFile1 = bacsPaymentFiles.get(0);
        PaymentFile paymentFile2 = bacsPaymentFiles.get(1);
        PaymentFile paymentFile3 = bacsPaymentFiles.get(2);
        PaymentFile paymentFile4 = bacsPaymentFiles.get(3);
        PaymentFile paymentFile5 = bacsPaymentFiles.get(4);
        PaymentFile paymentFile6 = bacsPaymentFiles.get(5);
        PaymentFile paymentFile7 = bacsPaymentFiles.get(6);
        PaymentFile paymentFile8 = bacsPaymentFiles.get(7);
        PaymentFile paymentFile9 = bacsPaymentFiles.get(8);
        PaymentFile paymentFile10 = bacsPaymentFiles.get(9);
        PaymentFile paymentFile11 = bacsPaymentFiles.get(10);

        assertEquals(5, paymentFile1.getDataRecords().size());
        assertEquals(1, paymentFile2.getDataRecords().size());
        assertEquals(1, paymentFile3.getDataRecords().size());
        assertEquals(1, paymentFile4.getDataRecords().size());
        assertEquals(1, paymentFile5.getDataRecords().size());
        assertEquals(1, paymentFile6.getDataRecords().size());
        assertEquals(1, paymentFile7.getDataRecords().size());
        assertEquals(1, paymentFile8.getDataRecords().size());
        assertEquals(1, paymentFile9.getDataRecords().size());
        assertEquals(1, paymentFile10.getDataRecords().size());
        assertEquals(2, paymentFile11.getDataRecords().size());

        assertEquals(SAN1, paymentFile1.getDataRecords().get(0).getServiceUserName().getValue());
        assertEquals(SAN2, paymentFile2.getDataRecords().get(0).getServiceUserName().getValue());
        assertEquals(SAN1, paymentFile3.getDataRecords().get(0).getServiceUserName().getValue());
        assertEquals(SAN1, paymentFile4.getDataRecords().get(0).getServiceUserName().getValue());
        assertEquals(SAN1, paymentFile5.getDataRecords().get(0).getServiceUserName().getValue());
        assertEquals(SAN1, paymentFile6.getDataRecords().get(0).getServiceUserName().getValue());
        assertEquals(SAN1, paymentFile7.getDataRecords().get(0).getServiceUserName().getValue());
        assertEquals(SAN1, paymentFile8.getDataRecords().get(0).getServiceUserName().getValue());
        assertEquals(SAN1, paymentFile9.getDataRecords().get(0).getServiceUserName().getValue());
        assertEquals(SAN1, paymentFile10.getDataRecords().get(0).getServiceUserName().getValue());
        assertEquals(SAN1, paymentFile11.getDataRecords().get(0).getServiceUserName().getValue());

        assertEquals(SUN1, paymentFile1.getHeaders().getHeaderLabelOne().getFileIdentifier().getServiceUserNumber().getValue());
        assertEquals(SUN2, paymentFile2.getHeaders().getHeaderLabelOne().getFileIdentifier().getServiceUserNumber().getValue());
        assertEquals(SUN1, paymentFile3.getHeaders().getHeaderLabelOne().getFileIdentifier().getServiceUserNumber().getValue());
        assertEquals(SUN1, paymentFile4.getHeaders().getHeaderLabelOne().getFileIdentifier().getServiceUserNumber().getValue());
        assertEquals(SUN1, paymentFile5.getHeaders().getHeaderLabelOne().getFileIdentifier().getServiceUserNumber().getValue());
        assertEquals(SUN1, paymentFile6.getHeaders().getHeaderLabelOne().getFileIdentifier().getServiceUserNumber().getValue());
        assertEquals(SUN1, paymentFile7.getHeaders().getHeaderLabelOne().getFileIdentifier().getServiceUserNumber().getValue());
        assertEquals(SUN1, paymentFile8.getHeaders().getHeaderLabelOne().getFileIdentifier().getServiceUserNumber().getValue());
        assertEquals(SUN1, paymentFile9.getHeaders().getHeaderLabelOne().getFileIdentifier().getServiceUserNumber().getValue());
        assertEquals(SUN1, paymentFile10.getHeaders().getHeaderLabelOne().getFileIdentifier().getServiceUserNumber().getValue());
        assertEquals(SUN1, paymentFile11.getHeaders().getHeaderLabelOne().getFileIdentifier().getServiceUserNumber().getValue());

        assertEquals(String.valueOf(1), paymentFile1.getHeaders().getHeaderLabelOne().getFileIdentifier().getNextFileSerialNumber());
        assertEquals(String.valueOf(2), paymentFile2.getHeaders().getHeaderLabelOne().getFileIdentifier().getNextFileSerialNumber());
        assertEquals(String.valueOf(3), paymentFile3.getHeaders().getHeaderLabelOne().getFileIdentifier().getNextFileSerialNumber());
        assertEquals(String.valueOf(4), paymentFile4.getHeaders().getHeaderLabelOne().getFileIdentifier().getNextFileSerialNumber());
        assertEquals(String.valueOf(5), paymentFile5.getHeaders().getHeaderLabelOne().getFileIdentifier().getNextFileSerialNumber());
        assertEquals(String.valueOf(6), paymentFile6.getHeaders().getHeaderLabelOne().getFileIdentifier().getNextFileSerialNumber());
        assertEquals(String.valueOf(7), paymentFile7.getHeaders().getHeaderLabelOne().getFileIdentifier().getNextFileSerialNumber());
        assertEquals(String.valueOf(8), paymentFile8.getHeaders().getHeaderLabelOne().getFileIdentifier().getNextFileSerialNumber());
        assertEquals(String.valueOf(9), paymentFile9.getHeaders().getHeaderLabelOne().getFileIdentifier().getNextFileSerialNumber());
        assertEquals(String.valueOf(1), paymentFile10.getHeaders().getHeaderLabelOne().getFileIdentifier().getNextFileSerialNumber());
        assertEquals(String.valueOf(2), paymentFile11.getHeaders().getHeaderLabelOne().getFileIdentifier().getNextFileSerialNumber());

        assertEquals("000", paymentFile1.getHeaders().getUserHeaderLabelOne().getFileNumber());
        assertEquals("001", paymentFile2.getHeaders().getUserHeaderLabelOne().getFileNumber());
        assertEquals("002", paymentFile3.getHeaders().getUserHeaderLabelOne().getFileNumber());
        assertEquals("003", paymentFile4.getHeaders().getUserHeaderLabelOne().getFileNumber());
        assertEquals("004", paymentFile5.getHeaders().getUserHeaderLabelOne().getFileNumber());
        assertEquals("005", paymentFile6.getHeaders().getUserHeaderLabelOne().getFileNumber());
        assertEquals("006", paymentFile7.getHeaders().getUserHeaderLabelOne().getFileNumber());
        assertEquals("007", paymentFile8.getHeaders().getUserHeaderLabelOne().getFileNumber());
        assertEquals("008", paymentFile9.getHeaders().getUserHeaderLabelOne().getFileNumber());
        assertEquals("009", paymentFile10.getHeaders().getUserHeaderLabelOne().getFileNumber());
        assertEquals("010", paymentFile11.getHeaders().getUserHeaderLabelOne().getFileNumber());
    }

    private BigInteger getTotalSubmittedCreditValue(List<Payment> paymentList) {
        BigInteger totalCreditValue = new BigInteger("0");
        for (Payment p: paymentList) {
            totalCreditValue = totalCreditValue.add(new BigInteger(p.getAmount().getNumber().toString()));
        }
        return totalCreditValue.multiply(new BigInteger("100"));
    }

    private UKBuildingSocietyAccount makeBuildingSocietyAccount(String sortCode, String accountNumber, String rollNumber) {
        return UKBuildingSocietyAccount.builder()
                .name("act")
                .sortCode(SortCode.fromString(sortCode))
                .accountNumber(UKAccountNumber.fromString(accountNumber))
                .currency("GBP")
                .rollNumber(rollNumber)
                .build();
    }

    private UKBankAccount makeBankAccount(String sortCode, String accountNumber) {
        return UKBankAccount.builder()
                .name("act")
                .sortCode(SortCode.fromString(sortCode))
                .accountNumber(UKAccountNumber.fromString(accountNumber))
                .currency("GBP").build();
    }

    private Payment makePayment(String amount, String serviceUserNumber,
                                String serviceAccountName, CashAccount creditorAccount,
                                Instant latestExecutionDate, List<MetadataField> creditorMetadata) {

        Map<String, Map<String, String>> methodProperties = new HashMap<>();
        Map<String, String> bacsProperties = new HashMap<>();
        bacsProperties.put("BACS_SUN", serviceUserNumber);
        bacsProperties.put("BACS_SAN", serviceAccountName);
        methodProperties.put(PaymentMethod.Bacs.name(), bacsProperties);

        return Payment.builder()
                .createdBy("user")
                .allowedMethods(List.of(PaymentMethod.Bacs))
                .creditor(PartyIdentification.builder().name("").build())
                .creditorAccount(creditorAccount)
                .debtor(PartyIdentification.builder()
                        .name("Debtor Name")
                        .build())
                .debtorAccount(UKBankAccount.builder()
                        .currency("GBP")
                        .sortCode(SortCode.fromString("654321"))
                        .accountNumber(UKAccountNumber.builder()
                                .value("87654321")
                                .build())
                        .name("Account Name")
                        .build())
                .amount(Money.parse(amount))
                .latestExecutionDate(latestExecutionDate)
                .product(new CompositeReference("",""))
                .creditorMetadata(creditorMetadata)
                .methodProperties(methodProperties)
                .build();
    }

}
