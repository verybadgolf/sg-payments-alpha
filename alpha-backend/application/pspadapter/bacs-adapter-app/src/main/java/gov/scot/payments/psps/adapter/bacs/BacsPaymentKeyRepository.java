package gov.scot.payments.psps.adapter.bacs;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Repository
public interface BacsPaymentKeyRepository extends JpaRepository<BacsPaymentKey,UUID> {

    List<BacsPaymentKey> findByProcessingDate(LocalDate date);

    @Modifying
    @Query("delete from BacsPaymentKey b where b.processingDate = ?1")
    void deleteByProcessingDate(LocalDate date);
}
