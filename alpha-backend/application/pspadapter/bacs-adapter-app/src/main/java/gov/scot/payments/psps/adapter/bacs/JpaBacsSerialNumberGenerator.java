package gov.scot.payments.psps.adapter.bacs;

import gov.scot.payments.bacs.standard18.input.SubmissionSerialNumber;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
public class JpaBacsSerialNumberGenerator implements BacsSerialNumberGenerator {

    private final BacsSubmissionSerialNumberRepository repository;

    @Override
    @Transactional
    public SubmissionSerialNumber generate() {
        BacsSubmissionSerialNumber sequence = repository.saveAndFlush(BacsSubmissionSerialNumber.builder().build());
        return SubmissionSerialNumber.fromString(sequence.getId());
    }
}
