package gov.scot.payments.psps.adapter.bacs.mock;

import gov.scot.payments.bacs.model.DailyBacsReports;
import gov.scot.payments.bacs.standard18.input.BacsSubmissionFile;
import gov.scot.payments.psps.adapter.bacs.BacsService;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.Optional;

@RequiredArgsConstructor
public class MockBacsService implements BacsService {

    private final gov.scot.payments.bacs.BacsService delegate;

    @Override
    public void submit(final BacsSubmissionFile bacsSubmissionFile) {
        delegate.processBacsFile(bacsSubmissionFile);
    }

    @Override
    public Optional<DailyBacsReports> getReports(final LocalDate date) {
        return delegate.getReports(date);
    }

    @Override
    public void updateProcessingDay(final LocalDate date) {
        delegate.updateProcessingDay(date);
    }
}
