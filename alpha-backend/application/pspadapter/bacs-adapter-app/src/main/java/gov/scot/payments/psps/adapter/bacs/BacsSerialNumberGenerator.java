package gov.scot.payments.psps.adapter.bacs;

import gov.scot.payments.bacs.standard18.input.SubmissionSerialNumber;

public interface BacsSerialNumberGenerator {
    SubmissionSerialNumber generate();
}
