package gov.scot.payments.psps.adapter.bacs.mock;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface DataRecordEntityRepository extends JpaRepository<DataRecordEntity,String> {

    List<DataRecordEntity> findByProcessingDateAndMultiDay(LocalDate currentDate, boolean multi);
}
