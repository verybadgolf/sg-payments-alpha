package gov.scot.payments.psps.adapter.transferwise.model.transfer;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransferWiseTransfer {
    private Integer targetAccount;
    private Integer quote;
    private UUID customerTransactionId;
    private TransferWiseTransferDetails transferwiseTransferDetails;

    public static TransferWiseTransfer createTransfer(int targetAccount, int quote, UUID customerTransactionId){
        return TransferWiseTransfer.builder()
                .targetAccount(targetAccount)
                .quote(quote)
                .customerTransactionId(customerTransactionId)
                .transferwiseTransferDetails(new TransferWiseTransferDetails())
                .build();
    }
}
