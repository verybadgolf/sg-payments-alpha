package gov.scot.payments.psps.adapter.transferwise.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.math.BigInteger;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
public class TransferWiseResource {
    @NonNull @JsonProperty("id") Integer id;
    @NonNull @JsonProperty("profile_id") Integer profileId;
    @NonNull @JsonProperty("account_id") Integer accountId;
    @NonNull @JsonProperty("type") String type;
}
