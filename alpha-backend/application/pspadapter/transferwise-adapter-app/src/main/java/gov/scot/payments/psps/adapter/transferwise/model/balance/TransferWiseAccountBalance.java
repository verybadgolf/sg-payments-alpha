package gov.scot.payments.psps.adapter.transferwise.model.balance;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class TransferWiseAccountBalance {
    private String balanceType;
    private String currency;
    private TransferWiseCurrencyAmount amount;
    private TransferWiseCurrencyAmount reservedAmount;
}
