package gov.scot.payments.psps.adapter.transferwise.model.quote;

import gov.scot.payments.psps.adapter.transferwise.model.TransferWisePaymentInstruction;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransferWiseQuote {
    private Integer profile;
    private String source;
    private String target;
    private String rateType;
    private BigDecimal sourceAmount;
    private String type;

    public static TransferWiseQuote createQuote(int profileId, TransferWisePaymentInstruction paymentInstruction){
        return TransferWiseQuote.builder()
                .profile(profileId)
                .source("GBP")
                .target(paymentInstruction.getTargetCurrency())
                .rateType("FIXED")
                .sourceAmount( paymentInstruction.getAmount())
                .type(TransferWiseQuoteType.BALANCE_PAYOUT.toString())
                .build();
    }

}
