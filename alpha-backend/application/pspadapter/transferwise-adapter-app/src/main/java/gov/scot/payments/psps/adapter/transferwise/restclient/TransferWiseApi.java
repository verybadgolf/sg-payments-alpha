package gov.scot.payments.psps.adapter.transferwise.restclient;

import gov.scot.payments.psps.adapter.transferwise.model.balance.TransferWiseCheckBalanceResponse;
import gov.scot.payments.psps.adapter.transferwise.model.quote.SubmitQuoteException;
import gov.scot.payments.psps.adapter.transferwise.model.quote.TransferWiseQuote;
import gov.scot.payments.psps.adapter.transferwise.model.quote.TransferWiseQuoteResponse;
import gov.scot.payments.psps.adapter.transferwise.model.recipient.TransferWiseAddRecipientException;
import gov.scot.payments.psps.adapter.transferwise.model.recipient.TransferWiseRecipient;
import gov.scot.payments.psps.adapter.transferwise.model.recipient.TransferWiseRecipientResponse;
import gov.scot.payments.psps.adapter.transferwise.model.transfer.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestOperations;

import java.util.List;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TransferWiseApi {
    private static final String BASE_URL = "https://api.sandbox.transferwise.tech/v1/";
    private static final String PROFILE_ENDPOINT = "profiles";
    private static final String QUOTES_ENDPOINT = "quotes";
    private static final String ACCOUNTS_ENDPOINT = "accounts";
    private static final String TRANSFERS_ENDPOINT = "transfers";
    private static final String PAYMENTS_ENDPOINT = "payments";
    private static final String ACCOUNT_BALANCE_ENDPOINT = "borderless-accounts?profileId=";

    // TODO Use WebClient instead ?
    @Autowired
    private RestOperations restOps;

    @Autowired
    private AuthToken authToken;

    TransferWiseQuoteResponse submitQuote(TransferWiseQuote transferwiseQuote) throws SubmitQuoteException {
        HttpHeaders headers = this.authToken.postHttpHeaders();
        HttpEntity<TransferWiseQuote> entity = new HttpEntity<>(transferwiseQuote, headers);
        try {
            ResponseEntity<TransferWiseQuoteResponse> responseEntity = this.restOps.exchange(
                    BASE_URL + QUOTES_ENDPOINT
                    , HttpMethod.POST
                    , entity
                    , TransferWiseQuoteResponse.class
            );
            return responseEntity.getBody();
        } catch (HttpClientErrorException e) {
            throw new SubmitQuoteException("Could not create quote" + e.getCause());
        }
    }

    TransferWiseRecipientResponse addRecipient(TransferWiseRecipient transferwiseRecipient) throws TransferWiseAddRecipientException {

        try {
            HttpHeaders headers = this.authToken.postHttpHeaders();
            HttpEntity<TransferWiseRecipient> entity = new HttpEntity<>(transferwiseRecipient, headers);
            return this.restOps.exchange(
                    BASE_URL + ACCOUNTS_ENDPOINT
                    , HttpMethod.POST
                    , entity
                    , new ParameterizedTypeReference<TransferWiseRecipientResponse>() {
                    }
            ).getBody();
        } catch (HttpClientErrorException e) {
            throw new TransferWiseAddRecipientException("Error Creating Recipient: " + e.getMessage());
        }
    }

    TransferWiseTransferResponse submitTransfer(TransferWiseTransfer transferwiseTransfer) {
        HttpHeaders headers = this.authToken.postHttpHeaders();
        HttpEntity<TransferWiseTransfer> entity = new HttpEntity<>(transferwiseTransfer, headers);
        ResponseEntity<TransferWiseTransferResponse> responseEntity = this.restOps.exchange(
                BASE_URL + TRANSFERS_ENDPOINT
                , HttpMethod.POST
                , entity
                , TransferWiseTransferResponse.class
        );
        return responseEntity.getBody();
    }

    TransferWiseFundTransferResponse fundTransfer(int transferId) throws TransferWiseFundTransferException {
        HttpHeaders headers = this.authToken.postHttpHeaders();
        HttpEntity<TransferWiseFundTransfer> entity = new HttpEntity<>(new TransferWiseFundTransfer(), headers);
        String transferIdUrl = "/" + transferId + "/";
        try {
            ResponseEntity<TransferWiseFundTransferResponse> responseEntity = this.restOps.exchange(
                    BASE_URL + TRANSFERS_ENDPOINT + transferIdUrl + PAYMENTS_ENDPOINT
                    , HttpMethod.POST
                    , entity
                    , TransferWiseFundTransferResponse.class
            );
            return responseEntity.getBody();
        } catch (HttpClientErrorException e) {
            throw new TransferWiseFundTransferException("Unable to Fund Transfer" + e.getCause());
        }

    }

    TransferWiseTransferStatusResponse checkTransferStatus(int transferId) {
        HttpHeaders headers = this.authToken.getHttpHeaders();
        HttpEntity entity = new HttpEntity<>(this.authToken.getHttpHeaders());
        ResponseEntity<TransferWiseTransferStatusResponse> responseEntity = this.restOps.exchange(
                BASE_URL + TRANSFERS_ENDPOINT + "/" + transferId
                , HttpMethod.GET
                , entity
                , TransferWiseTransferStatusResponse.class
        );
        return responseEntity.getBody();
    }

    TransferWiseCheckBalanceResponse checkAccountBalance(int profileId) {
        HttpEntity entity = new HttpEntity<>(this.authToken.getHttpHeaders());
        ResponseEntity<List<TransferWiseCheckBalanceResponse>> responseEntity = this.restOps.exchange(
                BASE_URL + ACCOUNT_BALANCE_ENDPOINT + profileId
                , HttpMethod.GET
                , entity
                , new ParameterizedTypeReference<List<TransferWiseCheckBalanceResponse>>() {
                }
        );
        return Objects.requireNonNull(responseEntity.getBody()).get(0);
    }

    List<TransferWiseTransferResponse> getTransfersByStatus(int profileId, String... args) {
        String statusQueryString = String.join(",", args);
        HttpEntity entity = new HttpEntity<>(this.authToken.getHttpHeaders());
        ResponseEntity<List<TransferWiseTransferResponse>> responseEntity = this.restOps.exchange(
                BASE_URL + TRANSFERS_ENDPOINT + "/?profile=" + profileId + "&status=" + statusQueryString
                , HttpMethod.GET
                , entity
                , new ParameterizedTypeReference<List<TransferWiseTransferResponse>>() {
                }
        );
        return responseEntity.getBody();
    }
}
