package gov.scot.payments.psps.adapter.transferwise;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.psps.adapter.BasePspAdapterApplication;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.psps.adapter.transferwise.restclient.AuthToken;
import gov.scot.payments.psps.adapter.transferwise.restclient.TransferWiseApi;
import gov.scot.payments.psps.adapter.transferwise.restclient.TransferWiseClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

@EnableAutoConfiguration(exclude = {HypermediaAutoConfiguration.class})
@ApplicationComponent
public class TransferWiseAdapterApplication extends BasePspAdapterApplication{

    @Override
    protected PaymentMethod paymentMethod() {
        return PaymentMethod.Sepa_DC;
    }

    public static void main(String[] args){
        BaseApplication.run(TransferWiseAdapterApplication.class,args);
    }

    @Bean
    public TransferWiseAdapterService transferWiseAdapterService(
            TransferWiseClient transferWiseClient,
            @Value("${transferwise.fixed_transaction_cost}") BigDecimal fixedCostPerTransaction,
            @Value("${transferwise.transaction_cost_percentage}") BigDecimal amountPercentageCost,
            @Value("${component.name}") String psp) {
        return new TransferWiseAdapterService(transferWiseClient, fixedCostPerTransaction, amountPercentageCost, psp);
    }

    @Bean TransferWiseApi transferWiseApi() {
        return new TransferWiseApi();
    }

    @Bean
    public TransferWiseClient transferwiseClient(TransferWiseApi transferWiseApi, @Value("${transferwise.profile_id:0}") int profileId) {
        return new TransferWiseClient(transferWiseApi, profileId);
    };

    @Bean
    public AuthToken authToken() {
        return new AuthToken();
    }

    @Bean
    RestOperations restOperations() {
        return new RestTemplate();
    }
}