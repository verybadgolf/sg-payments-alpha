package gov.scot.payments.psps.adapter.transferwise.model.transfer;

public class TransferWiseFundTransferException extends Exception {
    public TransferWiseFundTransferException(String message){
        super(message);
    }
}
