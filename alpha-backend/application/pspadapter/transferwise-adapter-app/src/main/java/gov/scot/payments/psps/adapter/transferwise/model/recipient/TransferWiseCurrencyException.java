package gov.scot.payments.psps.adapter.transferwise.model.recipient;

public class TransferWiseCurrencyException extends Exception {
    public TransferWiseCurrencyException(String message){
        super(message);
    }
}
