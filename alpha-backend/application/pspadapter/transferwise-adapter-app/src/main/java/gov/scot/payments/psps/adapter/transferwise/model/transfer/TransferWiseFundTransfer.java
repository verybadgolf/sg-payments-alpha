package gov.scot.payments.psps.adapter.transferwise.model.transfer;

import lombok.Getter;

@Getter
public class TransferWiseFundTransfer {
    private String type;
    public TransferWiseFundTransfer(){
        this.type="BALANCE";
    }
}
