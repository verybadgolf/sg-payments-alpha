{{- define "pspadapter-transferwise-adapter-app.env" -}}
- name: CLIENT_SECRET
  valueFrom:
    configMapKeyRef:
      name: cognito-info
      key: client_secret
- name: CLIENT_ID
  valueFrom:
    configMapKeyRef:
      name: cognito-info
      key: client_id
- name: TRANSFERWISE_TOKEN
  value: {{ .Values.${projectValuesName}.transferWiseToken | quote }}
- name: TRANSFERWISE_PROFILE_ID
  value: {{ .Values.${projectValuesName}.transferWiseProfileId | quote }}
- name: TRANSFERWISE_FIXED_COST
  value: "0.80"
- name: TRANSFERWISE_COST_PERCENTAGE
  value: "0.35"
- name: SUPPORTED_PAYMENT_METHODS
  value: sepa_dc
{{- end -}}