package gov.scot.payments.paymentfiles.ch.app;

import com.amazonaws.services.s3.event.S3EventNotification;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.messaging.MessageChannel;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;


public class S3EventListenerTest {
    private static String VERSION_ID = "1";
    private S3EventListener s3EventListener;
    private String desiredEventType;
    private String bucketUriStart;
    private String branchName;
    private String fileName;
    private String productId;
    private String customerId;
    private String bucketName;
    private MessageChannel mockMessageChannel;

    @BeforeEach
    public void setUp() {
        bucketUriStart = "s3://";
        mockMessageChannel = mock(MessageChannel.class);
        s3EventListener = new S3EventListener(new ObjectMapper(), mockMessageChannel);
        desiredEventType = "ObjectCreated:CompleteMultipartUpload";
        branchName = "dummy-branch-name";
        fileName = "dummyFileName";
        productId = "dummyProductId";
        customerId = "dummyCustomerId";
        bucketName = "bucketName";
    }

    @Test
    public void convertS3EventGeneratesFileUploadedEvent() throws JsonProcessingException, MalformedURLException, URISyntaxException {
        String folderAndFile = branchName + "/" + customerId + "/" + productId + "/" + fileName;
        var s3Notification = generateNotification(desiredEventType, bucketName, folderAndFile);
        var fileUploadedEvent = s3EventListener.convertS3Event(s3Notification);
        String expectedUriString = String.format(
                "%s%s%s%s%s%s%s%s%s%s%s%s",
                bucketUriStart,
                bucketName,
                "/",
                branchName,
                "/",
                customerId,
                "/",
                productId,
                "/",
                fileName,
                "#",
                VERSION_ID
        );
        URI expectedUri = new URI(expectedUriString);
       assertEquals(expectedUri, fileUploadedEvent.getPath());
       assertEquals(productId, fileUploadedEvent.getProduct().getComponent1());
       assertEquals(customerId, fileUploadedEvent.getProduct().getComponent0());
    }

    @Test
    public void eventWithOnly3PathElementsIsIgnored() throws JsonProcessingException, MalformedURLException, URISyntaxException {
        String folderAndNoFile = branchName + "/" + customerId + "/" + productId;
        var s3Notification = generateNotification(desiredEventType, bucketName, folderAndNoFile);
        var fileUploadedEvent = s3EventListener.convertS3Event(s3Notification);
        assertNull(fileUploadedEvent);
    }



    public static S3EventNotification generateNotification(String type, String s3Bucket, String folderAndFile) {
        Path folderAndFilePath = Path.of(folderAndFile);
        String file = URLEncoder.encode(folderAndFilePath.getName(folderAndFilePath.getNameCount() - 1).toString(), StandardCharsets.UTF_8);
        String folder = folderAndFilePath.subpath(0, folderAndFilePath.getNameCount() - 1).toString().replace(File.separatorChar, '/');
        String key = folder + "/" + file;
        S3EventNotification.UserIdentityEntity user = new S3EventNotification.UserIdentityEntity("application");
        S3EventNotification.S3BucketEntity bucket = new S3EventNotification.S3BucketEntity(s3Bucket
                , null
                , "");
        S3EventNotification.S3ObjectEntity object = new S3EventNotification.S3ObjectEntity(key
                , 0L
                , ""
                , VERSION_ID
                , "");
        S3EventNotification.S3Entity s3Entity = new S3EventNotification.S3Entity(""
                , bucket
                , object
                , "");
        S3EventNotification.S3EventNotificationRecord record = new S3EventNotification.S3EventNotificationRecord("eu-west-2"
                , type
                , ""
                , Instant.now().toString()
                , ""
                , null
                , null
                , s3Entity
                , user);
        return new S3EventNotification(Collections.singletonList(record));
    }
}
