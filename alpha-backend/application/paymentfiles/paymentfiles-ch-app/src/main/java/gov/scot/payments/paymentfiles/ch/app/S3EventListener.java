package gov.scot.payments.paymentfiles.ch.app;

import com.amazonaws.services.s3.event.S3EventNotification;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.paymentfiles.model.event.FileUploadedEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.cloud.aws.messaging.listener.SqsMessageDeletionPolicy;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;

import java.io.File;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;

@Slf4j
public class S3EventListener {
    private final ObjectMapper objectMapper;
    private final MessageChannel channel;

    public S3EventListener(ObjectMapper objectMapper, MessageChannel channel) {
        this.objectMapper = objectMapper;
        this.channel = channel;
    }

    @SqsListener(value = {"${file.event.queue}"},deletionPolicy = SqsMessageDeletionPolicy.ON_SUCCESS)
    public void listen(String sqsNotificationStr) {
        var s3Event = S3EventNotification.parseJson(sqsNotificationStr);
        onS3Event(s3Event);
    }

    public void onS3Event(S3EventNotification s3Event) {
        if (s3Event == null) {
            log.error("Couldn't find a valid message ");
            return;
        }
        if (s3Event.getRecords() == null || s3Event.getRecords().isEmpty()) {
            log.error("Skipping empty S3 event: ");
            return;
        }
        try {
            FileUploadedEvent event = convertS3Event(s3Event);
            if (event != null) {
                Message<FileUploadedEvent> message = MessageBuilder.withPayload(event)
                        .setHeader(gov.scot.payments.model.Message.CONTEXT_HEADER, "paymentfiles")
                        .setHeader(gov.scot.payments.model.Message.TYPE_HEADER, "fileUploaded")
                        .setHeader(HasKey.PARTITION_KEY_HEADER, event.getKey())
                        .setHeader(KafkaHeaders.MESSAGE_KEY, event.getKey().getBytes())
                        .build();
                channel.send(message);
            }
        } catch (Exception e) {
            log.error("Could not parse s3 event: ", e);
        }
    }

    FileUploadedEvent convertS3Event(S3EventNotification s3Event) throws JsonProcessingException {
        log.info("Handling S3 event: {}", objectMapper.writeValueAsString(s3Event));
        for (S3EventNotification.S3EventNotificationRecord record : s3Event.getRecords()) {
            Pair<String, String> customerAndProduct = parseFolder(record);
            if (customerAndProduct == null) {
                log.warn("Skipping event as file does not have correct number of path elements: {}", record.getS3().getObject().getKey());
                continue;
            }
            switch (record.getEventName()) {
                case "ObjectCreated:Post":
                case "ObjectCreated:Put":
                case "ObjectCreated:CompleteMultipartUpload":
                    var product = new CompositeReference(customerAndProduct.getLeft(), customerAndProduct.getRight());
                    var createdAt = Instant.ofEpochMilli(record.getEventTime().toInstant().getMillis());
                    String user = record.getUserIdentity().getPrincipalId();
                    FileUploadedEvent event = new FileUploadedEvent(product, buildS3Url(record), user, createdAt);
                    log.info("Handled Post/Put event from s3 and generated file-upload-event: {}", event);
                    return event;
                case "ObjectRemoved:Delete":
                case "ObjectRemoved:DeleteMarkerCreated":
                    break;
                default:
                    log.info("Ignoring S3 event.record: {}", objectMapper.writeValueAsString(record));
            }
        }
        return null;
    }

    private URI buildS3Url(S3EventNotification.S3EventNotificationRecord record) {
        String path = Path.of(record.getS3().getBucket().getName(), record.getS3().getObject().getKey()).toString().replace(File.separatorChar, '/');
        String versionId = record.getS3().getObject().getVersionId();
        return versionId == null ? URI.create(String.format("s3://%s", path)) : URI.create(String.format("s3://%s#%s", path, versionId));
    }

    private Pair<String, String> parseFolder(S3EventNotification.S3EventNotificationRecord record) {
        String key = record.getS3().getObject().getUrlDecodedKey();
        Path path = Paths.get(key);
        int nameCount = path.getNameCount();
        //must have 4 path elements to be a file upload: environment, customer, product, file name.
        if (nameCount < 4) {
            return null;
        }
        String productFolder = path.subpath(nameCount - 2, nameCount - 1).toString();
        String customerFolder = path.subpath(nameCount - 3, nameCount - 2).toString();
        return Pair.of(customerFolder, productFolder);
    }
}
