package gov.scot.payments.paymentfiles.ch.app;

import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;


public class UnableToDeleteException extends RuntimeException {

    @Getter
    private final List<String> folderPaths;

    public UnableToDeleteException(Throwable cause, List<String> folderPaths) {
        super(cause);
        this.folderPaths = folderPaths;
    }

    public UnableToDeleteException(String message, List<String> folderPaths) {
        super(message);
        this.folderPaths = folderPaths;
    }

    @Override
    public String toString() {
        return folderPaths.stream().collect(Collectors.joining(",", getMessage(), ""));
    }
}

