package gov.scot.payments.paymentfiles.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

import java.net.URL;

@Getter
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "paymentfiles", type = "fileResourceCreated")
@NoArgsConstructor
public class FileResourceCreatedEvent extends EventWithCauseImpl implements HasKey<String> {

    @NonNull private CompositeReference product;
    @NonNull private URL path;

    @Override
    @JsonIgnore
    public String getKey() {
        return path.toString();
    }
}
