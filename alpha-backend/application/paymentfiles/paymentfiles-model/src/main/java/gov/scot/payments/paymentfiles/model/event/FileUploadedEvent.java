package gov.scot.payments.paymentfiles.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.EventImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.net.URI;
import java.time.Instant;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "paymentfiles", type = "fileUploaded")
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FileUploadedEvent extends EventImpl implements HasKey<String> {

    @NonNull private CompositeReference product;
    @NonNull private URI path;
    @NonNull private String user;
    @NonNull private Instant createdAt;

    @Override
    @JsonIgnore
    public String getKey() {
        return path.toString();
    }

}
