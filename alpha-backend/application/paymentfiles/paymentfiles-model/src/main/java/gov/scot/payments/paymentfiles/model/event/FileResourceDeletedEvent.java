package gov.scot.payments.paymentfiles.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.customers.model.aggregate.Product;
import gov.scot.payments.model.*;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;
import org.apache.commons.lang3.tuple.Pair;

import java.net.URL;

@Getter
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "paymentfiles", type = "fileResourceDeleted")
@NoArgsConstructor
public class FileResourceDeletedEvent extends EventWithCauseImpl implements HasKey<String> {

    @NonNull private CompositeReference product;
    @NonNull private URL path;


    @Override
    @JsonIgnore
    public String getKey() {
        return path.toString();
    }
}
