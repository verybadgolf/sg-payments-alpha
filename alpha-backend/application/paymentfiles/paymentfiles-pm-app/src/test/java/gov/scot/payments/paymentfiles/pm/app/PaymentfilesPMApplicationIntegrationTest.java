package gov.scot.payments.paymentfiles.pm.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.customers.model.aggregate.CustomerStatus;
import gov.scot.payments.customers.model.aggregate.OutboundConfiguration;
import gov.scot.payments.customers.model.aggregate.Product;
import gov.scot.payments.customers.model.aggregate.ProductAdapter;
import gov.scot.payments.customers.model.aggregate.ProductAdapterType;
import gov.scot.payments.customers.model.aggregate.ProductConfiguration;
import gov.scot.payments.customers.model.event.CreateCustomerApprovedEvent;
import gov.scot.payments.customers.model.event.CreateProductApprovedEvent;
import gov.scot.payments.customers.model.event.DeleteCustomerApprovedEvent;
import gov.scot.payments.customers.model.event.DeleteProductApprovedEvent;
import gov.scot.payments.paymentfiles.model.command.CreateFileResourceCommand;
import gov.scot.payments.paymentfiles.model.command.DeleteFileResourceCommand;
import gov.scot.payments.payments.model.aggregate.PartyIdentification;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.payments.model.aggregate.SortCode;
import gov.scot.payments.payments.model.aggregate.UKAccountNumber;
import gov.scot.payments.payments.model.aggregate.UKBankAccount;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.spring.TestConfiguration;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import org.apache.kafka.streams.KafkaStreams;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;

import javax.money.Monetary;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;

import static java.time.Instant.now;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ApplicationIntegrationTest(classes = {PaymentfilesPMApplicationIntegrationTest.TestApplication.class}
        , componentType = ApplicationIntegrationTest.ComponentType.PROCESS_MANAGER
        , properties = {"cloud.aws.credentials.accessKey=test"
        , "cloud.aws.credentials.secretKey=test"
        , "cloud.aws.region.static=eu-west-2"
})
public class PaymentfilesPMApplicationIntegrationTest {

    @BeforeEach
    public void setUp(EmbeddedBrokerClient brokerClient, @Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter, @Value("${commands.destination}") String commandTopic) {
        brokerClient.getBroker().addTopicsIfNotExists(commandTopic);
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(10));
    }

    @Test
    public void testCreateCustomerApprovedEventIssuesCommand(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws InterruptedException {
        String customerName = "customerName";
        String user = "user";
        var createCustomerApprovedEvent = new CreateCustomerApprovedEvent(testCustomer(customerName, user, CustomerStatus.Live), user, 1L);
        brokerClient.sendToDestination("events-external", List.of(createCustomerApprovedEvent));

        var createdEvents = brokerClient.readAllFromTopic(commandTopic, CreateFileResourceCommand.class);
        assertEquals(1, createdEvents.size());
        assertEquals(customerName, createdEvents.head().getProduct().getComponent0());
        assertNull(createdEvents.head().getProduct().getComponent1());
    }

    @Test
    public void testDeleteCustomerApprovedEventIssuesCommand(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws InterruptedException {
        String customerName = "customerName";
        String user= "user";
        var customer = Customer.builder()
                .status(CustomerStatus.Live)
                .createdAt(Instant.now())
                .createdBy("creatorName")
                .lastModifiedBy("creatorName")
                .products(HashSet.empty())
                .name(customerName)
                .build();

        var deleteCustomerApprovedEvent = new DeleteCustomerApprovedEvent(testCustomer(customerName, user, CustomerStatus.Live), user, 1L);
        brokerClient.sendToDestination("events-external", List.of(deleteCustomerApprovedEvent));

        var createdEvents = brokerClient.readAllFromTopic(commandTopic, DeleteFileResourceCommand.class);
        assertEquals(1, createdEvents.size());
        assertEquals(customer.getKey(), createdEvents.head().getProduct().getComponent0());
        assertNull(createdEvents.head().getProduct().getComponent1());
    }

    @Test
    public void testCreateProductApprovedEventIssuesCommand(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws MalformedURLException, InterruptedException {
        String customerName = "customerName";
        String productName = "productName";
        String user = "user";

        var productAdapter = ProductAdapter.builder()
                .location("https://test.com")
                .type(ProductAdapterType.internal)
                .build();

        var createProductApprovedEvent = new CreateProductApprovedEvent(productName, testCustomer(customerName, user, CustomerStatus.Live), user, 1L);
        brokerClient.sendToDestination("events-external", List.of(createProductApprovedEvent));

        var createdEvents = brokerClient.readAllFromTopic(commandTopic, CreateFileResourceCommand.class);
        assertEquals(1, createdEvents.size());
        assertEquals(customerName, createdEvents.head().getProduct().getComponent0());
        assertEquals(productName, createdEvents.head().getProduct().getComponent1());
    }

    @Test
    public void testDeleteProductApprovedEventIssuesCommand(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws MalformedURLException, InterruptedException {
        String customerName = "customerName";
        String user = "user";
        String productName = "productName";

        var productAdapter = ProductAdapter.builder()
                .location("https://test.com")
                .type(ProductAdapterType.internal)
                .build();

        var deleteProductApprovedEvent = new DeleteProductApprovedEvent(productName, testCustomer(customerName, user, CustomerStatus.Live), user, 1L);
        brokerClient.sendToDestination("events-external", List.of(deleteProductApprovedEvent));

        var createdEvents = brokerClient.readAllFromTopic(commandTopic, DeleteFileResourceCommand.class);
        assertEquals(1, createdEvents.size());
        assertEquals(customerName, createdEvents.head().getProduct().getComponent0());
        assertEquals(productName, createdEvents.head().getProduct().getComponent1());
    }

    private Customer testCustomer(String customerName, String lastChangeUser, CustomerStatus customerStatus) {
        var account1 = UKBankAccount.builder()
                                    .name("acct")
                                    .accountNumber(UKAccountNumber.fromString("12345678"))
                                    .currencyUnit(Monetary.getCurrency("GBP"))
                                    .sortCode(new SortCode("123456"))
                                    .build();

        return Customer.builder()
                .name(customerName)
                .createdAt(now())
                .createdBy(lastChangeUser)
                .lastModifiedBy(lastChangeUser)
                .status(customerStatus)
                       .products(HashSet.of(Product.builder()
                                                   .name("productName")
                                                   .productConfiguration(ProductConfiguration.builder()
                                                                                             .outboundConfiguration(OutboundConfiguration.builder()
                                                                                                                                         .fileImports(true)
                                                                                                                                         .defaultPaymentMethods(List.of(PaymentMethod.Bacs))
                                                                                                                                         .payer(PartyIdentification.builder().name("name").build())
                                                                                                                                         .returns(account1)
                                                                                                                                         .source(account1)
                                                                                                                                         .build())
                                                                                             .build())
                                                   .build()))
                .build();
    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends PaymentfilesPMApplication {

    }
}