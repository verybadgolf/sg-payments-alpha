package gov.scot.payments.psps.method.bacs.ch.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.component.commandhandler.stateful.PerCommandStatefulCommandHandlerDelegate;
import gov.scot.payments.application.component.commandhandler.stateful.StatefulErrorHandler;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.psps.method.ch.app.*;
import gov.scot.payments.psps.method.ch.app.batcher.BatchingPaymentBatchEventGenerator;
import gov.scot.payments.psps.method.ch.app.batcher.PaymentBatchEventGenerator;
import gov.scot.payments.psps.method.ch.app.batcher.PaymentBatchEventGeneratorDelegate;
import gov.scot.payments.psps.method.ch.app.batcher.kafka.BatchSubmittingTransformer;
import gov.scot.payments.psps.method.ch.app.batcher.kafka.BatchSubmittingTransformerSupplier;
import gov.scot.payments.psps.method.ch.app.batcher.kafka.PaymentBatchers;
import gov.scot.payments.psps.method.ch.app.batcher.kafka.TransformerWindowingPaymentBatcher;
import gov.scot.payments.psps.method.model.PspAdapterPayment;
import gov.scot.payments.psps.method.model.command.MethodSubmitPaymentBatchCommand;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.kstream.ValueTransformerSupplier;
import org.apache.kafka.streams.state.KeyValueBytesStoreSupplier;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.time.Duration;
import java.util.function.Function;

@ApplicationComponent
public class BacsMethodCHApplication extends BasePspCommandHandler {

    @Override
    protected PaymentMethod paymentMethod() {
        return PaymentMethod.Bacs;
    }

    @Bean
    public PspClientFactory bacsClientFactory() {
        return BacsMethodClient::new;
    }

    @Bean
    @Primary
    public BatchSubmittingTransformerSupplier bacsBatchingTransformer(PaymentSubmitter submitter
            , PerCommandStatefulCommandHandlerDelegate<PspAdapterPayment, PspAdapterPaymentState,EventWithCauseImpl> delegate
            , Function<String, KeyValueBytesStoreSupplier> keyValueBytesStoreSupplier){
        return new BatchSubmittingTransformerSupplier() {
            @Override
            public String[] getStateStores() {
                return new String[]{delegate.getStoreName(),BacsBatchTransformer.STATE_STORE_NAME};
            }

            @Override
            public ValueTransformer<MethodSubmitPaymentBatchCommand, Iterable<EventWithCauseImpl>> get() {
                return new BacsBatchTransformer(submitter,delegate.getStoreName(),valueSerde(Command.class),paymentMethod());
            }

            @Override
            public void registerStateStores(StreamsBuilder streamsBuilder) {
                StoreBuilder builder = Stores.keyValueStoreBuilder(keyValueBytesStoreSupplier.apply(BacsBatchTransformer.STATE_STORE_NAME), Serdes.String(), Serdes.UUID());
                streamsBuilder.addStateStore(builder);
            }

        };
    }

    @Bean
    public PaymentBatchEventGenerator paymentBatchEventGenerator(Metrics metrics
            , StatefulErrorHandler<PspAdapterPayment, EventWithCauseImpl> errorHandler
            , TransformerWindowingPaymentBatcher batcher){

        return BatchingPaymentBatchEventGenerator.builder()
                .eventGenerator(new PaymentBatchEventGeneratorDelegate(paymentMethod()))
                .paymentMethod(paymentMethod())
                .metrics(metrics)
                .errorHandler(errorHandler)
                .commandSerde(valueSerde(Command.class))
                .batcher(batcher)
                .build();
    }

    @Bean
    public TransformerWindowingPaymentBatcher batcher(Function<String, KeyValueBytesStoreSupplier> keyValueBytesStoreSupplier,
                                                      SchemaRegistryClient confluentSchemaRegistryClient,
                                                      @Value("${application.batch-size}") int batchSize,
                                                      @Value("${application.batch-max-payment-age}") int maxPaymentAge,
                                                      @Value("${application.batch-resolution}") int resolution) {

        String stateStore = "batches";
        return PaymentBatchers.countWithTimeout(
                stateStore,
                keyValueBytesStoreSupplier.apply(stateStore),
                stateSerde(confluentSchemaRegistryClient, PspAdapterPaymentBatch.class),
                batchSize,
                Duration.ofMillis(maxPaymentAge),
                Duration.ofMillis(resolution),
                false
        ).groupingFunction(PaymentBatchers.noGrouping(
                stateSerde(confluentSchemaRegistryClient, Payment.class)
        ))
        .build();
    }

    public static void main(String[] args){
        BaseApplication.run(BacsMethodCHApplication.class,args);
    }

}