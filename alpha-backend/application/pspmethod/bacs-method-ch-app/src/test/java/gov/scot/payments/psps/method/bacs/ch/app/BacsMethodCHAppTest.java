package gov.scot.payments.psps.method.bacs.ch.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.Message;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.psps.method.ch.app.PspClientFactory;
import gov.scot.payments.psps.method.model.PspSubmissionRequest;
import gov.scot.payments.psps.method.model.PspSubmissionResponse;
import gov.scot.payments.psps.method.model.PspTransactionCostEstimateResponse;
import gov.scot.payments.psps.method.model.command.MethodAddPaymentToBatchCommand;
import gov.scot.payments.psps.method.model.command.MethodSubmitPaymentBatchCommand;
import gov.scot.payments.psps.method.model.event.MethodPaymentAcceptedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentBatchClosedEvent;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.vavr.collection.List;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.javamoney.moneta.Money;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.apache.kafka.streams.KafkaStreams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.reactive.server.WebTestClient;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.spring.TestConfiguration;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ApplicationIntegrationTest(classes = {BacsMethodCHAppTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.COMMAND_HANDLER
        ,properties = {"spring.cloud.discovery.client.simple.instances.adapter1[0].uri=http://localhost:8080"
        ,"spring.cloud.discovery.client.simple.instances.adapter1[0].metadata.scot.gov.payments/payment-method=bacs"
        ,"spring.cloud.discovery.client.simple.instances.adapter1[0].metadata.scot.gov.payments/service-type=psp"
        ,"spring.cloud.discovery.client.simple.instances.adapter1[0].metadata.app.kubernetes.io/name=direct"
        , "state.query.authority=bacs:Read"
        , "application.batch_size=5"
        , "application.batch_max_payment_age=2000"
        , "application.batch_resolution=1000"})
public class BacsMethodCHAppTest {

    @MockBean
    PspClientFactory clientFactory;

    @BeforeEach
    public void setUp(@Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter, EmbeddedBrokerClient brokerClient, @Value("${namespace}") String namespace){
        brokerClient.getBroker().addTopicsIfNotExists(namespace+"-bacsmethod-command-errors");
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(10));
    }

    @Test
    public void testSubmitPayments(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient) throws InterruptedException {

        BacsMethodClient bacsClient = mock(BacsMethodClient.class);
        when(clientFactory.apply(any())).thenReturn(bacsClient);

        when(bacsClient.getName()).thenReturn("bacs");
        when(bacsClient.estimateTransactionCosts(any(PspSubmissionRequest.class))).thenReturn(PspTransactionCostEstimateResponse.builder()
                .psp("bacs")
                .amount(Money.parse("GBP 1.00"))
                .build());
        when(bacsClient.submitPayments(any(PspSubmissionRequest.class))).thenReturn(PspSubmissionResponse.builder()
                .success(true)
                .psp("bacs")
                .build());

        Payment payment1 = makePayment();
        Payment payment2 = makePayment();
        Payment payment3 = makePayment();
        Payment payment4 = makePayment();
        Payment payment5 = makePayment();

        final KeyValueWithHeaders<String, MethodAddPaymentToBatchCommand> addPaymentToBatchCommand1 = makeAddPaymentToBatchCommand(payment1, payment1.getKey());
        final KeyValueWithHeaders<String, MethodAddPaymentToBatchCommand> addPaymentToBatchCommand2 = makeAddPaymentToBatchCommand(payment2, payment2.getKey());
        final KeyValueWithHeaders<String, MethodAddPaymentToBatchCommand> addPaymentToBatchCommand3 = makeAddPaymentToBatchCommand(payment3, payment3.getKey());
        final KeyValueWithHeaders<String, MethodAddPaymentToBatchCommand> addPaymentToBatchCommand4 = makeAddPaymentToBatchCommand(payment4, payment4.getKey());
        final KeyValueWithHeaders<String, MethodAddPaymentToBatchCommand> addPaymentToBatchCommand5 = makeAddPaymentToBatchCommand(payment5, payment5.getKey());

        brokerClient.sendKeyValuesToDestination("commands", List.of(
                addPaymentToBatchCommand1,
                addPaymentToBatchCommand2,
                addPaymentToBatchCommand3,
                addPaymentToBatchCommand4,
                addPaymentToBatchCommand5));

        var createdEvents = brokerClient.readAllFromDestination("events", MethodPaymentBatchClosedEvent.class);

        assertEquals(1, createdEvents.size());
        assertEquals(5, createdEvents.get(0).getPayments().size());

        assertEquals(payment1.getId(), createdEvents.get(0).getPayments().get(0));
        assertEquals(payment2.getId(), createdEvents.get(0).getPayments().get(1));
        assertEquals(payment3.getId(), createdEvents.get(0).getPayments().get(2));
        assertEquals(payment4.getId(), createdEvents.get(0).getPayments().get(3));
        assertEquals(payment5.getId(), createdEvents.get(0).getPayments().get(4));

        List<UUID> batch = createdEvents.get(0).getPayments();

        KeyValueWithHeaders<String, MethodSubmitPaymentBatchCommand> submitPaymentBatchCommand = makeSubmitPaymentBatchCommand(batch, createdEvents.get(0).getKey());

        brokerClient.sendKeyValuesToDestination("commands", List.of(submitPaymentBatchCommand));

        List<Event> events = brokerClient.readAllFromDestination("events", Event.class);

        verify(bacsClient, times(1)).estimateTransactionCosts(any());
        verify(bacsClient, times(1)).submitPayments(any());

        assertEquals(MethodPaymentAcceptedEvent.class, events.get(events.size()-1).getClass());
        assertEquals(MethodPaymentAcceptedEvent.class, events.get(events.size()-2).getClass());
        assertEquals(MethodPaymentAcceptedEvent.class, events.get(events.size()-3).getClass());
        assertEquals(MethodPaymentAcceptedEvent.class, events.get(events.size()-4).getClass());
        assertEquals(MethodPaymentAcceptedEvent.class, events.get(events.size()-5).getClass());

        MethodPaymentAcceptedEvent paymentAcceptedEvent1 = (MethodPaymentAcceptedEvent)events.get(events.size()-1);
        MethodPaymentAcceptedEvent paymentAcceptedEvent2 = (MethodPaymentAcceptedEvent)events.get(events.size()-2);
        MethodPaymentAcceptedEvent paymentAcceptedEvent3 = (MethodPaymentAcceptedEvent)events.get(events.size()-3);
        MethodPaymentAcceptedEvent paymentAcceptedEvent4 = (MethodPaymentAcceptedEvent)events.get(events.size()-4);
        MethodPaymentAcceptedEvent paymentAcceptedEvent5 = (MethodPaymentAcceptedEvent)events.get(events.size()-5);

        assertEquals(payment1.getId(), paymentAcceptedEvent5.getPaymentId());
        assertEquals(payment2.getId(), paymentAcceptedEvent4.getPaymentId());
        assertEquals(payment3.getId(), paymentAcceptedEvent3.getPaymentId());
        assertEquals(payment4.getId(), paymentAcceptedEvent2.getPaymentId());
        assertEquals(payment5.getId(), paymentAcceptedEvent1.getPaymentId());

    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends BacsMethodCHApplication {

    }

    private KeyValueWithHeaders<String, MethodSubmitPaymentBatchCommand> makeSubmitPaymentBatchCommand(List<UUID> payments, String key) {
        return KeyValueWithHeaders.msg(
                key,
                MethodSubmitPaymentBatchCommand.builder()
                        .payments(payments)
                        .key(key)
                        .reply(false)
                        .build(),
                new RecordHeader(
                        Message.CONTEXT_HEADER,
                        "bacsmethod".getBytes()));
    }

    private KeyValueWithHeaders<String, MethodAddPaymentToBatchCommand> makeAddPaymentToBatchCommand(Payment payment, String key) {
        return KeyValueWithHeaders.msg(
                key,
                MethodAddPaymentToBatchCommand.builder()
                        .payment(payment)
                        .reply(false)
                        .build(),
                new RecordHeader(
                        Message.CONTEXT_HEADER,
                        "bacsmethod".getBytes()));
    }

    private Payment makePayment() {
        return Payment.builder()
                .createdBy("user")
                .allowedMethods(List.of(PaymentMethod.Bacs))
                .creditor(PartyIdentification.builder().name("Test Creditor").build())
                .creditorAccount(UKBankAccount.builder()
                        .currency("GBP")
                        .accountNumber(UKAccountNumber.builder()
                                .value("12345678")
                                .build())
                        .sortCode(SortCode.fromString("123456"))
                        .name("Account Name")
                        .build())
                .debtor(PartyIdentification.builder()
                        .name("Debtor Name")
                        .build())
                .debtorAccount(UKBankAccount.builder()
                        .currency("GBP")
                        .sortCode(SortCode.fromString("654321"))
                        .accountNumber(UKAccountNumber.builder()
                                .value("87654321")
                                .build())
                        .name("Account Name")
                        .build())
                .amount(Money.of(new BigDecimal("100"), "GBP"))
                .latestExecutionDate(Instant.now())
                .product(new CompositeReference("customer","product"))
                .build();
    }
}