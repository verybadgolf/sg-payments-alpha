package gov.scot.payments.psps.method.bacs.ch.app;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.psps.method.model.PspSubmissionRequest;
import gov.scot.payments.psps.method.model.PspSubmissionResponse;
import gov.scot.payments.psps.method.model.PspTransactionCostEstimateResponse;
import io.vavr.collection.List;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BacsMethodClientTest {

    BacsMethodClient client;

    @BeforeEach
    private void setUp() {
        client = mock(BacsMethodClient.class);
    }

    @Test
    void testEstimateTransactionCost() {

        Money amount = Money.parse("GBP 12.00");
        String psp = "testpsp";

        when(client.estimateTransactionCosts(any(PspSubmissionRequest.class))).thenReturn(PspTransactionCostEstimateResponse.builder()
                .psp(psp)
                .amount(amount)
                .build());

        List<Payment> payments = List.of(makePayment());

        PspSubmissionRequest request = PspSubmissionRequest.builder()
                .payments(payments)
                .build();

        var response = client.estimateTransactionCosts(request);
        assertEquals(PspTransactionCostEstimateResponse.class, response.getClass());
        assertEquals(amount, response.getAmount());
        assertEquals(psp, response.getPsp());
    }

    @Test
    void testSubmitPayments() {

        when(client.submitPayments(any(PspSubmissionRequest.class))).thenReturn(PspSubmissionResponse.builder().build());

        List<Payment> payments = List.of(makePayment(), makePayment(), makePayment(), makePayment());

        PspSubmissionRequest request = PspSubmissionRequest.builder()
                .payments(payments)
                .build();

        var response = client.submitPayments(request);

        assertEquals(PspSubmissionResponse.class, response.getClass());

    }

    private Payment makePayment() {
        return Payment.builder()
                .createdBy("user")
                .allowedMethods(List.of(PaymentMethod.Bacs))
                .creditor(PartyIdentification.builder().name("").build())
                .creditorAccount(UKBankAccount.builder()
                        .currency("GBP")
                        .accountNumber(UKAccountNumber.builder()
                                .value("12345678")
                                .build())
                        .sortCode(SortCode.fromString("123456"))
                        .name("Account Name")
                        .build())
                .debtor(PartyIdentification.builder()
                        .name("Debtor Name")
                        .build())
                .debtorAccount(UKBankAccount.builder()
                        .currency("GBP")
                        .sortCode(SortCode.fromString("654321"))
                        .accountNumber(UKAccountNumber.builder()
                                .value("87654321")
                                .build())
                        .name("Account Name")
                        .build())
                .amount(Money.parse("GBP 1.00"))
                .latestExecutionDate(Instant.now())
                .product(new CompositeReference("",""))
                .build();
    }

}
