package gov.scot.payments.psps.method.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import io.vavr.collection.List;
import lombok.*;
import org.apache.avro.reflect.Nullable;

import java.util.Map;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@Builder(toBuilder = true)
@MessageType(context = "pspmethod", type = "payment")
public class PspAdapterPayment implements HasKey<String> {

    public enum Status {Registered, Accepted, Rejected, Complete, Returned};
    public enum CancelStatus {Live, Complete, Failed};

    @NonNull @EqualsAndHashCode.Include private Payment payment;
    @Nullable private String routedTo;
    @Nullable private Map<String,String> pspMetadata;
    @NonNull @Builder.Default private Status status = Status.Registered;
    @NonNull @Builder.Default private CancelStatus cancelStatus = CancelStatus.Live;
    @Nullable private String cancelErrorCode;
    @Nullable private String cancelErrorMessage;

    @Override
    @JsonIgnore
    public String getKey() {
        return payment.getKey();
    }

}
