package gov.scot.payments.psps.method.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Map;
import java.time.Instant;
import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "pspmethod", type = "paymentCancelFailed")
@NoArgsConstructor
@RequiredArgsConstructor
public class MethodPaymentCancelFailedEvent extends EventWithCauseImpl implements HasKey<String> {

    @NonNull private UUID paymentId;
    @NonNull private PaymentMethod method;
    private String psp;
    private Map<String,String> pspMetadata;
    @NonNull private String message;
    private String code;
    
    @Override
    @JsonIgnore
    public String getKey() {
        return paymentId.toString();
    }
}
