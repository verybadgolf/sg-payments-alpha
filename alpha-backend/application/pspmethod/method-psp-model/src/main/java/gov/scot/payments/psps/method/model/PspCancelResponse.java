package gov.scot.payments.psps.method.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;

import java.util.Map;
import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder(toBuilder = true)
public class PspCancelResponse {

    @NonNull private UUID paymentId;
    @NonNull private String psp;
    private boolean success;
    private String code;
    private String message;
    private Map<String,String> metadata;

    public static PspCancelResponse fromThrowable(final String psp,UUID paymentId, final Throwable e) {
        return PspCancelResponse.builder()
                                    .paymentId(paymentId)
                                    .message(e.getMessage())
                                    .success(false)
                                    .psp(psp)
                                    .build();
    }

}
