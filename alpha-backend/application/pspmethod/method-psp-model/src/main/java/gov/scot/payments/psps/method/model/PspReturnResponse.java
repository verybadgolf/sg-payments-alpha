package gov.scot.payments.psps.method.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;
import org.apache.avro.reflect.Nullable;
import org.javamoney.moneta.Money;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder(toBuilder = true)
public class PspReturnResponse {

    @NonNull private UUID paymentId;
    @NonNull private String code;
    @NonNull private String message;
    @NonNull @Builder.Default private Map<String,String> metadata = new HashMap<>();
    @Nullable private Money amount;
}
