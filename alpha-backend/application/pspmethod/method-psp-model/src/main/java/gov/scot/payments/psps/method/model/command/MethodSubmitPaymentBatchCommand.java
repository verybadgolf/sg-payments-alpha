package gov.scot.payments.psps.method.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.Payment;
import io.vavr.collection.List;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "pspmethod", type = "submitPayment")
@NoArgsConstructor
public class MethodSubmitPaymentBatchCommand extends CommandImpl implements HasKey<String> {

    @NonNull private String key;
    @NonNull private List<UUID> payments;

    public MethodSubmitPaymentBatchCommand(String key, List<UUID> payments, boolean reply) {
        super(reply);
        this.key = key;
        this.payments = payments;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return key;
    }
}