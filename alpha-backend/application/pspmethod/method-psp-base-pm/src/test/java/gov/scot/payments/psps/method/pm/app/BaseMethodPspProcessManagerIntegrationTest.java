package gov.scot.payments.psps.method.pm.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.Message;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.psps.adapter.model.event.AdapterPaymentCancelFailedEvent;
import gov.scot.payments.psps.adapter.model.event.AdapterPaymentCancelSuccessEvent;
import gov.scot.payments.psps.adapter.model.event.AdapterPaymentCompleteEvent;
import gov.scot.payments.psps.adapter.model.event.AdapterPaymentReturnedEvent;
import gov.scot.payments.psps.method.model.command.MethodAddPaymentToBatchCommand;
import gov.scot.payments.psps.method.model.command.MethodCancelPaymentCommand;
import gov.scot.payments.psps.method.model.command.MethodCompletePaymentCancelCommand;
import gov.scot.payments.psps.method.model.command.MethodCompletePaymentCommand;
import gov.scot.payments.psps.method.model.command.MethodFailPaymentCancelCommand;
import gov.scot.payments.psps.method.model.command.MethodReturnPaymentCommand;
import gov.scot.payments.psps.method.model.command.MethodSubmitPaymentBatchCommand;
import gov.scot.payments.psps.method.model.event.MethodPaymentBatchClosedEvent;
import gov.scot.payments.psps.model.event.PaymentCancelledEvent;
import gov.scot.payments.psps.model.event.PaymentRoutedEvent;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.spring.TestConfiguration;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.streams.KafkaStreams;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ApplicationIntegrationTest(classes = {BaseMethodPspProcessManagerIntegrationTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.PROCESS_MANAGER
        , properties = {"events.destinations=test"
        ,"context.name=bacspsp"
        ,"component.name=bacs-psp-pm"})
public class BaseMethodPspProcessManagerIntegrationTest {

    @BeforeEach
    public void setUp(@Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter){
            kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(10));
     }

    @Test
    public void testRouted(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic){
        var payment = makePayment();

        var noHeader = KeyValueWithHeaders.msg(new PaymentRoutedEvent(payment,PaymentMethod.Bacs));
        var invalidHeaderValue = KeyValueWithHeaders.msg(new PaymentRoutedEvent(payment,PaymentMethod.Bacs), new RecordHeader(Payment.METHOD_HEADER,"abc".getBytes()));
        var wrongHeaderValue = KeyValueWithHeaders.msg(new PaymentRoutedEvent(payment,PaymentMethod.FasterPayments), new RecordHeader(Payment.METHOD_HEADER,PaymentMethod.Chaps.name().getBytes()));
        var correctHeaderValue = KeyValueWithHeaders.msg(new PaymentRoutedEvent(payment.toBuilder().id(payment.getId()).build(),PaymentMethod.Bacs), new RecordHeader(Payment.METHOD_HEADER,PaymentMethod.Bacs.name().getBytes()));
        brokerClient.sendKeyValuesToDestination("events-external",List.of(noHeader,invalidHeaderValue,wrongHeaderValue,correctHeaderValue));
        List<MethodAddPaymentToBatchCommand> commands = brokerClient.readAllFromTopic(commandTopic, MethodAddPaymentToBatchCommand.class);
        assertEquals(1,commands.size());
        assertEquals(payment.getId(),commands.get(0).getPayment().getId());
    }

    @Test
    public void testCancelled(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic){
        var payment = makePayment();

        var noHeader = KeyValueWithHeaders.msg(new PaymentCancelledEvent(payment.getId(),PaymentMethod.Bacs));
        var invalidHeaderValue = KeyValueWithHeaders.msg(new PaymentCancelledEvent(payment.getId(),PaymentMethod.Bacs), new RecordHeader(Payment.METHOD_HEADER,"abc".getBytes()));
        var wrongHeaderValue = KeyValueWithHeaders.msg(new PaymentCancelledEvent(payment.getId(),PaymentMethod.FasterPayments), new RecordHeader(Payment.METHOD_HEADER,PaymentMethod.Chaps.name().getBytes()));
        var correctHeaderValue = KeyValueWithHeaders.msg(new PaymentCancelledEvent(payment.getId(),PaymentMethod.Bacs), new RecordHeader(Payment.METHOD_HEADER,PaymentMethod.Bacs.name().getBytes()));
        brokerClient.sendKeyValuesToDestination("events-external",List.of(noHeader,invalidHeaderValue,wrongHeaderValue,correctHeaderValue));
        List<MethodCancelPaymentCommand> commands = brokerClient.readAllFromTopic(commandTopic, MethodCancelPaymentCommand.class);
        assertEquals(1,commands.size());
        assertEquals(payment.getId(),commands.get(0).getPaymentId());
    }

    @Test
    public void testBatchClosed(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic){
        var payment = makePayment();
         var event = KeyValueWithHeaders.msg(new MethodPaymentBatchClosedEvent("key",List.of(payment.getId())), new RecordHeader(Payment.METHOD_HEADER,PaymentMethod.Bacs.name().getBytes()),new RecordHeader(Message.CONTEXT_HEADER,"bacspsp".getBytes()));
        brokerClient.sendKeyValuesToDestination("events-internal",List.of(event));
        List<MethodSubmitPaymentBatchCommand> commands = brokerClient.readAllFromTopic(commandTopic, MethodSubmitPaymentBatchCommand.class);
        assertEquals(1,commands.size());
        assertEquals(payment.getId(),commands.get(0).getPayments().get(0));
        assertEquals("key",commands.get(0).getKey());
    }

    @Test
    public void testCancelFail(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic){
        var payment = makePayment();
        AdapterPaymentCancelFailedEvent event = AdapterPaymentCancelFailedEvent.builder()
                                                   .method(PaymentMethod.Bacs)
                                                   .code("1")
                                                   .message("2")
                                                   .paymentId(payment.getId())
                                                   .psp("psp")
                                                   .pspMetadata(Map.of("a","b"))
                                                   .build();
        var message = KeyValueWithHeaders.msg(event, new RecordHeader(Payment.METHOD_HEADER,PaymentMethod.Bacs.name().getBytes()));
        brokerClient.sendKeyValuesToDestination("events-external",List.of(message));
        List<MethodFailPaymentCancelCommand> commands = brokerClient.readAllFromTopic(commandTopic, MethodFailPaymentCancelCommand.class);
        assertEquals(1,commands.size());
        assertEquals(payment.getId(),commands.get(0).getPaymentId());
    }

    @Test
    public void testCancelSuccess(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic){
        var payment = makePayment();
        AdapterPaymentCancelSuccessEvent event = AdapterPaymentCancelSuccessEvent.builder()
                                                                                .paymentId(payment.getId())
                                                                                 .method(PaymentMethod.Bacs)
                                                                                .psp("psp")
                                                                                .pspMetadata(Map.of("a","b"))
                                                                                .build();
        var message = KeyValueWithHeaders.msg(event, new RecordHeader(Payment.METHOD_HEADER,PaymentMethod.Bacs.name().getBytes()));
        brokerClient.sendKeyValuesToDestination("events-external",List.of(message));
        List<MethodCompletePaymentCancelCommand> commands = brokerClient.readAllFromTopic(commandTopic, MethodCompletePaymentCancelCommand.class);
        assertEquals(1,commands.size());
        assertEquals(payment.getId(),commands.get(0).getPaymentId());
    }

    @Test
    public void testComplete(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic){
        var payment = makePayment();
        AdapterPaymentCompleteEvent event = AdapterPaymentCompleteEvent.builder()
                                                                            .paymentId(payment.getId())
                                                                       .method(PaymentMethod.Bacs)
                                                                            .psp("psp")
                                                                            .pspMetadata(Map.of("a","b"))
                                                                            .build();
        var message = KeyValueWithHeaders.msg(event, new RecordHeader(Payment.METHOD_HEADER,PaymentMethod.Bacs.name().getBytes()));
        brokerClient.sendKeyValuesToDestination("events-external",List.of(message));
        List<MethodCompletePaymentCommand> commands = brokerClient.readAllFromTopic(commandTopic, MethodCompletePaymentCommand.class);
        assertEquals(1,commands.size());
        assertEquals(payment.getId(),commands.get(0).getPaymentId());
    }

    @Test
    public void testReturned(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic){
        var payment = makePayment();
        AdapterPaymentReturnedEvent event = AdapterPaymentReturnedEvent.builder()
                                                                       .paymentId(payment.getId())
                                                                       .method(PaymentMethod.Bacs)
                                                                       .psp("psp")
                                                                       .pspMetadata(Map.of("a","b"))
                                                                       .message("1")
                                                                       .code("2")
                                                                       .amount(Money.parse("GBP 1.00"))
                                                                       .build();
        var message = KeyValueWithHeaders.msg(event, new RecordHeader(Payment.METHOD_HEADER,PaymentMethod.Bacs.name().getBytes()));
        brokerClient.sendKeyValuesToDestination("events-external",List.of(message));
        List<MethodReturnPaymentCommand> commands = brokerClient.readAllFromTopic(commandTopic, MethodReturnPaymentCommand.class);
        assertEquals(1,commands.size());
        assertEquals(payment.getId(),commands.get(0).getPaymentId());
    }

    //adapter complete
    //adapter returned

    private Payment makePayment() {
        return Payment.builder()
                .createdBy("user")
                .allowedMethods(List.of(PaymentMethod.Bacs))
                      .debtor(PartyIdentification.builder().name("").build())
                      .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                      .creditor(PartyIdentification.builder().name("").build())
                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .amount(Money.parse("GBP 1.00"))
                .latestExecutionDate(Instant.now())
                .product(new CompositeReference("",""))
                .build();
    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends BaseMethodPspProcessManager {

        @Override
        protected Set<PaymentMethod> supportedPaymentMethods() {
            return HashSet.of(PaymentMethod.Bacs);
        }
    }
}