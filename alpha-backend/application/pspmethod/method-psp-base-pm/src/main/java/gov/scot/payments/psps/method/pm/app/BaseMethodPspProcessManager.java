package gov.scot.payments.psps.method.pm.app;

import gov.scot.payments.application.MessageTypeRegistry;
import gov.scot.payments.application.component.processmanager.EventTransformer;
import gov.scot.payments.application.component.processmanager.ProcessManagerApplication;
import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.payments.model.event.PaymentCancelApprovedEvent;
import gov.scot.payments.payments.model.event.PaymentReadyForSubmissionEvent;
import gov.scot.payments.psps.adapter.model.event.AdapterPaymentCancelFailedEvent;
import gov.scot.payments.psps.adapter.model.event.AdapterPaymentCancelSuccessEvent;
import gov.scot.payments.psps.adapter.model.event.AdapterPaymentCompleteEvent;
import gov.scot.payments.psps.adapter.model.event.AdapterPaymentReturnedEvent;
import gov.scot.payments.psps.method.model.command.*;
import gov.scot.payments.psps.method.model.event.MethodPaymentAcceptedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentBatchClosedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentCancelFailedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentCancelSuccessEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentCompleteEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentRejectedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentReturnedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentSubmissionFailedEvent;
import gov.scot.payments.psps.model.event.PaymentCancelledEvent;
import gov.scot.payments.psps.model.event.PaymentRoutedEvent;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import io.vavr.control.Try;
import org.apache.kafka.common.header.Headers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.util.ResourceUtils;

import java.util.function.Predicate;

@PropertySource(ResourceUtils.CLASSPATH_URL_PREFIX + "application-process-manager.properties")
@PropertySource(ResourceUtils.CLASSPATH_URL_PREFIX + "application-method-psp-pm.properties")
public abstract class BaseMethodPspProcessManager extends ProcessManagerApplication {

    protected abstract Set<PaymentMethod> supportedPaymentMethods();

    @Bean
    public EventTransformer eventTransformFunction() {
        return EventTransformer.patternMatching(b ->
                //from core
                b.match(PaymentRoutedEvent.class, e -> List.of(new MethodAddPaymentToBatchCommand(e.getPayment(),false)))
                .match(PaymentCancelledEvent.class, e -> List.of(new MethodCancelPaymentCommand(e.getPaymentId(),false)))

                 //from self
                 .match(MethodPaymentBatchClosedEvent.class,e -> List.of(new MethodSubmitPaymentBatchCommand(e.getKey(),e.getPayments(),false)))

                //from adapters
                .match(AdapterPaymentCancelFailedEvent.class, e -> List.of(handleCancelFailed(e)))
                .match(AdapterPaymentCancelSuccessEvent.class, e -> List.of(handleCancelSuccess(e)))
                .match(AdapterPaymentCompleteEvent.class, e -> List.of(handleComplete(e)))
                .match(AdapterPaymentReturnedEvent.class, e -> List.of(handleReturn(e)))
                 .orElse(List.empty())
        );
    }

    private MethodReturnPaymentCommand handleReturn(final AdapterPaymentReturnedEvent e) {
        return MethodReturnPaymentCommand.builder()
                                         .amount(e.getAmount())
                                         .code(e.getCode())
                                         .message(e.getMessage())
                                         .method(e.getMethod())
                                         .paymentId(e.getPaymentId())
                                         .psp(e.getPsp())
                                         .pspMetadata(e.getPspMetadata())
                                         .build();
    }

    private MethodCompletePaymentCommand handleComplete(final AdapterPaymentCompleteEvent e) {
        return MethodCompletePaymentCommand.builder()
                                         .method(e.getMethod())
                                         .paymentId(e.getPaymentId())
                                         .psp(e.getPsp())
                                         .pspMetadata(e.getPspMetadata())
                                         .build();
    }

    private MethodCompletePaymentCancelCommand handleCancelSuccess(final AdapterPaymentCancelSuccessEvent e) {
        return MethodCompletePaymentCancelCommand.builder()
                                           .method(e.getMethod())
                                           .paymentId(e.getPaymentId())
                                           .psp(e.getPsp())
                                           .pspMetadata(e.getPspMetadata())
                                           .build();
    }

    private MethodFailPaymentCancelCommand handleCancelFailed(final AdapterPaymentCancelFailedEvent e) {
        return MethodFailPaymentCancelCommand.builder()
                                         .code(e.getCode())
                                         .message(e.getMessage())
                                         .method(e.getMethod())
                                         .paymentId(e.getPaymentId())
                                         .psp(e.getPsp())
                                         .pspMetadata(e.getPspMetadata())
                                         .build();
    }

    @Bean
    public Predicate<Headers> externalEventHeaderFilter(MessageTypeRegistry messageTypeRegistry){
        return h -> (  messageTypeRegistry.matchesType(h,PaymentRoutedEvent.class, PaymentCancelledEvent.class) && isPaymentMethodSupported(h) )
                || messageTypeRegistry.matchesType(h
                , AdapterPaymentCancelFailedEvent.class
                , AdapterPaymentCancelSuccessEvent.class
                , AdapterPaymentCompleteEvent.class
                , AdapterPaymentReturnedEvent.class);
    }

    private boolean isPaymentMethodSupported(Headers h) {
        PaymentMethod method = Option.of(h.lastHeader(Payment.METHOD_HEADER))
                                     .map(b -> new String(b.value()))
                                     .map(s -> Try.ofSupplier(() -> PaymentMethod.valueOf(s)).getOrElse((PaymentMethod)null))
                                     .getOrNull();
        return method != null && supportedPaymentMethods().contains(method);
    }

}