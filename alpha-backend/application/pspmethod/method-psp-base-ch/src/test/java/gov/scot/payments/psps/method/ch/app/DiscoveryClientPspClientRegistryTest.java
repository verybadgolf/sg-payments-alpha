package gov.scot.payments.psps.method.ch.app;

import gov.scot.payments.psps.method.ch.app.DiscoveryClientPspClientRegistry;
import gov.scot.payments.psps.method.ch.app.PspClient;
import gov.scot.payments.psps.method.ch.app.PspClientFactory;
import io.vavr.collection.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class DiscoveryClientPspClientRegistryTest {

    private DiscoveryClientPspClientRegistry registry;
    private DiscoveryClient discoveryClient;
    private PspClientFactory factory;

    @BeforeEach
    public void setUp(){
        factory = mock(PspClientFactory.class);
        discoveryClient = mock(DiscoveryClient.class);
        registry = new DiscoveryClientPspClientRegistry(discoveryClient,factory);
    }

    @Test
    void listPspClients() {
        when(discoveryClient.getServices()).thenReturn(java.util.List.of("123"));
        ServiceInstance instance = mock(ServiceInstance.class);
        PspClient client = mock(PspClient.class);
        when(discoveryClient.getInstances(anyString())).thenReturn(java.util.List.of(instance));
        when(factory.apply(any())).thenReturn(client);
        List<PspClient> clients = registry.listPspClients();
        assertEquals(1,clients.size());
        verify(discoveryClient,times(1)).getServices();
        verify(discoveryClient,times(1)).getInstances("123");
        verify(factory,times(1)).apply(any());
    }

}