package gov.scot.payments.psps.method.ch.app;

import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentState;
import gov.scot.payments.psps.method.ch.app.PspAdapterStateUpdater;
import gov.scot.payments.psps.method.ch.app.PspClient;
import gov.scot.payments.psps.method.ch.app.PspClientRegistry;
import gov.scot.payments.psps.method.model.PspAdapterPayment;
import gov.scot.payments.psps.method.model.PspCancelResponse;
import gov.scot.payments.psps.method.model.command.MethodAddPaymentToBatchCommand;
import gov.scot.payments.psps.method.model.command.MethodCancelPaymentCommand;
import gov.scot.payments.psps.method.model.command.MethodCompletePaymentCancelCommand;
import gov.scot.payments.psps.method.model.command.MethodCompletePaymentCommand;
import gov.scot.payments.psps.method.model.command.MethodFailPaymentCancelCommand;
import gov.scot.payments.psps.method.model.command.MethodReturnPaymentCommand;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.collection.List;
import io.vavr.control.Option;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class PspAdapterStateUpdaterTest {

    private PspAdapterStateUpdater updater;
    private PspClientRegistry registry;

    @BeforeEach
    public void setUp(){
        final MicrometerMetrics metrics = new MicrometerMetrics(new SimpleMeterRegistry());
        registry = mock(PspClientRegistry.class);
        updater = new PspAdapterStateUpdater(metrics,registry);
    }

    @Test
    public void testAddToBatch(){
        var payment = makePayment();

        var command = MethodAddPaymentToBatchCommand.builder()
                                                    .payment(payment)
                                                    .build();

        var currentState = PspAdapterPayment.builder()
                                            .payment(payment)
                                     .build();
        assertThrows(IllegalStateException.class,() -> updater.apply(command, PspAdapterPaymentState.builder().current(currentState).build()));

        var updatedState = updater.apply(command,PspAdapterPaymentState.builder().build());
        assertEquals(payment.getId(),updatedState.getPayment().getId());
        assertEquals(PspAdapterPayment.CancelStatus.Live,updatedState.getCancelStatus());
        assertEquals(PspAdapterPayment.Status.Registered,updatedState.getStatus());

    }

    @Test
    public void testReturn(){
        var payment = makePayment();

        var command = MethodReturnPaymentCommand.builder()
                                                .paymentId(payment.getId())
                                                .pspMetadata(Map.of())
                                                .psp("psp")
                                                .method(PaymentMethod.Bacs)
                                                .message("m")
                                                .code("c")
                                                .amount(Money.parse("GBP 1.00"))
                                                .build();

        var currentState = PspAdapterPayment.builder()
                                            .payment(payment)
                                            .build();
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspAdapterPaymentState.builder().build()));

        var updatedState = updater.apply(command,PspAdapterPaymentState.builder().current(currentState).build());
        assertEquals(payment.getId(),updatedState.getPayment().getId());
        assertEquals(PspAdapterPayment.CancelStatus.Live,updatedState.getCancelStatus());
        assertEquals(PspAdapterPayment.Status.Returned,updatedState.getStatus());
    }

    @Test
    public void testComplete(){
        var payment = makePayment();

        var command = MethodCompletePaymentCommand.builder()
                                                  .paymentId(payment.getId())
                                                  .pspMetadata(Map.of())
                                                  .psp("psp")
                                                  .method(PaymentMethod.Bacs)
                                                  .build();

        var currentState = PspAdapterPayment.builder()
                                            .payment(payment)
                                            .build();
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspAdapterPaymentState.builder().build()));

        var updatedState = updater.apply(command,PspAdapterPaymentState.builder().current(currentState).build());
        assertEquals(payment.getId(),updatedState.getPayment().getId());
        assertEquals(PspAdapterPayment.CancelStatus.Live,updatedState.getCancelStatus());
        assertEquals(PspAdapterPayment.Status.Complete,updatedState.getStatus());
    }

    @Test
    public void testCompleteCancel(){
        var payment = makePayment();

        var command = MethodCompletePaymentCancelCommand.builder()
                                                        .paymentId(payment.getId())
                                                        .pspMetadata(Map.of())
                                                        .psp("psp")
                                                        .method(PaymentMethod.Bacs)
                                                        .build();

        var currentState = PspAdapterPayment.builder()
                                            .payment(payment)
                                            .build();
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspAdapterPaymentState.builder().build()));

        var updatedState = updater.apply(command,PspAdapterPaymentState.builder().current(currentState).build());
        assertEquals(payment.getId(),updatedState.getPayment().getId());
        assertEquals(PspAdapterPayment.CancelStatus.Complete,updatedState.getCancelStatus());
        assertEquals(PspAdapterPayment.Status.Registered,updatedState.getStatus());
    }

    @Test
    public void testFailCancel(){
        var payment = makePayment();

        var command = MethodFailPaymentCancelCommand.builder()
                                                    .paymentId(payment.getId())
                                                    .pspMetadata(Map.of())
                                                    .psp("psp")
                                                    .method(PaymentMethod.Bacs)
                                                    .code("c")
                                                    .message("m")
                                                    .build();

        var currentState = PspAdapterPayment.builder()
                                            .payment(payment)
                                            .build();
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspAdapterPaymentState.builder().build()));

        var updatedState = updater.apply(command,PspAdapterPaymentState.builder().current(currentState).build());
        assertEquals(payment.getId(),updatedState.getPayment().getId());
        assertEquals(PspAdapterPayment.CancelStatus.Failed,updatedState.getCancelStatus());
        assertEquals(PspAdapterPayment.Status.Registered,updatedState.getStatus());
    }

    @Test
    public void testCancelNoOp(){
        var payment = makePayment();

        var command = MethodCancelPaymentCommand.builder()
                                                .paymentId(payment.getId())
                                                .build();

        var currentState = PspAdapterPayment.builder()
                                            .payment(payment)
                                            .build();
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspAdapterPaymentState.builder().build()));

        var updatedState = updater.apply(command,PspAdapterPaymentState.builder().current(currentState).build());
        assertEquals(payment.getId(),updatedState.getPayment().getId());
        assertEquals(PspAdapterPayment.CancelStatus.Complete,updatedState.getCancelStatus());
        assertEquals(PspAdapterPayment.Status.Registered,updatedState.getStatus());
    }

    @Test
    public void testCancelNoClient(){
        var payment = makePayment();

        var command = MethodCancelPaymentCommand.builder()
                                                .paymentId(payment.getId())
                                                .build();

        var currentState = PspAdapterPayment.builder()
                                            .payment(payment)
                                            .status(PspAdapterPayment.Status.Accepted)
                                            .routedTo("psp")
                                            .build();
        when(registry.getClient(anyString())).thenReturn(Option.none());
        var updatedState = updater.apply(command,PspAdapterPaymentState.builder().current(currentState).build());
        assertEquals(payment.getId(),updatedState.getPayment().getId());
        assertEquals(PspAdapterPayment.CancelStatus.Failed,updatedState.getCancelStatus());
        assertEquals(PspAdapterPayment.Status.Accepted,updatedState.getStatus());
    }

    @Test
    public void testCancelClientError(){
        var payment = makePayment();

        var command = MethodCancelPaymentCommand.builder()
                                                .paymentId(payment.getId())
                                                .build();

        var currentState = PspAdapterPayment.builder()
                                            .payment(payment)
                                            .routedTo("psp")
                                            .status(PspAdapterPayment.Status.Accepted)
                                            .build();
        PspClient client = mock(PspClient.class);
        when(client.getName()).thenReturn("psp");
        when(registry.getClient(anyString())).thenReturn(Option.of(client));
        doThrow(new RuntimeException()).when(client).cancelPayment(any());
        var updatedState = updater.apply(command,PspAdapterPaymentState.builder().current(currentState).build());
        assertEquals(payment.getId(),updatedState.getPayment().getId());
        assertEquals(PspAdapterPayment.CancelStatus.Failed,updatedState.getCancelStatus());
        assertEquals(PspAdapterPayment.Status.Accepted,updatedState.getStatus());
    }

    @Test
    public void testCancelClientFailure(){
        var payment = makePayment();

        var command = MethodCancelPaymentCommand.builder()
                                                .paymentId(payment.getId())
                                                .build();

        var currentState = PspAdapterPayment.builder()
                                            .payment(payment)
                                            .routedTo("psp")
                                            .status(PspAdapterPayment.Status.Accepted)
                                            .build();
        PspClient client = mock(PspClient.class);
        when(registry.getClient(anyString())).thenReturn(Option.of(client));
        when(client.cancelPayment(any())).thenReturn(PspCancelResponse.builder()
                                                                      .paymentId(payment.getId())
                                                                      .psp("psp")
                                                                      .metadata(Map.of())
                                                                      .code("c")
                                                                      .message("m")
                                                                      .build());
        var updatedState = updater.apply(command,PspAdapterPaymentState.builder().current(currentState).build());
        assertEquals(payment.getId(),updatedState.getPayment().getId());
        assertEquals(PspAdapterPayment.CancelStatus.Failed,updatedState.getCancelStatus());
        assertEquals(PspAdapterPayment.Status.Accepted,updatedState.getStatus());
    }

    @Test
    public void testCancelClientSuccess(){
        var payment = makePayment();

        var command = MethodCancelPaymentCommand.builder()
                                                .paymentId(payment.getId())
                                                .build();

        var currentState = PspAdapterPayment.builder()
                                            .payment(payment)
                                            .routedTo("psp")
                                            .status(PspAdapterPayment.Status.Accepted)
                                            .build();
        PspClient client = mock(PspClient.class);
        when(registry.getClient(anyString())).thenReturn(Option.of(client));
        when(client.cancelPayment(any())).thenReturn(PspCancelResponse.builder()
                                                                      .paymentId(payment.getId())
                                                                      .psp("psp")
                                                                      .metadata(Map.of())
                                                                      .success(true)
                                                                      .build());
        var updatedState = updater.apply(command,PspAdapterPaymentState.builder().current(currentState).build());
        assertEquals(payment.getId(),updatedState.getPayment().getId());
        assertEquals(PspAdapterPayment.CancelStatus.Complete,updatedState.getCancelStatus());
        assertEquals(PspAdapterPayment.Status.Accepted,updatedState.getStatus());
    }

    private Payment makePayment() {
        return Payment.builder()
                      .createdBy("user")
                      .allowedMethods(List.of(PaymentMethod.Bacs))
                      .debtor(PartyIdentification.builder().name("").build())
                      .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                      .creditor(PartyIdentification.builder().name("").build())
                        .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                        .amount(Money.parse("GBP 1.00"))
                      .latestExecutionDate(Instant.now())
                      .product(new CompositeReference("",""))
                      .build();
    }

}