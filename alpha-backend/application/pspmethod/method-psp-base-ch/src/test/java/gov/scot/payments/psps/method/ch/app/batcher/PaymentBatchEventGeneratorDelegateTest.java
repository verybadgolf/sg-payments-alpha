package gov.scot.payments.psps.method.ch.app.batcher;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.psps.method.ch.app.batcher.PaymentBatchEventGeneratorDelegate;
import gov.scot.payments.psps.method.model.PspAdapterPayment;
import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentState;
import gov.scot.payments.psps.method.model.command.MethodCompletePaymentCancelCommand;
import gov.scot.payments.psps.method.model.command.MethodCompletePaymentCommand;
import gov.scot.payments.psps.method.model.command.MethodFailPaymentCancelCommand;
import gov.scot.payments.psps.method.model.command.MethodReturnPaymentCommand;
import gov.scot.payments.psps.method.model.event.MethodPaymentCancelFailedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentCancelSuccessEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentCompleteEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentReturnedEvent;
import io.vavr.collection.List;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class PaymentBatchEventGeneratorDelegateTest {

    private PaymentBatchEventGeneratorDelegate generator;
    private Payment payment;
    private PspAdapterPaymentState state;

    @BeforeEach
    public void setUp(){
        generator = new PaymentBatchEventGeneratorDelegate(PaymentMethod.Bacs);
        payment = makePayment();
        state = PspAdapterPaymentState.builder()
                               .current(PspAdapterPayment.builder()
                                                         .payment(payment)
                                                         .status(PspAdapterPayment.Status.Accepted)
                                                         .pspMetadata(Map.of("a","b"))
                                                         .build())
                               .build();
    }

    @Test
    public void testReturn(){
        var command = MethodReturnPaymentCommand.builder()
                                                .paymentId(payment.getId())
                                                .method(PaymentMethod.Bacs)
                                                .psp("psp")
                                                .message("1")
                                                .code("2")
                                                .amount(Money.parse("GBP 1.00"))
                                                .pspMetadata(Map.of("a","b"))
                                                .build();
        var events = generator.apply(command,state);
        assertEquals(1,events.size());
        var event = (MethodPaymentReturnedEvent)events.head();
        assertEquals(payment.getId(),event.getPaymentId());
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals("psp",event.getPsp());
        assertEquals(Map.of("a","b"),event.getPspMetadata());
        assertEquals("1",event.getMessage());
        assertEquals("2",event.getCode());
        assertEquals(Money.parse("GBP 1.00"),event.getAmount());
    }

    @Test
    public void testComplete(){
        var command = MethodCompletePaymentCommand.builder()
                                                  .paymentId(payment.getId())
                                                  .method(PaymentMethod.Bacs)
                                                  .psp("psp")
                                                  .pspMetadata(Map.of("a","b"))
                                                  .build();
        var events = generator.apply(command,state);
        assertEquals(1,events.size());
        var event = (MethodPaymentCompleteEvent)events.head();
        assertEquals(payment.getId(),event.getPaymentId());
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals("psp",event.getPsp());
        assertEquals(Map.of("a","b"),event.getPspMetadata());
    }

    @Test
    public void testCancelSuccess(){
        var command = MethodCompletePaymentCancelCommand.builder()
                                                        .paymentId(payment.getId())
                                                        .method(PaymentMethod.Bacs)
                                                        .psp("psp")
                                                        .pspMetadata(Map.of("a","b"))
                                                        .build();
        var events = generator.apply(command,state);
        assertEquals(1,events.size());
        var event = (MethodPaymentCancelSuccessEvent)events.head();
        assertEquals(payment.getId(),event.getPaymentId());
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals("psp",event.getPsp());
        assertEquals(Map.of("a","b"),event.getPspMetadata());
    }

    @Test
    public void testCancelFail(){
        var command = MethodFailPaymentCancelCommand.builder()
                                                    .paymentId(payment.getId())
                                                    .method(PaymentMethod.Bacs)
                                                    .psp("psp")
                                                    .message("1")
                                                    .code("2")
                                                    .pspMetadata(Map.of("a","b"))
                                                    .build();
        var events = generator.apply(command,state);
        assertEquals(1,events.size());
        var event = (MethodPaymentCancelFailedEvent)events.head();
        assertEquals(payment.getId(),event.getPaymentId());
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals("psp",event.getPsp());
        assertEquals(Map.of("a","b"),event.getPspMetadata());
        assertEquals("1",event.getMessage());
        assertEquals("2",event.getCode());
    }

    //cancel

    private Payment makePayment() {
        return Payment.builder()
                      .createdBy("user")
                      .allowedMethods(List.of(PaymentMethod.Bacs))
                      .debtor(PartyIdentification.builder().name("").build())
                      .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                      .creditor(PartyIdentification.builder().name("").build())
                        .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                        .amount(Money.parse("GBP 1.00"))
                      .latestExecutionDate(Instant.now())
                      .product(new CompositeReference("",""))
                      .build();
    }
}