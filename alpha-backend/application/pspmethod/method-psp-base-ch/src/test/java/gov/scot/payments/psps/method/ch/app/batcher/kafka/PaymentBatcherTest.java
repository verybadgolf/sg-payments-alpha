package gov.scot.payments.psps.method.ch.app.batcher.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentBatch;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.Set;
import io.vavr.jackson.datatype.VavrModule;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.TopologyTestDriver;
import org.assertj.core.api.Assertions;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.kafka.support.JacksonUtils;
import org.springframework.kafka.support.converter.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerde;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.messaging.MessageHeaders;

import javax.money.CurrencyUnit;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.Comparator;
import java.util.UUID;
import java.util.function.Predicate;

import static gov.scot.payments.testing.kafka.KafkaStreamsTestHarness.INPUT_TOPIC;
import static org.junit.jupiter.api.Assertions.assertEquals;


public abstract class PaymentBatcherTest {

    protected KafkaStreamsTestHarness harness;
    protected Serde<Payment> paymentSerde;
    protected Serde<PspAdapterPaymentBatch> batchSerde;

    @BeforeEach
    public void setUp(){
        ObjectMapper mapper = JacksonUtils.enhancedObjectMapper();
        mapper.activateDefaultTyping(
                BasicPolymorphicTypeValidator.builder()
                        .allowIfBaseType(Set.class)
                        .allowIfBaseType(Map.class)
                        .allowIfBaseType(java.util.Map.class)
                        .allowIfBaseType(List.class)
                        .allowIfBaseType(CurrencyUnit.class)
                        .build()
        );
        mapper.registerModule(new VavrModule());
        JsonSerde<PspAdapterPaymentBatch> batchSerde = new JsonSerde<>(PspAdapterPaymentBatch.class, mapper);
        JsonSerde<Payment> paymentSerde = new JsonSerde<>(Payment.class, mapper);
        DefaultJackson2JavaTypeMapper typeMapper = (DefaultJackson2JavaTypeMapper) ((JsonDeserializer<Payment>) paymentSerde.deserializer()).getTypeMapper();
        typeMapper.setClassIdFieldName(MessageHeaders.CONTENT_TYPE);
        final String mappingConfig = KafkaStreamsTestHarness.buildMappingsConfig(Payment.class);
        paymentSerde.configure(HashMap.of(JsonSerializer.TYPE_MAPPINGS, mappingConfig).toJavaMap(), false);

        this.paymentSerde = paymentSerde;
        this.batchSerde = batchSerde;
        harness = KafkaStreamsTestHarness.builderWithMappings(Serdes.ByteArray(),new gov.scot.payments.testing.kafka.JsonSerde(),Payment.class, PspAdapterPaymentBatch.class)
                .build();
    }

    protected void sleep(TopologyTestDriver topology){
        sleep(topology,Duration.ofSeconds(1));
    }

    protected void sleep(TopologyTestDriver topology, Duration wait){
        topology.advanceWallClockTime(wait);
    }

    protected List<PspAdapterPaymentBatch> getAndVerifyBatches(TopologyTestDriver topology, int size, int totalSize) {
        List<PspAdapterPaymentBatch> batches = getBatches(topology);
        verifyBatches(batches,size,totalSize);
        return batches;
    }

    protected void verifyBatch(PspAdapterPaymentBatch batch
            , int size
            , double total
            , Predicate<Payment>... paymentPredicates) {
        assertEquals(size,batch.getPayments().size());
        List.of(paymentPredicates).forEach(p -> Assertions.assertThat(batch.getPayments()).allMatch(p));
        assertEquals(total,batch.getPayments().map(pi -> pi.getAmount().getNumberStripped().doubleValue()).sum().doubleValue(),0.1);
    }

    protected void verifyBatches(List<PspAdapterPaymentBatch> batches,int size, int totalSize) {
        assertEquals(size,batches.size());
        List<UUID> paymentIds = batches.flatMap(b -> b.getPayments().map(Payment::getId));
        assertEquals(totalSize,paymentIds.size());
        Assertions.assertThat(paymentIds).doesNotHaveDuplicates();

    }

    protected Payment createPayment(CompositeReference service, double amount, String currency, Instant now){
        return Payment.builder()
                .createdBy("user")
                .allowedMethods(List.of(PaymentMethod.Bacs))
                      .debtor(PartyIdentification.builder().name("").build())
                      .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                      .creditor(PartyIdentification.builder().name("").build())
                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .amount(Money.of(new BigDecimal(amount),currency))
                .createdAt(now)
                .latestExecutionDate(Instant.now())
                .product(service)
                .build();
    }

    protected Payment createPayment(CompositeReference service, double amount, String currency){
        return createPayment(service, amount, currency,Instant.now());
    }

    protected List<Payment> sendPayments(TopologyTestDriver topology, CompositeReference service, int startingValue, int count, Instant now) {
        List<KeyValueWithHeaders<String,Payment>> instructions = createPayments(service, startingValue, count,now)
                .map(p -> KeyValueWithHeaders.msg(p,now.toEpochMilli()));
        harness.sendKeyValues(topology,INPUT_TOPIC,(List)instructions);
        return instructions.map(kvh -> kvh.value);
    }

    protected List<Payment> sendPayments(TopologyTestDriver topology, CompositeReference service, int startingValue, int count) {
        return sendPayments(topology, service, startingValue, count,Instant.now());
    }

    protected List<Payment> createPayments(CompositeReference service, int startingValue, int count, Instant now) {
        List<Payment> instructions = List.of();
        for(int i=startingValue;i<startingValue+count;i++){
            instructions = instructions.append(createPayment(service,i,"GBP",now));
        }
        return instructions;
    }

    protected List<Payment> createPayments(CompositeReference service, int startingValue, int count) {
        return createPayments(service, startingValue, count,Instant.now());
    }

    protected List<PspAdapterPaymentBatch> getBatches(TopologyTestDriver topology) {
        Comparator<PspAdapterPaymentBatch> c = Comparator.comparingInt(PspAdapterPaymentBatch::size)
                                                         .thenComparingDouble(pb -> pb.getPayments().map(p -> p.getAmount().getNumberStripped().doubleValue()).sum().doubleValue());
        final List<PspAdapterPaymentBatch> batches = harness.drain(topology, "batches");
        return batches.sortBy(c,p -> p).reverse();
    }

}
