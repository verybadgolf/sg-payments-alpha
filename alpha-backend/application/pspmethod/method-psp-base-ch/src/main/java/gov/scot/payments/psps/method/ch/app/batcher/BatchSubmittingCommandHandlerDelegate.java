package gov.scot.payments.psps.method.ch.app.batcher;

import gov.scot.payments.application.component.commandhandler.CommandHandlerDelegate;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.psps.method.ch.app.batcher.kafka.BatchSubmittingTransformerSupplier;
import gov.scot.payments.psps.method.model.command.MethodSubmitPaymentBatchCommand;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.ValueTransformerSupplier;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;

@Builder
@AllArgsConstructor
public class BatchSubmittingCommandHandlerDelegate implements CommandHandlerDelegate<EventWithCauseImpl>, BeanFactoryAware {

    private final CommandHandlerDelegate<EventWithCauseImpl> delegate;
    private final BatchSubmittingTransformerSupplier transformer;
    @Setter private BeanFactory beanFactory;

    @Override
    public KStream<byte[], EventWithCauseImpl> apply(final KStream<byte[], Command> stream) {
        try {
            String factoryName = String.format("&stream-builder-%s","handle");
            StreamsBuilder streamsBuilder = beanFactory.getBean(factoryName, StreamsBuilderFactoryBean.class).getObject();
            transformer.registerStateStores(streamsBuilder);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        KStream<byte[], Command>[] branches = stream.branch((k,v) -> MethodSubmitPaymentBatchCommand.class.isAssignableFrom(v.getClass()),(k,v) -> true);
        final KStream<byte[], EventWithCauseImpl> delegated = delegate.apply(branches[1]);
        String[] paymentStateStores = transformer.getStateStores();
        KStream<byte[], EventWithCauseImpl> batchSubmitter = branches[0].flatTransformValues((ValueTransformerSupplier)transformer, paymentStateStores);
        return batchSubmitter.merge(delegated);
    }

}
