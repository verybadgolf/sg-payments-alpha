package gov.scot.payments.psps.method.ch.app;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.application.component.commandhandler.stateful.AggregateState;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.model.CommandFailureInfo;
import gov.scot.payments.psps.method.model.PspAdapterPayment;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

@SuperBuilder
@NoArgsConstructor
@ToString(callSuper = true)
public class PspAdapterPaymentState extends AggregateState<PspAdapterPayment> {

    @Getter @Nullable private PspAdapterPayment previous;
    @Getter @Nullable private PspAdapterPayment current;

    @JsonCreator
    public PspAdapterPaymentState(@JsonProperty("message") SerializedMessage message
            , @JsonProperty("error") CommandFailureInfo error
            , @JsonProperty("previous") PspAdapterPayment previous
            , @JsonProperty("current") PspAdapterPayment current
            , @JsonProperty("currentVersion") Long currentVersion) {
        super(message, error, currentVersion);
        this.previous = previous;
        this.current = current;
    }

}
