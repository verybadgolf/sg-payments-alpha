package gov.scot.payments.psps.method.ch.app;

import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.client.discovery.DiscoveryClient;

@RequiredArgsConstructor
public class DiscoveryClientPspClientRegistry implements PspClientRegistry{

    private final DiscoveryClient discoveryClient;
    private final PspClientFactory clientFactory;

    @Override
    public List<PspClient> listPspClients() {
        return List.ofAll(discoveryClient.getServices())
                   .flatMap(s -> discoveryClient.getInstances(s))
                   .map(s ->  clientFactory.apply(s));
    }

    @Override
    public Option<PspClient> getClient(final String psp) {
        return listPspClients().find(c -> c.getName().equals(psp));
    }
}
