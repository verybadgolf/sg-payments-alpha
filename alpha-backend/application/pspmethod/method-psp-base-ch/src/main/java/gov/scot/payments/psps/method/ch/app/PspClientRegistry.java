package gov.scot.payments.psps.method.ch.app;

import io.vavr.collection.List;
import io.vavr.control.Option;

public interface PspClientRegistry {

    List<PspClient> listPspClients();

    Option<PspClient> getClient(String psp);
}
