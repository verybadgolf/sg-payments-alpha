package gov.scot.payments.psps.method.ch.app;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.RequiredArgsConstructor;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
public class CachingPspClientRegistry implements PspClientRegistry {

    private final LoadingCache<String,List<PspClient>> cache;

    public CachingPspClientRegistry(PspClientRegistry delegate, Duration timeout){
        this.cache = CacheBuilder.newBuilder()
                                 .expireAfterWrite(timeout.toMillis(), TimeUnit.MILLISECONDS)
                .build(new CacheLoader<>() {
                    @Override
                    public List<PspClient> load(final String key)  {
                        return delegate.listPspClients();
                    }
                });
    }

    @Override
    public List<PspClient> listPspClients() {
        return cache.getUnchecked("");
    }

    @Override
    public Option<PspClient> getClient(final String psp) {
        return listPspClients().find(c -> c.getName().equals(psp));
    }
}
