package gov.scot.payments.psps.method.ch.app;

import org.springframework.cloud.client.ServiceInstance;

import java.util.function.Function;

public interface PspClientFactory extends Function<ServiceInstance, PspClient> {
}
