package gov.scot.payments.psps.method.ch.app.batcher.kafka;

import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.psps.method.model.command.MethodSubmitPaymentBatchCommand;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.ValueTransformerSupplier;

public interface BatchSubmittingTransformerSupplier extends ValueTransformerSupplier<MethodSubmitPaymentBatchCommand, Iterable<EventWithCauseImpl>> {

    String[] getStateStores();

    default void registerStateStores(StreamsBuilder streamsBuilder) {}

}
