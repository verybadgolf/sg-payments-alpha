package gov.scot.payments.psps.method.ch.app.batcher.kafka;

import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentBatch;
import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;
import org.apache.kafka.streams.state.KeyValueIterator;

import java.time.Duration;

public class PeriodicWithCountTransformer extends CountTransformer {

    private final Duration window;

    public PeriodicWithCountTransformer(String stateStoreName,Duration window, int maxCount) {
        super(stateStoreName,maxCount);
        this.window = window;
    }

    @Override
    public void init(ProcessorContext processorContext) {
        super.init(processorContext);
        if(window != null){
            processorContext.schedule(window, PunctuationType.WALL_CLOCK_TIME, this::doPunctuate);
        }
    }

    private synchronized void doPunctuate(long timestamp) {
        KeyValueIterator<byte[], PspAdapterPaymentBatch> iterator = currentBatches.all();
        Map<byte[], PspAdapterPaymentBatch> toEmit = HashMap.empty();
        while (iterator.hasNext()) {
            KeyValue<byte[], PspAdapterPaymentBatch> keyValue = iterator.next();
            toEmit = toEmit.put(keyValue.key,keyValue.value);
        }
        toEmit.forEach((k,v) -> {
            processorContext.forward(k, v);
            currentBatches.delete(k);
        });
    }

}
