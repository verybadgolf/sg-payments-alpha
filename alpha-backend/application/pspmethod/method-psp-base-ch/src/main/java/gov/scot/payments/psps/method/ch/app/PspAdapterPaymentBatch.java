package gov.scot.payments.psps.method.ch.app;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.payments.model.aggregate.Payment;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import lombok.*;

import java.util.Comparator;
import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@Builder(toBuilder = true)
public class PspAdapterPaymentBatch {

    public static String BATCH_ID_KEY = "BATCH_ID";

    @NonNull @Builder.Default private List<Payment> payments = List.empty();
    @NonNull @EqualsAndHashCode.Include @ToString.Include @Builder.Default private UUID id = UUID.randomUUID();

    public PspAdapterPaymentBatch addPayment(Payment payment) {
        return this.toBuilder()
                   .payments(this.payments.append(payment))
                   .build();
    }

    public PspAdapterPaymentBatch merge(PspAdapterPaymentBatch batch) {
        Set<Payment> payments = this.payments.toSet();
        payments = payments.addAll(batch.getPayments());
        return this.toBuilder()
                   .payments(payments.toList())
                   .build();
    }

    @JsonIgnore
    public int size() {
        return payments.size();
    }

    @JsonIgnore
    public Option<Payment> getLatestPayment() {
        return payments.maxBy(Comparator.comparing(Payment::getCreatedAt));
    }

    @JsonIgnore
    public Option<Payment> getEarliestPayment() {
        return payments.minBy(Comparator.comparing(Payment::getCreatedAt));
    }
}
