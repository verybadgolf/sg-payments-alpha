package gov.scot.payments.psps.method.ch.app.batcher.kafka;

import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentBatch;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.processor.Cancellable;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.support.SimpleTriggerContext;

import java.time.Duration;
import java.util.Date;

public class ScheduledTransformer extends BaseTransformer {

    private final Trigger trigger;
    private Cancellable currentTask;

    public ScheduledTransformer(String stateStoreName,Trigger trigger) {
        super(stateStoreName);
        this.trigger = trigger;
    }

    @Override
    public void init(ProcessorContext processorContext) {
        super.init(processorContext);
        if(trigger != null){
            Duration duration = getNextWait(null);
            if(duration != null){
                currentTask = processorContext.schedule(duration, PunctuationType.WALL_CLOCK_TIME, this::doPunctuate);
            }
        }
    }

    private Duration getNextWait(Long timestamp) {
        Date lastDate = timestamp == null ? null : new Date(timestamp);
        TriggerContext context = new SimpleTriggerContext();
        Date nextDate = trigger.nextExecutionTime(context);
        if(nextDate == null){
            return null;
        }
        return Duration.ofMillis(nextDate.toInstant().toEpochMilli() - System.currentTimeMillis());
    }

    private synchronized void doPunctuate(long timestamp) {
        currentTask.cancel();
        KeyValueIterator<byte[], PspAdapterPaymentBatch> iterator = currentBatches.all();
        while (iterator.hasNext()) {
            KeyValue<byte[], PspAdapterPaymentBatch> keyValue = iterator.next();
            processorContext.forward(keyValue.key, keyValue.value);
            iterator.remove();
        }
        Duration duration = getNextWait(timestamp);
        if(duration != null){
            currentTask = processorContext.schedule(duration, PunctuationType.WALL_CLOCK_TIME, this::doPunctuate);
        }
    }

}
