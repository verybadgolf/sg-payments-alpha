package gov.scot.payments.psps.method.sepadc.ch.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.component.commandhandler.stateful.StatefulErrorHandler;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.psps.method.ch.app.BasePspCommandHandler;
import gov.scot.payments.psps.method.ch.app.PspClientFactory;
import gov.scot.payments.psps.method.ch.app.batcher.PaymentBatchEventGenerator;
import gov.scot.payments.psps.method.ch.app.batcher.PaymentBatchEventGeneratorDelegate;
import gov.scot.payments.psps.method.ch.app.batcher.PerPaymentBatchEventGenerator;
import gov.scot.payments.psps.method.model.PspAdapterPayment;
import org.springframework.context.annotation.Bean;

@ApplicationComponent
public class SepaDCMethodCHApplication extends BasePspCommandHandler {

    @Override
    protected PaymentMethod paymentMethod() {
        return PaymentMethod.Sepa_DC;
    }

    @Bean
    public PspClientFactory sepaDCClientFactory() {
        return serviceInstance -> new SepaDCClient(serviceInstance);
    }

    @Bean
    public PaymentBatchEventGenerator paymentBatchEventGenerator(Metrics metrics
            , StatefulErrorHandler<PspAdapterPayment, EventWithCauseImpl> errorHandler){
        //switch for batchingeventgenerator if batching is desired
        return PerPaymentBatchEventGenerator.builder()
                .eventGenerator(new PaymentBatchEventGeneratorDelegate(paymentMethod()))
                .paymentMethod(paymentMethod())
                .metrics(metrics)
                .errorHandler(errorHandler)
                .commandSerde(valueSerde(Command.class))
                .build();
    }

    public static void main(String[] args){
        BaseApplication.run(SepaDCMethodCHApplication.class,args);
    }

}