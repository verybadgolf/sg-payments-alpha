{{- define "sepadcmethod-sepadc-method-ch-app.env" -}}
- name: CLIENT_SECRET
  valueFrom:
    configMapKeyRef:
      name: cognito-info
      key: client_secret
- name: CLIENT_ID
  valueFrom:
    configMapKeyRef:
      name: cognito-info
      key: client_id
- name: SUPPORTED_PAYMENT_METHODS
  value: sepa_dc
- name: KUBERNETES
  value: "true"
{{- end -}}