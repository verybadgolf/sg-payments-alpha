package gov.scot.payments.psps.method.sepadc.ch.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.psps.method.ch.app.PspClientFactory;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.apache.kafka.streams.KafkaStreams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.reactive.server.WebTestClient;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.spring.TestConfiguration;

import java.time.Duration;

@ApplicationIntegrationTest(classes = {SepaDCMethodCHAppTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.COMMAND_HANDLER
        ,properties = {"spring.cloud.discovery.client.simple.instances.adapter1[0].uri=http://localhost:8080"
        ,"spring.cloud.discovery.client.simple.instances.adapter1[0].metadata.scot.gov.payments/payment-method=sepa_dc"
        ,"spring.cloud.discovery.client.simple.instances.adapter1[0].metadata.scot.gov.payments/service-type=psp"
        ,"spring.cloud.discovery.client.simple.instances.adapter1[0].metadata.app.kubernetes.io/name=direct"
        , "state.query.authority=sepa_dc:Read"}
)
public class SepaDCMethodCHAppTest {

    @MockBean
    PspClientFactory clientFactory;

    @BeforeEach
    public void setUp(@Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter, EmbeddedBrokerClient brokerClient, @Value("${namespace}") String namespace){
        brokerClient.getBroker().addTopicsIfNotExists(namespace+"-sepadcmethod-command-errors");
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(10));
    }

    @Test
    public void test(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){

    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends SepaDCMethodCHApplication{

    }
}