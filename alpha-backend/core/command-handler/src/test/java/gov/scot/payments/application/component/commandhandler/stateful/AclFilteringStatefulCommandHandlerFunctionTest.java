package gov.scot.payments.application.component.commandhandler.stateful;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.application.func.PatternMatchingBiFunctionDelegate;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.HasStateVersion;
import gov.scot.payments.testing.command.*;
import io.vavr.Function2;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.security.access.AccessDeniedException;

import java.time.Instant;
import java.util.UUID;

import static gov.scot.payments.application.component.commandhandler.stateful.AclFilteringStatefulCommandHandlerFunction.aclAndLock;
import static gov.scot.payments.application.component.commandhandler.stateful.AclFilteringStatefulCommandHandlerFunction.lock;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class AclFilteringStatefulCommandHandlerFunctionTest {

    @Test
    public void testAcl(){
        Function2<Command, TestEntityState,TestEntity> handler = PatternMatchingBiFunctionDelegate.delegate(b ->
                b.match2(TestCreateCommand.class, aclAndLock( (c, s) -> true, (c, s) -> new TestEntity(c.getKey(), c.getMessageId().toString())))
                .match2(TestUpdateCommand.class, aclAndLock( (c, s) -> false, (c, s) -> s.merge(c)))
                .match2(TestDeleteCommand.class, aclAndLock( (c, s) -> true, (c, s) -> null))
                .match2(TestErrorCommand.class, aclAndLock( (c, s) -> false, (c, s) -> {throw new RuntimeException();})));

        assertThrows(AccessDeniedException.class,() -> handler.apply(new TestUpdateCommand(),null));
        assertThrows(AccessDeniedException.class,() -> handler.apply(new TestErrorCommand(),null));
        assertNull(handler.apply(new TestDeleteCommand(),null));
        assertThat(handler.apply(new TestCreateCommand("1"),null)).isEqualToComparingOnlyGivenFields(new TestEntity("1",""),"id");
    }

    @Test
    public void testLocking(){
        Function2<Command, TestEntityState,TestEntity> handler = PatternMatchingBiFunctionDelegate.delegate(b ->
                b.match2(TestStateCommand.class, lock( (c, s) -> new TestEntity(c.getKey(), c.getMessageId().toString())))
        );
        assertThat(handler.apply(new TestStateCommand("1",null),null)).isEqualToComparingOnlyGivenFields(new TestEntity("1",""),"id");
        assertThat(handler.apply(new TestStateCommand("1",5L),null)).isEqualToComparingOnlyGivenFields(new TestEntity("1",""),"id");
        assertDoesNotThrow(() -> handler.apply(new TestStateCommand("1",null),new TestEntityState(null,null,null,new TestEntity("1",""),4L)));
        assertThrows(OptimisticLockException.class,() -> handler.apply(new TestStateCommand("1",5L),new TestEntityState(null,null,null,new TestEntity("1",""),4L)));
        assertThat(handler.apply(new TestStateCommand("1",5L),new TestEntityState(null,null,null,new TestEntity("1",""),5L))).isEqualToComparingOnlyGivenFields(new TestEntity("1",""),"id");
    }

    @NoArgsConstructor
    @EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
    public class TestStateCommand extends TestCommand implements HasStateVersion {

        @Getter
        private Long stateVersion;

        public TestStateCommand(String key, Long stateVersion) {
            super(key);
            this.stateVersion = stateVersion;
        }

        public TestStateCommand(String key, boolean reply) {
            super(key,reply);
        }

        public TestStateCommand(String key, boolean reply, int executionCount) {
            super(key,reply,executionCount);
        }


        @JsonCreator
        public TestStateCommand(@JsonProperty("messageId") final UUID messageId
                , @JsonProperty("timestamp") final Instant timestamp
                , @JsonProperty("reply") final boolean reply
                , @JsonProperty("executionCount") final int executionCount
                , @JsonProperty("key") final String key) {
            super(messageId, timestamp,reply,executionCount,key);
        }
    }

}