package gov.scot.payments.application.component.commandhandler.stateful;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.application.component.commandhandler.stateful.AggregateState;
import gov.scot.payments.application.component.commandhandler.stateful.TestEntity;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.model.CommandFailureInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

@SuperBuilder
@NoArgsConstructor
public class TestEntityState extends AggregateState<TestEntity> {

    @Getter @Nullable private TestEntity previous;
    @Getter @Nullable private TestEntity current;

    @JsonCreator
    public TestEntityState(@JsonProperty("message") SerializedMessage command
            , @JsonProperty("error") CommandFailureInfo error
            , @JsonProperty("previous") TestEntity previous
            , @JsonProperty("current") TestEntity current
            , @JsonProperty("currentVersion") Long currentVersion) {
        super(command, error, currentVersion);
        this.previous = previous;
        this.current = current;
    }

}
