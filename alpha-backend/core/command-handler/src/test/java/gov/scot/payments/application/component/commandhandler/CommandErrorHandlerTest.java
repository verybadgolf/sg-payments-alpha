package gov.scot.payments.application.component.commandhandler;

import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.model.CommandDeserializationErrorEvent;
import gov.scot.payments.model.HasCause;
import gov.scot.payments.model.HasKey;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.cloud.stream.binder.kafka.KafkaMessageChannelBinder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CommandErrorHandlerTest {

    private CommandErrorHandler handler;
    private Map<String,Object> headers;
    private MessageChannel events;
    private MessageChannel responses;

    @BeforeEach
    void setUp() {
        headers = new HashMap<>();
        events = mock(MessageChannel.class);
        responses = mock(MessageChannel.class);
        handler = new CommandErrorHandler(events,responses,new MicrometerMetrics(new SimpleMeterRegistry()));
    }

    @Test
    void testMalformedMessages() {
        assertThrows(IllegalArgumentException.class,() -> handler.handleCommandErrors(new MessageHeaders(headers),new byte[0]));
        headers.put(HasCause.CORRELATION_ID_HEADER,"a");
        assertThrows(IllegalArgumentException.class,() -> handler.handleCommandErrors(new MessageHeaders(headers),new byte[0]));
        headers.put(HasCause.EXECUTION_COUNT_HEADER,"c");
        assertThrows(IllegalArgumentException.class,() -> handler.handleCommandErrors(new MessageHeaders(headers),new byte[0]));
        headers.put(HasCause.EXECUTION_COUNT_HEADER,"1");
        assertThrows(IllegalArgumentException.class,() -> handler.handleCommandErrors(new MessageHeaders(headers),new byte[0]));
        headers.put(HasCause.CORRELATION_ID_HEADER, UUID.randomUUID().toString());
        handler.handleCommandErrors(new MessageHeaders(headers),new byte[0]);
    }

    @Test
    void testValidMessages() {
        byte[] cause = new byte[1];
        String correlation = UUID.randomUUID().toString();
        headers.put(HasCause.CORRELATION_ID_HEADER, correlation);
        headers.put(HasKey.PARTITION_KEY_HEADER,"b");
        headers.put(HasCause.EXECUTION_COUNT_HEADER,"1");
        headers.put(KafkaMessageChannelBinder.X_EXCEPTION_STACKTRACE,"d");
        headers.put(KafkaMessageChannelBinder.X_EXCEPTION_MESSAGE,"e");
        headers.put(KafkaMessageChannelBinder.X_EXCEPTION_FQCN,"f");
        handler.handleCommandErrors(new MessageHeaders(headers),cause);
        ArgumentCaptor<Message> eventCaptor = ArgumentCaptor.forClass(Message.class);
        ArgumentCaptor<Message> responseCaptor = ArgumentCaptor.forClass(Message.class);
        verify(events,times(1)).send(eventCaptor.capture());
        verify(responses,times(1)).send(responseCaptor.capture());
        Message event = eventCaptor.getValue();
        Message response = responseCaptor.getValue();
        assertThat(event).isEqualToComparingFieldByField(response);
        CommandDeserializationErrorEvent payload = (CommandDeserializationErrorEvent)event.getPayload();
        assertEquals(correlation,payload.getCorrelationId().toString());
        assertEquals(2,payload.getExecutionCount());
        assertEquals(cause,payload.getCause());
        assertEquals("d",payload.getError().getErrorDetail());
        assertEquals("e",payload.getError().getErrorMessage());
        assertEquals("f",payload.getError().getErrorType());
    }

    //check message contents
}