package gov.scot.payments.application.component.commandhandler.stateful;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.func.PatternMatchingBiFunctionDelegate;
import gov.scot.payments.application.kafka.MessageHeaderEnricher;
import gov.scot.payments.application.kafka.MessageKStream;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.model.*;
import gov.scot.payments.testing.command.TestCreateCommand;
import gov.scot.payments.testing.command.TestDeleteCommand;
import gov.scot.payments.testing.command.TestErrorCommand;
import gov.scot.payments.testing.command.TestUpdateCommand;
import gov.scot.payments.testing.event.TestCreateEvent;
import gov.scot.payments.testing.event.TestDeleteEvent;
import gov.scot.payments.testing.event.TestErrorEvent;
import gov.scot.payments.testing.event.TestUpdateEvent;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.Function3;
import io.vavr.Function5;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.Stores;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.kafka.support.JacksonUtils;
import org.springframework.kafka.support.converter.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerde;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.messaging.MessageHeaders;

import java.time.Instant;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class PerCommandStatefulCommandHandlerDelegateTest {

    private PerCommandStatefulCommandHandlerDelegate<TestEntity,TestEntityState,EventWithCauseImpl> processor;
    private KafkaStreamsTestHarness harness;
    private Function3<Command,TestEntity,Throwable,List<EventWithCauseImpl>> errorHandler;
    private TestStateUpdateFunc stateUpdateHandler;
    private TestHandler eventHandler;
    private JsonSerde<TestEntityState> stateSerde;
    private JsonSerde<Command> commandSerde;

    @BeforeEach
    void setUp() throws Exception {
        stateUpdateHandler = spy(new TestStateUpdateFunc());
        eventHandler = spy(new TestHandler());
        errorHandler = spy(new TestErrorHandler());
        final MicrometerMetrics metrics = new MicrometerMetrics(new SimpleMeterRegistry());
        ObjectMapper mapper = JacksonUtils.enhancedObjectMapper();
        mapper.activateDefaultTyping(BasicPolymorphicTypeValidator.builder().allowIfBaseType(Command.class).allowIfSubType(TestEntity.class).build());
        stateSerde = new JsonSerde<>(TestEntityState.class, mapper);
        commandSerde = new JsonSerde<>(Command.class, mapper);
        DefaultJackson2JavaTypeMapper typeMapper = (DefaultJackson2JavaTypeMapper)((JsonDeserializer)commandSerde.deserializer()).getTypeMapper();
        typeMapper.setClassIdFieldName(MessageHeaders.CONTENT_TYPE);
        final String mappingConfig = KafkaStreamsTestHarness.buildMappingsConfig(TestCreateCommand.class,TestUpdateCommand.class,TestDeleteCommand.class, TestErrorCommand.class);
        commandSerde.configure(HashMap.of(JsonSerializer.TYPE_MAPPINGS, mappingConfig).toJavaMap(),false);

        Function5<SerializedMessage, CommandFailureInfo,TestEntity,TestEntity,Long,TestEntityState> aggregateStateSupplier = TestEntityState::new;

        Materialized<byte[], TestEntityState,KeyValueStore<Bytes, byte[]>> store = Materialized.<byte[], TestEntityState>as(Stores.inMemoryKeyValueStore(KafkaStreamsTestHarness.STATE_STORE))
                .withKeySerde(Serdes.ByteArray())
                .withValueSerde(stateSerde);

        processor = PerCommandStatefulCommandHandlerDelegate.builder()
                .aggregateQueryService(mock(AggregateLookupService.class))
                .metrics(metrics)
                .aggregateStateSupplier(aggregateStateSupplier)
                .eventGenerateFunction(eventHandler)
                .errorHandler(errorHandler)
                .stateUpdateFunction(stateUpdateHandler)
                .stateStore(store)
                .commandSerde(commandSerde)
                .stateTopic("stateTopic")
                .stateHeaderEnricher(() -> new MessageHeaderEnricher("test"))
                .serializedMessageSerde(new JsonSerde<>(SerializedMessage.class, mapper))
                .build();
        harness = KafkaStreamsTestHarness.builderWithMappings(TestCreateEvent.class, TestUpdateEvent.class, TestDeleteEvent.class, TestErrorEvent.class
                ,TestCreateCommand.class,TestUpdateCommand.class,TestDeleteCommand.class, TestErrorCommand.class, TestEntity.class, CommandFailureInfo.class, TestEntityState.class)
                .build();

        processor.apply(harness.stream()).to(KafkaStreamsTestHarness.OUTPUT_TOPIC);
    }


    @Test
    public void test(){
        try (final TopologyTestDriver topology = harness.toTopology()) {
            KeyValueWithHeaders<String,TestCreateCommand> create1 = KeyValueWithHeaders.msg("1",new TestCreateCommand("1",false),new RecordHeader(MessageHeaders.CONTENT_TYPE,TestCreateCommand.class.getName().getBytes()));
            KeyValueWithHeaders<String,TestUpdateCommand> update1 = KeyValueWithHeaders.msg("1",new TestUpdateCommand("1"),new RecordHeader(MessageHeaders.CONTENT_TYPE,TestUpdateCommand.class.getName().getBytes()));
            KeyValueWithHeaders<String,TestDeleteCommand> delete1 = KeyValueWithHeaders.msg("1",new TestDeleteCommand("1"),new RecordHeader(MessageHeaders.CONTENT_TYPE,TestDeleteCommand.class.getName().getBytes()));
            KeyValueWithHeaders<String, TestErrorCommand> error1 = KeyValueWithHeaders.msg("1" ,new TestErrorCommand("1"),new RecordHeader(MessageHeaders.CONTENT_TYPE,TestErrorCommand.class.getName().getBytes()));

            harness.sendKeyValues(topology,create1,update1,delete1,error1);
            List<KeyValueWithHeaders<String,Event>> entities = harness.drainKeyValues(topology);
            assertEquals(4,entities.size());
            assertThat(entities.get(0).value).isEqualToIgnoringGivenFields(new TestCreateEvent(UUID.randomUUID(), Instant.now(),"1",create1.value.getMessageId(),1,false),"messageId","timestamp");
            assertThat(entities.get(1).value).isEqualToIgnoringGivenFields(new TestUpdateEvent(UUID.randomUUID(), Instant.now(),"1",update1.value.getMessageId(),1,true),"messageId","timestamp");
            assertThat(entities.get(2).value).isEqualToIgnoringGivenFields(new TestUpdateEvent(UUID.randomUUID(), Instant.now(),"1",update1.value.getMessageId(),1,true),"messageId","timestamp");
            assertThat(entities.get(3).value).isInstanceOf(TestErrorEvent.class);
        }
        verify(stateUpdateHandler,times(4)).apply(any(),any());
        verify(eventHandler,times(4)).apply(any(),any());
    }

    @Test
    public void testState(){
        try (final TopologyTestDriver topology = harness.toTopology()) {
            KeyValueWithHeaders<String,TestCreateCommand> create1 = KeyValueWithHeaders.msg("1",new TestCreateCommand("1",false),new RecordHeader(MessageHeaders.CONTENT_TYPE,TestCreateCommand.class.getName().getBytes()));
            KeyValueWithHeaders<String,TestUpdateCommand> update1 = KeyValueWithHeaders.msg("1",new TestUpdateCommand("1"),new RecordHeader(MessageHeaders.CONTENT_TYPE,TestUpdateCommand.class.getName().getBytes()));

            harness.sendKeyValues(topology,create1,update1);
            List<KeyValueWithHeaders<String,Event>> entities = harness.drainKeyValues(topology);
            //Map<String,AggregateState<TestEntity>> tableState = harness.drainTable(topology,"",stateSerde.deserializer());
            KeyValueStore<byte[], AggregateState<TestEntity>> stateStore = topology.getKeyValueStore(KafkaStreamsTestHarness.STATE_STORE);

            AggregateState<TestEntity> state = stateStore.get("1".getBytes());
            assertNull(state.getError());
            List<TestEntity> fromStateTopic = harness.drain(topology,"stateTopic");
            assertEquals(2,fromStateTopic.size());
        }
        verify(stateUpdateHandler,times(2)).apply(any(),any());
    }

    public static class TestStateUpdateFunc extends PatternMatchingBiFunctionDelegate<Command, TestEntityState,TestEntity> {

        @Override
        protected void handlers(final PatternMatcher.Builder2<Command, TestEntityState,TestEntity> builder) {
            builder.match2(TestCreateCommand.class, (c, s) -> new TestEntity(c.getKey(), c.getMessageId().toString()))
                   .match2(TestUpdateCommand.class,(c,s) -> s.getCurrent().merge(c))
                   .match2(TestDeleteCommand.class,(c,s) -> null)
                   .match2(TestErrorCommand.class,(c,s) -> {throw new RuntimeException();});
        }
    }

    public static class TestHandler extends PatternMatchingBiFunctionDelegate<Command,TestEntityState, List<EventWithCauseImpl>> {

        @Override
        protected void handlers(final PatternMatcher.Builder2<Command, TestEntityState, List<EventWithCauseImpl>> builder) {
            builder.match2(c -> true,AggregateState.failure(),(c,as) -> List.of(new TestErrorEvent(""))) //1 - error event
                   .match2(TestCreateCommand.class,(c,as) -> List.of(new TestCreateEvent(c.getKey()))) //1
                   .match2(TestUpdateCommand.class, (c,as) -> List.of(new TestUpdateEvent(c.getKey()), new TestUpdateEvent(c.getKey()))) //2
                   .match2(TestDeleteCommand.class, (c,as) -> List.of()) //0
                   .build();
        }
    }

    public static class TestErrorHandler implements Function3<Command, TestEntity,Throwable,List<EventWithCauseImpl>> {

        @Override
        public List<EventWithCauseImpl> apply(Command command, TestEntity entity, Throwable throwable) {
            return List.of(GenericErrorEvent.from(command, throwable));
        }
    }

}