package gov.scot.payments.application.component.commandhandler.stateful;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.component.commandhandler.*;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.func.PatternMatchingBiFunctionDelegate;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.model.*;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Scope;
import gov.scot.payments.model.user.Subject;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.command.*;
import gov.scot.payments.testing.event.TestCreateEvent;
import gov.scot.payments.testing.event.TestErrorEvent;
import gov.scot.payments.testing.event.TestEvent;
import gov.scot.payments.testing.event.TestUpdateEvent;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.spring.TestConfiguration;
import io.confluent.kafka.schemaregistry.client.MockSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.vavr.collection.List;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.state.HostInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.reactive.context.ReactiveWebApplicationContext;
import org.springframework.boot.web.reactive.context.ReactiveWebServerInitializedEvent;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.boot.web.server.WebServer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.messaging.MessageHeaders;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.Duration;
import java.util.function.Function;

import static gov.scot.payments.application.component.commandhandler.stateful.AclFilteringStatefulCommandHandlerFunction.aclAndLock;
import static gov.scot.payments.application.component.commandhandler.stateful.AggregateState.failure;
import static gov.scot.payments.testing.security.MockSubjectAuthentication.user;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockAuthentication;

@ApplicationIntegrationTest(classes = {StatefulCommandHandlerIntegrationTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.COMMAND_HANDLER
        ,properties = {"command.packages=gov.scot.payments.application.component.commandhandler,gov.scot.payments.model,gov.scot.payments.testing.event"
        ,"state.query.authority=entity:Read"
        ,"component.name=test"
        ,"context.name=test"})
public class StatefulCommandHandlerIntegrationTest {

    @BeforeEach
    public void setUp(@Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter, EmbeddedBrokerClient brokerClient){
        brokerClient.getBroker().addTopicsIfNotExists("test-test-command-errors");
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(10));
    }


    @Test
    public void testCommandWithReplySuccess(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient, @Value("${state.topic}") String stateTopic){
        client
                .mutateWith(mockAuthentication(user("test", Role.parse("world/entity:Write"))))
                .put()
                .uri(uri -> uri.path("/command/command3")
                        .queryParam("reply",true)
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"hello\"")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.key").isEqualTo("hello");

        List<KeyValueWithHeaders<String,TestCommand3>> commands = brokerClient.readAllKeyValuesFromDestination("commands", TestCommand3.class);
        assertEquals(1, commands.size());
        assertEquals("hello",commands.get(0).value.getKey());
        assertNotNull(commands.get(0).headers.lastHeader(Message.CONTEXT_HEADER));
        assertNotNull(commands.get(0).headers.lastHeader(MessageHeaders.CONTENT_TYPE));
        List<KeyValueWithHeaders<String, TestCreateEvent>> events = brokerClient.readAllKeyValuesFromDestination("events", TestCreateEvent.class);
        assertEquals(1, events.size());
        assertEquals("hello",events.get(0).value.getKey());
        assertNotNull(events.get(0).headers.lastHeader(Message.CONTEXT_HEADER));
        assertNotNull(events.get(0).headers.lastHeader(MessageHeaders.CONTENT_TYPE));
        List<KeyValueWithHeaders<String,TestCreateEvent>> responses = brokerClient.readAllKeyValuesFromDestination("responses", TestCreateEvent.class);
        assertEquals(1, responses.size());
        assertEquals("hello",responses.get(0).value.getKey());
        assertNotNull(responses.get(0).headers.lastHeader(Message.CONTEXT_HEADER));
        assertNotNull(responses.get(0).headers.lastHeader(MessageHeaders.CONTENT_TYPE));
        assertEquals(0,brokerClient.readAllFromTopic("test-test-command-errors", TestCommand.class).size());

        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("world/entity:Read"))))
                .get()
                .uri("/state/hello")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.id").isEqualTo("hello");

        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("world/entity:Write"))))
                .get()
                .uri("/state/hello")
                .exchange()
                .expectStatus()
                .isForbidden();

        client
                .mutateWith(mockAuthentication(user("fail", Role.parse("hello/entity:Read"))))
                .get()
                .uri("/state/hello")
                .exchange()
                .expectStatus()
                .isForbidden();

        List<TestEntity> state = brokerClient.readAllFromTopic(stateTopic,TestEntity.class);
        assertEquals(1,state.size());
    }

    @Test
    public void testCommandWithReplyError(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){
        client
                .mutateWith(mockAuthentication(user("test", Role.parse("test/entity:Write"))))
                .put()
                .uri(uri -> uri.path("/command/command1")
                        .queryParam("reply",true)
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"hello\"")
                .exchange()
                .expectStatus()
                .isBadRequest()
                .expectBody()
                .jsonPath("$.error.errorMessage").isEqualTo("world");

        List<KeyValueWithHeaders<String,TestCommand1>> commands = brokerClient.readAllKeyValuesFromDestination("commands", TestCommand1.class);
        assertEquals(1, commands.size());
        assertEquals("hello",commands.get(0).value.getKey());
        assertNotNull(commands.get(0).headers.lastHeader(Message.CONTEXT_HEADER));
        assertNotNull(commands.get(0).headers.lastHeader(MessageHeaders.CONTENT_TYPE));
        List<KeyValueWithHeaders<String,GenericErrorEvent>> events = brokerClient.readAllKeyValuesFromDestination("events", GenericErrorEvent.class);
        assertEquals(1, events.size());
        assertEquals("world",events.get(0).value.getError().getErrorMessage());
        assertNotNull(events.get(0).headers.lastHeader(Message.CONTEXT_HEADER));
        assertNotNull(events.get(0).headers.lastHeader(MessageHeaders.CONTENT_TYPE));
        List<KeyValueWithHeaders<String,GenericErrorEvent>> responses = brokerClient.readAllKeyValuesFromDestination("responses", GenericErrorEvent.class);
        assertEquals(1, responses.size());
        assertEquals("world",responses.get(0).value.getError().getErrorMessage());
        assertNotNull(responses.get(0).headers.lastHeader(Message.CONTEXT_HEADER));
        assertNotNull(responses.get(0).headers.lastHeader(MessageHeaders.CONTENT_TYPE));
        assertEquals(0,brokerClient.readAllFromTopic("test-test-command-errors", TestCommand.class).size());

    }

    @Test
    public void testCommandWithReplyAclFailure(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){
        client
                .mutateWith(mockAuthentication(user("fail", Role.parse("test/entity:Write"))))
                .put()
                .uri(uri -> uri.path("/command/command3")
                        .queryParam("reply",true)
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"hello\"")
                .exchange()
                .expectStatus()
                .isForbidden()
                .expectBody()
                .jsonPath("$.reason").isEqualTo("Access Denied");

        List<KeyValueWithHeaders<String,TestCommand3>> commands = brokerClient.readAllKeyValuesFromDestination("commands", TestCommand3.class);
        assertEquals(1, commands.size());
        assertEquals("hello",commands.get(0).value.getKey());
        assertNotNull(commands.get(0).headers.lastHeader(Message.CONTEXT_HEADER));
        assertNotNull(commands.get(0).headers.lastHeader(MessageHeaders.CONTENT_TYPE));
        List<KeyValueWithHeaders<String,AccessDeniedEvent>> events = brokerClient.readAllKeyValuesFromDestination("events", AccessDeniedEvent.class);
        assertEquals(1, events.size());
        assertEquals("Access Denied",events.get(0).value.getReason());
        assertNotNull(events.get(0).headers.lastHeader(Message.CONTEXT_HEADER));
        assertNotNull(events.get(0).headers.lastHeader(MessageHeaders.CONTENT_TYPE));
        List<KeyValueWithHeaders<String,AccessDeniedEvent>> responses = brokerClient.readAllKeyValuesFromDestination("responses", AccessDeniedEvent.class);
        assertEquals(1, responses.size());
        assertEquals("Access Denied",responses.get(0).value.getReason());
        assertNotNull(responses.get(0).headers.lastHeader(Message.CONTEXT_HEADER));
        assertNotNull(responses.get(0).headers.lastHeader(MessageHeaders.CONTENT_TYPE));
        assertEquals(0,brokerClient.readAllFromTopic("test-test-command-errors", TestCommand.class).size());

    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends StatefulCommandHandlerApplication<TestEntity,TestEntityState, EventWithCauseImpl> {

        @Override
        protected Class<TestEntity> stateEntityClass() {
            return TestEntity.class;
        }

        @Override
        protected Class<TestEntityState> stateClass() {
            return TestEntityState.class;
        }

        @Override
        protected TestEntityState createNewAggregateInstance(SerializedMessage command, CommandFailureInfo error, TestEntity previous, TestEntity current, Long currentVersion) {
            return new TestEntityState(command, error, previous, current, currentVersion);
        }

        @Override
        protected Scope extractScope(final TestEntityState state) {
            return new Scope("world");
        }

        @Bean
        public TestStateUpdateFunc stateUpdateFunc(){
            return new TestStateUpdateFunc();
        }

        @Bean
        public TestHandler testHandler(){
            return new TestHandler();
        }

    }

    public static class TestStateUpdateFunc extends StateUpdateFunction.PatternMatching<TestEntityState,TestEntity> {

        @Override
        protected void handlers(final PatternMatcher.Builder2<Command, TestEntityState,TestEntity> builder) {
            builder.match2(TestCommand3.class,aclAndLock( (c, s) -> !c.getUser().equals("fail") ,(c, s) -> new TestEntity(c.getKey(), c.getMessageId().toString())))
                    .match2(TestCommand4.class,aclAndLock( (c, s) -> true ,(c, s) -> s))
                    .match2(TestCommand1.class,aclAndLock( (c, s) -> true , (c, s) -> {throw new RuntimeException("world");}));
        }
    }

    public static class TestHandler extends EventGeneratorFunction.PatternMatching<TestEntityState, EventWithCauseImpl> {

        @Override
        protected void handlers(final PatternMatcher.Builder2<Command, TestEntityState, List<EventWithCauseImpl>> builder) {
            builder.match2(TestCommand3.class, (c,s) -> List.of(new TestCreateEvent(c.getKey())))
                    .match2(TestCommand4.class, (c,s) -> List.of())
                    .match2(c -> true, failure(), (c,s) -> List.of(GenericErrorEvent.from(c,s.getError())));
        }
    }
}
