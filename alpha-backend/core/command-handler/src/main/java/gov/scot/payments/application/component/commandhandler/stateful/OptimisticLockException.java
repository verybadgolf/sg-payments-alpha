package gov.scot.payments.application.component.commandhandler.stateful;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class OptimisticLockException extends RuntimeException {

    private final Long actual;
    private final Long provided;

    @Override
    public String getMessage() {
        return String.format("Expected version: %s, got version: %s",provided,actual);
    }
}
