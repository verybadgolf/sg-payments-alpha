package gov.scot.payments.application.component.commandhandler.stateful;

import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.user.Subject;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.vavr.control.Option;
import lombok.Builder;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.state.HostInfo;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.springframework.cloud.stream.binder.kafka.streams.InteractiveQueryService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

import java.util.function.BiPredicate;

import static gov.scot.payments.util.LazyLogMessageEvaulator.msg;

@RequestMapping("/state")
@Slf4j
@Builder
public class AggregateLookupService<T, S extends AggregateState<T>> {

    private static final Serializer<byte[]> KEY_SERIALIZER = Serdes.ByteArray().serializer();

    @NonNull private final InteractiveQueryService queryService;
    @NonNull private final WebClient webClient;
    @NonNull private final Metrics metrics;
    @NonNull private final Class<S> entityClass;
    @Builder.Default private final BiPredicate<S,Subject> aclCheck = (s,sub) -> true;

    private String store;

    @GetMapping("/{key}")
    @ApiResponses({
            @ApiResponse(responseCode = "200" ,ref = "stateSchema")
            ,@ApiResponse(responseCode = "404" ,description = "not found")
            ,@ApiResponse(responseCode = "403" ,description = "not authorized")
    })
    public Mono<ResponseEntity<T>> getAggregateById(@PathVariable String key, @AuthenticationPrincipal Subject subject) {
        log.info("Retrieving Entity with id: {}",key);
        return Mono.fromSupplier(() -> getHostInfo(key))
                .flatMap(h -> queryService.getCurrentHostInfo().equals(h) ? Mono.justOrEmpty(getQueryableStore().get(key.getBytes())) : lookupRemote(h, key))
                .map(s -> checkAcl(s,subject))
                .map(body -> {
                    ResponseEntity.BodyBuilder resp = ResponseEntity.ok();
                    String stateVersion = Option.of(body.getCurrentVersion())
                                                .map(Object::toString)
                                                .getOrNull();
                    if(stateVersion != null){
                        resp = resp.header(HttpHeaders.ETAG, stateVersion);
                    }
                    return resp.body(body.getCurrent());
                })
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    private HostInfo getHostInfo(final String key) {
        return queryService.getHostInfo(store,key.getBytes(),KEY_SERIALIZER);
    }

    @GetMapping("/local/{key}")
    @Hidden
    public Mono<S> getAggregateByIdLocal(@PathVariable String key, @AuthenticationPrincipal Subject subject ) {
        log.info("Entity with id: {} is managed by this host, performing local lookup",key);
        return Mono.fromSupplier(metrics.time("state.lookup.local",() -> Option.of(getQueryableStore().get(key.getBytes()))))
                .filter(Option::isDefined)
                .map(state -> checkAcl(state.get(),subject));
    }

    public void init(String queryableStoreName) {
        this.store = queryableStoreName;
    }

    private ReadOnlyKeyValueStore<byte[], S> getQueryableStore() {
        return queryService.getQueryableStore(store, QueryableStoreTypes.keyValueStore());
    }

    private Mono<S> lookupRemote(HostInfo hostInfo, String key) {
        log.info("Entity with id: {} is not managed by this host, performing remote lookup to {}"
                ,key
                ,msg(() -> String.format("https://%s:%s/state/local/%s",hostInfo.host(),hostInfo.port(),key)));
        return webClient.get()
                        .uri("https://{host}:{port}/state/local/{key}",hostInfo.host(),hostInfo.port(),key)
                        .retrieve()
                        .onStatus(HttpStatus.NOT_FOUND::equals, r -> Mono.empty())
                        .bodyToMono(entityClass);
    }

    private S checkAcl(S state, Subject subject) {
        if(!aclCheck.test(state,subject)){
            throw new AccessDeniedException("Access Denied");
        }
        return state;
    }

}
