package gov.scot.payments.application.component.commandhandler;

import gov.scot.payments.application.MessageTypeRegistry;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.user.Subject;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.vavr.Tuple2;
import io.vavr.collection.Map;
import io.vavr.control.Option;
import io.vavr.control.Try;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.reactive.FluxSender;
import org.springframework.core.ResolvableType;
import org.springframework.core.codec.Decoder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.messaging.Message;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.util.MimeType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.lang.reflect.Method;
import java.time.Duration;
import java.util.Collections;

@Slf4j
public class MessageTypeRegistryCommandHandlerResource extends CommandHandlerResource {

    @Getter private final Map<String, MessageTypeRegistry.MessageConstructorSpec<? extends Command,?>> commands;

    private final Decoder<Object> decoder;

    public MessageTypeRegistryCommandHandlerResource(Flux<Message<Event>> responses
            , final FluxSender commandSender
            , final MessageTypeRegistry registry
            , final String context
            , final Decoder<Object> decoder) {
        super(commandSender,responses,registry);
        this.decoder = decoder;
        this.commands = registry.getAllConstructorsForContext(Command.class,context)
                .toMap(i -> new Tuple2<>(registry.getInfo(i.getMessageType()).get().getType(),i));
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE
            ,produces = MediaType.APPLICATION_JSON_VALUE
            ,value = "/{command}")
    @ApiResponses({
            @ApiResponse(responseCode = "404" ,description = "not found")
            ,@ApiResponse(responseCode = "403" ,description = "not authorized")
            ,@ApiResponse(responseCode = "400" ,description = "bad request")
            ,@ApiResponse(responseCode = "412" ,description = "precondition failed")
    })
    public Mono<ResponseEntity<Object>> handleCommand(@PathVariable("command") String command
            , @RequestParam(name = "timeout",required = false, defaultValue = "PT5S") Duration timeout
            , @RequestParam(name = "reply",required = false, defaultValue = "false") boolean reply
            , @RequestHeader(name = HttpHeaders.IF_MATCH, required = false) String targetVersionString
            , @AuthenticationPrincipal Subject subject
            , ServerHttpRequest request
    ){
        Option<MessageTypeRegistry.MessageConstructorSpec<? extends Command,?>> commandClass = commands.get(command);
        if(commandClass.isEmpty()){
            return Mono.just(ResponseEntity.notFound().build());
        }

        Long targetVersion = Option.of(targetVersionString)
                .map(s -> s.replace("\"", ""))
                .map(Long::parseLong)
                .getOrNull();

        Mono<? extends Command> commandMono = decoder.decodeToMono(request.getBody(), ResolvableType.forClass(commandClass.get().getRequestArgumentType()), MimeType.valueOf(MediaType.APPLICATION_JSON_VALUE), Collections.emptyMap())
                .map(body -> constructCommand(commandClass.get().getMessageType(), commandClass.get().getConstructor(), body, targetVersion, reply, subject));
        return execute(timeout, commandMono);
    }

    <T extends Command,V> T constructCommand(final Class<T> commandClass
            , Method constructorMethod
            , V constructorArg
            , Long targetVersion
            , boolean reply
            , Subject subject) {
        return Try.of(() -> commandClass.cast(constructorMethod.invoke(null,constructorArg,reply,targetVersion,subject)))
                .onFailure(t -> {
                    log.debug("Failed to create a {} from request: body={}, stateVersion={}, reply={}, subject={}",
                            commandClass.getSimpleName(), constructorArg, targetVersion, reply, subject, t);
                })
                .get();
    }


}
