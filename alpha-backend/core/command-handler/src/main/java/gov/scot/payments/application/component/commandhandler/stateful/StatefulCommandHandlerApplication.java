package gov.scot.payments.application.component.commandhandler.stateful;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.application.MessageDeDuplicator;
import gov.scot.payments.application.VavrModelResolver;
import gov.scot.payments.application.component.commandhandler.CommandHandler;
import gov.scot.payments.application.component.commandhandler.CommandHandlerBaseApplication;
import gov.scot.payments.application.component.commandhandler.CommandHandlerBinding;
import gov.scot.payments.application.component.commandhandler.CommandHandlerDelegate;
import gov.scot.payments.application.component.commandhandler.DefaultCommandHandler;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.application.security.Oauth2ServiceToServiceConfiguration;
import gov.scot.payments.application.security.SecurityCustomizer;
import gov.scot.payments.model.*;
import gov.scot.payments.model.user.Action;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Scope;
import gov.scot.payments.model.user.Subject;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.swagger.v3.core.converter.AnnotatedType;
import io.swagger.v3.core.converter.ModelConverter;
import io.swagger.v3.core.converter.ModelConverterContext;
import io.swagger.v3.core.converter.ModelConverterContextImpl;
import io.swagger.v3.core.converter.ModelConverters;
import io.swagger.v3.core.jackson.ModelResolver;
import io.swagger.v3.oas.models.media.Schema;
import io.vavr.collection.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.ValueTransformerWithKeySupplier;
import org.apache.kafka.streams.state.KeyValueBytesStoreSupplier;
import org.apache.kafka.streams.state.KeyValueStore;
import org.springdoc.api.OpenApiCustomiser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.binder.kafka.streams.InteractiveQueryService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiPredicate;
import java.util.function.Function;

@Import(Oauth2ServiceToServiceConfiguration.class)
@PropertySource(ResourceUtils.CLASSPATH_URL_PREFIX + "application-stateful-command-handler.properties")
@EnableBinding(CommandHandlerBinding.class)
@Slf4j
public abstract class StatefulCommandHandlerApplication<State,AS extends AggregateState<State>,T extends Event & HasCause> extends CommandHandlerBaseApplication<T> {

    protected abstract Class<State> stateEntityClass();
    protected abstract Class<AS> stateClass();
    protected abstract AS createNewAggregateInstance(SerializedMessage command, CommandFailureInfo error, State previous, State current, Long currentVersion);
    protected abstract Scope extractScope(AS state);

    //stateUpdateFunction
    //eventGenerateFunction

    @Bean
    public OpenApiCustomiser lookupStateCustomizer(ObjectMapper mapper){
        return api -> {
            ModelConverter converter = new VavrModelResolver(mapper);
            ModelConverterContext converterContext = new ModelConverterContextImpl(java.util.List.of(converter));
            Schema<State> schema = converterContext.resolve(new AnnotatedType().type(stateEntityClass()));
            converterContext.getDefinedModels().entrySet().forEach( e -> api.getComponents().addSchemas(e.getKey(),e.getValue()));
            api.getComponents().addSchemas("stateSchema",schema);
        };
    }

    @Bean
    public Materialized<byte[], AS, KeyValueStore<Bytes, byte[]>> stateStore(@Value("${component.name}") String appName
            , Function<String, KeyValueBytesStoreSupplier> keyValueBytesStoreSupplier
            , SchemaRegistryClient confluentSchemaRegistryClient){
        return Materialized.<byte[], AS>as(keyValueBytesStoreSupplier.apply(appName + "-store"))
                .withCachingDisabled()
                .withKeySerde(Serdes.ByteArray())
                .withValueSerde(stateSerde(confluentSchemaRegistryClient,stateClass()))
                .withCachingDisabled();
    }

    @Bean
    public PerCommandStatefulCommandHandlerDelegate<State, AS,T> delegate(StateUpdateFunction<AS,State> stateUpdateFunction
            , @Autowired(required = false) StateTableToEventStreamFunction<AS,T> stateTableToEventStreamFunction
            , @Autowired(required = false) EventGeneratorFunction<AS,T> eventGenerateFunction
            , StatefulErrorHandler<State,T> errorHandler
            , AggregateLookupService<State,AS> aggregateQueryService
            , Materialized<byte[], AS, KeyValueStore<Bytes, byte[]>> stateStore
            , Metrics metrics
            , @Value("${state.topic}") String stateTopic
            , @Qualifier("eventHeaderEnricher") ValueTransformerWithKeySupplier<byte[], AS,AS> stateHeaderEnricher
            , SchemaRegistryClient confluentSchemaRegistryClient) {
        return PerCommandStatefulCommandHandlerDelegate.<State, AS, T>builder()
                .aggregateQueryService(aggregateQueryService)
                .aggregateStateSupplier(this::createNewAggregateInstance)
                .errorHandler(errorHandler)
                .eventGenerateFunction(eventGenerateFunction)
                .stateToEvents(stateTableToEventStreamFunction)
                .metrics(metrics)
                .stateUpdateFunction(stateUpdateFunction)
                .stateStore(stateStore)
                .commandSerde(valueSerde(Command.class))
                .stateTopic(stateTopic)
                .stateHeaderEnricher(stateHeaderEnricher)
                .serializedMessageSerde(stateSerde(confluentSchemaRegistryClient,SerializedMessage.class))
                .build();
    }

    @Bean
    @ConditionalOnMissingBean
    public CommandHandler<T> commandHandler(MessageDeDuplicator<Command,T> deDuplicator
            , CommandHandlerDelegate<T> delegate){
        return DefaultCommandHandler.<T>builder()
                .delegate(delegate)
                .deDuplicator(deDuplicator)
                .build();
    }

    @Bean
    public AggregateLookupService<State,AS> aggregateQueryService(InteractiveQueryService queryService
            , @Qualifier("serviceToServiceWebClient") WebClient webClient
            , @Value("${state.query.authority:}") String authority
            , Metrics metrics){
        BiPredicate<AS, Subject> aclCheck;
        if(!StringUtils.isEmpty(authority)){
            aclCheck = (s,sub) -> {
                Scope extracted = extractScope(s);
                log.info("extracted scope: {}",extracted);
                Role role = new Role(Action.parse(authority),extracted);
                return sub.hasAccess(role);
            };
        } else {
            aclCheck = (s,sub) -> true;
        }

        return AggregateLookupService.<State, AS>builder()
                .aclCheck(aclCheck)
                .entityClass(stateClass())
                .metrics(metrics)
                .queryService(queryService)
                .webClient(webClient)
                .build();
    }

    @Bean
    public SecurityCustomizer lookupServiceSecurity(@Value("${state.query.authority:}") String authority){
        return r -> {
            if(!StringUtils.isEmpty(authority)){
                r.pathMatchers(HttpMethod.GET,"/state/**").hasAuthority(authority);
            }
        };
    }

    @Bean
    @ConditionalOnMissingBean
    public StatefulErrorHandler<State,?> defaultErrorHandler(){
        return (c,s,t) -> List.of(GenericErrorEvent.from(c,t));
    }

    // Create state topic if auto-create-topics is set (e.g., when running locally)
    @Bean
    @ConditionalOnProperty(prefix = "kafka", name = "auto-create-topics", havingValue = "true")
    public NewTopic stateTopic(@Value("${state.topic}") String topicName) {
        return TopicBuilder
                .name(topicName)
                .partitions(1)
                .replicas(1)
                .compact()
                .build();
    }
}
