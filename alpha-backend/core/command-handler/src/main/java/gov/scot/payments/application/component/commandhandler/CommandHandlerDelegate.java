package gov.scot.payments.application.component.commandhandler;

import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.HasCause;
import org.apache.kafka.streams.kstream.KStream;

import java.util.function.Function;

public interface CommandHandlerDelegate<T extends Event & HasCause> extends Function<KStream<byte[], Command>, KStream<byte[],T>> {
}
