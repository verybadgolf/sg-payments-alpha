package gov.scot.payments.application.component.commandhandler;

import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.HasCause;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.SendTo;

import static gov.scot.payments.application.kafka.MessageKStream.messageStream;

@Slf4j
@SuperBuilder
public class DefaultCommandHandler<T extends Event & HasCause> extends CommandHandler<T> {

    @NonNull private final CommandHandlerDelegate<T> delegate;

    @StreamListener
    @SendTo({"events","responses"})
    public KStream<?, ?>[] handle(@Input("commands") KStream<byte[], Command> commands) throws Exception {
        KStream<byte[],T> events = deDuplicator.deduplicate(messageStream(commands).unwrap(),delegate,"handle");
        return handleResponses(events);
    }


}
