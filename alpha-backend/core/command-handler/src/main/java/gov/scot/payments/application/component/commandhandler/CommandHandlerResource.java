package gov.scot.payments.application.component.commandhandler;

import com.google.common.base.Charsets;
import gov.scot.payments.application.MessageTypeRegistry;
import gov.scot.payments.model.*;
import io.vavr.control.Option;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.reactive.FluxSender;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.web.bind.annotation.RequestMapping;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.concurrent.TimeoutException;

@Slf4j
@RequestMapping("/command")
public abstract class CommandHandlerResource {

    private final FluxSender commandSender;
    private final Flux<Message<Event>> responses;
    private final MessageTypeRegistry registry;

    public CommandHandlerResource(FluxSender commandSender, Flux<Message<Event>> responses, MessageTypeRegistry registry) {
        this.commandSender = commandSender;
        this.responses = responses;
        this.registry = registry;
        this.responses.subscribe();
    }

    protected Mono<VoidObject> sendCommand(Command command){
        Message message = buildMessage(command);
        return commandSender.send(Flux.just(message)).map(v -> new VoidObject()).defaultIfEmpty(new VoidObject());
    }

     protected Mono<Event> sendCommandAndWait(Command command, Duration timeout){
        if(!command.isReply()){
            return Mono.error(new IllegalArgumentException("Command is not replying"));
        }
        return Mono.defer(() -> commandSender.send(Flux.just(buildMessage(command))))
                .thenMany(responses
                        .filter(m -> command.getMessageId().toString().equals(m.getHeaders().get(HasCause.CORRELATION_ID_HEADER,String.class)))
                        .map(Message::getPayload))
                .next()
                .doOnNext(e -> {})
                .timeout(timeout)
                .cache();
    }

    protected Mono<ResponseEntity<Object>> execute(Duration timeout, Mono<? extends Command> commandMono) {
        return commandMono
                .flatMap(t -> t.isReply() ? sendCommandAndWait(t,timeout): sendCommand(t))
                .map(response -> {
                    if(AccessDeniedEvent.class.isAssignableFrom(response.getClass())){
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(response);
                    } else if(OptimisticLockFailureEvent.class.isAssignableFrom(response.getClass())){
                        return ResponseEntity
                                .status(HttpStatus.PRECONDITION_FAILED)
                                .body(response);
                    } else if(GenericErrorEvent.class.isAssignableFrom(response.getClass())){
                        return ResponseEntity
                                .status(HttpStatus.BAD_REQUEST)
                                .body(response);
                    }
                    else{
                        ResponseEntity.BodyBuilder resp = ResponseEntity.ok();
                        if(VoidObject.class.isAssignableFrom(response.getClass())){
                            return ResponseEntity.ok().build();
                        }
                        if(HasStateVersion.class.isAssignableFrom(response.getClass())){
                            String stateVersion = Option.of(  ((HasStateVersion) response).getStateVersion())
                                    .map(Object::toString)
                                    .getOrNull();
                            if(stateVersion != null){
                                resp = resp.header(HttpHeaders.ETAG, stateVersion);
                            }
                        }
                        return resp.body(response);
                    }
                })
                /*error occured creating or submitting command, or a timeout was received*/
                .onErrorResume(error -> {
                    if(TimeoutException.class.isAssignableFrom(error.getClass())){
                        return Mono.just(ResponseEntity.status(HttpStatus.REQUEST_TIMEOUT).body("No response received within timeout period"));
                    } else{
                        return Mono.just(ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error.getMessage()));
                    }
                })
                /*success with no reply*/
                .defaultIfEmpty(ResponseEntity.ok().build());
    }

    private Message buildMessage(Command command) {
        byte[] key = null;
        if(command instanceof HasKey){
            HasKey hasKey = (HasKey)command;
            key = Option.of(hasKey.getKey())
                    .map(Object::toString)
                    .map(s -> s.getBytes(Charsets.UTF_8))
                    .getOrNull();
        }
        MessageTypeRegistry.MessageTypeInfo info = registry.getInfo(command.getClass()).get();
        return MessageBuilder.withPayload(command)
                .setHeader(gov.scot.payments.model.Message.CONTEXT_HEADER,info.getContext())
                .setHeader(gov.scot.payments.model.Message.TYPE_HEADER,info.getType())
                .setHeader(KafkaHeaders.MESSAGE_KEY,key)
                .build();
    }


}
