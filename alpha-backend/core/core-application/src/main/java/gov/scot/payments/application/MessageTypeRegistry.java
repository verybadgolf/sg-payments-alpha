package gov.scot.payments.application;

import gov.scot.payments.model.Message;
import gov.scot.payments.model.MessageConstructor;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import lombok.Value;
import org.apache.kafka.common.header.Headers;

import java.lang.reflect.Method;

public interface MessageTypeRegistry {

    Option<Class<?>> getForType(String context, String type);
    <T> List<Class<? extends T>> getAllForContext(String context);
    Option<MessageTypeInfo> getInfo(Class<?> clazz);
    <T> Option<MessageConstructorSpec<? extends T,?>> getConstructor(Class<T> messageType);

    default Option<Class<?>> getForType(Headers headers){
        String context = Option.of(headers.lastHeader(Message.CONTEXT_HEADER)).map(b -> new String(b.value())).getOrNull();
        String type = Option.of(headers.lastHeader(Message.TYPE_HEADER)).map(b -> new String(b.value())).getOrNull();
        return getForType(context,type);
    }

    default boolean matchesType(Headers headers, Class<?>... clazz){
        return getForType(headers).filter(c -> HashSet.of(clazz).contains(c)).isDefined();
    }

    default <T> Option<Class<? extends T>> getForType(Class<T> baseClass, String context, String type){
        return getForType(context, type)
                .filter(baseClass::isAssignableFrom)
                .map(c -> (Class<T>) c);
    }

    default <T> List<Class<? extends T>> getAllForContext(Class<T> baseClass, String context){
        return getAllForContext(context)
                .filter(baseClass::isAssignableFrom)
                .map(c -> (Class<T>) c);
    }

    default <T> List<MessageConstructorSpec<? extends T,?>> getAllConstructorsForContext(Class<T> baseClass, String context){
        return getAllForContext(baseClass,context)
                .map(this::getConstructor)
                .filter(Option::isDefined)
                .map(Option::get);
    }

    @Value
    class MessageTypeInfo {

        private String context;
        private String type;

        public boolean matchesHeaders(final Headers h) {
            String context = Option.of(h.lastHeader(Message.CONTEXT_HEADER)).map(b -> new String(b.value())).getOrNull();
            String type = Option.of(h.lastHeader(Message.TYPE_HEADER)).map(b -> new String(b.value())).getOrNull();
            return this.context.equals(context) && this.type.equals(type);
        }
    }

    @Value
    class MessageConstructorSpec<T,C> {
        private Class<T> messageType;
        private Method constructor;
        private MessageConstructor annotation;
        private Class<C> requestArgumentType;
    }
}
