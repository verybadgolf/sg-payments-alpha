package gov.scot.payments.application.kafka;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.streams.kstream.ValueTransformerWithKey;
import org.apache.kafka.streams.processor.ProcessorContext;

import java.util.function.Predicate;

@RequiredArgsConstructor
public class RichFilter<K,T> implements ValueTransformerWithKey<K,T,T> {

    private ProcessorContext context;
    private final Predicate<KeyValueWithHeaders<K, T>> filter;

    @Override
    public void init(final ProcessorContext context) {
        this.context = context;
    }

    @Override
    public T transform(K key, final T value) {
        Headers headers = context.headers();
        if(filter.test(new KeyValueWithHeaders<>(key,value,context.timestamp(),headers))){
            return value;
        }
        return null;
    }

    @Override
    public void close() {

    }
}
