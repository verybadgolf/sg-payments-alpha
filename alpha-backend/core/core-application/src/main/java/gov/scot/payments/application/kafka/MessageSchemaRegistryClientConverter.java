package gov.scot.payments.application.kafka;

import gov.scot.payments.application.MessageTypeRegistry;
import gov.scot.payments.avro.VavrDatumReader;
import gov.scot.payments.avro.VavrDatumWriter;
import gov.scot.payments.avro.VavrReflectData;
import io.vavr.control.Option;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Conversions;
import org.apache.avro.Schema;
import org.apache.avro.data.TimeConversions;
import org.apache.avro.generic.GenericContainer;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.reflect.ReflectData;
import org.apache.avro.reflect.ReflectDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.avro.specific.SpecificRecord;
import org.springframework.beans.DirectFieldAccessor;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cloud.stream.schema.ParsedSchema;
import org.springframework.cloud.stream.schema.SchemaNotFoundException;
import org.springframework.cloud.stream.schema.SchemaReference;
import org.springframework.cloud.stream.schema.SchemaRegistrationResponse;
import org.springframework.cloud.stream.schema.avro.AvroSchemaRegistryClientMessageConverter;
import org.springframework.cloud.stream.schema.avro.SubjectNamingStrategy;
import org.springframework.cloud.stream.schema.client.SchemaRegistryClient;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.converter.MessageConversionException;
import org.springframework.util.Assert;
import org.springframework.util.MimeType;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;

@Slf4j
public class MessageSchemaRegistryClientConverter extends AvroSchemaRegistryClientMessageConverter {

    private final MessageTypeRegistry registry;
    private final SchemaRegistryClient schemaRegistryClient;
    private final CacheManager cacheManager;

    public MessageSchemaRegistryClientConverter(SchemaRegistryClient schemaRegistryClient
            , CacheManager cacheManager
            , MessageTypeRegistry registry
            , SubjectNamingStrategy subjectNamingStrategy) {
        super(schemaRegistryClient, cacheManager);
        this.registry = registry;
        this.cacheManager = cacheManager;
        this.schemaRegistryClient = schemaRegistryClient;
        setSubjectNamingStrategy(subjectNamingStrategy);
    }

    @Override
    protected DatumReader<Object> getDatumReader(final Class<Object> type, final Schema schema, final Schema writerSchema) {
        DatumReader<Object>  reader = new VavrDatumReader<>(type);
        reader.setSchema(writerSchema);
        log.debug("reading {} using schemas {} \n {}",type,schema,writerSchema);
        return reader;
    }

    @Override
    protected Object convertFromInternal(Message<?> message, Class<?> targetClass, Object conversionHint) {
        String type = message.getHeaders().get(gov.scot.payments.model.Message.TYPE_HEADER,String.class);
        String context = message.getHeaders().get(gov.scot.payments.model.Message.CONTEXT_HEADER,String.class);
        final Class<?> clazz = registry.getForType(context, type)
                .orElse(() -> registry.getForType("*", type))
                .getOrElse(targetClass);
        log.debug("Using class {} for message type {}:{}",clazz,context,type);
        return super.convertFromInternal(message, clazz, conversionHint);
    }

    @Override
    protected Object convertToInternal(Object payload, MessageHeaders headers,
                                       Object conversionHint) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            MimeType hintedContentType = null;
            if (conversionHint instanceof MimeType) {
                hintedContentType = (MimeType) conversionHint;
            }
            Schema schema = resolveSchemaForWriting(payload, headers, hintedContentType);
            log.debug("schema written for {}, {}",payload,schema.toString());
            @SuppressWarnings("unchecked")
            DatumWriter<Object> writer = getDatumWriter(
                    (Class<Object>) payload.getClass(), schema);
            Encoder encoder = EncoderFactory.get().binaryEncoder(baos, null);
            writer.write(payload, encoder);
            encoder.flush();
        }
        catch (IOException e) {
            throw new MessageConversionException("Failed to write payload", e);
        }
        return baos.toByteArray();
    }

    @Override
    protected Schema resolveSchemaForWriting(Object payload, MessageHeaders headers,
                                             MimeType hintedContentType) {

        Schema schema;
        schema = extractSchemaForWriting(payload);
        ParsedSchema parsedSchema = this.getCache(REFERENCE_CACHE_NAME).get(schema,
                ParsedSchema.class);

        if (parsedSchema == null) {
            parsedSchema = new ParsedSchema(schema);
            this.getCache(REFERENCE_CACHE_NAME).putIfAbsent(schema, parsedSchema);
        }

        if (parsedSchema.getRegistration() == null) {
            final String subject = toSubject(schema);
            log.debug("Registering schema for {} when writing {}",subject,payload);
            SchemaRegistrationResponse response = this.schemaRegistryClient.register(
                    subject, AVRO_FORMAT, parsedSchema.getRepresentation());
            parsedSchema.setRegistration(response);
        }

        SchemaReference schemaReference = parsedSchema.getRegistration()
                .getSchemaReference();

        DirectFieldAccessor dfa = new DirectFieldAccessor(headers);
        @SuppressWarnings("unchecked")
        Map<String, Object> _headers = (Map<String, Object>) dfa
                .getPropertyValue("headers");
        _headers.put(MessageHeaders.CONTENT_TYPE,
                "application/" + "vnd" + "." + schemaReference.getSubject() + ".v"
                        + schemaReference.getVersion() + "+" + AVRO_FORMAT);
        log.debug("Using schema: {} version: {} (content type: {}) to serialize {}",schemaReference.getSubject(),schemaReference.getVersion(),_headers.get(MessageHeaders.CONTENT_TYPE),payload);

        return schema;
    }

    @Override
    protected Schema resolveWriterSchemaForDeserialization(MimeType mimeType) {
        log.debug("reading using written mime: {}",mimeType);
        return super.resolveWriterSchemaForDeserialization(mimeType);
    }

    private Schema extractSchemaForWriting(Object payload) {
        Schema schema = null;
        if (this.logger.isDebugEnabled()) {
            this.logger.debug("Obtaining schema for class " + payload.getClass());
        }
        if (GenericContainer.class.isAssignableFrom(payload.getClass())) {
            schema = ((GenericContainer) payload).getSchema();
            if (this.logger.isDebugEnabled()) {
                this.logger.debug("Avro type detected, using schema from object");
            }
        }
        else {
            schema = this.getCache(REFLECTION_CACHE_NAME)
                    .get(payload.getClass().getName(), Schema.class);
            if (schema == null) {
                if (!isDynamicSchemaGenerationEnabled()) {
                    throw new SchemaNotFoundException(String.format(
                            "No schema found in the local cache for %s, and dynamic schema generation "
                                    + "is not enabled",
                            payload.getClass()));
                }
                else {
                    schema = VavrReflectData.get().getSchema(payload.getClass());
                }
                this.getCache(REFLECTION_CACHE_NAME).put(payload.getClass().getName(),
                        schema);
            }
        }
        return schema;
    }

    private Cache getCache(String name) {
        Cache cache = this.cacheManager.getCache("");
        Assert.notNull(cache, "Cache by the name '" + name
                + "' is not present in this CacheManager - '" + this.cacheManager
                + "'. Typically caches are auto-created by the CacheManagers. "
                + "Consider reporting it as an issue to the developer of this CacheManager.");
        return cache;
    }

    private DatumWriter<Object> getDatumWriter(Class<Object> type, Schema schema) {
        DatumWriter<Object> writer;
        this.logger.debug("Finding correct DatumWriter for type " + type.getName());
        if (SpecificRecord.class.isAssignableFrom(type)) {
            if (schema != null) {
                writer = new SpecificDatumWriter<>(schema);
            }
            else {
                writer = new SpecificDatumWriter<>(type);
            }
        }
        else if (GenericRecord.class.isAssignableFrom(type)) {
            writer = new GenericDatumWriter<>(schema);
        }
        else {
            if (schema != null) {
                writer = new VavrDatumWriter<>(schema);
            }
            else {
                writer = new VavrDatumWriter<>(type);
            }
        }
        return writer;
    }
}