package gov.scot.payments.application.metrics;

import io.micrometer.core.instrument.*;
import io.micrometer.core.instrument.binder.MeterBinder;
import io.micrometer.core.lang.NonNullApi;
import io.micrometer.core.lang.NonNullFields;
import io.micrometer.core.lang.Nullable;

import javax.management.*;
import java.lang.management.ManagementFactory;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.function.ToDoubleFunction;

import static java.util.Collections.emptyList;

@NonNullApi
@NonNullFields
public class KafkaStreamsMetrics implements MeterBinder, AutoCloseable {
    private static final String JMX_DOMAIN = "kafka.streams";
    private static final String METRIC_NAME_PREFIX = "kafka.streams.";

    private final MBeanServer mBeanServer;

    private final Iterable<Tag> tags;

    @Nullable
    private Integer kafkaMajorVersion;

    private final List<Runnable> notificationListenerCleanUpRunnables = new CopyOnWriteArrayList<>();

    public KafkaStreamsMetrics() {
        this(emptyList());
    }

    public KafkaStreamsMetrics(Iterable<Tag> tags) {
        this(getMBeanServer(), tags);
    }

    public KafkaStreamsMetrics(MBeanServer mBeanServer, Iterable<Tag> tags) {
        this.mBeanServer = mBeanServer;
        this.tags = tags;
    }

    private static MBeanServer getMBeanServer() {
        List<MBeanServer> mBeanServers = MBeanServerFactory.findMBeanServer(null);
        if (!mBeanServers.isEmpty()) {
            return mBeanServers.get(0);
        }
        return ManagementFactory.getPlatformMBeanServer();
    }

    @Override
    public void bindTo(MeterRegistry registry) {
        registerMetricsEventually("stream-metrics", (o, tags) -> {
            registerFunctionCounterForObject(registry, o, "skipped-records-total", tags, "The total number of records skipped.", "records");
            for(String type : new String[]{"punctuate","poll","commit","process"}){
                registerTimeGaugeForObject(registry, o, String.format("%s-latency-avg",type), tags, String.format("The average time taken for a %s request",type));
                registerTimeGaugeForObject(registry, o, String.format("%s-latency-max",type), tags, String.format("The max time taken for a %s request",type));
            }
        });

        registerMetricsEventually("task-metrics", (o, tags) -> {
            registerTimeGaugeForObject(registry, o, "record-lateness-avg", tags, "The average time records are delayed");
            registerTimeGaugeForObject(registry, o, "record-lateness-max", tags, "The max time records were delayed");
        });
    }

    private void registerGaugeForObject(MeterRegistry registry, ObjectName o, String jmxMetricName, String meterName, Tags allTags, String description, @Nullable String baseUnit) {
        final AtomicReference<Gauge> gauge = new AtomicReference<>();
        gauge.set(Gauge
                .builder(METRIC_NAME_PREFIX + meterName, mBeanServer,
                        getJmxAttribute(registry, gauge, o, jmxMetricName))
                .description(description)
                .baseUnit(baseUnit)
                .tags(allTags)
                .register(registry));
    }

    private void registerGaugeForObject(MeterRegistry registry, ObjectName o, String jmxMetricName, Tags allTags, String description, @Nullable String baseUnit) {
        registerGaugeForObject(registry, o, jmxMetricName, sanitize(jmxMetricName), allTags, description, baseUnit);
    }

    private void registerFunctionCounterForObject(MeterRegistry registry, ObjectName o, String jmxMetricName, Tags allTags, String description, @Nullable String baseUnit) {
        final AtomicReference<FunctionCounter> counter = new AtomicReference<>();
        counter.set(FunctionCounter
                .builder(METRIC_NAME_PREFIX + sanitize(jmxMetricName), mBeanServer,
                        getJmxAttribute(registry, counter, o, jmxMetricName))
                .description(description)
                .baseUnit(baseUnit)
                .tags(allTags)
                .register(registry));
    }

    private void registerTimeGaugeForObject(MeterRegistry registry, ObjectName o, String jmxMetricName,
            String meterName, Tags allTags, String description, TimeUnit timeUnit) {
        final AtomicReference<TimeGauge> timeGauge = new AtomicReference<>();
        timeGauge.set(TimeGauge.builder(METRIC_NAME_PREFIX + meterName, mBeanServer, timeUnit,
                getJmxAttribute(registry, timeGauge, o, jmxMetricName))
                .description(description)
                .tags(allTags)
                .register(registry));
    }

    private void registerTimeGaugeForObject(MeterRegistry registry, ObjectName o, String jmxMetricName,
            String meterName, Tags allTags, String description) {
        registerTimeGaugeForObject(registry, o, jmxMetricName, meterName, allTags, description, TimeUnit.MILLISECONDS);
    }

    private ToDoubleFunction<MBeanServer> getJmxAttribute(MeterRegistry registry, AtomicReference<? extends Meter> meter,
                                                          ObjectName o, String jmxMetricName) {
        return s -> safeDouble(jmxMetricName, () -> {
            if (!s.isRegistered(o)) {
                registry.remove(meter.get());
            }
            return s.getAttribute(o, jmxMetricName);
        });
    }

    private void registerTimeGaugeForObject(MeterRegistry registry, ObjectName o, String jmxMetricName, Tags allTags, String description) {
        registerTimeGaugeForObject(registry, o, jmxMetricName, sanitize(jmxMetricName), allTags, description);
    }

    int kafkaMajorVersion(Tags tags) {
        if (kafkaMajorVersion == null || kafkaMajorVersion == -1) {
            kafkaMajorVersion = tags.stream().filter(t -> "client.id".equals(t.getKey())).findAny()
                    .map(clientId -> {
                        try {
                            String version = (String) mBeanServer.getAttribute(new ObjectName(JMX_DOMAIN + ":type=app-info,client-id=" + clientId.getValue()), "version");
                            return Integer.parseInt(version.substring(0, version.indexOf('.')));
                        } catch (Throwable e) {
                            // this can happen during application bootstrapping
                            // in this case, JMX bean is not available yet and version cannot be determined yet
                            return -1;
                        }
                    })
                    .orElse(-1);
        }
        return kafkaMajorVersion;
    }

    private void registerMetricsEventually(String type, BiConsumer<ObjectName, Tags> perObject) {
        try {
            Set<ObjectName> objs = mBeanServer.queryNames(new ObjectName(JMX_DOMAIN + ":type=" + type + ",*"), null);
            if (!objs.isEmpty()) {
                for (ObjectName o : objs) {
                    perObject.accept(o, Tags.concat(tags, nameTag(o)));
                }
                return;
            }
        } catch (MalformedObjectNameException e) {
            throw new RuntimeException("Error registering Kafka JMX based metrics", e);
        }

        registerNotificationListener(type, perObject);
    }

    /**
     * This notification listener should remain indefinitely since new Kafka consumers can be added at any time.
     *
     * @param type      The Kafka JMX type to listen for.
     * @param perObject Metric registration handler when a new MBean is created.
     */
    private void registerNotificationListener(String type, BiConsumer<ObjectName, Tags> perObject) {
        NotificationListener notificationListener = (notification, handback) -> {
            MBeanServerNotification mbs = (MBeanServerNotification) notification;
            ObjectName o = mbs.getMBeanName();
            perObject.accept(o, Tags.concat(tags, nameTag(o)));
        };

        NotificationFilter filter = (NotificationFilter) notification -> {
            if (!MBeanServerNotification.REGISTRATION_NOTIFICATION.equals(notification.getType()))
                return false;
            ObjectName obj = ((MBeanServerNotification) notification).getMBeanName();
            return obj.getDomain().equals(JMX_DOMAIN) && obj.getKeyProperty("type").equals(type);
        };

        try {
            mBeanServer.addNotificationListener(MBeanServerDelegate.DELEGATE_NAME, notificationListener, filter, null);
            notificationListenerCleanUpRunnables.add(() -> {
                try {
                    mBeanServer.removeNotificationListener(MBeanServerDelegate.DELEGATE_NAME, notificationListener);
                } catch (InstanceNotFoundException | ListenerNotFoundException ignored) {
                }
            });
        } catch (InstanceNotFoundException e) {
            throw new RuntimeException("Error registering Kafka MBean listener", e);
        }
    }

    private double safeDouble(String jmxMetricName, Callable<Object> callable) {
        try {
            return Double.parseDouble(callable.call().toString());
        } catch (Exception e) {
            return Double.NaN;
        }
    }

    private Iterable<Tag> nameTag(ObjectName name) {
        Tags tags = Tags.empty();

        String clientId = name.getKeyProperty("thread.client-id");
        if (clientId != null) {
            tags = Tags.concat(tags, "client.id", clientId);
        }

        clientId = name.getKeyProperty("client-id");
        if (clientId != null) {
            tags = Tags.concat(tags, "client.id", clientId);
        }

        String task = name.getKeyProperty("task-id");
        if (task != null) {
            tags = Tags.concat(tags, "task", task);
        }

        return tags;
    }

    private static String sanitize(String value) {
        return value.replaceAll("-", ".");
    }

    @Override
    public void close() {
        notificationListenerCleanUpRunnables.forEach(Runnable::run);
    }
}
