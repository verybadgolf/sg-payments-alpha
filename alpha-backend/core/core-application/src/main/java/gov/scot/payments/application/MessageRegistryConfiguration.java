package gov.scot.payments.application;

import io.vavr.collection.HashMap;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.util.StringUtils;

@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
public class MessageRegistryConfiguration {

    @Bean
    public MessageTypeRegistry messageTypeRegistry(@Value("${command.packages:gov.scot.payments}") String[] pkgs
            ,@Value("${context.name}") String context
            ,@Value("${context.parent.name:}") String parentContext){
        if(!StringUtils.isEmpty(parentContext)){
            return new ReflectiveMessageTypeRegistry(HashMap.of(parentContext,context),pkgs);
        }
        return new ReflectiveMessageTypeRegistry(pkgs);
    }
}
