package gov.scot.payments.application.func;

import java.util.function.Consumer;
import java.util.function.Function;

public abstract class PatternMatchingFunctionDelegate<T,K> extends PatternMatchingDelegate<T,K> implements Function<T, K> {

    @Override
    public K apply(final T value) {
        return matcher.of(value);
    }


    public static <T,K> Function<T, K> delegate(Consumer<PatternMatcher.Builder<T,K>> c){
        return new PatternMatchingFunctionDelegate<>() {
            @Override
            protected void handlers(PatternMatcher.Builder<T, K> builder) {
                c.accept(builder);
            }
        };
    }
}
