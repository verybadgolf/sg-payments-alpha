package gov.scot.payments.application.kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.To;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import static gov.scot.payments.util.LazyLogMessageEvaulator.msg;

@Slf4j
@RequiredArgsConstructor
public class DelayTransformer<K,V> implements Transformer<K, V, KeyValue<K,V>> {

    private final Duration delay;

    private ProcessorContext processorContext;

    @Override
    public void init(ProcessorContext context) {
        this.processorContext = context;
    }

    @Override
    public KeyValue<K,V> transform(K key, V value) {
        long timestamp = processorContext.timestamp();
        long currentTime = System.currentTimeMillis();
        Duration recordAge = Duration.ofMillis(currentTime - timestamp);
        Duration wait = delay.minus(recordAge);

        if(!wait.isNegative() && !wait.isZero()){
            try {
                log.info("Target Delay {}, Sleeping for {} until {} before sending message with original delivery time {}"
                        , delay
                        , wait
                        , msg(() -> LocalDateTime.ofInstant(Instant.ofEpochMilli(currentTime + wait.toMillis()), ZoneId.systemDefault()))
                        , msg(() -> LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), ZoneId.systemDefault())));
                Thread.sleep(wait.toMillis());
            } catch (InterruptedException e) {}
        }
        log.info("Sending message key: {} value: {}",key,value);
        processorContext.forward(key,value, To.all().withTimestamp(System.currentTimeMillis()));
        return null;
    }

    @Override
    public void close() {
        // NO-OP
    }
}
