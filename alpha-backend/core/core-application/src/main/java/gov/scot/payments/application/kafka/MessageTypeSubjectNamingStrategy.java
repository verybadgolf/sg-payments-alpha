package gov.scot.payments.application.kafka;

import gov.scot.payments.application.MessageTypeRegistry;
import io.vavr.control.Option;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.springframework.cloud.stream.schema.avro.DefaultSubjectNamingStrategy;

@RequiredArgsConstructor
@Slf4j
public class MessageTypeSubjectNamingStrategy extends DefaultSubjectNamingStrategy {

    @NonNull private final String namespace;
    @NonNull private final String context;
    @NonNull private final MessageTypeRegistry registry;

    @Override
    public String toSubject(Schema schema) {
        Class<?> clazz;
        try {
            clazz = Class.forName(schema.getFullName());
        } catch (ClassNotFoundException e) {
            try {
                clazz = Class.forName(String.format("%s$%s",schema.getNamespace(),schema.getName()));
            } catch (ClassNotFoundException e1) {
                log.error("Could not extract subject from schema {}",schema);
                return null;
            }
        }

        final String subject = registry.getInfo(clazz)
                .map(i -> {
                    String context = i.getContext().equals("*") ? this.context : i.getContext();
                    return context+"1"+i.getType();
                }).orElse(() -> Option.of(super.toSubject(schema)))
                .map(s -> (namespace + "0" + s).toLowerCase())
                .get();
        log.debug("Using subject name {} for schema {}",subject,schema);
        return subject;
    }
}
