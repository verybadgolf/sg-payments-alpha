package gov.scot.payments.application.kafka;

import gov.scot.payments.application.MessageTypeRegistry;
import gov.scot.payments.model.*;
import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.streams.kstream.ValueTransformerWithKey;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.springframework.messaging.MessageHeaders;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Slf4j
@RequiredArgsConstructor
public class MessageHeaderEnricher<T> implements ValueTransformerWithKey<byte[],T,T> {

    @NonNull private final String context;
    @NonNull private final Function<Class<?>,Option<MessageTypeRegistry.MessageTypeInfo>> registry;
    private final Map<Class<?>,Header> typeHeaderCache = new HashMap<>();

    public MessageHeaderEnricher(String context){
        this.context = context;
        this.registry = c -> Option.of(c.getAnnotation(MessageType.class)).map(a -> new MessageTypeRegistry.MessageTypeInfo(a.context(),a.type()));
    }

    private ProcessorContext processorContext;

    @Override
    public void init(ProcessorContext context) {
        this.processorContext = context;
    }

    @Override
    public T transform(byte[] key,T value) {
        Headers headers = this.processorContext.headers();
        log.debug("Headers before enriching {}",headers);
        headers.remove(Message.CONTEXT_HEADER);
        headers.remove(Message.TYPE_HEADER);
        headers.remove(HasCause.CORRELATION_ID_HEADER);
        headers.remove(HasCause.EXECUTION_COUNT_HEADER);
        headers.remove(HasKey.PARTITION_KEY_HEADER);
        headers.remove(MessageHeaders.CONTENT_TYPE);
        headers.add(header(Message.CONTEXT_HEADER, context));
        if(key != null){
            headers.add(new RecordHeader(HasKey.PARTITION_KEY_HEADER, key));
        }

        if(value != null){
            Header type = getTypeHeader(value.getClass());
            if(type != null){
                headers.add(type);
            }

            if(HasCause.class.isAssignableFrom(value.getClass())){
                final HasCause cause = (HasCause) value;
                String correlationId = cause.getCorrelationId().toString();
                headers.add(header(HasCause.CORRELATION_ID_HEADER, correlationId));
                int executionCount = cause.getExecutionCount();
                headers.add(header(HasCause.EXECUTION_COUNT_HEADER, String.valueOf(executionCount)));
            }
            if(HasAdditionalHeaders.class.isAssignableFrom(value.getClass())){
                final HasAdditionalHeaders withHeaders = (HasAdditionalHeaders) value;
                withHeaders.additionalHeaders()
                        .forEach( (k,v) -> {
                            headers.remove(k);
                            headers.add(header(k,v));
                        });
            }
        }

        log.debug("Headers after enriching {}",headers);
        return value;
    }

    private RecordHeader header(final String contextHeader, final String context) {
        return new RecordHeader(contextHeader, context.getBytes(StandardCharsets.UTF_8));
    }

    private synchronized Header getTypeHeader(final Class<?> clazz) {
        Header header = typeHeaderCache.get(clazz);
        if(header == null){
            Option<MessageTypeRegistry.MessageTypeInfo> info = registry.apply(clazz);
            if(info.isDefined()){
                MessageTypeRegistry.MessageTypeInfo typeInfo = info.get();
                if(!typeInfo.getContext().equals(context) && !typeInfo.getContext().equals("*")){
                    throw new InvalidContextException(String.format("Illegal attempt to write message %s:%s from context %s",typeInfo.getContext(),typeInfo.getType(),context));
                }
                header = header(Message.TYPE_HEADER, typeInfo.getType());
                typeHeaderCache.put(clazz,header);
            }
        }
        return header;
    }

    @Override
    public void close() {
        // NO-OP
    }

}
