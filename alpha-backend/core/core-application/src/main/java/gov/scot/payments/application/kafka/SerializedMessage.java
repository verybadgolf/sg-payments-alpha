package gov.scot.payments.application.kafka;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.kafka.common.serialization.Serde;

@Getter
@NoArgsConstructor
@ToString
public class SerializedMessage {

    private byte[] message;
    private MessageInfo info;

    @JsonCreator
    public SerializedMessage(@JsonProperty("message") final byte[] message
            ,@JsonProperty("info") final MessageInfo info) {
        this.message = message;
        this.info = info;
    }

    public <T> T deseralize(Serde<T> serde){
        return serde.deserializer().deserialize("",info.toHeaders(),message);
    }
}
