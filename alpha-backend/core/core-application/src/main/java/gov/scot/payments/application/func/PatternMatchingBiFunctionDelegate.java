package gov.scot.payments.application.func;

import io.vavr.Function2;
import io.vavr.Tuple2;

import java.util.function.Consumer;

public abstract class PatternMatchingBiFunctionDelegate<T,T1,K> implements Function2<T,T1, K> {

    protected abstract void handlers(PatternMatcher.Builder2<T,T1,K> builder);

    protected final PatternMatcher<Tuple2<T,T1>,K> matcher;

    public PatternMatchingBiFunctionDelegate(){
        matcher = PatternMatcher.<T,T1,K>builder2()
                .apply2(this::handlers)
                .build();
    }

    public PatternMatchingBiFunctionDelegate(K wildcard){
        matcher = PatternMatcher.<T,T1,K>builder2()
                .apply2(this::handlers)
                .orElse(wildcard)
                .build();
    }

    @Override
    public K apply(final T t, final T1 t1) {
        return matcher.of(new Tuple2<>(t,t1));
    }

    public static <T,T1,K> Function2<T,T1, K> delegate(Consumer<PatternMatcher.Builder2<T,T1,K>> c){
        return new PatternMatchingBiFunctionDelegate<>() {
            @Override
            protected void handlers(PatternMatcher.Builder2<T, T1, K> builder) {
                c.accept(builder);
            }
        };
    }
}
