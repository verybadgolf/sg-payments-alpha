package gov.scot.payments.application.kafka;

import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.apache.kafka.streams.KeyValue;

public class KeyValueWithHeaders<K, V> extends KeyValue<K, V> {

    /**
     * Timestamp of Kafka message (milliseconds since the epoch).
     */
    public final Headers headers;
    public final long timestamp;

    public KeyValueWithHeaders(final K key, final V value, long timestamp, final Headers headers) {
        super(key, value);
        this.headers = headers;
        this.timestamp = timestamp;
    }

    public KeyValueWithHeaders(final K key, final V value) {
        super(key, value);
        this.headers = new RecordHeaders();
        this.timestamp = System.currentTimeMillis();
    }
}
