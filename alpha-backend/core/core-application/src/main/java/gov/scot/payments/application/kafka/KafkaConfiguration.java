package gov.scot.payments.application.kafka;

import gov.scot.payments.application.MessageTypeRegistry;
import io.confluent.kafka.schemaregistry.client.CachedSchemaRegistryClient;
import io.vavr.Function4;
import lombok.NonNull;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.state.KeyValueBytesStoreSupplier;
import org.apache.kafka.streams.state.Stores;
import org.apache.kafka.streams.state.WindowBytesStoreSupplier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.cloud.stream.annotation.StreamMessageConverter;
import org.springframework.cloud.stream.binder.PartitionKeyExtractorStrategy;
import org.springframework.cloud.stream.binder.PartitionSelectorStrategy;
import org.springframework.cloud.stream.binder.kafka.streams.KafkaHeaderConfiguration;
import org.springframework.cloud.stream.schema.avro.AvroMessageConverterProperties;
import org.springframework.cloud.stream.schema.avro.SubjectNamingStrategy;
import org.springframework.cloud.stream.schema.client.ConfluentSchemaRegistryClient;
import org.springframework.cloud.stream.schema.client.SchemaRegistryClient;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.kafka.config.KafkaStreamsCustomizer;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.function.Function;

@Configuration
@EnableConfigurationProperties({ AvroMessageConverterProperties.class })
@Import(KafkaHeaderConfiguration.class)
public class KafkaConfiguration {

    @Bean
    public KafkaStreamsCustomizer kafkaStreamsCustomizer(ApplicationEventPublisher eventPublisher){
        return ks -> ks.setStateListener((KafkaStreams.StateListener) (newState, oldState) -> eventPublisher.publishEvent(newState));
    }

    @Bean
    @StreamMessageConverter
    MessageConverter kafkaNullConverter() {
        return new KafkaNullConverter();
    }

    @Bean
    @ConditionalOnMissingBean
    public io.confluent.kafka.schemaregistry.client.SchemaRegistryClient confluentSchemaRegistryClient(@Value("${schema.registry.url}") String endpoint
            , @Value("${schema.registry.key}") String key
            , @Value("${schema.registry.secret}") String secret){
        java.util.Map<String,Object> clientConfig = new java.util.HashMap<>();
        clientConfig.put("basic.auth.credentials.source","USER_INFO");
        clientConfig.put("schema.registry.basic.auth.user.info",String.format("%s:%s",key,secret));
        return new CachedSchemaRegistryClient(endpoint,10,clientConfig);
    }

    @Bean
    @ConditionalOnMissingBean
    public SchemaRegistryClient springSchemaRegistryClient(@Value("${schema.registry.url}") String endpoint
        , @Value("${schema.registry.key}") String key
        , @Value("${schema.registry.secret}") String secret){
        RestTemplate template = new RestTemplate();
        template.getInterceptors().add(
                new BasicAuthenticationInterceptor(key, secret));
        ConfluentSchemaRegistryClient client = new ConfluentSchemaRegistryClient(template);
        client.setEndpoint(endpoint);
        return client;
    }

    @Bean
    @ConditionalOnMissingBean
    @StreamMessageConverter
    public MessageSchemaRegistryClientConverter avroSchemaMessageConverter(SchemaRegistryClient schemaRegistryClient
            , MessageTypeRegistry messageTypeRegistry
            , SubjectNamingStrategy subjectNamingStrategy
            , AvroMessageConverterProperties avroMessageConverterProperties){
        final MessageSchemaRegistryClientConverter messageSchemaRegistryClientConverter = new MessageSchemaRegistryClientConverter(schemaRegistryClient
                , new ConcurrentMapCacheManager()
                , messageTypeRegistry
                , subjectNamingStrategy);
        messageSchemaRegistryClientConverter.setDynamicSchemaGenerationEnabled(avroMessageConverterProperties.isDynamicSchemaGenerationEnabled());
        return messageSchemaRegistryClientConverter;
    }

    @Bean
    @ConditionalOnMissingBean
    public SubjectNamingStrategy subjectNamingStrategy(@Value("${namespace}") String namespace
            ,@Value("${context.name}") String context
            , MessageTypeRegistry registry){
        return new MessageTypeSubjectNamingStrategy(namespace,context,registry);
    }

    @Bean
    @ConditionalOnMissingBean
    public PartitionKeyExtractorStrategy partitionKeyExtractor() {
        return new MessagePartitionKeyExtractor();
    }

    @Bean
    @ConditionalOnMissingBean
    public PartitionSelectorStrategy partitionSelector() {
        return new MessagePartitionSelector();
    }

    @Bean
    @ConditionalOnMissingBean
    public Function<String, KeyValueBytesStoreSupplier> keyValueBytesStoreSupplier(){
        return Stores::persistentKeyValueStore;
    }

    @Bean
    @ConditionalOnMissingBean
    public Function4<String, Duration,Duration,Boolean, WindowBytesStoreSupplier> windowBytesStoreSupplier(){
        return Stores::persistentWindowStore;
    }
}
