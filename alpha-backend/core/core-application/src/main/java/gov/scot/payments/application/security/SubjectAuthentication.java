package gov.scot.payments.application.security;

import gov.scot.payments.model.user.Subject;
import org.springframework.security.oauth2.server.resource.authentication.AbstractOAuth2TokenAuthenticationToken;

import java.util.Map;

public class SubjectAuthentication extends AbstractOAuth2TokenAuthenticationToken<CognitoJwt> {

    public SubjectAuthentication(final CognitoJwt token
            , final Subject principal) {
        super(token, principal, token, principal.getAuthorities().toJavaSet());
        this.setAuthenticated(true);
    }

    @Override
    public Map<String, Object> getTokenAttributes() {
        return getToken().getClaims();
    }
}
