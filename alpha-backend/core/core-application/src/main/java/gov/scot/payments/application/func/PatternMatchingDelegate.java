package gov.scot.payments.application.func;

public abstract class PatternMatchingDelegate<T,K> {

    protected abstract void handlers(PatternMatcher.Builder<T,K> builder);

    protected final PatternMatcher<T,K> matcher;

    public PatternMatchingDelegate(){
        matcher = PatternMatcher.<T,K>builder()
                .apply(this::handlers)
                .build();
    }

    public PatternMatchingDelegate(K wildcard){
        matcher = PatternMatcher.<T,K>builder()
                .apply(this::handlers)
                .orElse(wildcard)
                .build();
    }

}
