package org.springframework.cloud.stream.binder.kafka.streams;

import com.google.common.base.Charsets;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.kstream.ValueTransformerWithKey;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.springframework.beans.DirectFieldAccessor;
import org.springframework.cloud.stream.binder.kafka.streams.KafkaStreamsMessageConversionDelegate;
import org.springframework.cloud.stream.binder.kafka.streams.properties.KafkaStreamsBinderConfigurationProperties;
import org.springframework.cloud.stream.config.BindingProperties;
import org.springframework.cloud.stream.converter.CompositeMessageConverterFactory;
import org.springframework.kafka.support.KafkaHeaderMapper;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.messaging.support.MessageBuilder;

import java.util.HashMap;
import java.util.Map;


public class HeaderPropogatingKafkaStreamsMessageConversionDelegate extends KafkaStreamsMessageConversionDelegate {

    private final KafkaHeaderMapper headerMapper;
    private final WrappedKafkaStreamsBindingInformationCatalogue KafkaStreamsBindingInformationCatalogue;
    private final CompositeMessageConverterFactory compositeMessageConverterFactory;

    public HeaderPropogatingKafkaStreamsMessageConversionDelegate(final CompositeMessageConverterFactory compositeMessageConverterFactory
            , final SendToDlqAndContinue sendToDlqAndContinue
            , final WrappedKafkaStreamsBindingInformationCatalogue kafkaStreamsBindingInformationCatalogue
            , final KafkaStreamsBinderConfigurationProperties binderConfigurationProperties
            , KafkaHeaderMapper headerMapper) {
        super(compositeMessageConverterFactory, sendToDlqAndContinue, kafkaStreamsBindingInformationCatalogue, binderConfigurationProperties);
        this.headerMapper = headerMapper;
        this.KafkaStreamsBindingInformationCatalogue = kafkaStreamsBindingInformationCatalogue;
        this.compositeMessageConverterFactory = compositeMessageConverterFactory;
    }

    @SuppressWarnings("unchecked")
    @Override
    public KStream serializeOnOutbound(KStream<?, ?> outboundBindTarget) {
        String contentType = this.KafkaStreamsBindingInformationCatalogue.getContentType(outboundBindTarget);
        MessageConverter messageConverter = this.compositeMessageConverterFactory.getMessageConverterForAllRegistered();
        return outboundBindTarget.transformValues(() -> new HeaderPreservingConverter(messageConverter,contentType));
    }

    @SuppressWarnings("unchecked")
    @Override
    public KStream deserializeOnInbound(final Class<?> valueClass, final KStream<?, ?> bindingTarget) {
        KStream stream = bindingTarget.transformValues(() -> new SpringHeaderEnricher());
        registerUpdatedBindings(bindingTarget,stream);
        return super.deserializeOnInbound(valueClass,stream);
    }

    @SuppressWarnings("unchecked")
    private void registerUpdatedBindings(final KStream<?, ?> original, final KStream updated) {
        DirectFieldAccessor accessor = new DirectFieldAccessor(KafkaStreamsBindingInformationCatalogue);
        Map<KStream<?, ?>, BindingProperties> bindingProperties = (Map<KStream<?, ?>, BindingProperties>) accessor.getPropertyValue("bindingProperties");
        KafkaStreamsBindingInformationCatalogue.registerAlias(original,updated);
        KafkaStreamsBindingInformationCatalogue.registerBindingProperties(updated,bindingProperties.get(original));
    }

    private class HeaderPreservingConverter implements ValueTransformerWithKey {

        private ProcessorContext processorContext;

        private final MessageConverter messageConverter;

        private final String contentType;

        public HeaderPreservingConverter(MessageConverter messageConverter, String contentType) {
            this.messageConverter = messageConverter;
            this.contentType = contentType;
        }

        @Override
        public void init(ProcessorContext context) {
            this.processorContext = context;
        }

        @Override
        public Object transform(Object readOnlyKey, Object value) {
            processorContext.headers().remove(MessageHeaders.CONTENT_TYPE);
            Map<String,Object> headers = new HashMap<>();
            headerMapper.toHeaders(processorContext.headers(),headers);
            headers.put(MessageHeaders.CONTENT_TYPE,contentType);
            Message<?> message;
            if(value instanceof Message){
                message = MessageBuilder.fromMessage((Message<?>) value)
                        .copyHeaders(headers)
                        .build();
            } else {
                message = MessageBuilder.withPayload(value).copyHeaders(headers)
                        .build();
            }
            Message<?> converted = messageConverter.toMessage(message.getPayload(),
                            message.getHeaders());
            processorContext.headers().add(MessageHeaders.CONTENT_TYPE,converted.getHeaders().get(MessageHeaders.CONTENT_TYPE).toString().getBytes(Charsets.UTF_8));
            return converted.getPayload();
        }


        @Override
        public void close() {

        }
    }

    private class SpringHeaderEnricher<T> implements ValueTransformer<T,Message<?>> {

        private ProcessorContext processorContext;

        @Override
        public void init(ProcessorContext context) {
            this.processorContext = context;
        }

        @Override
        public Message<?> transform(T value) {
            if(value != null){
                Map<String,Object> headers = new HashMap<>();
                headerMapper.toHeaders(processorContext.headers(),headers);
                if(value instanceof Message){
                    return MessageBuilder.fromMessage((Message<?>) value)
                                         .copyHeaders(headers)
                                            .build();
                } else {
                    return MessageBuilder.withPayload(value).copyHeaders(headers)
                            .build();
                }
            }
            return null;
        }

        @Override
        public void close() {

        }
    }
}
