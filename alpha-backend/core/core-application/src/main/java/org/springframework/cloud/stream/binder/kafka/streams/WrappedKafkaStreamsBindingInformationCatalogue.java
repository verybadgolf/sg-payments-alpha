package org.springframework.cloud.stream.binder.kafka.streams;

import org.apache.kafka.streams.kstream.KStream;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class WrappedKafkaStreamsBindingInformationCatalogue extends KafkaStreamsBindingInformationCatalogue {

    private final Map<KStream, KStream> aliases = new ConcurrentHashMap<>();

    public void registerAlias(KStream orig, KStream updated){
        aliases.put(updated,orig);
    }

    @Override
    boolean isDlqEnabled(final KStream<?, ?> bindingTarget) {
        try{
            return super.isDlqEnabled(bindingTarget);
        } catch (Exception e){
            return super.isDlqEnabled(aliases.get(bindingTarget));
        }

    }
}
