package gov.scot.payments.application.kafka;

import gov.scot.payments.model.Message;
import gov.scot.payments.model.MessageImpl;
import gov.scot.payments.testing.event.TestCreateEvent;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.vavr.collection.List;
import io.vavr.control.Option;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.UUID;

import static gov.scot.payments.application.kafka.RichKStream.rich;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class RichKStreamTest {

    @Test
    void testRichFilter() {
        final KeyValueWithHeaders<String, Message> notPresent = KeyValueWithHeaders.msg(new TestMessage());
        final KeyValueWithHeaders<String,Message> nullValue = KeyValueWithHeaders.msg(new TestMessage(),new RecordHeader("test",(byte[])null));
        final KeyValueWithHeaders<String,Message> fail = KeyValueWithHeaders.msg(new TestMessage(),new RecordHeader("test","reject".getBytes()));
        final KeyValueWithHeaders<String,Message> pass = KeyValueWithHeaders.msg(new TestMessage(),new RecordHeader("test","accept".getBytes()));

        KafkaStreamsTestHarness harness = KafkaStreamsTestHarness.builderWithMappings(TestMessage.class)
                                                                 .build();
        rich(harness.stream())
                .richFilter(kvh -> Arrays.equals("accept".getBytes(), Option.of(kvh.headers.lastHeader("test")).map(r -> r.value()).getOrNull()))
                .to(KafkaStreamsTestHarness.OUTPUT_TOPIC);
        try (final TopologyTestDriver topology = harness.toTopology()) {
            harness.sendKeyValues(topology,notPresent, nullValue,fail, pass);
            final List<Message> actualValues = harness.drain(topology);
            assertThat(actualValues).containsExactly(pass.value);
        }
    }

    @Test
    void testEnrich() {
        final KeyValueWithHeaders<String, Message> message = KeyValueWithHeaders.msg(new TestMessage());
        KafkaStreamsTestHarness harness = KafkaStreamsTestHarness.builderWithMappings(TestMessage.class)
                                                                 .build();
        final RichKStream<byte[],Message> rich = rich(harness.stream());
        rich
                .enrich(() -> new MessageHeaderEnricher<>("test"))
                .to(KafkaStreamsTestHarness.OUTPUT_TOPIC);
        final RecordHeader expectedContextHeader = new RecordHeader(Message.CONTEXT_HEADER, "test".getBytes());
        try (final TopologyTestDriver topology = harness.toTopology()) {
            harness.sendKeyValues(topology,message);
            final List<KeyValueWithHeaders<String,Message>> actualValues = harness.drainKeyValues(topology);
            assertThat(actualValues.get(0).headers).containsExactly(expectedContextHeader);
        }
    }

    @Test
    void testDelegatedTransform() {
        final KeyValueWithHeaders<String, Message> message = KeyValueWithHeaders.msg(new TestMessage());
        KafkaStreamsTestHarness harness = KafkaStreamsTestHarness.builderWithMappings(TestMessage.class)
                                                                 .build();
        final RichKStream<byte[],Message> rich = rich(harness.stream());
        rich
                .delegatedTransform(s -> s.mapValues(Message::getMessageId))
                .to(KafkaStreamsTestHarness.OUTPUT_TOPIC);
        final RecordHeader expectedContextHeader = new RecordHeader(Message.CONTEXT_HEADER, "test".getBytes());
        try (final TopologyTestDriver topology = harness.toTopology()) {
            harness.sendKeyValues(topology,message);
            final List<UUID> actualValues = harness.drain(topology);
            assertEquals(message.value.getMessageId(),actualValues.get(0));
        }
    }

    private static class TestMessage extends MessageImpl {

    }
}