package gov.scot.payments.application.security;

import gov.scot.payments.model.user.Action;
import gov.scot.payments.model.user.Application;
import gov.scot.payments.model.user.Group;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jose.jws.JwsAlgorithms;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtValidationException;

import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.security.oauth2.jwt.JwtClaimNames.ISS;

public class CognitoJwtTest {

    @Test
    public void testCreateUser() {
        Map<String,Object> map = new HashMap<>();
        map.put("token_use","id");
        assertThrows(JwtValidationException.class,() -> createJwt(map).createUser());
        map.put("email","a@b.com");
        assertThrows(JwtValidationException.class,() -> createJwt(map).createUser());
        map.put("cognito:username","user");
        User user = createJwt(map).createUser();
        assertEquals("a@b.com",user.getEmail());
        assertEquals("user",user.getName());
        assertEquals(0,user.getGroups().size());
        assertEquals(0,user.getRoles().size());

        map.put("cognito:groups", List.of("1","2"));
        user = createJwt(map).createUser();
        assertThat(user.getGroups()).containsExactlyInAnyOrder(new Group("1"),new Group("2"));

        map.put("custom:permitted_actions", "1,2");
        assertThrows(JwtValidationException.class,() -> createJwt(map).createUser());

        map.put("custom:permitted_actions", "a:b;c:d");
        user = createJwt(map).createUser();
        assertThat(user.getRoles().toJavaSet())
                .hasSize(2);
    }

    @Test
    public void testCreateApiClient() {
        Map<String, Object> map = new HashMap<>();
        map.put("token_use", "access");
        assertThrows(JwtValidationException.class, () -> createJwt(map).createApiClient());
        map.put("client_id", "client");
        Application apiClient = createJwt(map).createApiClient();
        assertEquals("client", apiClient.getClientId());
        assertEquals("client", apiClient.getName());
        assertEquals(0, apiClient.getRoles().size());
        map.put("scope", "a");
        assertThrows(JwtValidationException.class, () -> createJwt(map).createApiClient());
        map.put("scope", "a/b");
        assertThrows(JwtValidationException.class, () -> createJwt(map).createApiClient());
        map.put("scope", "a/b:c;d/e:f");
        apiClient = createJwt(map).createApiClient();
        assertThat(apiClient.getRoles().toJavaSet())
                .hasSize(2);
    }

    @Test
    public void testExtractUserAuth(){
        Map<String,Object> map = new HashMap<>();
        map.put("token_use","id");
        map.put("email","a@b.com");
        map.put("cognito:username","user");
        map.put("cognito:groups", List.of("1","2"));
        map.put("custom:permitted_actions", "a:b;c:d");
        Authentication authentication = createJwt(map).toAuthentication();
        assertEquals(User.class,authentication.getPrincipal().getClass());
        final GrantedAuthority[] expectedAuthorities = {Action.parse("a:b").toAuthority(), Action.parse("c:d").toAuthority()};
        assertThat((Collection<GrantedAuthority>)authentication.getAuthorities())
                .containsExactlyInAnyOrder(expectedAuthorities);
    }

    @Test
    public void testExtractApiAuth(){
        Map<String,Object> map = new HashMap<>();
        map.put("token_use", "access");
        map.put("client_id", "client");
        map.put("scope", "a/b:c;d/e:f");
        Authentication authentication = createJwt(map).toAuthentication();
        assertEquals(Application.class,authentication.getPrincipal().getClass());
        final GrantedAuthority[] expectedAuthorities = {Action.parse("b:c").toAuthority(), Action.parse("e:f").toAuthority()};
        assertThat((Collection<GrantedAuthority>)authentication.getAuthorities())
                .containsExactlyInAnyOrder(expectedAuthorities);
    }


    private CognitoJwt createJwt(Map<String, Object> map) {
        Jwt jwt = Jwt.withTokenValue("123")
                .header("a","b")
                .claims(m -> m.putAll(map))
                .build();
        return new CognitoJwt(jwt);
    }

}