package gov.scot.payments.application;

import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.MessageType;
import io.vavr.collection.HashMap;
import io.vavr.collection.HashMultimap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.reflections.util.ClasspathHelper;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class ReflectiveMessageTypeRegistryTest {

    private MessageTypeRegistry registry;

    @BeforeEach
    void setUp() {
        registry = new ReflectiveMessageTypeRegistry(HashMap.empty(),List.of(ClasspathHelper.forClass(Message1.class)
                ,ClasspathHelper.forClass(Message2.class)
                ,ClasspathHelper.forClass(Message3.class)));

    }

    @Test
    public void testRegistry(){
        assertEquals(Message2.class,registry.getForType("1","type2").get());
        assertTrue(registry.getForType("1","type3").isEmpty());
        assertEquals(Message2.class,registry.getForType(Event.class,"1","type2").get());
        assertTrue(registry.getForType(Command.class,"1","type2").isEmpty());
        assertEquals("1",registry.getInfo(Message1.class).get().getContext());
        assertEquals("type1",registry.getInfo(Message1.class).get().getType());
        assertEquals(2,registry.getAllForContext(Object.class,"1").size());
        assertEquals(1,registry.getAllForContext(Event.class,"1").size());
        assertEquals(0,registry.getAllForContext(Event.class,"2").size());
        assertEquals(0,registry.getAllForContext(Event.class,"3").size());
    }

    @MessageType(type = "type1",context = "1")
    public static class Message1 {

    }

    @MessageType(type = "type2",context = "1")
    public static class Message2 implements Event {

        @Override
        public UUID getMessageId() {
            return null;
        }

        @Override
        public Instant getTimestamp() {
            return null;
        }
    }

    @MessageType(type = "type1",context = "2")
    public static class Message3 {

    }
}