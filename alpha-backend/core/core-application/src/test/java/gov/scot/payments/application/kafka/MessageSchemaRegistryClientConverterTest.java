package gov.scot.payments.application.kafka;

import gov.scot.payments.application.MessageTypeRegistry;
import gov.scot.payments.testing.spring.MockSchemaRegistryClient;
import io.vavr.control.Option;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Value;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.NoOpCacheManager;
import org.springframework.cloud.stream.schema.avro.DefaultSubjectNamingStrategy;
import org.springframework.cloud.stream.schema.client.SchemaRegistryClient;
import org.springframework.core.io.Resource;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class MessageSchemaRegistryClientConverterTest {

    private MessageSchemaRegistryClientConverter converter;
    private MockSchemaRegistryClient client;
    private MessageTypeRegistry registry;

    @BeforeEach
    void setUp() throws Exception{
        client = new MockSchemaRegistryClient();
        registry = mock(MessageTypeRegistry.class);
        CacheManager cacheManager = new NoOpCacheManager();
        converter = new MessageSchemaRegistryClientConverter(client,cacheManager,registry,new DefaultSubjectNamingStrategy());
        converter.setDynamicSchemaGenerationEnabled(true);
        converter.afterPropertiesSet();
        when(registry.getForType("test","payload1")).thenReturn(Option.of(Payload1.class));
        when(registry.getForType("test","payload2")).thenReturn(Option.of(Payload2.class));
    }

    @Test
    public void test(){
        MessageHeaders headers1 = new MessageHeaders(Map.of(gov.scot.payments.model.Message.TYPE_HEADER,"payload1",gov.scot.payments.model.Message.CONTEXT_HEADER,"test"));
        MessageHeaders headers2 = new MessageHeaders(Map.of(gov.scot.payments.model.Message.TYPE_HEADER,"payload2",gov.scot.payments.model.Message.CONTEXT_HEADER,"test"));
        Payload1 payload1 = new Payload1("hello");
        Payload2 payload2 = new Payload2(3);
        Message<?> message1 = converter.toMessage(payload1,headers1);
        Message<?> message2 = converter.toMessage(payload2,headers2);
        assertEquals(2,client.getSchemas().size());
        assertEquals("application/vnd.payload1.v1+avro",message1.getHeaders().get(MessageHeaders.CONTENT_TYPE,String.class));
        assertEquals("application/vnd.payload2.v1+avro",message2.getHeaders().get(MessageHeaders.CONTENT_TYPE,String.class));
        Payload1 converted1 = (Payload1)converter.fromMessage(message1,Object.class);
        Payload2 converted2 = (Payload2)converter.fromMessage(message2,Object.class);
        assertEquals("hello",converted1.getData());
        assertEquals(3,converted2.getCount());
    }

    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Payload1 {

        private String data;
    }

    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Payload2 {

        private Integer count;
    }
}