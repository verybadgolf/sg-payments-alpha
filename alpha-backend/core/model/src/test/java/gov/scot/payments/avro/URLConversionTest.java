package gov.scot.payments.avro;

import org.apache.avro.AvroRuntimeException;
import org.apache.avro.LogicalType;
import org.apache.avro.LogicalTypes;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

class URLConversionTest {

    private static final LogicalType STRING_TYPE = new LogicalType("string");

    @Test
    @DisplayName("Given a valid URL string, then fromCharSequence creates a URL")
    void givenValidURLStringThenFromCharSequenceCreatesURL() {

        String urlString = "https://somewhere.com/my/file";

        URL url = new URLConversion().fromCharSequence(urlString, null, STRING_TYPE);

        assertThat(url.toString(), is(urlString));
    }

    @Test
    @DisplayName("Given an invalid URL string, then fromCharSequence throws an AvroRuntimeException")
    void givenInvalidURLStringThenFromCharSequenceThrowsAvroRuntimeException() {

        String urlString = "xx:://somewhere.com/my/file";

        var exception = assertThrows(AvroRuntimeException.class,
                () -> new URLConversion().fromCharSequence(urlString, null, STRING_TYPE));

        assertThat(exception.getMessage(), is("Invalid URL: " + urlString));
    }

    @Test
    @DisplayName("Given a URL, then toCharSequence returns a String")
    void toCharSequence() throws MalformedURLException {

        String expectedUrlString = "https://somewhere.com/my/file";
        URL url = new URL(expectedUrlString);

        CharSequence urlString = new URLConversion().toCharSequence(url, null, STRING_TYPE);

        assertThat(urlString.toString(), is(expectedUrlString));
    }
}
