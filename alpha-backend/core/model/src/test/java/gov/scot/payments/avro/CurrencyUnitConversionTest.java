package gov.scot.payments.avro;

import org.apache.avro.AvroRuntimeException;
import org.apache.avro.LogicalType;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import java.net.MalformedURLException;
import java.net.URL;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CurrencyUnitConversionTest {

    private static final LogicalType STRING_TYPE = new LogicalType("string");

    @Test
    @DisplayName("Given a valid currency string, then fromCharSequence creates a CurrencyUnit")
    void givenValidCurrencyStringThenFromCharSequenceCreatesCurrencyUnit() {

        var currencyString = "GBP";

        var currencyUnit = new CurrencyUnitConversion().fromCharSequence(currencyString, null, STRING_TYPE);

        assertThat(currencyUnit.getCurrencyCode(), is(currencyString));
    }

    @Test
    @DisplayName("Given an invalid currency string, then fromCharSequence throws an AvroRuntimeException")
    void givenInvalidCurrencyStringThenFromCharSequenceThrowsAvroRuntimeException() {

        var currencyString = "AM$";

        var exception = assertThrows(AvroRuntimeException.class,
                () -> new CurrencyUnitConversion().fromCharSequence(currencyString, null, STRING_TYPE));

        assertThat(exception.getMessage(), is("Unknown currency: " + currencyString));
    }

    @Test
    @DisplayName("Given a CurrencyUnit, then toCharSequence returns a CharSequence of the currency")
    void toCharSequence() throws MalformedURLException {

        String expectedCurrencyString = "GBP";
        CurrencyUnit currencyUnit = Monetary.getCurrency(expectedCurrencyString);

        CharSequence currencyChars = new CurrencyUnitConversion().toCharSequence(currencyUnit, null, STRING_TYPE);

        assertThat(currencyChars.toString(), is(expectedCurrencyString));
    }

}