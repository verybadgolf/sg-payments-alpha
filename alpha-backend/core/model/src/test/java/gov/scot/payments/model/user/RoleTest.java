package gov.scot.payments.model.user;

import io.vavr.collection.List;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RoleTest {

    @Test
    void test_hasPermissionForResource() {
        var role = Role.parse("a/myresource:Write");
        assertTrue(role.hasPermissionForResource("myresource"));
        assertFalse(role.hasPermissionForResource("notmyresource"));
    }

    @Test
    void test_hasPermissionForPaymentFilesResource() {
        var role = Role.parse("test-scope/paymentsFiles:write");
        assertTrue(role.hasPermissionForResource("paymentsFiles"));
        assertFalse(role.hasPermissionForResource("notmyresource"));
    }
}