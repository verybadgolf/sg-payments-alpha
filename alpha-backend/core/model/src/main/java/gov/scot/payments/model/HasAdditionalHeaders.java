package gov.scot.payments.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Map;

public interface HasAdditionalHeaders {

    @JsonIgnore
    Map<String,String> additionalHeaders();
}
