package gov.scot.payments.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import lombok.*;
import org.apache.avro.reflect.Nullable;
import org.springframework.security.core.GrantedAuthority;

@Getter
@RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Builder(toBuilder = true)
public class User implements Subject {

    @EqualsAndHashCode.Include @NonNull private String name;
    @NonNull private String email;
    @NonNull private Set<Group> groups;
    @NonNull private Set<Role> roles;
    @Nullable private String sshPublicKey;

    @JsonIgnore
    @Override
    public List<? extends GrantedAuthority> getAuthorities() {
        return roles.flatMap(Role::toAuthorities).toList();
    }

}
