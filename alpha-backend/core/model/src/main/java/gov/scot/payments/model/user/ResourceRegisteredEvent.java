package gov.scot.payments.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.EventImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import io.vavr.collection.Set;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "*", type = "resourceRegistered")
@NoArgsConstructor
public class ResourceRegisteredEvent extends EventImpl implements HasKey<String> {

    @NonNull private ResourceWithVerbs resource;

    public ResourceRegisteredEvent(ResourceWithVerbs resource){
        this.resource = resource;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return resource.getResource();
    }
}
