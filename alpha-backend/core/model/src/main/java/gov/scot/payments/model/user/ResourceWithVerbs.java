package gov.scot.payments.model.user;

import gov.scot.payments.model.user.Action;
import gov.scot.payments.model.user.Scope;
import io.vavr.Value;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Singular;

@Getter
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class ResourceWithVerbs {

    private String resource;
    private Set<String> verbs;

    public Set<Action> toActions() {
        return verbs.map(v -> new Action(resource,v));
    }
}
