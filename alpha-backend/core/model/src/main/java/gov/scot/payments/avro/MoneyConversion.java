package gov.scot.payments.avro;

import org.apache.avro.AvroRuntimeException;
import org.apache.avro.Conversion;
import org.apache.avro.LogicalType;
import org.apache.avro.Schema;
import org.javamoney.moneta.Money;

public class MoneyConversion extends Conversion<Money> {

    public static final String MONEY_LOGICAL_TYPE = "money";

    @Override
    public Class<Money> getConvertedType() {
        return Money.class;
    }

    @Override
    public String getLogicalTypeName() {
        return MONEY_LOGICAL_TYPE;
    }

    @Override
    public Schema getRecommendedSchema() {
        return  new LogicalType(MONEY_LOGICAL_TYPE).addToSchema(Schema.create(Schema.Type.STRING));
    }

    @Override
    public Money fromCharSequence(CharSequence value, Schema schema, LogicalType type) {

        try {
            return Money.parse(value);
        } catch (RuntimeException e) {
            throw new AvroRuntimeException("Invalid Money: " + value);
        }
    }

    @Override
    public CharSequence toCharSequence(Money value, Schema schema, LogicalType type) {
        return value.toString();
    }
}
