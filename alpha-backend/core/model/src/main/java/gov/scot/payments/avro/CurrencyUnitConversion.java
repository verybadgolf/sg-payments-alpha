package gov.scot.payments.avro;

import org.apache.avro.AvroRuntimeException;
import org.apache.avro.Conversion;
import org.apache.avro.LogicalType;
import org.apache.avro.Schema;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.UnknownCurrencyException;

public class CurrencyUnitConversion extends Conversion<CurrencyUnit> {
    @Override
    public Class<CurrencyUnit> getConvertedType() {
        return CurrencyUnit.class;
    }

    @Override
    public String getLogicalTypeName() {
        return "currencyUnit";
    }

    @Override
    public Schema getRecommendedSchema() {
        return new LogicalType("currencyUnit").addToSchema(Schema.create(Schema.Type.STRING));
    }

    @Override
    public CurrencyUnit fromCharSequence(CharSequence value, Schema schema, LogicalType type) {

        try {
            return Monetary.getCurrency(value.toString());
        } catch (UnknownCurrencyException e) {
            throw new AvroRuntimeException("Unknown currency: " + value);
        }
    }

    @Override
    public CharSequence toCharSequence(CurrencyUnit value, Schema schema, LogicalType type) {
        return value.getCurrencyCode();
    }
}
