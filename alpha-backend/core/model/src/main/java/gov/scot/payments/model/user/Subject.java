package gov.scot.payments.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import org.springframework.security.core.GrantedAuthority;

import java.util.stream.Collectors;

public interface Subject {

    String getName();
    List<? extends GrantedAuthority> getAuthorities();
    Set<Role> getRoles();

    @JsonIgnore
    default Set<Scope> getScopes() { return getRoles().map(Role::getScope); }

    @JsonIgnore
    default String getScopesAsString(){
        return getScopes().map(Scope::toString).collect(Collectors.joining(","));
    }


    @JsonIgnore
    default String getRootScopesAsString(){
        return getScopes().filter(s -> s.getParent() == null).map(Scope::toString).collect(Collectors.joining(","));
    }

    default boolean hasGlobalAccess(){
        return getScopes().find(s -> s.toString().equals(".")).isDefined();
    }
    default boolean hasAction(Action action){
        return getRoles().find(r -> r.hasAction(action)).isDefined();
    }
    default boolean hasAccess(Role role){
        return getRoles().find(r -> r.matches(role)).isDefined();
    }

}
