package gov.scot.payments.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.vavr.control.Option;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "*", type = "genericError")
public class GenericErrorEvent extends EventWithCauseImpl implements HasKey<String>{

    private String key;
    @NonNull private CommandFailureInfo error;

    public GenericErrorEvent(){}

    @JsonCreator
    public GenericErrorEvent(@JsonProperty("messageId") final UUID messageId
            , @JsonProperty("timestamp") final Instant timestamp
            , @JsonProperty("correlationId") final UUID correlationId
            , @JsonProperty("executionCount") final int executionCount
            , @JsonProperty("reply") final boolean reply
            , @JsonProperty("key") final String key
            , @NonNull @JsonProperty("error") final CommandFailureInfo error) {
        super(messageId, timestamp,correlationId,executionCount,reply);
        this.key = key;
        this.error = error;
    }

    public static GenericErrorEvent from(final Command command, final CommandFailureInfo error) {
        return GenericErrorEvent.builder()
                                .correlationId(command.getMessageId())
                                .executionCount(command.getExecutionCount())
                                .key(HasKey.class.isAssignableFrom(command.getClass()) ? Option.of(((HasKey) command).getKey()).map(Object::toString).getOrNull() : null)
                                .error(error)
                                .build();
    }

    public static GenericErrorEvent from(final Command command, final Throwable throwable) {
        return from(command,CommandFailureInfo.from(throwable));
    }
}
