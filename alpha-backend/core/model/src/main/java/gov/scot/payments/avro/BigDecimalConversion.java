package gov.scot.payments.avro;

import org.apache.avro.Conversion;
import org.apache.avro.LogicalType;
import org.apache.avro.Schema;

import java.math.BigDecimal;

public class BigDecimalConversion extends Conversion<BigDecimal> {
    @Override
    public Class<BigDecimal> getConvertedType() {
        return BigDecimal.class;
    }

    @Override
    public String getLogicalTypeName() {
        return "bigdecimal";
    }

    @Override
    public Schema getRecommendedSchema() {
        return  new LogicalType("bigdecimal").addToSchema(Schema.create(Schema.Type.STRING));
    }

    @Override
    public BigDecimal fromCharSequence(CharSequence value, Schema schema, LogicalType type) {
            return new BigDecimal(value.toString());
    }

    @Override
    public CharSequence toCharSequence(BigDecimal value, Schema schema, LogicalType type) {
        return value.toString();
    }
}