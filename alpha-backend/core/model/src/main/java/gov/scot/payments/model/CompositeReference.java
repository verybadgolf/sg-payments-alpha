package gov.scot.payments.model;

import gov.scot.payments.model.user.Scope;
import lombok.*;
import org.apache.avro.reflect.Nullable;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class CompositeReference {

    @NonNull private String component0;
    @Nullable private String component1;

    public static CompositeReference parse(String s) {
        // Do I need to be able to handle strings with no product?
        String[] components = s.split("\\.");
        return new CompositeReference(components[0],components[1]);
    }

    public Scope toScope(){
        Scope scope = new Scope(component0);
        if(component1 != null){
            scope = new Scope(component1,scope);
        }
        return scope;
    }

    @Override
    public String toString() {
        return component1 != null ?
                String.format("%s.%s", component0, component1) : component0;

    }

}
