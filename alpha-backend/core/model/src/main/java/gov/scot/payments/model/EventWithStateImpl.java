package gov.scot.payments.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

import java.time.Instant;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@Getter
public abstract class EventWithStateImpl<T> extends EventWithCauseImpl implements HasState<T>{

    protected abstract void setCarriedState(T state);

    private Long stateVersion;

    public EventWithStateImpl(){
        super();
    }

    public EventWithStateImpl(Long stateVersion){
        this();
        this.stateVersion = stateVersion;
    }

    public EventWithStateImpl(final UUID correlationId
            , final int executionCount
            , boolean reply){
        super(correlationId, executionCount, reply);
    }

    public EventWithStateImpl(UUID messageId
            , Instant timestamp
            , final UUID correlationId
            , final int executionCount
            , boolean reply) {
        super(messageId, timestamp, correlationId, executionCount, reply);
    }

    @JsonIgnore
    public void setCarriedState(T state, Long version) {
        setCarriedState(state);
        stateVersion = version;

    }

}
