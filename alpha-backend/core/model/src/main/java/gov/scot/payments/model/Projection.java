package gov.scot.payments.model;

import java.time.Instant;

public interface Projection<K> {

    Instant getProcessingTime();
    K getId();

}
