package gov.scot.payments.model;

public interface HasState<T> extends Event, HasStateVersion {

    T getCarriedState();
    void setCarriedState(T state, Long version);
}
