package gov.scot.payments.application.component.processmanager;

import gov.scot.payments.application.MessageDeDuplicator;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.func.PatternMatchingFunctionDelegate;
import gov.scot.payments.application.kafka.MessageDuplicateFilterFactory;
import gov.scot.payments.application.kafka.MessageHeaderEnricher;
import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.GenericErrorEvent;
import gov.scot.payments.testing.command.TestCreateCommand;
import gov.scot.payments.testing.command.TestDeleteCommand;
import gov.scot.payments.testing.command.TestErrorCommand;
import gov.scot.payments.testing.command.TestUpdateCommand;
import gov.scot.payments.testing.event.TestCreateEvent;
import gov.scot.payments.testing.event.TestDeleteEvent;
import gov.scot.payments.testing.event.TestErrorEvent;
import gov.scot.payments.testing.event.TestUpdateEvent;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.collection.List;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.state.Stores;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;

import java.time.Duration;
import java.util.function.BiConsumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class DefaultProcessManagerTest {

    private ProcessManagerDelegate delegate;
    private DefaultProcessManager processor;
    private KafkaStreamsTestHarness harness;
    private TestHandler handler;

    @BeforeEach
    void setUp() throws Exception {
        final Duration windowSize = Duration.ofMinutes(10);
        handler = spy(new TestHandler());
        BeanFactory beanFactory = mock(BeanFactory.class);
        StreamsBuilderFactoryBean factory = mock(StreamsBuilderFactoryBean.class);
        when(beanFactory.getBean(anyString(),eq(StreamsBuilderFactoryBean.class))).thenReturn(factory);

        final MicrometerMetrics metrics = new MicrometerMetrics(new SimpleMeterRegistry());
        MessageDuplicateFilterFactory duplicateFilterFactory = new MessageDuplicateFilterFactory(windowSize, KafkaStreamsTestHarness.STATE_STORE
                ,metrics
                , Stores.inMemoryWindowStore(KafkaStreamsTestHarness.STATE_STORE, windowSize, windowSize, false));
        MessageDeDuplicator deDuplicator = MessageDeDuplicator.builder()
                                                              .messageEnricher(() -> new MessageHeaderEnricher<>("test"))
                                                              .duplicateFilterFactory(duplicateFilterFactory)
                                                              .build();


        delegate = new PerEventStatelessProcessManagerDelegate(mock(BiConsumer.class),handler,metrics);

        harness = KafkaStreamsTestHarness.builderWithMappingsAndKeySerde(Serdes.ByteArraySerde.class,TestCreateEvent.class, TestUpdateEvent.class, TestDeleteEvent.class, TestErrorEvent.class, GenericErrorEvent.class
                ,TestCreateCommand.class,TestUpdateCommand.class,TestDeleteCommand.class, TestErrorCommand.class)
                .build();

        when(factory.getObject()).thenReturn(harness.getBuilder());
        deDuplicator.setBeanFactory(beanFactory);

        RetryHandler retryHandler = RetryHandler.builder()
                                                .valueSerde(harness.getSerde())
                                                .topicNameExtractor((k,v,r) -> KafkaStreamsTestHarness.OUTPUT_TOPIC)
                                                .build();

        processor = (DefaultProcessManager) DefaultProcessManager.builder()
                                                                 .deDuplicator(deDuplicator)
                                                                 .retryHandler(retryHandler)
                                                                 .delegate(delegate)
                                                                 .build();

        processor.handle(harness.stream("internal"),harness.stream("external"));
    }

    @Test
    public void test(){
        try (final TopologyTestDriver topology = harness.toTopology()) {
            KeyValueWithHeaders<String,TestCreateEvent> create1 = KeyValueWithHeaders.msg("1",new TestCreateEvent("1"));
            KeyValueWithHeaders<String,TestUpdateEvent> update1 = KeyValueWithHeaders.msg("2",new TestUpdateEvent("2"));

            harness.sendKeyValues(topology,"internal",create1,update1,create1);
            List<KeyValueWithHeaders<String,Command>> entities = harness.drainKeyValues(topology);

            assertEquals(3,entities.size());
            assertThat(entities.get(0).value).isEqualToComparingOnlyGivenFields(new TestCreateCommand("1"),"key");
            assertThat(entities.get(1).value).isEqualToComparingOnlyGivenFields(new TestUpdateCommand("2"),"key");
            assertThat(entities.get(2).value).isEqualToComparingOnlyGivenFields(new TestDeleteCommand("2"),"key");

        }
        verify(handler,times(2)).apply(any());
    }

    public static class TestHandler extends PatternMatchingFunctionDelegate<Event, List<Command>> {

        @Override
        protected void handlers(final PatternMatcher.Builder<Event, List<Command>> builder) {
            builder.match(TestCreateEvent.class, c -> List.of(new TestCreateCommand(c.getKey())))
                    .match(TestUpdateEvent.class, c -> List.of(new TestUpdateCommand(c.getKey()),new TestDeleteCommand(c.getKey())))
                    .match(TestDeleteEvent.class, c -> List.empty())
                    .match(TestErrorEvent.class, c -> {throw new RuntimeException();});
        }
    }

}