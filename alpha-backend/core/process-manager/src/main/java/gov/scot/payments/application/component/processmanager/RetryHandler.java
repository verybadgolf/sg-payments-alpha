package gov.scot.payments.application.component.processmanager;

import gov.scot.payments.model.Command;
import lombok.Builder;
import lombok.NonNull;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.processor.TopicNameExtractor;

@Builder
public class RetryHandler {

    @NonNull private final TopicNameExtractor<byte[], Command> topicNameExtractor;
    @NonNull private final Serde valueSerde;

    @SuppressWarnings("unchecked")
    public void route(KStream<byte[],Command> stream){
        stream.to(topicNameExtractor, Produced.with(Serdes.ByteArray(),valueSerde));
    }

}
