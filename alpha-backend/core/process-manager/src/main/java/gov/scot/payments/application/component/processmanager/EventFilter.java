package gov.scot.payments.application.component.processmanager;

import gov.scot.payments.application.kafka.KeyValueWithHeaders;
import gov.scot.payments.model.Event;

import java.util.function.Predicate;

public interface EventFilter extends Predicate<KeyValueWithHeaders<byte[], Event>> {
}
