package gov.scot.payments.application.component.processmanager;

import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.MessageDeDuplicator;
import gov.scot.payments.application.kafka.HeaderPreservingCompositeNonNativeSerde;
import gov.scot.payments.application.kafka.StreamJoiner;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.processor.TopicNameExtractor;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.kafka.config.KafkaStreamsConfiguration;
import org.springframework.kafka.core.CleanupConfig;
import org.springframework.util.ResourceUtils;

import java.time.Duration;
import java.util.Map;
import java.util.Properties;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@EnableBinding(ProcessManagerBinding.class)
@PropertySource(ResourceUtils.CLASSPATH_URL_PREFIX + "application-process-manager.properties")
public abstract class ProcessManagerApplication extends BaseApplication<Event,Command> {

    //eventHandler

    @Bean
    public RetryHandler retryHandler(TopicNameExtractor<byte[],Command> topicNameExtractor){
        return RetryHandler.builder()
                           .topicNameExtractor(topicNameExtractor)
                           .valueSerde(valueSerde(Command.class))
                           .build();
    }

    @Bean
    @ConditionalOnMissingBean
    public ProcessManagerDelegate delegate(EventTransformer eventHandler
            , ErrorHandler errorHandler
            , Metrics metrics) {
            return PerEventStatelessProcessManagerDelegate.builder()
                                                                                 .errorHandler(errorHandler)
                                                                                 .eventTransformer(eventHandler)
                                                                                 .metrics(metrics)
                                                                                 .build();
    }
    @Bean
    @ConditionalOnMissingBean
    public ProcessManager processManager(@Autowired(required = false) StreamJoiner<Event,Event,Event> joiner
            , @Autowired(required = false)org.apache.kafka.streams.kstream.Predicate<byte[],Event> joinPredicate
            , MessageDeDuplicator<Event,Command> deDuplicator
            , EventFilter externalEventFilter
            , RetryHandler retryHandler
            , ProcessManagerDelegate delegate){
        if(joiner != null){
            return JoiningProcessManager.builder()
                    .delegate(delegate)
                    .deDuplicator(deDuplicator)
                    .externalEventFilter(externalEventFilter)
                    .joiner(joiner)
                    .joinPredicate(joinPredicate == null ? (k,v) -> true : joinPredicate)
                    .retryHandler(retryHandler)
                    .build();
        } else {
            return DefaultProcessManager.builder()
                    .delegate(delegate)
                    .deDuplicator(deDuplicator)
                    .externalEventFilter(externalEventFilter)
                    .retryHandler(retryHandler)
                    .build();
        }
    }

    @Bean
    @ConditionalOnMissingBean
    public Predicate<Headers> defaultExternalEventHeaderFilter(){
        return h -> true;
    }

    @Bean
    @ConditionalOnMissingBean
    public EventFilter defaultExternalEventFilter(){
        return (s) -> true;
    }

    @Bean
    @ConditionalOnMissingBean
    public ErrorHandler defaultErrorHandler(){
        return (e,t) -> {};
    }

    @Bean
    public TopicNameExtractor<byte[],Command> topicNameExtractor(@Value("${commands.destination}") String topic
            , @Value("${retry.count:0}") int retryCount ){
        return new RetryTopicRouter(topic,retryCount);
    }

    @Bean
    @ConditionalOnExpression("${retry.count} > 0")
    public RetryProcessor retryProcessor(KafkaStreamsConfiguration streamsConfig
            , BackoffFunction backoffFunction
            , @Value("${commands.destination}") String topic
            , @Value("${retry.count:0}") int retryCount
            , BeanFactory beanFactory  ){
        Map<String,Object> properties = streamsConfig.asProperties().entrySet().stream().collect(Collectors.toMap(e -> e.getKey().toString(),e -> e.getValue()));
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG,properties.get(StreamsConfig.APPLICATION_ID_CONFIG)+"-retry");
        KafkaStreamsConfiguration retryStreamsConfig = new KafkaStreamsConfiguration(properties);
        HeaderPreservingCompositeNonNativeSerde<Command> valueSerde = valueSerde(Command.class);
        return new RetryProcessor(retryStreamsConfig,new CleanupConfig(),topic,retryCount,backoffFunction,beanFactory,valueSerde);
    }

    @Bean
    public BackoffFunction backoffFunction(@Value("${retry.initial-delay:PT1S}") Duration initialDelay){
        return count -> {
            long multiplier = (long)Math.pow(2,count-1);
            return initialDelay.multipliedBy(multiplier);
        };
    }

}
