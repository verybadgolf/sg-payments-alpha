package gov.scot.payments.application.component.processmanager;

import gov.scot.payments.model.Command;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.processor.RecordContext;
import org.apache.kafka.streams.processor.TopicNameExtractor;
import org.springframework.retry.ExhaustedRetryException;

@RequiredArgsConstructor
@Slf4j
public class RetryTopicRouter implements TopicNameExtractor<byte[], Command> {

    private final String mainTopic;
    private final int numRetryTopics;

    @Override
    public String extract(final byte[] key, final Command value, final RecordContext recordContext) {
        int execCount = value.getExecutionCount();
        if(execCount == 0){
            return mainTopic;
        }
        if(execCount > numRetryTopics){
            String msg = String.format("Number of retires (%s) exceeded for command %s. Throwing",numRetryTopics,value.getMessageId());
            log.warn(msg);
            throw new ExhaustedRetryException(msg);
        }
        final String topic = String.format("%s-retry-%s", mainTopic, execCount);
        log.info("Retry count {} of {} for command {}. Sending to {} ",execCount,numRetryTopics,value.getMessageId(),topic);
        return topic;
    }
}
