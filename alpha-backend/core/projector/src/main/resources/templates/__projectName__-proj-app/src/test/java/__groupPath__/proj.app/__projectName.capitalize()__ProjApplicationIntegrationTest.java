package @group@.@namePackage@.proj.app;

import gov.scot.payments.application.ApplicationComponent;
import org.springframework.context.annotation.Import;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.apache.kafka.streams.KafkaStreams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.spring.TestConfiguration;

import java.time.Duration;

@ApplicationIntegrationTest(classes = {@projectName.capitalize()@ProjApplicationIntegrationTest.TestApplication.class}
        , componentType = ApplicationIntegrationTest.ComponentType.PROJECTOR
        , properties = {
            "spring.flyway.schemas= "
           , "spring.jpa.properties.hibernate.default_schema="
        }
)
public class @projectName.capitalize()@ProjApplicationIntegrationTest {

    @BeforeEach
    public void setUp(@Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter, EmbeddedBrokerClient brokerClient){
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(10));
    }

    @Test
    public void test(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){

    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    @EnableAutoConfiguration(exclude = {HypermediaAutoConfiguration.class})
    public static class TestApplication extends @projectName.capitalize()@ProjApplication{

    }
}