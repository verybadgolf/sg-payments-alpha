package @group@.@namePackage@.app;

import lombok.extern.slf4j.Slf4j;
import gov.scot.payments.application.component.projector.springdata.RepositoryMutatingStorageService;
import gov.scot.payments.application.component.projector.springdata.ProjectionRepository;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Event;
import gov.scot.payments.application.func.PatternMatcher;
import @group@.@namePackage@.model.@projectName.capitalize()@;

@Slf4j
public class @projectName.capitalize()@StorageService extends RepositoryMutatingStorageService<String,@projectName.capitalize()@>{

    public @projectName.capitalize()@StorageService(final Metrics metrics, final ProjectionRepository<String, @projectName.capitalize()@> repository) {
        super(metrics, repository);
    }

    @Override
    protected boolean shouldDelete(Event event) {

    }

    @Override
    protected void idExtractors(PatternMatcher.Builder2<Event,String,String> builder) {

    }

    @Override
    protected void createHandlers(PatternMatcher.Builder<Event,@projectName.capitalize()@> builder) {

    }

    @Override
    protected void updateHandlers(PatternMatcher.Builder2<Event,@projectName.capitalize()@,@projectName.capitalize()@> builder) {

    }
}
