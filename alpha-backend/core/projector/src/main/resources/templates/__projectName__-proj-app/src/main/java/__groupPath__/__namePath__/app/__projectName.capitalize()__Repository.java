package @group@.@namePackage@.app;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import gov.scot.payments.application.component.projector.springdata.jpa.JpaProjectionRepository;
import @group@.@namePackage@.model.@projectName.capitalize()@;
import gov.scot.payments.model.user.Subject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.Optional;

@Repository
public interface @projectName.capitalize()@Repository extends JpaProjectionRepository<String,@projectName.capitalize()@>{

    @Query()
    Page<T> findAll(Pageable pageable, Subject subject);

    @Query()
    Optional<T> findById(String id, Subject subject);

}
