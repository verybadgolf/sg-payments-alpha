package gov.scot.payments.application.component.projector;

import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.Projection;
import io.vavr.Tuple2;
import io.vavr.Value;
import io.vavr.control.Option;
import io.vavr.control.Try;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public abstract class MutatingStorageService<ID,T extends Projection<ID>> extends AppendingStorageService<ID,T> {

    private final PatternMatcher<Tuple2<Event,String>,ID> idExtractor;
    private final PatternMatcher<Tuple2<Event,T>,T> updateHandlers;

    protected abstract boolean shouldDelete(Event event);
    protected abstract void updateHandlers(PatternMatcher.Builder2<Event,T,T> builder);
    protected abstract void idExtractors(PatternMatcher.Builder2<Event,String,ID> builder);

    protected abstract boolean delete(ID id);
    protected abstract Option<T> find(ID id);

    public MutatingStorageService(Metrics metrics) {
        super(metrics);
        idExtractor = PatternMatcher.<Event,String,ID>builder2().apply2(this::idExtractors).build();
        updateHandlers = PatternMatcher.<Event,T,T>builder2().apply2(this::updateHandlers).build();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected Tuple2<String, T> doApply(String key, Event event) {
        return idExtractor.option(new Tuple2<>(event,key))
                .map(id -> {
                    if(shouldDelete(event)){
                        boolean deleted = metrics.execute("storage.delete",() -> delete(id));
                        return deleted ? new Tuple2<String,T>(key,null) : null;
                    }
                    return Try.ofSupplier(metrics.time("storage.find",() -> find(id)))
                            .map(o -> o.fold(() -> createHandlers.option(event), s -> updateProjection(s,event)))
                            .map(o -> o.map(metrics.time("storage.upsert",v -> new Tuple2(key,insert(v)))))
                            .map(Value::getOrNull)
                            .get();
                })
                .getOrNull();
    }

    private Option<T> updateProjection(T current, final Event event){
        return updateHandlers.option(new Tuple2<>(event,current));
    }

}
