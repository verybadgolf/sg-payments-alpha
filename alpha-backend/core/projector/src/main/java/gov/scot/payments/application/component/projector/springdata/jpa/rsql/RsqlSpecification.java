package gov.scot.payments.application.component.projector.springdata.jpa.rsql;

import cz.jirutka.rsql.parser.ast.ComparisonOperator;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class RsqlSpecification<T> implements Specification<T> {

    private final String property;
    private final ComparisonOperator operator;
    private final List<String> arguments;

    @SuppressWarnings("unchecked")
    @Override
    public Predicate toPredicate(@NonNull final Root<T> root, @NonNull final CriteriaQuery<?> query, @NonNull final CriteriaBuilder builder) {
        RsqlSearchOperation oper = RsqlSearchOperation.getSimpleOperator(operator);
        if(oper != null) {
            Path path = getPath(root);
            final List<Object> args = castArguments(path);
            final Object argument = args.get(0);
            switch (oper) {
                case EQUAL: {
                    if (argument instanceof String) {
                        return builder.like(path, argument.toString().replace('*', '%'));
                    } else if (argument == null) {
                        return builder.isNull(path);
                    } else {
                        return builder.equal(path, argument);
                    }
                }
                case NOT_EQUAL: {
                    if (argument instanceof String) {
                        return builder.notLike(path, argument.toString().replace('*', '%'));
                    } else if (argument == null) {
                        return builder.isNotNull(path);
                    } else {
                        return builder.notEqual(path, argument);
                    }
                }
                case GREATER_THAN: {
                    return builder.greaterThan(path, (Comparable)argument);
                }
                case GREATER_THAN_OR_EQUAL: {
                    return builder.greaterThanOrEqualTo(path, (Comparable)argument);
                }
                case LESS_THAN: {
                    return builder.lessThan(path, (Comparable)argument);
                }
                case LESS_THAN_OR_EQUAL: {
                    return builder.lessThanOrEqualTo(path, (Comparable)argument);
                }
                case IN:
                    return path.in(args);
                case NOT_IN:
                    return builder.not(path.in(args));
            }
        }

        return null;
    }

    private <V> Path<V> getPath(Root<T> root) {
        String[] nodes = property.split("\\.");
        Path<V> path = root.get(nodes[0]);
        for(int i=1;i<nodes.length;i++){
            path = path.get(nodes[i]);
        }
        return path;
    }

    private List<Object> castArguments(final Path<?> root) {
        final Class<?> type = root.getJavaType();
        return arguments.stream()
                        .map(argument -> castArgument(argument,type))
                        .collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    private Object castArgument(String argument, Class<?> type){
        Object toReturn;
        if (type.equals(Integer.class)) {
            toReturn = Integer.parseInt(argument);
        } else if (type.equals(Long.class)) {
            toReturn = Long.parseLong(argument);
        } else if (type.equals(BigDecimal.class)) {
            toReturn = new BigDecimal(argument);
        } else if (type.equals(LocalDate.class)) {
            toReturn = LocalDate.parse(argument);
        } else if (type.equals(Instant.class)) {
            toReturn = Instant.ofEpochMilli(Long.parseLong(argument));
        } else if (type.equals(LocalDateTime.class)) {
            toReturn = LocalDateTime.parse(argument);
        } else if (type.equals(CurrencyUnit.class)) {
            toReturn = Monetary.getCurrency(argument.toUpperCase());
        } else if (type.equals(UUID.class)) {
            toReturn = UUID.fromString(argument);
        } else if (type.isEnum()) {
            toReturn = Enum.valueOf((Class<? extends Enum>)type,argument);
        } else {
            toReturn = argument;
        }
        return toReturn;
    }

}
