package gov.scot.payments.application.component.projector.springdata;

import gov.scot.payments.application.component.projector.MutatingStorageService;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Projection;
import io.vavr.control.Option;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@SuperBuilder
public abstract class RepositoryMutatingStorageService<ID,T extends Projection<ID>> extends MutatingStorageService<ID,T> {

    protected final ProjectionRepository<ID,T> repository;

    public RepositoryMutatingStorageService(Metrics metrics, ProjectionRepository<ID,T> repository) {
        super(metrics);
        this.repository = repository;
    }

    @Override
    @Transactional("transactionManager")
    protected T insert(final T state) {
        return repository.save(state);
    }


    @Override
    @Transactional(transactionManager = "transactionManager",readOnly = true)
    protected Option<T> find(ID id) {
        log.debug("Looking up {}",id);
        return Option.ofOptional(repository.findById(id));
    }

    @Override
    @Transactional("transactionManager")
    protected boolean delete(ID id) {
        log.debug("Deleting {}",id);
        return Option.ofOptional(repository.findById(id))
                .peek(i -> repository.deleteById(id))
                .isDefined();
    }

}
