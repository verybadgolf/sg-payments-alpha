package gov.scot.payments.application.component.projector.springdata.jpa;

import gov.scot.payments.model.Projection;
import gov.scot.payments.model.user.Subject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.util.function.Function;

import static gov.scot.payments.application.component.projector.springdata.jpa.rsql.RsqlParser.parseQuery;
import static org.springframework.util.StringUtils.isEmpty;

public abstract class SearchResource<T extends Projection<?>> {

    protected final JpaSpecificationExecutor<T> repository;
    protected final PagedResourcesAssembler<T> assembler;

    protected Function<Subject,Specification<T>> aclCheck;

    public SearchResource(JpaSpecificationExecutor<T> repository, PagedResourcesAssembler<T> assembler, Function<Subject,Specification<T>> aclCheck) {
        this.repository = repository;
        this.aclCheck = aclCheck;
        this.assembler = assembler;
    }

    @GetMapping(value = "/search")
    @ResponseBody
    public Mono<ResponseEntity<PagedModel<EntityModel<T>>>> search(@RequestParam(value = "query", defaultValue = "") String query
        , @PageableDefault(size = 100,sort = "processingTime",direction = Sort.Direction.DESC) Pageable paging
        , @AuthenticationPrincipal Subject subject){
        Specification<T> spec = getSpecificationFromQuery(query);
        final Specification<T> aclCheck = this.aclCheck.apply(subject);
        if(aclCheck != null){
            spec = spec.and(aclCheck);
        }
        Specification<T> specToUse = spec;
        return Mono.fromCallable(() -> assembler.toModel(repository.findAll(specToUse, paging)))
                   .map(ResponseEntity::ok)
                   .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    private Specification<T> getSpecificationFromQuery(String query) {
        if(isEmpty(query)){
            return Specification.where(null);
        }
        Specification<T> spec;
        try {
            spec = parseQuery(query);
        } catch (Exception exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid query format, see: https://github.com/jirutka/rsql-parser for grammar", exception);
        }
        return spec;
    }
}
