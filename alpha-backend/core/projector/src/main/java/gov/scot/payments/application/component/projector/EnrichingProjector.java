package gov.scot.payments.application.component.projector;

import gov.scot.payments.kafka.EmptyStream;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.Projection;
import io.vavr.Function2;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.GlobalKTable;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.SendTo;

@Slf4j
@SuperBuilder
public class EnrichingProjector<K,T extends Projection<K>> extends Projector<K,T>{

    @NonNull private final Function2<byte[],Event,String> kvMapper;
    @NonNull private final Function2<Event,Event,Event> joiner;

    @StreamListener
    @SendTo("output")
    public KStream<byte[],T> handle(@Input("events-internal") KStream<byte[], Event> internalEvents
            , @Input("events-external") GlobalKTable<String, Event> externalEvents) throws Exception {
        final KStream<byte[], Event> mergedEvents;
        if(externalEvents instanceof EmptyStream){
            mergedEvents = internalEvents;
        } else{
            mergedEvents = internalEvents.join(externalEvents
                    , kvMapper::apply
                    , joiner::apply);
        }
        return handle(mergedEvents,"handle");
    }

}
