package gov.scot.payments.application.component.projector;

import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.Projection;
import io.vavr.Tuple2;
import io.vavr.Value;
import io.vavr.control.Try;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public abstract class AppendingStorageService<K,T extends Projection<K>> extends AbstractStorageService<K,T>{

    protected final PatternMatcher<Event,T> createHandlers;

    protected abstract void createHandlers(PatternMatcher.Builder<Event,T> builder);

    protected abstract T insert(T state);

    public AppendingStorageService(Metrics metrics) {
        super(metrics);
        createHandlers = PatternMatcher.<Event,T>builder().apply(this::createHandlers).build();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected Tuple2<String, T> doApply(String key, Event event) {
        return Try.ofSupplier(() -> createHandlers.option(event))
                .map(o -> o.map(metrics.time("storage.insert",v -> new Tuple2(key,insert(v)))))
                .map(Value::getOrNull)
                .get();
    }

}
