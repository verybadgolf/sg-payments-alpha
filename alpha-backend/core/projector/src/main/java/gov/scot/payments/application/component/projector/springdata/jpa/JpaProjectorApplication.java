package gov.scot.payments.application.component.projector.springdata.jpa;

import gov.scot.payments.application.component.projector.ProjectorApplication;
import gov.scot.payments.model.Projection;
import org.springframework.context.annotation.PropertySource;
import org.springframework.util.ResourceUtils;

@PropertySource(ResourceUtils.CLASSPATH_URL_PREFIX + "application-projector-jpa.properties")
public abstract class JpaProjectorApplication<ID,T extends Projection<ID>> extends ProjectorApplication<ID,T> {

    //enable repositories, entity scan
    //appending / mutating storage service
    //define repository - needs to be readonlyjparepo (and optinoally temporalprojrepo)
    //define entity (possibly extending generic temporal and annotating with @table and jsonb)

}
