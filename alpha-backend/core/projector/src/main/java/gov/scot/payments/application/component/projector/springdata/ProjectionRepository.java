package gov.scot.payments.application.component.projector.springdata;

import gov.scot.payments.model.Projection;
import gov.scot.payments.model.user.Subject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

@NoRepositoryBean
public interface ProjectionRepository<ID,T extends Projection<ID>>{

    <S extends T> S save(S entity);

    void deleteById(ID id);

    Optional<T> findById(ID id);

    Page<T> findAll(Pageable pageable, Subject subject);

    Optional<T> findById(ID id, Subject subject);

}
