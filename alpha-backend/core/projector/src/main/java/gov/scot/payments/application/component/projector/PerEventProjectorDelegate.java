package gov.scot.payments.application.component.projector;

import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.Projection;
import io.vavr.Tuple2;
import io.vavr.control.Try;
import lombok.Builder;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;

@Slf4j
@Builder
public class PerEventProjectorDelegate<K,T extends Projection<K>> implements ProjectorDelegate<K,T> {

    @Builder.Default private final BiConsumer<Event,Throwable> errorHandler = (e, t) -> {};
    @NonNull private final BiFunction<String,Event, Tuple2<String,T>> recordTransformer;
    @NonNull private final Metrics metrics;

    @Override
    public KStream<byte[], T> apply(final KStream<byte[], Event> stream) {
        return stream
                .mapValues(this::wrap)
                .filter((k,v) -> v != null)
                .mapValues(t -> t._2);
    }

    private Tuple2<String,T> wrap(final byte[] key, final Event event) {
        log.debug("Handling event: {}",event);
        return Try.ofSupplier(metrics.time("projection.update",() -> recordTransformer.apply(key == null ? null : new String(key), event)))
                  .onFailure(e -> log.warn("Handling error: ",e))
                  .onFailure(e -> metrics.increment("projection.update.error"))
                  .onFailure(e -> errorHandler.accept(event,e))
                  .get();
    }
}
