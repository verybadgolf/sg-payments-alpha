package gov.scot.payments.application.component.projector;

import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;

public interface ConsumingProjectorBinding {

    @Input("events-internal")
    KStream<?,?> internalEvents();

    @Input("events-external")
    KStream<?,?> externalEvents();
}
