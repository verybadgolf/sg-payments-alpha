package gov.scot.payments.application.component.projector;

import gov.scot.payments.model.Event;

import java.util.function.BiConsumer;

public interface ConsumingStorageService extends BiConsumer<String, Event> {
}
