package gov.scot.payments.testing.spring;

import io.vavr.Function4;
import org.apache.kafka.streams.state.KeyValueBytesStoreSupplier;
import org.apache.kafka.streams.state.Stores;
import org.apache.kafka.streams.state.WindowBytesStoreSupplier;
import org.springframework.cloud.stream.schema.client.SchemaRegistryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.time.Duration;
import java.util.function.Function;

@Configuration
public class TestConfiguration {

    //@MockBean
    //ReactiveJwtDecoder jwtDecoder;

    @Bean
    public KafkaStreamsStateWaiter kafkaStreamsWaiter(){
        return new KafkaStreamsStateWaiter();
    }

    @Bean
    @Primary
    public SchemaRegistryClient mockSchemaRegistryClient(){
        return new MockSchemaRegistryClient();
    }

    @Bean
    @Primary
    public Function<String, KeyValueBytesStoreSupplier> inMemoryKeyValueBytesStoreSupplier(){
        return Stores::inMemoryKeyValueStore;
    }

    @Bean
    @Primary
    public Function4<String, Duration,Duration,Boolean, WindowBytesStoreSupplier> inMemoryWindowBytesStoreSupplier(){
        return Stores::inMemoryWindowStore;
    }

    @Bean
    @Primary
    public io.confluent.kafka.schemaregistry.client.SchemaRegistryClient mockConfluentClient(){
        return new io.confluent.kafka.schemaregistry.client.MockSchemaRegistryClient();
    }
}
