package gov.scot.payments.testing.kafka;

import org.junit.jupiter.api.extension.*;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.lang.reflect.AnnotatedElement;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.extension.ExtensionContext.Namespace.GLOBAL;

public class EmbeddedKafkaExtension implements ParameterResolver, BeforeAllCallback, ExtensionContext.Store.CloseableResource{

    private static final String EMBEDDED_BROKER = "embedded-kafka";

    private static AtomicReference<EmbeddedKafkaBroker> broker = new AtomicReference<>();

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {

        return parameterContext.getParameter().getType().equals(EmbeddedKafkaBroker.class);
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext context)
            throws ParameterResolutionException {

        EmbeddedKafkaBroker instance = broker.get();
        Assert.state(instance != null, "Could not find embedded broker instance");
        return instance;
    }

    @Override
    public synchronized void beforeAll(ExtensionContext context) throws Exception {
        if (broker.get() == null) {
            Optional<AnnotatedElement> element = context.getElement();
            if (element.isPresent()){
                EmbeddedKafka embedded = AnnotatedElementUtils.findMergedAnnotation(element.get(), EmbeddedKafka.class);
                if (embedded != null) {
                    EmbeddedKafkaBroker instance = broker.get();
                    if (instance == null) {
                        instance = createBroker(embedded);
                        broker.set(instance);
                    }
                }
            }
            context.getRoot().getStore(GLOBAL).put(EMBEDDED_BROKER, this);
        }
    }

    @Override
    public synchronized void close() throws Throwable {
        EmbeddedKafkaBroker instance = broker.get();
        if (instance != null) {
            instance.destroy();
            broker.set(null);
        }
    }

    @SuppressWarnings("unchecked")
    private EmbeddedKafkaBroker createBroker(EmbeddedKafka embedded) {
        EmbeddedKafkaBroker broker;
        broker = new EmbeddedKafkaBroker(new org.springframework.kafka.test.EmbeddedKafkaBroker(embedded.count(), embedded.controlledShutdown(),embedded.partitions(), embedded.topics())
                .brokerProperty("log.retention.ms",-1)
                .brokerProperty("transaction.state.log.replication.factor",(short)1)
                .brokerProperty("transaction.state.log.min.isr",1)
                .brokerProperty("transaction.state.log.num.partitions",1)
                .zkPort(embedded.zookeeperPort())
                .kafkaPorts(embedded.ports()));
        Properties properties = new Properties();

        for (String pair : embedded.brokerProperties()) {
            if (!StringUtils.hasText(pair)) {
                continue;
            }
            try {
                properties.load(new StringReader(pair));
            }
            catch (Exception ex) {
                throw new IllegalStateException("Failed to load broker property from [" + pair + "]",
                        ex);
            }
        }
        if (StringUtils.hasText(embedded.brokerPropertiesLocation())) {
            Resource propertiesResource = new PathMatchingResourcePatternResolver()
                    .getResource(embedded.brokerPropertiesLocation());
            if (!propertiesResource.exists()) {
                throw new IllegalStateException(
                        "Failed to load broker properties from [" + propertiesResource
                                + "]: resource does not exist.");
            }
            try (InputStream in = propertiesResource.getInputStream()) {
                Properties p = new Properties();
                p.load(in);
                p.forEach(properties::putIfAbsent);
            }
            catch (IOException ex) {
                throw new IllegalStateException(
                        "Failed to load broker properties from [" + propertiesResource + "]", ex);
            }
        }
        broker.brokerProperties((Map<String, String>) (Map<?, ?>) properties);
        if (StringUtils.hasText(embedded.bootstrapServersProperty())) {
            broker.brokerListProperty(embedded.bootstrapServersProperty());
        }
        broker.afterPropertiesSet();
        return broker;
    }

    public static EmbeddedKafkaBroker getBroker(){
        return broker.get();
    }

}
