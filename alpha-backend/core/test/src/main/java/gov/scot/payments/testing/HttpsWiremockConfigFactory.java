package gov.scot.payments.testing;

import com.github.tomakehurst.wiremock.common.Slf4jNotifier;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import ru.lanwen.wiremock.config.WiremockConfigFactory;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class HttpsWiremockConfigFactory implements WiremockConfigFactory {

    @Override
    public WireMockConfiguration create() {
        return options()
                .dynamicPort()
                .dynamicHttpsPort()
                .notifier(new Slf4jNotifier(true));
    }
}
