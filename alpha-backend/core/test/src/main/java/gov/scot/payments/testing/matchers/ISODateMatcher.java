package gov.scot.payments.testing.matchers;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import java.time.Instant;

/**
 * Checks if an ISO 8601 format date string, such as "2020-01-17T10:21:14.268",
 * is within a given number of seconds of a given instant.
 *
 * For example:
 *
 * ISODateMatcher.isWithin(5).secondsOf(Instant.now())
 *
 * Or, with static includes:
 *
 * isWithin(5).secondsOf(now())
 *
 */
public class ISODateMatcher extends TypeSafeMatcher<String> {

    private final Instant instant;
    private final int timeWindow;

    private ISODateMatcher(Instant instant, int timeWindow) {
        this.instant = instant;
        this.timeWindow = timeWindow;
    }

    @Override
    protected boolean matchesSafely(String item) {
        var testInstant = Instant.parse(item);
        return instant.minusSeconds(timeWindow).isBefore(testInstant)
                && instant.plusSeconds(timeWindow).isAfter(testInstant);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText(String.format("Occurred within %d seconds of \"%s\"", timeWindow, instant));
    }

    /**
     * Sets the number of seconds that the time can differ from the specified instant.
     * @param seconds the number of seconds that the time can differ from the specified instant
     */
    public static ISODateMatcherBuilder isWithin(int seconds) {
        return new ISODateMatcherBuilder(seconds);
    }

    public static class ISODateMatcherBuilder {
        private int timeWindow;

        private ISODateMatcherBuilder(int seconds) {
            timeWindow = seconds;
        }

        /**
         * Sets the Instant to test against.
         * @param instant the Instant to test against
         */
        public ISODateMatcher secondsOf(Instant instant) {
            return new ISODateMatcher(instant, timeWindow);
        }
    }
}
