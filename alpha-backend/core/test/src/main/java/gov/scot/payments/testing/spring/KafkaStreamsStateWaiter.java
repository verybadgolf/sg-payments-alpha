package gov.scot.payments.testing.spring;

import org.apache.kafka.streams.KafkaStreams;
import org.awaitility.Awaitility;
import org.springframework.context.event.EventListener;

import java.time.Duration;

public class KafkaStreamsStateWaiter {

    private KafkaStreams.State state;

    public void waitForState(KafkaStreams.State state, Duration timeout){
        Awaitility.await().atMost(timeout).until(() -> this.state == state);
    }

    @EventListener
    public void onStreamsState(KafkaStreams.State state){
        this.state = state;
    }
}
