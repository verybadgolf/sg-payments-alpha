package gov.scot.payments.testing.kafka;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

@ExtendWith(EmbeddedKafkaExtension.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Tag("kafka")
public @interface EmbeddedKafka {

    /**
     * @return the number of brokers
     */
    @AliasFor("count")
    int value() default 1;

    /**
     * @return the number of brokers
     */
    @AliasFor("value")
    int count() default 1;

    /**
     * @return passed into {@code kafka.utils.TestUtils.createBrokerConfig()}.
     */
    boolean controlledShutdown() default false;

    /**
     * Set explicit ports on which the kafka brokers will listen. Useful when running an
     * embedded broker that you want to access from other processes.
     * A port must be provided for each instance, which means the number of ports must match the value of the count attribute.
     * @return ports for brokers.
     * @since 2.2.4
     */
    int[] ports() default {0};

    /**
     * Set the port on which the embedded Zookeeper should listen;
     * @return the port.
     * @since 2.3
     */
    int zookeeperPort() default 0;

    /**
     * @return partitions per topic
     */
    int partitions() default 1;

    /**
     * Topics that should be created Topics may contain property place holders, e.g.
     * {@code topics = "${kafka.topic.one:topicOne}"} The topics will be created with
     * {@link #partitions()} partitions; to provision other topics with other partition
     * counts call the {@code addTopics(NewTopic... topics)} method on the autowired
     * broker.
     * Place holders will only be resolved when there is a Spring test application
     * context present (such as when using {@code @SpringJunitConfig or @SpringRunner}.
     * @return the topics to create
     */
    String[] topics() default { };

    /**
     * Properties in form {@literal key=value} that should be added to the broker config
     * before runs. When used in a Spring test context, properties may contain property
     * place holders, e.g. {@code delete.topic.enable=${topic.delete:true}}.
     * Place holders will only be resolved when there is a Spring test application
     * context present (such as when using {@code @SpringJunitConfig or @SpringRunner}.
     * @return the properties to add
     * @see #brokerPropertiesLocation()
     * @see org.springframework.kafka.test.EmbeddedKafkaBroker#brokerProperties(java.util.Map)
     */
    String[] brokerProperties() default { };

    /**
     * Spring {@code Resource} url specifying the location of properties that should be
     * added to the broker config. When used in a Spring test context, the
     * {@code brokerPropertiesLocation} url and the properties themselves may contain
     * place holders that are resolved during initialization. Properties specified by
     * {@link #brokerProperties()} will override properties found in
     * {@code brokerPropertiesLocation}.
     * Place holders will only be resolved when there is a Spring test application
     * context present (such as when using {@code @SpringJunitConfig or @SpringRunner}.
     * @return a {@code Resource} url specifying the location of properties to add
     * @see #brokerProperties()
     * @see org.springframework.kafka.test.EmbeddedKafkaBroker#brokerProperties(java.util.Map)
     */
    String brokerPropertiesLocation() default "";

    /**
     * The property name to set with the bootstrap server addresses instead of the default
     * {@value org.springframework.kafka.test.EmbeddedKafkaBroker#SPRING_EMBEDDED_KAFKA_BROKERS}.
     * @return the property name.
     * @since 2.3
     * @see org.springframework.kafka.test.EmbeddedKafkaBroker#brokerListProperty(String)
     */
    String bootstrapServersProperty() default "";
}
