package gov.scot.payments.testing.architecture;


import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.library.GeneralCodingRules;

public class CodingStandardsRules {

    @ArchTest
    public static final ArchRule doNotUseStandardOut = GeneralCodingRules.NO_CLASSES_SHOULD_ACCESS_STANDARD_STREAMS;

    @ArchTest
    public static final ArchRule doNotThrowGenericExceptions = GeneralCodingRules.NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS;

    @ArchTest
    public static final ArchRule doNotUseJodaTime = GeneralCodingRules.NO_CLASSES_SHOULD_USE_JODATIME;
}
