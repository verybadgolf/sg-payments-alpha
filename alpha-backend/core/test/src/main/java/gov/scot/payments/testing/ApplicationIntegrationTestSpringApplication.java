package gov.scot.payments.testing;

import gov.scot.payments.testing.spring.KafkaStreamsOverridingContext;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

public class ApplicationIntegrationTestSpringApplication extends SpringApplication {

    @Override
    public ConfigurableApplicationContext run(final String... args) {
        setApplicationContextClass(KafkaStreamsOverridingContext.class);
        return super.run(args);
    }
}
