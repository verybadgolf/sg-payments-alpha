package gov.scot.payments.testing;

import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.Message;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.testing.kafka.EmbeddedKafkaBroker;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import gov.scot.payments.testing.kafka.StateStoreType;
import gov.scot.payments.testing.spring.HeaderPreservingCompositeNonNativeSerde;
import gov.scot.payments.testing.spring.StreamsBuilderTestingDecorator;
import io.vavr.Tuple2;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.Getter;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.StreamsBuilder;
import org.springframework.cloud.stream.binder.kafka.streams.InteractiveQueryService;
import org.springframework.cloud.stream.converter.CompositeMessageConverterFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.support.KafkaHeaderMapper;
import org.springframework.kafka.test.utils.KafkaTestUtils;

import java.time.Duration;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static gov.scot.payments.testing.kafka.KeyValueWithHeaders.msg;

public class EmbeddedBrokerClient {

    private static final Duration DEFAULT_TIMEOUT = Duration.ofSeconds(1);

    @Getter private final EmbeddedKafkaBroker broker;
    private final Map<String,String> bindingMap;
    private final CompositeMessageConverterFactory compositeMessageConverterFactory;
    @Getter private final InteractiveQueryService queryService;
    private final List<StreamsBuilderTestingDecorator> streamBuilders;
    private final KafkaHeaderMapper kafkaHeaderMapper;

    public EmbeddedBrokerClient(final ApplicationContext springContext
            , final EmbeddedKafkaBroker broker
            , final String[] bindings) {
        this.broker = broker;
        this.streamBuilders = List.of(springContext.getBeansOfType(StreamsBuilder.class)
                                                                  .values())
                                                 .filter(s -> s instanceof StreamsBuilderTestingDecorator)
                                                 .map(s -> (StreamsBuilderTestingDecorator)s);
        bindingMap = List.of(bindings)
                         .flatMap(b -> List.of(b.split(":")))
                         .toMap(b -> new Tuple2<>(b,springContext.getEnvironment().getProperty(String.format("spring.cloud.stream.bindings.%s.destination",b))));
        queryService = springContext.getBean(InteractiveQueryService.class);
        compositeMessageConverterFactory = springContext.getBean(CompositeMessageConverterFactory.class);
        kafkaHeaderMapper = springContext.getBean(KafkaHeaderMapper.class);
    }

    @SuppressWarnings("unchecked")
    public void sendKeyValuesToTopic(String topic, Serializer<?> valueSerde, List<KeyValueWithHeaders<String,?>> values){
        java.util.Map<String, Object> props = KafkaTestUtils.producerProps(broker.getDelegate());
        DefaultKafkaProducerFactory<String,?> cf = new DefaultKafkaProducerFactory<>(props,Serdes.String().serializer(),valueSerde);
        Producer<String,?> producer = cf.createProducer();
        try{
            values
                    .map(kvh -> kvh.toProducerRecord(topic,registerSchemaAndExtractHeaders(kvh.value)))
                    .forEach(pr -> {
                        try {
                            RecordMetadata metadata = (RecordMetadata) producer.send((ProducerRecord) pr).get();
                            //System.out.println("SENT: "+metadata.topic()+" "+metadata.offset());
                        } catch (InterruptedException | ExecutionException e) {}
                    });
        } finally {
            producer.flush();
            producer.close();
        }


    }

    public void sendKeyValuesToTopic(String topic,List<KeyValueWithHeaders<String,?>> values){
        sendKeyValuesToTopic(topic, defaultSerde(Object.class).serializer(), values);
    }

    public  void sendToTopic(String topic,List<?> values){
        sendToTopic(topic, defaultSerde(Object.class).serializer(), values);
    }

    public void sendToTopic(String topic, Serializer<?> valueSerde, List<?> values){
        sendKeyValuesToTopic(topic, valueSerde, values.map(v -> convertToMessage(v)));
    }

    public <V> void sendKeyValuesToDestination(String binding, Serializer<?> valueSerde, List<KeyValueWithHeaders<String,?>> values){
        String topic = bindingMap.get(binding).get();
        sendKeyValuesToTopic(topic,valueSerde,values);
    }

    public void sendKeyValuesToDestination(String topic,List<KeyValueWithHeaders<String,?>> values){
        sendKeyValuesToDestination(topic, defaultSerde(Object.class).serializer(), values);
    }

    public void sendToDestination(String topic, Serializer<?> valueSerde, List<?> values){
        sendKeyValuesToDestination(topic, valueSerde, values.map(v -> convertToMessage(v)));
    }

    public void sendToDestination(String topic,List<?> values){
        sendToDestination(topic, defaultSerde(Object.class).serializer(), values);
    }

    public <V> List<V> readAllFromTopic(String topic, Deserializer<V> valueSerde, Duration timeout){
        return readAllKeyValuesFromTopic(topic,valueSerde,timeout).map(kvh -> kvh.value);
    }

    public <V> List<V> readAllFromTopic(String topic, Class<V> valueClass,Duration timeout){
        return readAllFromTopic(topic,defaultSerde(valueClass).deserializer(),timeout);
    }

    public <V> List<V> readAllFromTopic(String topic, Class<V> valueClass){
        return readAllFromTopic(topic,valueClass,DEFAULT_TIMEOUT);
    }

    public <V> List<V> readAllFromDestination(String topic, Deserializer<V> valueSerde, Duration timeout){
        return readAllKeyValuesFromDestination(topic,valueSerde,timeout).map(kvh -> kvh.value);
    }

    public <V> List<V> readAllFromDestination(String topic, Class<V> valueClass, Duration timeout){
        return readAllFromDestination(topic,defaultSerde(valueClass).deserializer(),timeout);
    }

    public <V> List<V> readAllFromDestination(String topic, Class<V> valueClass){
        return readAllFromDestination(topic,valueClass,DEFAULT_TIMEOUT);
    }



    public <V> List<KeyValueWithHeaders<String,V>> readAllKeyValuesFromTopic(String topic, Deserializer<V> valueSerde, Duration timeout){
        java.util.Map<String, Object> consumerProps = KafkaTestUtils.consumerProps("group123456", "false", broker.getDelegate());
        consumerProps.put("enable.auto.commit", "false");
        DefaultKafkaConsumerFactory<String,V> cf = new DefaultKafkaConsumerFactory<>(consumerProps,Serdes.String().deserializer(),valueSerde);
        org.apache.kafka.clients.consumer.Consumer<String,V> consumer = cf.createConsumer();
        java.util.List<KeyValueWithHeaders<String,V>> recordList = new ArrayList<>();
        try{
            broker.consumeFromAnEmbeddedTopicWithOffset(consumer,topic);
            ConsumerRecords<String, V> received = consumer.poll(timeout);
            while(!received.isEmpty()){
                received.records(topic).forEach(cr -> recordList.add(msg(cr)));
                consumer.commitSync();
                received = consumer.poll(timeout);
            }
        } finally {
            consumer.close();
        }
        return List.ofAll(recordList);
    }

    public <V> List<KeyValueWithHeaders<String,V>> readAllKeyValuesFromTopic(String topic, Class<V> valueClass, Duration timeout){
        return readAllKeyValuesFromTopic(topic,defaultSerde(valueClass).deserializer(),timeout);
    }

    public <V> List<KeyValueWithHeaders<String,V>> readAllKeyValuesFromTopic(String topic, Class<V> valueClass){
        return readAllKeyValuesFromTopic(topic,valueClass,DEFAULT_TIMEOUT);
    }

    public <V> List<KeyValueWithHeaders<String,V>> readAllKeyValuesFromDestination(String binding, Deserializer<V> valueSerde, Duration timeout){
        String topic = bindingMap.get(binding).get();
        return readAllKeyValuesFromTopic(topic,valueSerde,timeout);
    }

    public <V> List<KeyValueWithHeaders<String,V>> readAllKeyValuesFromDestination(String topic, Class<V> valueClass, Duration timeout){
        return readAllKeyValuesFromDestination(topic,defaultSerde(valueClass).deserializer(),timeout);
    }

    public <V> List<KeyValueWithHeaders<String,V>> readAllKeyValuesFromDestination(String topic, Class<V> valueClass){
        return readAllKeyValuesFromDestination(topic,valueClass,DEFAULT_TIMEOUT);
    }

    public void cleanStateStore(final String name, final StateStoreType type) {
        streamBuilders.forEach( s -> s.cleanStateStore(name));
    }

    private KeyValueWithHeaders<String, ?> convertToMessage(Object v) {
        if(HasKey.class.isAssignableFrom(v.getClass())){
            String key = ((HasKey)v).getKey().toString();
            return msg(key,v,new RecordHeader(HasKey.PARTITION_KEY_HEADER,key.getBytes()));
        } else {
            return msg(v);
        }
    }

    private <V> List<Header> registerSchemaAndExtractHeaders(final V value) {
        MessageType typeInfo = value.getClass().getAnnotation(MessageType.class);
        List<Header> headers = List.of();
        //String subject = value.getClass().getSimpleName().toLowerCase();
        if(typeInfo != null){
            //subject = typeInfo.context()+typeInfo.type();
            headers = headers
                    .append(new RecordHeader(Message.TYPE_HEADER,typeInfo.type().getBytes()))
                    .append(new RecordHeader(Message.CONTEXT_HEADER,typeInfo.context().getBytes()));
        }
        /*
        SchemaReference reference = new SchemaReference(subject,1,"avro");
        String storedSchema = schemaRegistryClient.fetch(reference);
        if(storedSchema == null){
            Schema schema = ReflectData.get().getSchema(value.getClass());
            ParsedSchema parsedSchema = new ParsedSchema(schema);
            schemaRegistryClient.register(subject,"avro",parsedSchema.getRepresentation());
        }
        */
       // String contentType = String.format("application/vnd.%s.v%s+avro",subject,1);
       // headers = headers.append(new RecordHeader("contentType",contentType.getBytes()));
        return headers;
    }


    private <V> Serde<V> defaultSerde(Class<V> valueClass){
        HeaderPreservingCompositeNonNativeSerde<V> valueSerde = new HeaderPreservingCompositeNonNativeSerde<>(compositeMessageConverterFactory,kafkaHeaderMapper);
        final String contentType = "application/*+avro";
        Map<String, Object> config = HashMap.of("valueClass", valueClass,"contentType", contentType);
        valueSerde.configure(config.toJavaMap(),false);
        return valueSerde;
    }

}
