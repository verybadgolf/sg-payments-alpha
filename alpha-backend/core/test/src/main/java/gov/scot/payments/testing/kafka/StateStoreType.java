package gov.scot.payments.testing.kafka;

public enum StateStoreType {

    KeyValue, Window
}
