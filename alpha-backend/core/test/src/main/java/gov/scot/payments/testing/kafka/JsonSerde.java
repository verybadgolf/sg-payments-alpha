package gov.scot.payments.testing.kafka;

import gov.scot.payments.json.ObjectMapperProvider;
import org.springframework.kafka.support.converter.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.Map;

public class JsonSerde<T> extends org.springframework.kafka.support.serializer.JsonSerde<T> {

    public JsonSerde() {
        super(ObjectMapperProvider.MAPPER);
    }

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        super.configure(configs, isKey);
        dontRemoveTypeHeaders();
        ((DefaultJackson2JavaTypeMapper)((JsonDeserializer)deserializer()).getTypeMapper()).setClassIdFieldName("contentType");
        ((DefaultJackson2JavaTypeMapper)((JsonSerializer)serializer()).getTypeMapper()).setClassIdFieldName("contentType");

    }
}
