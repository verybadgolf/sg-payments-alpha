package gov.scot.payments.testing.kafka;

import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.apache.kafka.common.record.TimestampType;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;
import org.apache.kafka.test.TestUtils;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.time.Duration;
import java.time.Instant;
import java.util.Properties;
import java.util.UUID;

@Builder
public class KafkaStreamsTestHarness {

    public static final String OUTPUT_TOPIC = "output";
    public static final String INPUT_TOPIC = "input";
    public static final String STATE_STORE = "store";

    @NonNull private Properties streamsConfiguration;
    @Builder.Default @Getter private StreamsBuilder builder = new StreamsBuilder();
    @NonNull @Getter private Serde serde;

    public static KafkaStreamsTestHarness.KafkaStreamsTestHarnessBuilder builderWithMappings(Serde keySerde, Serde valueSerde, Class... mappings){
        return KafkaStreamsTestHarness.builder()
                                      .streamsConfiguration(KafkaStreamsTestHarness.properties(keySerde.getClass(),valueSerde.getClass(),mappings))
                                      .serde(KafkaStreamsTestHarness.serde(valueSerde,mappings));
    }

    public static KafkaStreamsTestHarness.KafkaStreamsTestHarnessBuilder builderWithMappingsAndKeySerde(Class<? extends Serde> keySerde, Class... mappings){
        return KafkaStreamsTestHarness.builder()
                                      .streamsConfiguration(KafkaStreamsTestHarness.properties(keySerde,org.springframework.kafka.support.serializer.JsonSerde.class,mappings))
                                      .serde(KafkaStreamsTestHarness.defaultSerde(mappings));
    }

    public static KafkaStreamsTestHarness.KafkaStreamsTestHarnessBuilder builderWithMappings(Class<?>... mappings){
        return KafkaStreamsTestHarness.builder()
                                         .streamsConfiguration(KafkaStreamsTestHarness.defaultProperties(mappings))
                                         .serde(KafkaStreamsTestHarness.defaultSerde(mappings));
    }

    public TopologyTestDriver toTopology(){
        return toTopology(Instant.now());
    }

    public TopologyTestDriver toTopology(Instant time){
        return new TopologyTestDriver(builder.build(), streamsConfiguration,time);
    }

    public KafkaStreamsTestHarness withKvStore(Serde serde) {
        return withKvStore(STATE_STORE,serde);
    }

    public KafkaStreamsTestHarness withKvStore(String storeName, Serde serde) {
        return withKvStore(storeName,Serdes.ByteArray(),serde);
    }

    public KafkaStreamsTestHarness withKvStore(String storeName, Serde keySerde, Serde valueSerde) {
        final StoreBuilder storeBuilder = Stores.timestampedKeyValueStoreBuilder(
                Stores.inMemoryKeyValueStore(storeName),
                keySerde,
                valueSerde)
                .withCachingDisabled();
        builder.addStateStore(storeBuilder);
        return this;
    }

    public KafkaStreamsTestHarness withSessionStore(final Duration retentionPeriod) {
        return withSessionStore(STATE_STORE,retentionPeriod);
    }

    public KafkaStreamsTestHarness withWindowStore(final Duration windowSize
            , final Duration retentionPeriod) {
        return withWindowStore(STATE_STORE,windowSize,retentionPeriod);
    }

    @SuppressWarnings("unchecked")
    public KafkaStreamsTestHarness withWindowStore(final String storeName
            , final Duration windowSize
            , final Duration retentionPeriod) {
        final StoreBuilder storeBuilder = Stores.windowStoreBuilder(
                Stores.inMemoryWindowStore(storeName,
                        retentionPeriod,
                        windowSize,
                        false
                ),
                Serdes.String(),
                serde)
                .withCachingDisabled();

        builder.addStateStore(storeBuilder);
        return this;
    }

    public KafkaStreamsTestHarness withSessionStore(final String storeName, final Duration retentionPeriod) {
        final StoreBuilder storeBuilder = Stores.sessionStoreBuilder(
                Stores.inMemorySessionStore(storeName,
                        retentionPeriod
                ),
                Serdes.String(),
                serde)
                .withCachingDisabled();

        builder.addStateStore(storeBuilder);
        return this;
    }

    public <K,V> KTable<K,V> table(String topic){
        return builder.table(topic);
    }

    public <K,V> KTable<K,V> table(){
        return table(INPUT_TOPIC);
    }


    public <K,V> KStream<K,V> stream(String topic){
        return builder.stream(topic);
    }

    public <K,V> KStream<K,V> stream(){
        return stream(INPUT_TOPIC);
    }

    public <T> List<T> drain(final TopologyTestDriver topologyTestDriver) {
        return drain(topologyTestDriver, OUTPUT_TOPIC);
    }

    public <T> List<KeyValueWithHeaders<String, T>> drainKeyValues(final TopologyTestDriver topologyTestDriver) {
        return drainKeyValues(topologyTestDriver, OUTPUT_TOPIC);
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> drain(final TopologyTestDriver topologyTestDriver, final String topic) {
        return drainKeyValues(topologyTestDriver, topic).map(kv -> (T)kv.value);
    }

    @SuppressWarnings("unchecked")
    public <T> List<KeyValueWithHeaders<String, T>> drainKeyValues(final TopologyTestDriver topologyTestDriver
            , final String topic) {
        List<KeyValueWithHeaders<String, T>> results = List.of();
        while (true) {
            final ProducerRecord<byte[], byte[]> rawRecord = topologyTestDriver.readOutput(topic);
            if (rawRecord == null) {
                break;
            }
            final String key = Serdes.String().deserializer().deserialize(rawRecord.topic(), rawRecord.headers(), rawRecord.key());
            final T value = (T)serde.deserializer().deserialize(rawRecord.topic(), rawRecord.headers(),rawRecord.value());
            final ProducerRecord<String, T> record = new ProducerRecord<>(rawRecord.topic(), rawRecord.partition(), rawRecord.timestamp(), key, value, rawRecord.headers());
            results = results.append(new KeyValueWithHeaders<>(record.key(), record.value(),record.timestamp(),record.headers()));
        }
        return results;
    }

    public <T> Map<String, T> drainTable(final TopologyTestDriver topologyTestDriver
            , final String topic
            , Deserializer<T> deserializer) {

        Map<String, T> results = HashMap.empty();
        while (true) {
            final ProducerRecord<String, T> record = topologyTestDriver.readOutput(topic, Serdes.String().deserializer(), deserializer);
            // Tables ignore records with null keys.
            if (record == null || record.key() == null) {
                break;
            } else {
                // For tables, a null-valued record represents a "DELETE" or tombstone for the corresponding key.
                if (record.value() == null) {
                    results = results.remove(record.key());
                } else {
                    results = results.put(record.key(), record.value());
                }
            }
        }
        return results;
    }

    @SuppressWarnings("unchecked")
    public void sendKeyValues(final TopologyTestDriver topologyTestDriver
            , final String topic
            , final List<KeyValueWithHeaders<String, ?>> inputValues) {
        for (final KeyValueWithHeaders<String, ?> entity : inputValues) {
            final ConsumerRecord<byte[], byte[]> consumerRecord = new ConsumerRecord<>(
                    topic,
                    0,
                    0,
                    entity.timestamp,
                    TimestampType.LOG_APPEND_TIME,
                    -1L,
                    ConsumerRecord.NULL_SIZE,
                    ConsumerRecord.NULL_SIZE,
                    Serdes.String().serializer().serialize(topic, entity.headers,entity.key),
                    serde.serializer().serialize(topic, entity.headers,entity.value),
                    entity.headers
            );
            topologyTestDriver.pipeInput(consumerRecord);
        }
    }

    public void sendValues(final TopologyTestDriver topologyTestDriver
            , final String topic
            , final List<?> inputValues) {
        sendKeyValues(topologyTestDriver, topic, inputValues.map(t -> new KeyValueWithHeaders<>(null,t,0,new RecordHeaders())));
    }

    public void send(final TopologyTestDriver topologyTestDriver
            , final Object... inputValues) {
        sendValues(topologyTestDriver, INPUT_TOPIC,List.of(inputValues));
    }

    @SafeVarargs
    public final void sendKeyValues(final TopologyTestDriver topologyTestDriver
            , final KeyValueWithHeaders<String, ?>... inputValues) {
        sendKeyValues(topologyTestDriver, INPUT_TOPIC,List.of(inputValues));
    }

    public void send(final TopologyTestDriver topologyTestDriver
            , final String inputTopic
            , final Object... inputValues) {
        sendValues(topologyTestDriver, inputTopic,List.of(inputValues));
    }

    @SafeVarargs
    public final void sendKeyValues(final TopologyTestDriver topologyTestDriver
            , final String inputTopic
            , final KeyValueWithHeaders<String, ?>... inputValues) {
        sendKeyValues(topologyTestDriver, inputTopic,List.of(inputValues));
    }

    public static Properties properties(Class keySerde,Class valueSerde, Class... mappings) {
        final Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, UUID.randomUUID().toString());
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy config");
        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, keySerde.getName());
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, valueSerde.getName() );
        // Use a temporary directory for storing state, which will be automatically removed after the test.
        streamsConfiguration.put(StreamsConfig.STATE_DIR_CONFIG, TestUtils.tempDirectory().getAbsolutePath());
        streamsConfiguration.put(JsonSerializer.TYPE_MAPPINGS, buildMappingsConfig(mappings));
        return streamsConfiguration;
    }

    public static Properties defaultProperties(Class... mappings) {
        return properties(Serdes.ByteArraySerde.class,org.springframework.kafka.support.serializer.JsonSerde.class,mappings);
    }

    @SuppressWarnings("unchecked")
    public static <T> Serde<T> serde(Serde serde, Class... mappings) {
        final String mappingConfig = buildMappingsConfig(mappings);
        serde.configure(HashMap.of(JsonSerializer.TYPE_MAPPINGS, mappingConfig).toJavaMap(),false);
        return serde;
    }

    public static <T> Serde<T> defaultSerde(Class... mappings) {
        return serde(new org.springframework.kafka.support.serializer.JsonSerde(),mappings);
    }

    public static String buildMappingsConfig(final Class... mappings) {
        return List.of(mappings)
                   .map(c -> c.getName() + ":" + c.getName())
                   .intersperse(",")
                   .foldLeft(new StringBuilder(), StringBuilder::append)
                   .toString();
    }
}
