package gov.scot.payments.testing.spring;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.core.env.Environment;
import org.springframework.kafka.config.KafkaStreamsConfiguration;
import org.springframework.kafka.config.KafkaStreamsCustomizer;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;
import org.springframework.kafka.core.CleanupConfig;
import org.springframework.util.Assert;

import java.io.File;

@Slf4j
public class StreamsBuilderFactoryBeanDecorator extends StreamsBuilderFactoryBean {

    private final BeanFactory beanFactory;

    public StreamsBuilderFactoryBeanDecorator(KafkaStreamsConfiguration streamsConfig
            , CleanupConfig cleanupConfig
            ,BeanFactory beanFactory) {
        super(streamsConfig, cleanupConfig);
        this.beanFactory = beanFactory;
    }

    @Override
    protected StreamsBuilder createInstance() {
        if (this.isAutoStartup()) {
            Assert.state(this.getStreamsConfiguration() != null,
                    "'streams configuration properties must not be null");
        }
        return new StreamsBuilderTestingDecorator(beanFactory);
    }

    @Override
    public synchronized void start() {
        KafkaStreamsCustomizer customizer = beanFactory.getBean(KafkaStreamsCustomizer.class);
        setKafkaStreamsCustomizer(customizer);
        super.start();
    }

    @Override
    public synchronized void stop() {
        try{
            super.stop();
        } catch(Exception e){
            //ignore
        }

        if (System.getProperty("os.name").toLowerCase().contains("windows")) {
            log.info("WINDOWS OS MODE - Cleanup state store.");
            try {
                String applicationId = getStreamsConfiguration().getProperty(StreamsConfig.APPLICATION_ID_CONFIG);
                String tmpDir = getStreamsConfiguration().getProperty(StreamsConfig.STATE_DIR_CONFIG);
                FileUtils.deleteQuietly(new File(tmpDir,applicationId));
            } catch(Exception e) {
                log.error("could not stop streams",e);
            }
        }
    }
}
