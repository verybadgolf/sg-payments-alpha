package gov.scot.payments.application.component.projector.temporal.springdata.jpa;

import gov.scot.payments.application.component.projector.springdata.jpa.JpaProjectionRepository;
import gov.scot.payments.model.user.Subject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TestEntityRepository extends JpaProjectionRepository<String,TestEntity> {

    @Query("from TestEntity e")
    Page<TestEntity> findAll(Pageable pageable, Subject subject);

    @Query("from TestEntity e where e.id = ?1")
    Optional<TestEntity> findById(String id, Subject subject);
}
