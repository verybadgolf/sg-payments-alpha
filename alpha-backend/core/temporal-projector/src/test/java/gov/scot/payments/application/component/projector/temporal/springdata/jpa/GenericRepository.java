package gov.scot.payments.application.component.projector.temporal.springdata.jpa;

import gov.scot.payments.model.user.Subject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface GenericRepository extends GenericTemporalProjectionJpaRepository<TestEntity,GenericTemporalEntity> {

    @Override
    @Query(LATEST_BY_ID_QUERY + "and e.logicalId in ?#{#principal.getScopesAsString()}" + TEMPORAL_ORDER)
    Page<GenericTemporalEntity> findLatestById(String id
            , Instant processingTime
            , Instant eventTime
            , Pageable pageable
            , @Param("principal") Subject principal);

    @Override
    @Query(ALL_BY_ID_QUERY + "and e.logicalId in ?#{#principal.getScopesAsString()}" + TEMPORAL_ORDER)
    Page<GenericTemporalEntity> findAllById(String id
            , Instant processingTimeFrom
            , Instant processingTimeTo
            , Instant eventTimeFrom
            , Instant eventTimeTo
            , Pageable pageable
            , @Param("principal") Subject principal);

    @Query("from GenericTemporalEntity e")
    Page<GenericTemporalEntity> findAll(Pageable pageable, Subject subject);

    @Query("from GenericTemporalEntity e")
    Optional<GenericTemporalEntity> findById(UUID id, Subject subject);
}
