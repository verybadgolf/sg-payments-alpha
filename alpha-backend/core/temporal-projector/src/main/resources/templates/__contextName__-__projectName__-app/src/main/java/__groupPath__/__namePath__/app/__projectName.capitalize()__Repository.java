package @group@.@namePackage@.app;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import gov.scot.payments.application.component.projector.temporal.springdata.jpa.GenericTemporalProjectionJpaRepository;
import gov.scot.payments.model.user.Subject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import @group@.@namePackage@.model.@projectName.capitalize()@;
import @group@.@namePackage@.model.@projectName.capitalize()@TemporalEntity;

import java.time.Instant;

@Repository
public interface @projectName.capitalize()@Repository extends GenericTemporalProjectionJpaRepository<@projectName.capitalize()@,@projectName.capitalize()@TemporalEntity> {

    @Override
    @Query(LATEST_BY_ID_QUERY + "and e.logicalId in ?#{#principal.getScopesAsString()}" + TEMPORAL_ORDER)
        Page<@projectName.capitalize()@TemporalEntity> findLatestById(String id
            , Instant processingTime
            , Instant eventTime
            , Pageable pageable
            , @Param("principal") Subject principal);

    @Override
    @Query(ALL_BY_ID_QUERY + "and e.logicalId in ?#{#principal.getScopesAsString()}" + TEMPORAL_ORDER)
        Page<@projectName.capitalize()@TemporalEntity> findAllById(String id
            , Instant processingTimeFrom
            , Instant processingTimeTo
            , Instant eventTimeFrom
            , Instant eventTimeTo
            , Pageable pageable
            , @Param("principal") Subject principal);
}
