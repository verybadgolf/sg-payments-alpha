package gov.scot.payments.application.component.projector.temporal.springdata.jpa;


import gov.scot.payments.application.component.projector.temporal.springdata.TemporalProjectionRepository;
import gov.scot.payments.model.TemporalProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface JpaTemporalProjectionRepository<ID,PK,T extends TemporalProjection<ID,PK>> extends TemporalProjectionRepository<ID,PK,T>, JpaRepository<T,PK> {

}
