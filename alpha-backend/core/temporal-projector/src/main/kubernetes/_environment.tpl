{{- define "${contextName}-${projectName}.env" -}}
- name: SPRING_DATASOURCE_URL
  valueFrom:
    secretKeyRef:
      name: rds-credentials
      key: rds_url
- name: SPRING_DATASOURCE_PASSWORD
  valueFrom:
    secretKeyRef:
      name: rds-credentials
      key: rds_password
- name: SPRING_DATASOURCE_USERNAME
  valueFrom:
    secretKeyRef:
      name: rds-credentials
      key: rds_user_name
{{- end -}}