import { App } from "./App";
import React from "react";
import { shallow } from "enzyme";

describe("The App component", () => {
    it("renders", () => {
        const wrapper = shallow(<App />);
        expect(wrapper.exists()).toBe(true);
    });
});
