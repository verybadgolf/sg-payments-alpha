import { ThunkResult } from "../../state/rootReducer";

export const SET_STRING = "SET_STRING";

export const setString = (str: string): ThunkResult<void> => dispatch => {
    dispatch({ type: SET_STRING, payload: str });
};
