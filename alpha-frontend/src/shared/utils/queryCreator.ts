import { Sort } from "enums/sorting";

export const createPageSizeQuery = (pageSize?: number) => pageSize && `${pageSize}`;

export const createPageNumberQuery = (pageNumber?: number) => pageNumber && `${pageNumber}`;

export const createSortQuery = (sort: Sort<unknown>) => (sort.order !== "none" ? `${sort.field},${sort.order}` : null);
