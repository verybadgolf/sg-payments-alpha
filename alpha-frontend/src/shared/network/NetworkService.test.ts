import { LoginStatus } from "enums/loginStatus";
import { NetworkService } from "./NetworkService";
import state from "state";

describe("Given a NetworkService", () => {
    const testUrl = "http://test.api.endpoint.com";
    const testPath = "/test/path";
    const testIdToken = "test id token";
    const testAccessToken = "tests access token";

    let networkService: NetworkService;
    let mockGet: jest.Mock;
    let response: Promise<string | undefined>;

    beforeEach(() => {
        process.env = Object.assign(process.env, {
            REACT_APP_API_ENDPOINT: testUrl,
        });
        networkService = new NetworkService();
        mockGet = jest.fn();
        networkService.apiInstance.get = mockGet;
        state.getState().loginState = {
            status: LoginStatus.loggedIn,
            idToken: testIdToken,
            accessToken: testAccessToken,
        };
    });

    it("then the base uri is configured", () => {
        expect(networkService.apiInstance.defaults.baseURL).toBe(testUrl);
    });

    it("then the timeout is configured", () => {
        expect(networkService.apiInstance.defaults.timeout).toBe(5000);
    });

    describe("when get is called sucessfully", () => {
        const testResponseData = "test response text";

        beforeEach(() => {
            mockGet.mockReturnValue(Promise.resolve({ data: testResponseData }));
        });

        describe("and there are no parameters provided", () => {
            beforeEach(() => {
                response = networkService.get<string>(testPath);
            });

            it("then the response is returned", done => {
                response.then(data => {
                    expect(data).toBe(testResponseData);
                    done();
                });
            });

            it("then the correct path is used", done => {
                response.then(() => {
                    expect(mockGet.mock.calls[0][0]).toBe(testPath);
                    done();
                });
            });

            it("then the endpoint is called once", done => {
                response.then(() => {
                    expect(mockGet).toBeCalledTimes(1);
                    done();
                });
            });

            it("then the authentication header is included", done => {
                response.then(() => {
                    const options = mockGet.mock.calls[0][1];
                    expect(options.headers).toBeTruthy();
                    expect(options.headers.Authorization).toBe(`Bearer ${testIdToken}`);
                    done();
                });
            });
        });

        describe("and there are parameters provided", () => {
            const testParams = { foo: "bar" };
            beforeEach(() => {
                response = networkService.get<string>(testPath, testParams);
            });

            it("then the parameters are included", done => {
                response.then(() => {
                    const options = mockGet.mock.calls[0][1];
                    expect(options.params).toBe(testParams);
                    done();
                });
            });
        });
    });

    describe("when the get call fails", () => {
        const testError = new Error("test error");
        beforeEach(() => {
            mockGet.mockImplementation(() => {
                throw testError;
            });
            response = networkService.get<string>(testPath);
        });

        it("then the promise is rejected", done => {
            response
                .then(() => {
                    fail("Should not resolve");
                })
                .catch(err => {
                    expect(err).toBe(testError);
                    done();
                });
        });
    });
});
