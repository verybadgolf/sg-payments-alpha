import { Action, AnyAction, Dispatch, Reducer, combineReducers } from "redux";
import { LoginState, loginReducer } from "components/login/state/rootLoginState";
import { SandboxState, sandboxReducer } from "../sandbox/state/rootSandboxState";
import { ThunkAction, ThunkDispatch } from "redux-thunk";

import { Page } from "./page";
import { SubmissionRow } from "components/submissions/submissions-table/SubmissionsTable";
import { SubmissionsFilter } from "components/submissions/submissions-filter/submissionsFilter";
import { TransactionRow } from "components/transactions/transactions-table/TransactionsTable";
import { TransactionsFilter } from "components/transactions/transactions-filter/transactionsFilter";
import { submissionsFilterReducer } from "components/submissions/submissions-filter/state/submissionsFilterReducer";
import { submissionsReducer } from "components/submissions/state/submissionsState";
import { transactionsFilterReducer } from "components/transactions/transactions-filter/state/transactionFilterReducer";
import { transactionsReducer } from "components/transactions/state/transactionsState";

export interface RootState {
    sandboxState: SandboxState;
    loginState: LoginState;
    submissionsPage: Page<SubmissionRow> | null;
    transactionsPage: Page<TransactionRow> | null;
    transactionsFilter: TransactionsFilter;
    submissionsFilter: SubmissionsFilter;
}

export const reducers: Reducer<RootState> = combineReducers<RootState>({
    sandboxState: sandboxReducer,
    loginState: loginReducer,
    submissionsPage: submissionsReducer,
    transactionsPage: transactionsReducer,
    transactionsFilter: transactionsFilterReducer,
    submissionsFilter: submissionsFilterReducer,
});

export type ThunkResult<R> = ThunkAction<R, RootState, undefined, Action>;

export type AppDispatch = Dispatch & ThunkDispatch<RootState, undefined, AnyAction>;
