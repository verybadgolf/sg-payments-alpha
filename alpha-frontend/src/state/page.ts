export interface Page<T> {
    content: T[];
    totalElements: number;
    pageNumber: number;
    pageSize: number;
    totalPages: number;
}
