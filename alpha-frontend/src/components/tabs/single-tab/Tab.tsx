import { NavLink } from "react-router-dom";
import React from "react";

interface TabProps {
    label: string;
    path: string;
}

export class Tab extends React.Component<TabProps> {
    private tabRef: React.RefObject<HTMLAnchorElement>;

    constructor(props: TabProps) {
        super(props);
        this.tabRef = React.createRef();
    }

    private blurTabOnClick = () => {
        this.tabRef.current?.blur();
    };

    render = () => (
        <NavLink
            aria-label={`${this.props.label}`}
            to={this.props.path}
            activeClassName="active"
            className="single-tab"
            innerRef={this.tabRef}
            onClick={this.blurTabOnClick}
        >
            <span className="text-tab">{this.props.label}</span>
        </NavLink>
    );
}
