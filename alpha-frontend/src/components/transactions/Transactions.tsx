import "./Transactions.scss";

import { AppDispatch, RootState } from "state/rootReducer";
import React, { Fragment } from "react";

import { Paging } from "components/paging/Paging";
import SelectedTransactionsFilters from "components/selected-filter/selected-transactions-filter/SelectedTransactionsFilters";
import TransactionTable from "./transactions-table/TransactionsTable";
import { TransactionsFilter } from "./transactions-filter/transactionsFilter";
import { connect } from "react-redux";
import { fetchTransactions } from "./state/transactionsActions";
import { formatTransactionsNumber } from "./transactionsFormatters";
import { getNewPageNumber } from "components/paging/pageUtils";

const mapStateToProps = (state: RootState) => ({
    transactionsPage: state.transactionsPage,
    transactionsFilter: state.transactionsFilter,
});

const mapDispatchToProps = (dispatch: AppDispatch) => ({
    fetchTransactions: (query: TransactionsFilter) => dispatch(fetchTransactions(query)),
});

type ConnectedTransactionsProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;

export class Transactions extends React.Component<ConnectedTransactionsProps> {
    componentDidMount = () => {
        this.props.fetchTransactions(this.props.transactionsFilter);
    };

    componentDidUpdate = (oldProps: ConnectedTransactionsProps) => {
        if (oldProps.transactionsFilter !== this.props.transactionsFilter) {
            this.props.fetchTransactions(this.props.transactionsFilter);
        }
    };

    private changePageSize = (pageSize: number): void => {
        this.props.transactionsPage &&
            this.props.fetchTransactions({
                ...this.props.transactionsFilter,
                pageNumber: getNewPageNumber(this.props.transactionsPage, pageSize),
                pageSize: pageSize,
            });
    };

    private changePageNumber = (pageNumber: number): void => {
        this.props.fetchTransactions({ ...this.props.transactionsFilter, pageNumber: pageNumber });
    };

    render = () => (
        <div className="transaction-parent-container">
            <span className="transaction-page-title">Transactions</span>
            <div className="transaction-page-result-number-container">
                <span className="transaction-page-result-number">
                    {formatTransactionsNumber(this.props.transactionsPage?.totalElements)}
                </span>
            </div>
            <SelectedTransactionsFilters />
            <div className="transaction-page-table">
                {this.props.transactionsPage?.totalElements ? (
                    <Fragment>
                        <TransactionTable />
                        <Paging
                            pageProperty={this.props.transactionsPage}
                            changePageSize={this.changePageSize}
                            changePageNumber={this.changePageNumber}
                        />
                    </Fragment>
                ) : (
                    <p className="no-result">No results match the search criteria</p>
                )}
            </div>
        </div>
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(Transactions);
