import { ShallowWrapper, shallow } from "enzyme";

import React from "react";
import TransactionTable from "./transactions-table/TransactionsTable";
import { Transactions } from "./Transactions";

describe("Given a transactions component", () => {
    let wrapper: ShallowWrapper;
    let mockFetch: jest.Mock;
    beforeEach(() => {
        mockFetch = jest.fn();
    });

    describe("when the number of transactions is undefined", () => {
        beforeEach(() => {
            wrapper = shallow(
                <Transactions
                    transactionsPage={{ totalElements: undefined }}
                    transactionsFilter={{}}
                    fetchTransactions={mockFetch}
                />,
            );
        });

        it("then transactions are fetched", () => {
            expect(mockFetch).toBeCalledTimes(1);
        });

        it("then it displays a no result message", () => {
            expect(wrapper.find(".transaction-page-result-number").text()).toBe("0 transactions");
        });

        it("then hides the table and displays a custom message", () => {
            expect(wrapper.contains(<TransactionTable />)).toBe(false);
            expect(wrapper.find(".no-result").text()).toBe("No results match the search criteria");
        });
    });

    describe("when the number of transactions is defined", () => {
        const numberOfTransactions = 2;
        beforeEach(() => {
            wrapper = shallow(
                <Transactions
                    transactionsPage={{ totalElements: numberOfTransactions }}
                    transactionsFilter={{}}
                    fetchTransactions={mockFetch}
                />,
            );
        });

        it("then transactions are still fetched", () => {
            expect(mockFetch).toBeCalledTimes(1);
        });

        it("then displays the number of transactions", () => {
            expect(wrapper.find(".transaction-page-result-number").text()).toBe("2 transactions");
        });

        it("then it displays the transaction table", () => {
            expect(wrapper.contains(<TransactionTable />)).toBe(true);
        });
    });
});
