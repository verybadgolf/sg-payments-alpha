import { TransactionsFilter } from "./transactionsFilter";

export const constructTransactionFilterQuery = (queryObject: TransactionsFilter): string | null => {
    const query: string[] = [];
    //Apply paging filter
    if (queryObject.exactBatch) {
        query.push(`batchId==${queryObject.exactBatch.id}`);
    }

    if (queryObject.statuses && queryObject.statuses.length > 0) {
        query.push(`status=in=(${queryObject.statuses.join(",")})`);
    }

    return query.length > 0 ? query.join(";") : null;
};
