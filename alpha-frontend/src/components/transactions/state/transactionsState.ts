import { Page } from "state/page";
import { Reducer } from "redux";
import { SET_TRANSACTIONS_PAGE } from "./transactionsActions";
import { TransactionRow } from "../transactions-table/TransactionsTable";

export const transactionsReducer: Reducer<Page<TransactionRow> | null> = (state = null, action) => {
    switch (action.type) {
        case SET_TRANSACTIONS_PAGE:
            return action.payload;
        default:
            return state;
    }
};
