import { AppDispatch, RootState } from "state/rootReducer";
import { CellRenderers, Column, Table } from "components/reusables/table/Table";

import React from "react";
import { Sort } from "enums/sorting";
import SortableHeader from "components/reusables/table/custom-header/sortable/SortableHeader";
import { Status } from "components/reusables/status-label/StatusLabel";
import StatusCell from "../status-cell/StatusCell";
import { TransactionType } from "../state/transactionsMapper";
import { connect } from "react-redux";
import { formatDate } from "shared/utils/formatters";
import moment from "moment";
import { setTransactionsSort } from "../transactions-filter/state/transactionFilterActions";

export type TransactionColumnName = "Amount" | "Status" | "Message" | "Payment Date" | "Product" | "Batch ID";
export type TransactionColumnFields = "amount" | "status" | "message" | "paymentDate" | "product" | "batchId";

export interface TransactionRow {
    id: string;
    amount: string;
    status: Status;
    message: string;
    paymentDate: moment.Moment;
    product: string;
    batchId: string;
    createdAt: moment.Moment;
    type: TransactionType;
}

const cellRenders: CellRenderers<TransactionRow> = { statusRenderer: StatusCell };

const mapStateToProps = (state: RootState) => ({
    rowData: state.transactionsPage ? state.transactionsPage.content : [],
    transactionsSort: state.transactionsFilter.sortBy,
});

const mapDispatchToProps = (dispatch: AppDispatch) => ({
    setTransactionsSort: (sort: Sort<TransactionColumnFields>) => dispatch(setTransactionsSort(sort)),
});

type ConnectedTransactionTableProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;

class TransactionTable extends React.Component<ConnectedTransactionTableProps> {
    private getCurrentSorting = (): Sort<TransactionColumnFields> => this.props.transactionsSort;

    private columns: Column<TransactionRow, TransactionColumnFields>[] = [
        {
            field: "amount",
            headerComponentFramework: SortableHeader,
            headerComponentParams: {
                name: "Amount",
                field: "amount",
                sort: this.props.setTransactionsSort,
                getCurrentSorting: this.getCurrentSorting,
            },
            width: 150,
        },
        {
            field: "status",
            headerComponentFramework: SortableHeader,
            headerComponentParams: {
                name: "Status",
                field: "status",
                sort: this.props.setTransactionsSort,
                getCurrentSorting: this.getCurrentSorting,
            },
            width: 140,
            cellRenderer: "statusRenderer",
        },
        {
            field: "message",
            headerComponentFramework: SortableHeader,
            headerComponentParams: {
                name: "Message",
                field: "message",
                sort: this.props.setTransactionsSort,
                getCurrentSorting: this.getCurrentSorting,
            },
        },
        {
            field: "paymentDate",
            headerComponentFramework: SortableHeader,
            headerComponentParams: {
                name: "Payment Date",
                field: "paymentDate",
                sort: this.props.setTransactionsSort,
                getCurrentSorting: this.getCurrentSorting,
            },
            tooltipValueGetter: formatDate,
            valueFormatter: formatDate,
            width: 110,
        },
        {
            field: "product",
            headerComponentFramework: SortableHeader,
            headerComponentParams: {
                name: "Product",
                field: "product",
                sort: this.props.setTransactionsSort,
                getCurrentSorting: this.getCurrentSorting,
            },
        },
        {
            field: "batchId",
            headerComponentFramework: SortableHeader,
            headerComponentParams: {
                name: "Batch ID",
                field: "batchId",
                sort: this.props.setTransactionsSort,
                getCurrentSorting: this.getCurrentSorting,
            },
        },
    ];

    render = () => {
        return (
            <Table
                columns={this.columns}
                rows={this.props.rowData}
                cellRenderers={cellRenders}
                sort={this.props.transactionsSort}
            />
        );
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(TransactionTable);
