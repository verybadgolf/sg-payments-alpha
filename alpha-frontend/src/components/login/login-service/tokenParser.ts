export interface Tokens {
    idToken: string;
    accessToken: string;
}

export const parseToken = (url: string): Tokens => {
    const tokens: string[] = url.substring(0).split("&");
    const tokenObj: Tokens = {
        idToken: "",
        accessToken: "",
    };

    for (let i = 0; i < tokens.length; i++) {
        const tokenKv = tokens[i].split("=");
        if (tokenKv[0].includes("id_token")) {
            tokenObj.idToken = tokenKv[1];
        }
        if (tokenKv[0].includes("access_token")) {
            tokenObj.accessToken = tokenKv[1];
        }
    }
    return tokenObj;
};
