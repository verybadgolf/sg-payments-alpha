import "./Login.scss";

import { AppDispatch, RootState } from "state/rootReducer";

import { LoadingButton } from "components/reusables/loading-button/LoadingButton";
import { LoginStatus } from "enums/loginStatus";
import React from "react";
import { TextButton } from "components/reusables/text-button/TextButton";
import { connect } from "react-redux";
import { getAuthUrl } from "./login-service/loginService";
import { setLoggingIn } from "./state/statusAction";

const mapStateToProps = (state: RootState) => ({
    status: state.loginState.status,
});

const mapDispatchToProps = (dispatch: AppDispatch) => ({
    setLoggingIn: () => dispatch(setLoggingIn()),
});

type ConnectedLoginProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;

export class Login extends React.Component<ConnectedLoginProps> {
    private onLogInClick = () => {
        this.props.setLoggingIn();
        window.location.href = getAuthUrl();
    };

    private onCreateAccountClick = () => {
        console.log("creating account");
    };

    render = () => (
        <div className="login-parent-container">
            <span className="login-title">Make and track payments</span>
            <span className="login-detail">
                If you work in the public sector, use the Payments platform to make payments
            </span>
            <div className="login-actions">
                <LoadingButton
                    onClick={this.onLogInClick}
                    defaultText={"Sign in"}
                    loadingText={"Signing in"}
                    isLoading={this.props.status === LoginStatus.loggingIn}
                />
                <span className="action-text">{`If you don't have an account, you can`}</span>
                <div className="text-button-login">
                    {<TextButton onClick={this.onCreateAccountClick} value={"create one now"} />}
                </div>
            </div>
        </div>
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
