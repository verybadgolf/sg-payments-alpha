import { LoginStatus } from "enums/loginStatus";
import { ThunkResult } from "state/rootReducer";

export const SET_LOGGED_IN_STATUS = "SET_LOGGED_IN_STATUS";

export const setLoggedOut = (): ThunkResult<void> => dispatch => {
    dispatch({
        type: SET_LOGGED_IN_STATUS,
        payload: { status: LoginStatus.loggedOut, idToken: null, accessToken: null },
    });
};

export const setLoggingIn = (): ThunkResult<void> => dispatch => {
    dispatch({
        type: SET_LOGGED_IN_STATUS,
        payload: { status: LoginStatus.loggingIn, idToken: null, accessToken: null },
    });
};

export const setLoggedIn = (idToken: string, accessToken: string): ThunkResult<void> => dispatch => {
    dispatch({ type: SET_LOGGED_IN_STATUS, payload: { status: LoginStatus.loggedIn, idToken, accessToken } });
};
