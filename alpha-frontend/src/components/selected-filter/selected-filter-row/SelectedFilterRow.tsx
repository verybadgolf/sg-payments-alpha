import "./SelectedFilterRow.scss";

import { Pill } from "components/reusables/pills/Pill";
import React from "react";

interface SelectedFilterRowProps<T> {
    filterTitle: string;
    filterContent: T[];
    removeFilter: (data: T) => void;
    extractName: (data: T) => string;
}

export class SelectedFilterRow<T> extends React.Component<SelectedFilterRowProps<T>> {
    render = () =>
        this.props.filterContent.length > 0 && (
            <div className="filter-row-parent-container">
                <span className="filter-row-title">{this.props.filterTitle}</span>
                {this.props.filterContent.map(filter => {
                    const filterName = this.props.extractName(filter);
                    return (
                        <Pill
                            key={filterName}
                            content={filterName}
                            onClick={this.props.removeFilter}
                            dataToRemove={filter}
                        />
                    );
                })}
            </div>
        );
}
