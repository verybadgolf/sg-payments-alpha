import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import "./Submissions.scss";

import { AppDispatch, RootState } from "state/rootReducer";
import React, { Fragment } from "react";

import { Paging } from "components/paging/Paging";
import { SubmissionsFilter } from "./submissions-filter/submissionsFilter";
import SubmissionsTable from "./submissions-table/SubmissionsTable";
import { connect } from "react-redux";
import { fetchSubmissions } from "./state/submissionsActions";
import { formatResultsCount } from "./submissionsFormatters";
import { getNewPageNumber } from "components/paging/pageUtils";

const mapStateToProps = (state: RootState) => ({
    submissionsPage: state.submissionsPage,
    submissionsFilter: state.submissionsFilter,
});

const mapDispatchToProps = (dispatch: AppDispatch) => ({
    fetchSubmissions: (query: SubmissionsFilter) => dispatch(fetchSubmissions(query)),
});

type ConnectedSubmissionsPageProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;

export class Submissions extends React.Component<ConnectedSubmissionsPageProps> {
    componentDidMount = () => {
        this.props.fetchSubmissions(this.props.submissionsFilter);
    };

    componentDidUpdate = (prevProps: ConnectedSubmissionsPageProps) => {
        if (this.props.submissionsFilter !== prevProps.submissionsFilter) {
            this.props.fetchSubmissions(this.props.submissionsFilter);
        }
    };

    private changePageSize = (pageSize: number): void => {
        this.props.submissionsPage &&
            this.props.fetchSubmissions({
                ...this.props.submissionsFilter,
                pageNumber: getNewPageNumber(this.props.submissionsPage, pageSize),
                pageSize: pageSize,
            });
    };

    private changePageNumber = (pageNumber: number): void => {
        this.props.fetchSubmissions({ ...this.props.submissionsFilter, pageNumber: pageNumber });
    };

    render = () => (
        <div className="submission-page-parent-container">
            <div className="submissions-page-title-container">
                <span className="submissions-title">Submissions</span>
            </div>
            {/* <div className="submissions-filters"></div> */}
            <div className="submissions-page-result-number-container">
                <span className="result-number">{formatResultsCount(this.props.submissionsPage?.totalElements)}</span>
            </div>
            <div className="submissions-table-container">
                {this.props.submissionsPage?.totalElements ? (
                    <Fragment>
                        <SubmissionsTable />
                        <Paging
                            pageProperty={this.props.submissionsPage}
                            changePageSize={this.changePageSize}
                            changePageNumber={this.changePageNumber}
                        />
                    </Fragment>
                ) : (
                    <p className="no-result-text">No results match the search criteria</p>
                )}
            </div>
        </div>
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(Submissions);
