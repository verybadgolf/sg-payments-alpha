import { DtoSubmissions, mapSubmissionsToState } from "./submissionsMapper";
import { createPageNumberQuery, createPageSizeQuery, createSortQuery } from "shared/utils/queryCreator";

import { DtoPage } from "shared/network/DtoPage";
import NetworkService from "shared/network/NetworkService";
import { SubmissionsFilter } from "../submissions-filter/submissionsFilter";
import { ThunkResult } from "state/rootReducer";
import { constructSubmissionFilterQuery } from "../submissions-filter/submissionsFilterMapper";

const submissionsUrl = "/submissions";
const sbmissionsUrlQuery = "/submissions/search";

export const SET_SUBMISSIONS_PAGE = "SET_SUBMISSIONS_PAGE";

const getUrl = (queryObject: SubmissionsFilter): string => constructSubmissionFilterQuery(queryObject) === null ? submissionsUrl : sbmissionsUrlQuery;

export const fetchSubmissions = (queryObject: SubmissionsFilter): ThunkResult<void> => dispatch => {
    NetworkService.get<DtoPage<DtoSubmissions>>(getUrl(queryObject), {
        query: constructSubmissionFilterQuery(queryObject),
        sort: createSortQuery(queryObject.sortBy),
        size: createPageSizeQuery(queryObject.pageSize),
        page: createPageNumberQuery(queryObject.pageNumber),
    }).then(json => {
        if (json) {
            dispatch({
                type: SET_SUBMISSIONS_PAGE,
                payload: mapSubmissionsToState(json),
            });
        }
    });
};
