import { Sort } from "enums/sorting";
import { SubmissionsColumnFields } from "components/submissions/submissions-table/SubmissionsTable";
import { ThunkResult } from "state/rootReducer";

export const SET_SUBMISSIONS_SORT = "SET_SUBMISSIONS_SORT";

export const setSubmissionsSort = (sort: Sort<SubmissionsColumnFields>): ThunkResult<void> => dispatch => {
    dispatch({ type: SET_SUBMISSIONS_SORT, payload: sort });
};
