import { PagingFilter } from "components/paging/pagingFilter";
import { SubmissionsColumnFields } from "../submissions-table/SubmissionsTable";
import { sortType } from "enums/sorting";

export interface SubmissionsFilter extends PagingFilter {
    sortBy: { order: sortType; field: SubmissionsColumnFields };
}
