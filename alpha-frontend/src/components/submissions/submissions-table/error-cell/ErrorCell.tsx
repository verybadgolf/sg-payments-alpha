import "./ErrorCell.scss";

import { RouteComponentProps, withRouter } from "react-router-dom";
import { formatErrorsCount, formatPendingApprovalCount } from "../../submissionsFormatters";

import { AppDispatch } from "state/rootReducer";
import { CellRendererParams } from "components/reusables/table/Table";
import { ErrorIcon } from "resources/svgs/ErrorIcon";
import React from "react";
import { Status } from "components/reusables/status-label/StatusLabel";
import { SubmissionRow } from "../SubmissionsTable";
import { TRANSACTIONS_PATH } from "routes/routes";
import { TextButton } from "components/reusables/text-button/TextButton";
import { connect } from "react-redux";
import { setExactBatchFilter } from "components/transactions/transactions-filter/state/transactionFilterActions";

const mapDispatchToProps = (dispatch: AppDispatch) => ({
    setExactBatchFliter: (batchId: string, batchName: string, status: Status) =>
        dispatch(setExactBatchFilter(batchId, batchName, status)),
});

type ConnectedErrorCellProps = RouteComponentProps &
    ReturnType<typeof mapDispatchToProps> &
    CellRendererParams<SubmissionRow>;

class ErrorCell extends React.Component<ConnectedErrorCellProps> {
    private onErrorsClick = () => {
        this.props.setExactBatchFliter(this.props.data.batchId, this.props.data.name, "Error");
        this.props.history.push(TRANSACTIONS_PATH);
    };

    private onPendingApprovalClick = () => {
        this.props.setExactBatchFliter(this.props.data.batchId, this.props.data.name, "AwaitingApproval");
        this.props.history.push(TRANSACTIONS_PATH);
    };

    render = () => {
        const errors: JSX.Element[] = [];
        if (this.props.data.status === "Failed") {
            errors.push(
                <span key={this.props.value} className="error-text">
                    {this.props.value}
                </span>,
            );
        } else {
            if (this.props.data.totalErrorPayments > 0) {
                const value = formatErrorsCount(this.props.data.totalErrorPayments);
                errors.push(<TextButton key={value} value={value} onClick={this.onErrorsClick} />);
            }
            if (this.props.data.totalPendingPayments > 0) {
                const value = formatPendingApprovalCount(this.props.data.totalPendingPayments);
                errors.push(<TextButton key={value} value={value} onClick={this.onPendingApprovalClick} />);
            }
        }
        return (
            errors.length > 0 && (
                <div className="error-cell">
                    <ErrorIcon height={24} width={24} className="error-icon" />
                    {errors}
                </div>
            )
        );
    };
}

export default connect(null, mapDispatchToProps)(withRouter(ErrorCell));
