import { RouteComponentProps, withRouter } from "react-router-dom";

import { AppDispatch } from "state/rootReducer";
import { CellRendererParams } from "components/reusables/table/Table";
import React from "react";
import { SubmissionRow } from "../SubmissionsTable";
import { TRANSACTIONS_PATH } from "routes/routes";
import { TextButton } from "components/reusables/text-button/TextButton";
import { connect } from "react-redux";
import { setExactBatchFilter } from "components/transactions/transactions-filter/state/transactionFilterActions";

const mapDispatchToProps = (dispatch: AppDispatch) => ({
    setExactBatchFilter: (batchId: string, batchName: string) => dispatch(setExactBatchFilter(batchId, batchName)),
});

type ConnectedBatchIdCellProps = RouteComponentProps &
    ReturnType<typeof mapDispatchToProps> &
    CellRendererParams<SubmissionRow>;

class BatchIdCell extends React.Component<ConnectedBatchIdCellProps> {
    private onClick = () => {
        this.props.setExactBatchFilter(this.props.value, this.props.data.name);
        this.props.history.push(TRANSACTIONS_PATH);
    };

    render = () =>
        this.props.data.status !== "Failed" ? (
            <TextButton value={this.props.data.name} onClick={this.onClick} />
        ) : (
            `${this.props.data.name}`
        );
}

export default connect(null, mapDispatchToProps)(withRouter(BatchIdCell));
