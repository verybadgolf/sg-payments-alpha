import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import "./Table.scss";

import {
    ColDef,
    GridApi,
    GridOptions,
    GridReadyEvent,
    ICellRendererParams,
    ValueFormatterParams,
} from "ag-grid-community";

import { AgGridReact } from "ag-grid-react";
import React from "react";
import { Sort } from "enums/sorting";
import SortableHeader from "./custom-header/sortable/SortableHeader";

export interface FormatterParams<T> extends ValueFormatterParams {
    data: T;
}

export interface Column<T, K> extends ColDef {
    field: string;
    headerComponentFramework?: typeof SortableHeader;
    valueFormatter?: (params: FormatterParams<T>) => string | string;
}

export interface CellRendererParams<T> extends ICellRendererParams {
    data: T;
}

export interface CellRenderers<T> {
    [index: string]: React.ComponentType<CellRendererParams<T>>;
}

export interface Options<T> extends GridOptions {
    rowClassRules?: {
        [cssClassName: string]: ((params: { data: T }) => boolean) | string;
    };
}

interface TableProps<T, K> {
    columns: Column<T, K>[];
    rows: T[];
    sort: Sort<K>;
    cellRenderers?: CellRenderers<T>;
    gridOptions?: Options<T>;
}

export class Table<T, K> extends React.Component<TableProps<T, K>> {
    private api: GridApi | null | undefined;

    componentDidUpdate(prevProps: TableProps<T, K>) {
        if (prevProps.sort !== this.props.sort) {
            this.api?.refreshHeader();
        }
    }

    private onGridReady = (params: GridReadyEvent) => {
        this.api = params.api;
        params.api.sizeColumnsToFit();

        window.onresize = () => {
            params.api.sizeColumnsToFit();
        };
    };

    render = () => (
        <div className="ag-theme-balham" tabIndex={0}>
            {/* ADD pagination -> Props: pagination, paginationPageSize={pageNum} */}
            <AgGridReact
                unSortIcon={true}
                headerHeight={58}
                rowHeight={58}
                ensureDomOrder={true}
                columnDefs={this.props.columns}
                rowData={this.props.rows}
                domLayout="autoHeight"
                onGridReady={this.onGridReady}
                onRowDataChanged={this.onGridReady}
                onGridColumnsChanged={this.onGridReady}
                frameworkComponents={this.props.cellRenderers}
                gridOptions={this.props.gridOptions}
                enableBrowserTooltips={true}
                suppressHorizontalScroll
                suppressMovableColumns={true}
            />
        </div>
    );
}
