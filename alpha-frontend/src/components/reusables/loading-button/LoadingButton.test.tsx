import { ShallowWrapper, shallow } from "enzyme";

import { LoadingButton } from "./LoadingButton";
import React from "react";

describe("Given a loading button", () => {
    let wrapper: ShallowWrapper;
    let onClickFunction: jest.Mock;
    const defaultValue = "Submit";
    const loadingValue = "Submitting";

    describe("when no loading props", () => {
        beforeEach(() => {
            onClickFunction = jest.fn();
            wrapper = shallow(
                <LoadingButton defaultText={defaultValue} loadingText={loadingValue} onClick={onClickFunction} />,
            );
        });

        describe("when the button is clicked", () => {
            beforeEach(() => {
                wrapper.find(".loader-button").simulate("click");
            });

            it("its state is set to true", () => {
                expect(wrapper.state("isLoading")).toBe(true);
            });

            it("it renders the spinner icon", () => {
                expect(wrapper.find(".spinner")).toHaveLength(1);
            });

            it("it changes the display value", () => {
                expect(wrapper.find("button").text()).toEqual(loadingValue);
            });

            it("it calls its callback onClick function", () => {
                expect(onClickFunction).toBeCalled();
            });
        });

        it("then no spinner icon is rendered", () => {
            expect(wrapper.find(".spinner")).toHaveLength(0);
        });

        describe("when the is loading prop changes to true", () => {
            beforeEach(() => {
                wrapper.setProps({ isLoading: true });
            });

            it("then it renders the spinner icon", () => {
                expect(wrapper.find(".spinner")).toHaveLength(1);
            });
        });
    });
});
