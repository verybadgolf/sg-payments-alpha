import "./LoadingButton.scss";

import React from "react";

interface LoadingButtonProps {
    onClick: () => void;
    defaultText: string;
    loadingText: string;
    isLoading?: boolean;
}

interface LoadingButtonState {
    isLoading: boolean;
}

export class LoadingButton extends React.Component<LoadingButtonProps, LoadingButtonState> {
    private loadingButtonRef: React.RefObject<HTMLButtonElement>;

    constructor(props: LoadingButtonProps) {
        super(props);
        this.state = {
            isLoading: Boolean(this.props.isLoading),
        };
        this.loadingButtonRef = React.createRef();
    }

    private onClick = () => {
        this.loadingButtonRef.current?.blur();
        this.setState({ isLoading: true });
        this.props.onClick();
    };

    componentDidUpdate = (prevProps: LoadingButtonProps) => {
        if (prevProps.isLoading !== this.props.isLoading) {
            this.setState({ isLoading: Boolean(this.props.isLoading) });
        }
    };

    render = () => (
        <button
            className="loader-button"
            onClick={this.onClick}
            ref={this.loadingButtonRef}
            disabled={this.state.isLoading}
            aria-label={this.state.isLoading ? this.props.loadingText : this.props.defaultText}
        >
            <span className="button-text">
                {this.state.isLoading ? this.props.loadingText : this.props.defaultText}
            </span>
            {this.state.isLoading && <div className="spinner" />}
        </button>
    );
}
