import { ShallowWrapper, shallow } from "enzyme";

import React from "react";
import { TextButton } from "./TextButton";

describe("Given a text button", () => {
    const jestFunction: jest.Mock = jest.fn();
    let wrapper: ShallowWrapper;

    beforeEach(() => {
        wrapper = shallow(<TextButton onClick={jestFunction} value={"TextButton value"} />);
    });

    describe("When the button text is clicked", () => {
        beforeEach(() => {
            wrapper.find(".text-button").simulate("click");
        });

        it("then calls the function passed as props", () => {
            expect(jestFunction).toBeCalled();
        });
    });
});
