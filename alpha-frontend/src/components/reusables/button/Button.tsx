import "./Button.scss";

import React from "react";

interface ButtonProps {
    display: string;
    onClick: () => void;
    isPrimary: boolean;
}

export class Button extends React.Component<ButtonProps> {
    render = () => (
        <button
            className={`${this.props.isPrimary ? "button-primary" : "button-secondary"}`}
            onClick={this.props.onClick}
            aria-label={this.props.display}
        >
            {this.props.display}
        </button>
    );
}
