import "./PaginationButton.scss";

import React from "react";

interface PaginationButtonProps {
    value: number;
    selectedPage: number;
    onClick: (pageNum: number) => void;
}

export class PaginationButton extends React.Component<PaginationButtonProps> {
    private buttonRef: React.RefObject<HTMLButtonElement>;

    constructor(props: PaginationButtonProps) {
        super(props);
        this.buttonRef = React.createRef();
    }

    private isPageSelected = (): boolean => this.props.value === this.props.selectedPage;

    private paginationClick = () => {
        if (!this.isPageSelected()) {
            this.buttonRef.current?.blur();
            this.props.onClick(this.props.value);
        }
    };

    render = () => (
        <button
            className={`pagination-button ${this.isPageSelected() && "pagination-button-selected"}`}
            onClick={this.paginationClick}
            aria-label={`Page ${this.props.value}`}
            tabIndex={this.isPageSelected() ? -1 : 0}
            ref={this.buttonRef}
        >
            {this.props.value}
        </button>
    );
}
