import "./List.scss";

import React from "react";

export interface RowDef<T> {
    key: string;
    extractContent: (data: T) => string | JSX.Element;
}

interface ListProps<T> {
    title: string;
    data: T;
    rows: RowDef<T>[];
}

export class List<T> extends React.Component<ListProps<T>> {
    render = () => (
        <div className="list-parent-container">
            <div className="title-container" aria-label={`${this.props.title} table`}>
                <span className="list-title">{this.props.title}</span>
            </div>
            <table className="list-table">
                <tbody>
                    {this.props.rows.map(row => (
                        <tr key={row.key} className="table-row">
                            <td className="row-key">{row.key}</td>
                            <td className="row-content">
                                <div className="center-content">{row.extractContent(this.props.data)}</div>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}
