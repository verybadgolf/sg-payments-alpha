import StatusLabel, { Status, StyleClass } from "./StatusLabel";

import { BlockIcon } from "resources/svgs/BlockIcon";
import { CheckIcon } from "resources/svgs/CheckIcon";
import { CloseIcon } from "resources/svgs/CloseIcon";
import { ErrorOutlineIcon } from "resources/svgs/ErrorOutlineIcon";
import { KeyboardReturnIcon } from "resources/svgs/KeyboardReturnIcon";
import React from "react";
import { SVGProps } from "resources/svgs/svgProps";
import { SeeMoreHorizontalIcon } from "resources/svgs/SeeMoreHorizontalIcon";

const iconDimension = 24;

const getStyle = (statusText: Status): StyleClass => {
    switch (statusText) {
        case "Paid":
        case "InProgress":
            return "status-ok";
        case "AwaitingApproval":
            return "status-warn";
        case "Error":
        case "Returned":
            return "status-error";
        case "Cancelled":
            return "status-cancel";
    }
};

const getIcon = (statusText: Status): React.ReactElement<SVGProps> => {
    switch (statusText) {
        case "Paid":
            return <CheckIcon width={iconDimension} height={iconDimension} />;
        case "InProgress":
            return <SeeMoreHorizontalIcon width={iconDimension} height={iconDimension} />;
        case "AwaitingApproval":
            return <ErrorOutlineIcon height={iconDimension} width={iconDimension} />;
        case "Returned":
            return <KeyboardReturnIcon height={iconDimension} width={iconDimension} />;
        case "Error":
            return <CloseIcon height={iconDimension} width={iconDimension} />;
        case "Cancelled":
            return <BlockIcon height={iconDimension} width={iconDimension} />;
    }
};

const getText = (statusText: Status): string => {
    switch (statusText) {
        case "InProgress":
            return "In progress";
        case "AwaitingApproval":
            return "Pending approval";
        default:
            return statusText;
    }
};

export const createStatusLabel = (type: Status) => {
    return <StatusLabel styleClass={getStyle(type)} icon={getIcon(type)} statusText={getText(type)} />;
};
