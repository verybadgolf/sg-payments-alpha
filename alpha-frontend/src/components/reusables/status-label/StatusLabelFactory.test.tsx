import { ShallowWrapper, shallow } from "enzyme";

import { BlockIcon } from "resources/svgs/BlockIcon";
import { CheckIcon } from "resources/svgs/CheckIcon";
import { CloseIcon } from "resources/svgs/CloseIcon";
import { ErrorOutlineIcon } from "resources/svgs/ErrorOutlineIcon";
import { KeyboardReturnIcon } from "resources/svgs/KeyboardReturnIcon";
import { SeeMoreHorizontalIcon } from "resources/svgs/SeeMoreHorizontalIcon";
import { Status } from "./StatusLabel";
import { createStatusLabel } from "./StatusLabelFactory";

describe("Given the status label factory", () => {
    let factoryResult: ShallowWrapper;
    let statusType: Status;

    describe("When passed the Label content 'Pending approval'", () => {
        beforeEach(() => {
            statusType = "AwaitingApproval";
            factoryResult = shallow(createStatusLabel(statusType));
        });

        it("it generates a JSX element with orange background color", () => {
            expect(factoryResult.find(".status-warn")).toHaveLength(1);
        });

        it("it contains the correct label", () => {
            expect(factoryResult.text()).toContain("Pending approval");
        });

        it("renders the correct icon", () => {
            expect(factoryResult.find(ErrorOutlineIcon)).toBeTruthy();
        });
    });

    describe("When passed the Label content 'In Progress'", () => {
        beforeEach(() => {
            statusType = "InProgress";
            factoryResult = shallow(createStatusLabel(statusType));
        });

        it("it generates a JSX element with green background color", () => {
            expect(factoryResult.find(".status-ok")).toHaveLength(1);
        });

        it("it contains the correct label", () => {
            expect(factoryResult.text()).toContain("In progress");
        });

        it("renders the correct icon", () => {
            expect(factoryResult.find(SeeMoreHorizontalIcon)).toBeTruthy();
        });
    });

    describe("When passed the Label content 'Paid'", () => {
        beforeEach(() => {
            statusType = "Paid";
            factoryResult = shallow(createStatusLabel(statusType));
        });

        it("it generates a JSX element with green background color", () => {
            expect(factoryResult.find(".status-ok")).toHaveLength(1);
        });

        it("it contains the correct label", () => {
            expect(factoryResult.text()).toContain(statusType);
        });

        it("renders the correct icon", () => {
            expect(factoryResult.find(CheckIcon)).toBeTruthy();
        });
    });

    describe("When passed the Label content 'Returned'", () => {
        beforeEach(() => {
            statusType = "Returned";
            factoryResult = shallow(createStatusLabel(statusType));
        });

        it("it generates a JSX element with red background color", () => {
            expect(factoryResult.find(".status-error")).toHaveLength(1);
        });

        it("it contains the correct label", () => {
            expect(factoryResult.text()).toContain(statusType);
        });

        it("renders the correct icon", () => {
            expect(factoryResult.find(KeyboardReturnIcon)).toBeTruthy();
        });
    });

    describe("When passed the Label content 'Error'", () => {
        beforeEach(() => {
            statusType = "Error";
            factoryResult = shallow(createStatusLabel(statusType));
        });

        it("it generates a JSX element with red background color", () => {
            expect(factoryResult.find(".status-error")).toHaveLength(1);
        });

        it("it contains the correct label", () => {
            expect(factoryResult.text()).toContain(statusType);
        });

        it("renders the correct icon", () => {
            expect(factoryResult.find(CloseIcon)).toBeTruthy();
        });
    });

    describe("When passed the Label content 'Cancelled'", () => {
        beforeEach(() => {
            statusType = "Cancelled";
            factoryResult = shallow(createStatusLabel(statusType));
        });

        it("it generates a JSX element with grey background color", () => {
            expect(factoryResult.find(".status-cancel")).toHaveLength(1);
        });

        it("it contains the correct label", () => {
            expect(factoryResult.text()).toContain(statusType);
        });

        it("renders the correct icon", () => {
            expect(factoryResult.find(BlockIcon)).toBeTruthy();
        });
    });
});
