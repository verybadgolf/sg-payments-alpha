import "./StatusLabel.scss";

import React from "react";
import { SVGProps } from "resources/svgs/svgProps";

export type Status = "AwaitingApproval" | "InProgress" | "Paid" | "Returned" | "Error" | "Cancelled";
export type StyleClass = "status-ok" | "status-warn" | "status-error" | "status-cancel";

export interface StatusLabelProps {
    statusText: string;
    styleClass: StyleClass;
    icon: React.ReactElement<SVGProps>;
}

class StatusLabel extends React.Component<StatusLabelProps> {
    render = () => (
        <div className={`status-label-main-container ${this.props.styleClass}`} aria-label={this.props.statusText}>
            {this.props.icon}
            <span className="status-text">{this.props.statusText}</span>
        </div>
    );
}

export default StatusLabel;
