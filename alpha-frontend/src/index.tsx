import "react-app-polyfill/ie11";
import "react-app-polyfill/stable";
import "./index.css";

import * as serviceWorker from "./serviceWorker";

import { HashRouter, Route } from "react-router-dom";

import App from "./App";
import { Provider } from "react-redux";
import React from "react";
import ReactDOM from "react-dom";
import store from "./state";

ReactDOM.render(
    <Provider store={store}>
        <HashRouter hashType="slash">
            <Route path="/" component={App} />
        </HashRouter>
    </Provider>,
    document.getElementById("root"),
);

serviceWorker.unregister();
