import React, { Component } from 'react';
import InboundPayment from "./InboundPayments"

class InboundPaymentsDataTable extends Component {

    render(){
        return (     
            <table className="payments-table" >
            <tbody>
                <tr className="payments-table-header-row">
                    <th className="symbol-row"></th>
                    <th className ="id-row">Pay ID</th>
                    <th className ="large-fixed-width-cell">Created</th>
                    <th className ="fixed-width-cell">Service</th>
                    <th className ="large-fixed-width-cell">Reference</th>
                    <th className="fixed-width-cell">Channel</th>
                    <th className ="large-fixed-width-cell">Status</th>
                    <th className ="fixed-width-cell">£ Amount</th>
                </tr>
                    {
                        this.props.payments.map((payment, i) => <InboundPayment key={i} payment={payment} onRowClick={this.props.onRowClick} />) 
                    }
            </tbody>               
            </table>            
        )
    }
}

export default InboundPaymentsDataTable;