import sys

from payment_poc_common import *
from payments_poc_bottomline_env import *
from payments_poc_stack_env import *


class AWSArnInfo:
    vpc_id = ""
    load_balancer_arn = ""
    subnet_security_group = ""
    execution_role_arn = ""
    subnet_array = []
    lb_target_group_arn = ""


arn_info = AWSArnInfo()


def populate_arn_info_execution_role():
    response = iam_client.get_role(
        RoleName=execution_role_name
    )
    if len(response['Role']) == 0:
        raise Exception(f"Could not find role {execution_role_name}")

    arn_info.execution_role_arn = response['Role']['Arn']
    print(f"Found ARN for role {execution_role_name}" + arn_info.execution_role_arn)


def populate_arn_info():
    populate_arn_info_from_loadbalancer(load_balancer_name)
    populate_arn_info_execution_role()
    arn_info.subnet_array = get_subnet_info(arn_info.vpc_id)


def get_task_role_arn():
    return iam_client.get_role(
        RoleName=taskRoleName
    )['Role']['Arn']


def get_rds_instance_endpoint():
    instances = rds_client.describe_db_instances(
        DBInstanceIdentifier=rds_instance_name
    )['DBInstances']

    if len(instances) == 0:
        raise Exception(f"Missing RDS instance {rds_instance_name}")

    endpoint_data = instances[0]['Endpoint']

    return f"{endpoint_data['Address']}:{endpoint_data['Port']}"

def get_api_gateway_base_uri():
    return f"https://{stack_name}.{domain_name}/{stack_name}/{task_name_base}-core"

def get_api_gateway_template_uri():
    return f"https://{stack_name}.{domain_name}/{stack_name}/{task_name_base}-{{name}}/api"


def register_task_definition(task_name, image, params):

    task_role_arn = get_task_role_arn()
    print(f"Using task role arn {task_role_arn}")

    port = get_port(task_name)

    # Create a task definition
    response = ecs_client.register_task_definition(
        containerDefinitions=[
            {
                "logConfiguration": {
                    "logDriver": "awslogs",
                    "options": {
                        "awslogs-group": "/ecs/" + task_name,
                        "awslogs-region": region,
                        "awslogs-stream-prefix": "ecs"
                    }
                },
                "portMappings": [
                    {
                        "hostPort": port,
                        "protocol": "tcp",
                        "containerPort": port
                    }
                ],
                "memoryReservation": 512,
                "volumesFrom": [],
                "image": image,
                "name": task_name,
                "environment": get_task_environment(port, params),
                "secrets": get_task_secrets()
            }
        ],
        family=task_name,
        taskRoleArn=task_role_arn,
        cpu="1024",
        memory="2048",
        networkMode='awsvpc',
        requiresCompatibilities=['FARGATE'],
        executionRoleArn=arn_info.execution_role_arn,
    )

    task_definition_arn = response['taskDefinition']['taskDefinitionArn']
    print("Registered task definition " + task_definition_arn)


def get_additional_parameters(parameters):

    aws_param = []
    for parameter in parameters:
        parameter_array = parameter.split("=")
        converted_parameter = {
            "name": parameter_array[0],
            "value": parameter_array[1]
        }
        aws_param.append(converted_parameter)

    return aws_param;

def get_task_environment(port, parameters):
    zookeeper_servers = get_kafka_zookeepers(kafka_cluster_name)
    kafka_brokers = get_kafka_brokers(kafka_cluster_name)
    rds_instance_endpoint = get_rds_instance_endpoint()
    api_gateway_gateway_template_uri = get_api_gateway_template_uri()
    api_gateway_base_uri = get_api_gateway_base_uri()
    env = [
        {
            "name": "SERVER_PORT",
            "value": str(port)
        },
        {
            "name": "PAYMENTS_ENVIRONMENT",
            "value": branch_name
        },
        {
            "name": "ZOOKEEPER_SERVERS",
            "value": zookeeper_servers
        },
        {
            "name": "KAFKA_BROKERS",
            "value": kafka_brokers
        },
        {
            "name": "RDS_DB_NAME",
            "value": branch_db_name
        },
        {
            "name": "RDS_INSTANCE_ENDPOINT",
            "value": rds_instance_endpoint
        },
        {
            "name": "COGNITO_USER_POOL_ID",
            "value": cognito_user_pool_id
        },
        {
            "name": "UI_COGNITO_APP_CLIENT_ID",
            "value": ui_cognito_app_client_id
        },
        {
            "name": "SERVICE_TO_SERVICE_COGNITO_APP_CLIENT_ID",
            "value": service_to_service_cognito_app_client_id
        },
        {
            "name": "COGNITO_DOMAIN",
            "value": cognito_domain
        }
    ]
    if component == 'core':
        env.extend([
            {
                "name": "AWS_STACK_NAME",
                "value": stack_name
            },
            {
                "name": "AWS_ACCOUNT_ID",
                "value": accountId
            },
            {
                "name": "QUICK_SIGHT_ROLE_ARN",
                "value": quick_sight_role_arn
            },
            {
                "name": "QUICK_SIGHT_ROLE_NAME",
                "value": quick_sight_role_name
            },
            {
                "name": "SPRING_PROFILES_ACTIVE",
                "value": "aws,cognito,rds,msk,s3,quicksight"
            },
            {
                "name": "SQS_FILE_EVENT_QUEUE",
                "value": sqs_file_event_queue
            },
            {
                "name": "API_GATEWAY_GATEWAY_TEMPLATE_URI",
                "value": api_gateway_gateway_template_uri
            },
            {
                "name": "API_GATEWAY_BASE_URI",
                "value": api_gateway_base_uri
            }
        ])
    else:
        # gateways
        lb_host = get_load_balancer(load_balancer_name)['DNSName']
        env.extend([
            {
                "name": "GATEWAY_NAME",
                "value": component
            },
            {
                "name": "SPRING_PROFILES_ACTIVE",
                "value": "aws,cognito,rds,msk"
            },
            {
                "name": "LB_HOST",
                "value": lb_host
            },
            {
                "name": "LB_PORT",
                "value": str(port)
            },
            {
                "name": "API_GATEWAY_GATEWAY_TEMPLATE_URI",
                "value": api_gateway_gateway_template_uri
            }
        ])

        if(len(parameters) > 0):
            env.extend(parameters)

        if component == "bottomline":
            env.extend([
                {
                    "name": "BOTTOMLINE_BACS_PROFILE_ID",
                    "value": bottomline_bacs_profile
                },
                {
                    "name": "BOTTOMLINE_FPS_PROFILE_ID",
                    "value": bottomline_fps_profile
                },
                {
                    "name": "PTX_ENDPOINT",
                    "value": ptx_endpoint
                }
            ])

        if component == 'worldpay':
            env.extend([
                {
                    "name": "WORLDPAY_API_ENDPOINT",
                    "value": worldpay_api_endpoint
                }
            ])

    return env


def get_task_secrets():

    rds_user_name_secret_name = f"/RDS/{rds_instance_name}/{branch_db_name}/RDS_USERNAME"
    rds_user_pwd_secret_name = f"/RDS/{rds_instance_name}/{branch_db_name}/RDS_PASSWORD"
    service_to_service_cognito_app_client_secret_name = f"/KEYS/{full_task_name}/SERVICE_TO_SERVICE_COGNITO_APP_CLIENT_SECRET"

    ssm_client.put_parameter(
        Name=rds_user_name_secret_name,
        Value=rds_master_user,
        Type="SecureString",
        Overwrite=True
    )

    ssm_client.put_parameter(
        Name=rds_user_pwd_secret_name,
        Value=rds_master_user_password,
        Type="SecureString",
        Overwrite=True
    )

    ssm_client.put_parameter(
        Name=service_to_service_cognito_app_client_secret_name,
        Value=service_to_service_cognito_app_client_secret,
        Type="SecureString",
        Overwrite=True
    )
    secrets = [
        {
            "name": "RDS_USERNAME",
            "valueFrom": rds_user_name_secret_name
        },
        {
            "name": "RDS_PASSWORD",
            "valueFrom": rds_user_pwd_secret_name
        },
        {
            "name": "SERVICE_TO_SERVICE_COGNITO_APP_CLIENT_SECRET",
            "valueFrom": service_to_service_cognito_app_client_secret_name
        }
    ]

    if component == 'core':
        access_key_id_secret_name = f"/KEYS/{full_task_name}/AWS_ACCESS_KEY_ID"
        secret_access_key_secret_name = f"/KEYS/{full_task_name}/AWS_SECRET_ACCESS_KEY"
        ssm_client.put_parameter(
            Name=access_key_id_secret_name,
            Value=access_key,
            Type="SecureString",
            Overwrite=True
        )
        ssm_client.put_parameter(
            Name=secret_access_key_secret_name,
            Value=secret_key,
            Type="SecureString",
            Overwrite=True
        )
        secrets.extend([
            {
                "name": "AWS_ACCESS_KEY_ID",
                "valueFrom": access_key_id_secret_name
            },
            {
                "name": "AWS_SECRET_ACCESS_KEY",
                "valueFrom": secret_access_key_secret_name
            }
        ])

    if component == 'bottomline':
        ptxusername_secret_name = f"/KEYS/{full_task_name}/PTX_USERNAME"
        ptxusername_secret_password = f"/KEYS/{full_task_name}/PTX_PASSWORD"

        ssm_client.put_parameter(
            Name=ptxusername_secret_name,
            Value=ptx_username,
            Type="SecureString",
            Overwrite=True
        )
        ssm_client.put_parameter(
            Name=ptxusername_secret_password,
            Value=ptx_password,
            Type="SecureString",
            Overwrite=True
        )
        secrets.extend([
            {
                "name": "PTX_USERNAME",
                "valueFrom": ptxusername_secret_name
            },
            {
                "name": "PTX_PASSWORD",
                "valueFrom": ptxusername_secret_password
            }
        ])

    if component == 'worldpay':
        worldpay_service_key_name = f"/KEYS/{full_task_name}/WORLDPAY_SERVICE_KEY"
        worldpay_client_key_name = f"/KEYS/{full_task_name}/WORLDPAY_CLIENT_KEY"

        ssm_client.put_parameter(
            Name=worldpay_service_key_name,
            Value=worldpay_service_key,
            Type="SecureString",
            Overwrite=True
        )
        ssm_client.put_parameter(
            Name=worldpay_client_key_name,
            Value=worldpay_client_key,
            Type="SecureString",
            Overwrite=True
        )
        secrets.extend([
            {
                "name": "WORLDPAY_SERVICE_KEY",
                "valueFrom": worldpay_service_key_name
            },
            {
                "name": "WORLDPAY_CLIENT_KEY",
                "valueFrom": worldpay_client_key_name
            }
        ])

    return secrets


def get_lb_port_for_branch():
    if branch_name == 'master':
        return int(os.environ['LB_PROD_PORT'])

    return int(os.environ['LB_DEV_PORT'])


def populate_arn_info_from_loadbalancer(lb_name):
    global arn_info

    response = elb_client.describe_load_balancers(
        Names=[lb_name]
    )

    if len(response['LoadBalancers']) == 0:
        raise Exception(f'Count not find load balancer for {lb_name}')

    load_balancer = response['LoadBalancers'][0]

    arn_info.vpc_id = load_balancer['VpcId']
    arn_info.load_balancer_arn = load_balancer['LoadBalancerArn']
    arn_info.subnet_security_group = load_balancer['SecurityGroups'][0]

    print(
        f"Populated arn info.. \n VPC ID {arn_info.vpc_id} \n Load Balancer ARN {arn_info.load_balancer_arn} \n Subnet {arn_info.subnet_security_group}")


def describe_service(service_name):
    ecs_client.describe_services(
        cluster=ecs_cluster_name,
        services=[service_name]
    )
    print(f"Describing services")


populate_arn_info()
component = sys.argv[1]
image_component = sys.argv[2]
parameters = sys.argv[3:]

aws_param = get_additional_parameters(parameters)
full_task_name = f"{task_name_base}-{component}"
image = f"{ecr_repo_arn}:{task_name_base}-{image_component}"

print(f" * Registering new Task Definition {full_task_name} * ")
print(f" * Loading parameters {aws_param} * ")

register_task_definition(full_task_name, image, aws_param)
