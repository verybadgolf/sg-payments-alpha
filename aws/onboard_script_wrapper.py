import os

# To build a runnable executable, install PyInstaller and run the following command:
# pyinstaller --onefile --hidden-import configparser .\onboard_script_wrapper.py

default_region = "eu-west-1"
default_api_url = "payments-poc.sgpaypoc.org.uk/payments-poc"


def read_input(prompt):
    response = None
    while response is None or response is '':
        response = input(prompt)
        if response is None or response is '':
            print("Please enter a value.")
    return response


os.environ['APP_NAME'] = "payments-poc"
os.environ['BRANCH'] = "develop"

os.environ['AWS_ACCOUNT_ID'] = read_input("AWS account id: ")
os.environ['AWS_ACCESS_KEY_ID'] = read_input("AWS access key id: ")
os.environ['AWS_SECRET_ACCESS_KEY'] = read_input("AWS secret access key: ")
os.environ['AWS_REGION'] = input(f"AWS region [{default_region}]: ") or default_region
os.environ['API_URL_PORT'] = input(f"API URL [{default_api_url}]: ") or default_api_url
os.environ['COGNITO_USER_POOL_ID'] = read_input("Cognito user pool id: ")
os.environ['COGNITO_CLIENT_ID'] = read_input("Cognito client id: ")


from onboard_script import run_onboarding
run_onboarding()
