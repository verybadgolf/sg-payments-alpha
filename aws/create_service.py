import sys

import psycopg2
from payments_poc_stack_env import *
from payment_poc_common import *
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT


sqs_file_upload_queue_policy_file_path = "sqs_file_upload_queue_policy.json"


class AWSArnInfo:
    vpc_id = ""
    load_balancer_arn = ""
    ecs_security_group = ""
    elb_security_group = ""
    execution_role_arn = ""
    subnet_array = []
    lb_target_group_arn = ""


arn_info = AWSArnInfo()


def populate_arn_info_execution_role():
    response = iam_client.get_role(
        RoleName=execution_role_name
    )
    if len(response['Role']) == 0:
        raise Exception(f"Could not find role {execution_role_name}")

    arn_info.execution_role_arn = response['Role']['Arn']
    print(f"Found ARN for role {execution_role_name}"+arn_info.execution_role_arn)


def populate_arn_info_from_stack():

    global arn_info
    arn_info.vpc_id = get_resource_id_from_stack(stack_name, 'Vpc')
    arn_info.load_balancer_arn = get_resource_id_from_stack(stack_name, 'ElasticLoadBalancer')
    arn_info.ecs_security_group = get_resource_id_from_stack(stack_name, 'EcsSecurityGroup')
    arn_info.elb_security_group = get_resource_id_from_stack(stack_name, 'ElbSecurityGroup')
    print(f"Populated arn info.. \n VPC ID {arn_info.vpc_id} \n Load Balancer ARN {arn_info.load_balancer_arn} \n ECS Security group {arn_info.ecs_security_group}")


def populate_arn_info():
    populate_arn_info_from_stack()
    populate_arn_info_execution_role()
    arn_info.subnet_array = get_subnet_info(arn_info.vpc_id)


def create_target_group(target_group_name):

    created_target_arn = ""

    print(f"Creating {target_group_name}...")
    response = elb_client.create_target_group(
        Name=target_group_name,
        Protocol='HTTP',
        Port=port,
        HealthCheckProtocol='HTTP',
        HealthCheckPath="/actuator/health",
        HealthCheckIntervalSeconds=10,
        HealthCheckTimeoutSeconds=2,
        HealthyThresholdCount=2,
        UnhealthyThresholdCount=10,
        TargetType='ip',
        VpcId=arn_info.vpc_id
    )
    print("Created target group...")

    for targetGroup in response['TargetGroups']:
        if targetGroup['TargetGroupName'] == target_group_name:
            print(f"Found target group {target_group_name}")
            created_target_arn = targetGroup['TargetGroupArn']

    return created_target_arn


def setup_load_balancer_if_not_exists(target_group_name):

    # Create a task definition
    response = elb_client.describe_target_groups(
        LoadBalancerArn=arn_info.load_balancer_arn
    )

    created_target_arn = ""

    does_target_group_exist = False
    for targetGroup in response['TargetGroups']:
        if targetGroup['TargetGroupName'] == target_group_name:
            print(f"Found target group {target_group_name}")
            does_target_group_exist = True
            created_target_arn = targetGroup['TargetGroupArn']

    print("Creating target group...")
    if not does_target_group_exist:
        created_target_arn = create_target_group(target_group_name)

    listener_exists = does_listener_exist(arn_info.load_balancer_arn, port)

    if not listener_exists:
        create_listener(arn_info.load_balancer_arn, created_target_arn, port)
        print(f"Created listener for target group {created_target_arn}")
        if not does_open_port_exist(port, arn_info.elb_security_group):
            open_load_balancer_port(port, arn_info.elb_security_group)
    else:
        print(f"Listener for port {port} already exists")

    arn_info.lb_target_group_arn = created_target_arn


def create_sqs_queue_if_not_exists():
    print("Checking for SQS queue " + sqs_file_event_queue)
    response = sqs_client.list_queues(
        QueueNamePrefix=sqs_file_event_queue
    )
    if 'QueueUrls' not in response or len(response['QueueUrls']) == 0:
        create_sqs_queue()


def create_sqs_queue():
    print("Creating SQS queue " + sqs_file_event_queue)
    topic_arn = f"arn:aws:sns:{region}:{accountId}:{sns_file_event_topic}"
    queue_arn = f"arn:aws:sqs:{region}:{accountId}:{sqs_file_event_queue}"
    sqs_file_upload_queue_policy = open(sqs_file_upload_queue_policy_file_path, "r").read()
    sqs_file_upload_queue_policy = sqs_file_upload_queue_policy.replace("{topic_arn}", topic_arn)
    sqs_file_upload_queue_policy = sqs_file_upload_queue_policy.replace("{queue_arn}", queue_arn)
    sqs_client.create_queue(
        QueueName=sqs_file_event_queue,
        Attributes={
            'Policy': sqs_file_upload_queue_policy
        }
    )
    sns_client.subscribe(
        TopicArn=topic_arn,
        Protocol='sqs',
        Endpoint=queue_arn
    )


def create_service(task_name):

    print(f"Creating service for task {task_name}...")

    port = get_port(task_name)

    response = ecs_client.create_service(
        cluster=ecs_cluster_name,
        serviceName=task_name,
        taskDefinition=task_name,
        desiredCount=1,
        deploymentConfiguration={
            'maximumPercent': 100,
            'minimumHealthyPercent': 0
        },
        launchType='FARGATE',
        loadBalancers=[
            {
                'targetGroupArn': arn_info.lb_target_group_arn,
                'containerName': task_name,
                'containerPort': port
            },
        ],
        networkConfiguration={
            'awsvpcConfiguration': {
                'subnets': arn_info.subnet_array,
                'securityGroups': [
                    arn_info.ecs_security_group
                ],
                'assignPublicIp': 'ENABLED'
            }
        },
        healthCheckGracePeriodSeconds=health_check_grace_period_seconds
    )

    print("Created service " + response['service']['serviceArn'])


def update_service(task_name):
    print(f"Calling update service for task {task_name}...")
    ecs_client.update_service(
        cluster=ecs_cluster_name,
        service=task_name,
        taskDefinition=task_name,
        forceNewDeployment=True,
        healthCheckGracePeriodSeconds=health_check_grace_period_seconds
    )


def create_or_update_service(task_name):

    print("looking for service " + task_name)

    try:
        response = ecs_client.describe_services(
            cluster=ecs_cluster_name,
            services=[task_name]
        )
        if len(response['services']) == 0 or response['services'][0]['status'] == 'INACTIVE':
            create_service(task_name)
        else:
            update_service(task_name)

    except Exception:
        raise Exception(f"Could not query cluster {ecs_cluster_name} to describe service")


def get_tables(cur):
    cur.execute('SELECT datname FROM pg_database WHERE datistemplate = false')
    tables = list(map(lambda tup: tup[0], cur.fetchall()))
    return tables


def create_db_if_not_exists():

    instances = rds_client.describe_db_instances(
        DBInstanceIdentifier=rds_instance_name
    )['DBInstances']

    if len(instances) == 0:
        raise Exception("No RDS instance found")

    endpoint = instances[0]['Endpoint']

    conn = None
    try:
        conn = psycopg2.connect(
            host=endpoint['Address'],
            database=rds_master_db_name,
            user=rds_master_user,
            password=rds_master_user_password,
            port=endpoint['Port']
        )
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur = conn.cursor()

        existing_tables = get_tables(cur)
        if branch_db_name in existing_tables:
            print(f"Table {branch_db_name} already exists")
            print("Tables: " + ', '.join(existing_tables))
        else:
            print(f"Creating table {branch_db_name}")
            cur.execute(f"CREATE DATABASE {branch_db_name}")
            existing_tables = get_tables(cur)
            print("Tables: " + ', '.join(existing_tables))
            if branch_db_name in existing_tables:
                print(f"Table {branch_db_name} created")
            else:
                raise Exception(f"Failed to create table {branch_db_name}")

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')


def create_apigateway_stage_if_not_exists(rest_api_logical_name, stage_name):

    if get_api_gateway_stage(stack_name, rest_api_logical_name, stage_name) is None:
        add_api_gateway_stage(stack_name, rest_api_logical_name, stage_name, port)


def sanitise_target_group_name(name):
    if len(name) > 32:
        name = name[len(name) - 32:]
    if name.endswith('-'):
        name = name[:-1]
    if name.startswith('-'):
        name = name[1:]
    return name


component = sys.argv[1]
full_task_name = f"{task_name_base}-{component}"
port = get_port(full_task_name)

# target group name must be less than 32 characters
target_group_name = sanitise_target_group_name(full_task_name)

populate_arn_info()
create_log_group_if_not_exists(full_task_name)
setup_load_balancer_if_not_exists(target_group_name)
create_sqs_queue_if_not_exists()
create_or_update_service(full_task_name)
create_db_if_not_exists()
create_apigateway_stage_if_not_exists('PaymentsPOCRestAPI', full_task_name)
