import os

# Stack Details
stack_name = os.environ['APP_NAME']
db_stack_name = stack_name + "-db"
load_balancer_name = os.environ['APP_NAME']+"-elb"
file_upload_bucket_name = stack_name + "-file-upload-bucket"
frontend_bucket_name = stack_name + "-frontend"
kafka_cluster_name = stack_name
ecs_cluster_name = stack_name

branch_name = os.environ['CIRCLE_BRANCH_STR']

# Container and tasks
task_name_base = os.environ['BRANCH_TAG']
execution_role_name = os.environ['IAM_EXECUTION_ROLE']
ecr_repo_arn = os.environ['ECR_REPO_ARN']
taskRoleName = "PaymentsPocTaskRole"
health_check_grace_period_seconds = 120
msk_volume_size_gb = 10

# RDS setup
rds_instance_name = db_stack_name + "-rds-instance"
rds_master_db_name = "masterdb"
rds_master_user = "sysadmin"
rds_master_user_password = os.environ['RDS_MASTER_USER_PASSWORD']
branch_db_name = os.environ['BRANCH_DB_NAME']

# API gateway setup
domain_name = os.environ['DOMAIN_NAME']
domain_certificate_arn = os.environ['DOMAIN_CERTIFICATE_ARN']
cognito_user_pool_id = os.environ['COGNITO_USER_POOL_ID']
cognito_domain = os.environ['COGNITO_DOMAIN']
ui_cognito_app_client_id = os.environ['UI_COGNITO_APP_CLIENT_ID']
service_to_service_cognito_app_client_id = os.environ['SERVICE_TO_SERVICE_COGNITO_APP_CLIENT_ID']
service_to_service_cognito_app_client_secret = os.environ['SERVICE_TO_SERVICE_COGNITO_APP_CLIENT_SECRET']

quick_sight_role_arn = os.environ['QUICK_SIGHT_ROLE_ARN']
quick_sight_role_name = os.environ['QUICK_SIGHT_ROLE_NAME']
cloudfront_domain_certificate_arn = os.environ['CLOUDFRONT_DOMAIN_CERTIFICATE_ARN']
sqs_file_event_queue = os.environ['SQS_FILE_EVENT_QUEUE']
sns_file_event_topic = stack_name + "-file-event-topic"

# Worldpay setup
worldpay_api_endpoint = os.environ['WORLDPAY_API_ENDPOINT']
worldpay_service_key = os.environ['WORLDPAY_SERVICE_KEY']
worldpay_client_key = os.environ['WORLDPAY_CLIENT_KEY']

