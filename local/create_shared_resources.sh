#!/bin/bash

set -eo pipefail

#resource requirements
##SERVICE   MIN_MEMORY  MIN_CPU   MAX_MEMORY  MAX_CPU
#kafka      512         0.05      2048        0.5
#zk         128         0.05      1024        0.5
#schemareg  512         0.05      1024        0.5
#operator   64          0.01      128         0.1
#manager    64          0.05      128         0.1
#jwt-server 128         0.01      256         0.05
#postgres   256         0.05      512         0.5
#localstack 1024        0.5       1024        0.5

#ports
##SERVICE         INTERNAL  EXTERNAL  HOST    INTERNAL URL                                    EXTERNAL URL
##postgres        5432      5432      30432   postgres-postgresql.localkube.svc.cluster.local localhost:30432
##localstack-s3   4572      4572      30572                                                   localhost:30572
##localstack-sqs  4576      4576      30576                                                   localhost:30576
##localstack-web  8080      8055      30055                                                   localhost:30055
##jwt-server      8991      9321      30001                                                   localhost:30001
##zookeeper       2181      2181      N/A     N/A                                             N/A
##kafka           9071      9092      9071    kafka:9071                                      kafka.localhost:9092 / localhost:30627
##schemaregistry  8081      8081      8081    schemaregistry:8081                             schemaregistry.localhost:8081

## Add and configure aws (add a profile)
export BRANCH_TAG=local
kubectl config use-context docker-desktop

echo "Building Mock JWT Server"
cd mock-jwt-server
./build_jwt_server.sh
echo "Installing Mock JWT Server"
helm upgrade --wait --install --namespace=localkube mock-jwt-server ./helm
cd ../

#localstack
echo "Installing Localstack"
kubectl apply -n localkube -f localstack-k8s.yml

#postgres
echo "Installing Postgres"
helm upgrade --wait --install --namespace=localkube -f postgres-values.yml postgres stable/postgresql

#kafka
echo "Installing Confluent"
rm -rf confluent-operator
mkdir confluent-operator
tar xzf confluent-operator.tar.gz -C confluent-operator

helm upgrade --wait --install -f confluent-values.yml --namespace=localkube \
--set operator.enabled=true \
confluent-operator confluent-operator/helm/confluent-operator

kubectl -n localkube patch serviceaccount default -p '{"imagePullSecrets": [{"name": "confluent-docker-registry" }]}'
while [[ $(kubectl get pods -n localkube -l app=cc-operator -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; do echo "waiting for cc-operator" && sleep 5; done
while [[ $(kubectl get pods -n localkube -l app=cc-manager -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; do echo "waiting for cc-manager" && sleep 5; done

until [[ $(kubectl get crd -n localkube) == *zookeeperclusters.cluster.confluent.com* ]]; do
  echo "waiting for zookeeper crd"
  sleep 5
done;

until [[ $(kubectl get crd -n localkube) == *kafkaclusters.cluster.confluent.com* ]]; do
  echo "waiting for kafka crd"
  sleep 5
done;

helm upgrade --install -f confluent-values.yml --namespace=localkube \
--set zookeeper.enabled=true \
zookeeper confluent-operator/helm/confluent-operator --no-hooks

while [[ $(kubectl get zookeeper zookeeper -n localkube -ojsonpath='{..status.phase}') != "RUNNING" ]]; do echo "waiting for zookeeper" && sleep 5; done

helm upgrade --install -f confluent-values.yml --namespace=localkube \
--set kafka.enabled=true \
kafka confluent-operator/helm/confluent-operator

while [[ $(kubectl get kafka kafka -n localkube -ojsonpath='{..status.phase}') != "RUNNING" ]]; do echo "waiting for kafka" && sleep 5; done

helm upgrade --wait --install -f confluent-values.yml --namespace=localkube \
--set schemaregistry.enabled=true \
schemaregistry confluent-operator/helm/confluent-operator

while [[ $(kubectl get pods -n localkube -l type=schemaregistry -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; do echo "waiting for schema registry" && sleep 5; done

rm -rf confluent-operator

# Create kuberentes config / secrets
echo "Creating secrets"

echo "Create rds-credentials"
kubectl create secret generic -n localkube rds-credentials \
  --from-literal=rds_user_name=sysadmin \
  --from-literal=rds_password=adminpwd \
  --from-literal=rds_url=jdbc:postgresql://postgres-postgresql.localkube.svc.cluster.local/localtest \
  -o yaml --dry-run | kubectl apply -f -

echo "Create aws-credentials"
kubectl create secret generic -n localkube aws-credentials \
  --from-literal=aws_account_id=LOCAL_USER_DUMMY_ID \
  --from-literal=aws_access_key=LOCAL_USER_DUMMY_KEY \
  --from-literal=aws_secret_key=LOCAL_USER_DUMMY_KEY \
  --from-literal=aws_region=eu-west-2 \
  -o yaml --dry-run | kubectl apply -f -

## Adding secrets for cluster
echo "Creating config map"
kubectl create configmap -n localkube cluster-brokers \
  --from-literal=kafka_brokers=kafka.localkube:9071 \
  --from-literal=schema_registry=http://schemaregistry.localkube:8081 \
  -o yaml --dry-run | kubectl apply -f -

## Updating secrets for kafka
echo "Create kafka-credentials"
kubectl create secret generic -n localkube kafka-credentials \
    --from-literal=kafka_key=local_key \
    --from-literal=kafka_password=local_secret \
    -o yaml --dry-run | kubectl apply -f -

#Cognito http://localhost:30001
echo "Create cognito-map"
kubectl create configmap -n localkube cognito-info \
  --from-literal=domain=http://localhost:30001/oauth/token \
  --from-literal=user_pool=http://mock-jwt-server.localkube.svc.cluster.local:8991 \
  --from-literal=user_pool_id=local_cognito \
  --from-literal=client_id=local_service_client_id \
  --from-literal=client_secret=local_service_client_secret \
  -o yaml --dry-run | kubectl apply -f -

#Cognito
echo "Create sftp-configmap"
#SFTP
kubectl create configmap -n localkube sftp-info \
  --from-literal=bucket_name=localbucket \
  --from-literal=server_id=local \
  --from-literal=user_role_arn=local \
  --from-literal=sqs_queue_url=http://localstack.localkube:30576/queue/testqueue \
  -o yaml --dry-run | kubectl apply -f -


#Quicksight
echo "Create quicksight-configmap"
#SFTP
kubectl create configmap -n localkube quicksight-info \
  --from-literal=role_arn=local_arn \
  -o yaml --dry-run | kubectl apply -f -

echo "Created secrets"

echo "Creating aws resources"
aws configure set aws_access_key_id LOCAL_USER_DUMMY_KEY --profile localuser
aws configure set aws_secret_access_key LOCAL_USER_DUMMY_SECRET --profile localuser
aws configure set region eu-west-2 --profile localuser
aws configure set output_format text --profile localuser

## Create aws resoruces
echo "Creating S3 Bucket"
aws --endpoint-url=http://localhost:30572 s3 mb s3://localbucket --region eu-west-2 --profile localuser

echo "Updating Bucket Permissions"
aws --endpoint-url=http://localhost:30572 s3api put-bucket-acl --bucket localbucket --acl public-read --profile localuser

echo "Creating SQS Queue"
aws --endpoint-url=http://localhost:30576 sqs create-queue --queue-name testqueue  --region eu-west-2 --profile localuser

echo "Creating SQS Notification "
aws --endpoint-url=http://localhost:30572 s3api put-bucket-notification-configuration --bucket localbucket --notification-configuration file://s3-notification.json  --region eu-west-2 --profile localuser
