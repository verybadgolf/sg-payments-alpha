@echo off

SETLOCAL ENABLEDELAYEDEXPANSION

set help=false

set numFiles=0
for %%x in (*.pid) do (
  set /a numFiles+=1
)

IF %numFiles% gtr 0 (
    @ECHO application is running please run stop script
    GOTO Exit
)

if not exist %cd%\tmp mkdir %cd%\tmp

IF "%~1"=="distributed" (
    SET LOADER_MAIN=gov.scot.payments.DistributedApplicationWrapper
    IF "%~2"=="--help" (
        java -jar local-application.jar --help
        set help=true
    ) ELSE (
        @ECHO Launching Core
        START java -Djava.io.tmpdir=%cd%\tmp -jar local-application.jar @payments-poc-distributed-config.txt
    )
) ELSE (
    SET LOADER_MAIN=gov.scot.payments.LocalApplication
    IF "%~1"=="--help" (
        java -jar local-application.jar --help
        set help=true
    ) ELSE (
        @ECHO Launching Core
        START java -Djava.io.tmpdir=%cd%\tmp -jar local-application.jar embedded @payments-poc-embedded-config.txt
    )
)

IF %help% == true (
    GOTO Exit
)

:CheckForCorePort
IF EXIST core.port GOTO CoreStarted
@ECHO Waiting for Core to start...
TIMEOUT /T 10 >nul
GOTO CheckForCorePort

:CoreStarted
@ECHO Core started
set /p service_port=<core.port
@ECHO core is running at http://localhost:%service_port%/resources/swagger-ui.html
@ECHO core is running at http://localhost:%service_port%/resources/ui/index.html

START "" http://localhost:%service_port%/resources/swagger-ui.html
START "" http://localhost:%service_port%/resources/ui/index.html

IF "%~1"=="distributed" (
    set /p db_web_port=<db.port
    @ECHO DB Console is running at http://localhost:!db_web_port!
    START "" http://localhost:!db_web_port!
) ELSE (
    @ECHO DB Console is running at http://localhost:%service_port%/h2-console
    START "" http://localhost:%service_port%/h2-console
)

for %%x in (*.inbound.args) do (
    @ECHO Launching Inbound Gateway %%x
    START java -jar local-card-gateway.jar @%%x
)
for %%x in (*.outbound.args) do (
    @ECHO Launching Outbound Gateway %%x
    START java -jar local-gateway.jar @%%x
)

@ECHO Launching Customer Site

IF "%~1"=="distributed" (
    set /p customer_port=<customer-site.port
    START java -jar mock-customer-service.jar local @payments-poc-mock-customer-site-config.txt -service.port %service_port% -port !customer_port!
) ELSE (
    START java -jar mock-customer-service.jar local @payments-poc-mock-customer-site-config.txt -service.port %service_port% -portFile customer-site.port
)

:CheckForCustomerPid
IF EXIST customer-site.pid GOTO CustomerStarted
@ECHO Waiting for Customer Site to start...
TIMEOUT /T 5 >nul
GOTO CheckForCustomerPid

:CustomerStarted
TIMEOUT /T 10 >nul
@ECHO Customer Site started
set /p customer_web_port=<customer-site.port
@ECHO customer site is running at http://localhost:%customer_web_port%
START "" http://localhost:%customer_web_port%

:Exit