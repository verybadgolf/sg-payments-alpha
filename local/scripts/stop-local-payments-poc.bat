@echo off

SETLOCAL ENABLEDELAYEDEXPANSION
for %%x in (*.pid) do (
  set /p pid=<%%x
  @echo pid for %%x is !pid!
  taskkill /PID !pid! /F
  del %%x
)
for %%x in (*.port) do (
  del %%x
)
for %%x in (*.args) do (
  del %%x
)

TIMEOUT /T 5 >nul

RMDIR tmp /S /Q