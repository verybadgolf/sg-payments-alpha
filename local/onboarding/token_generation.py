
import sys

sys.path.insert(0, "../..")
sys.path.insert(0, "../infrastructure/aws")

from payments_alpha_common import *
from payments_alpha_stack_env import app_name, branch_name

user_pool_stack_name = f"{app_name}-{branch_name}-user-pool"
cognito_user_pool_id = get_resource_id_from_stack_output(user_pool_stack_name, f"{branch_name}-UserPoolId")
cognito_client_id = get_resource_id_from_stack_output(user_pool_stack_name, f"{branch_name}-UserPoolUiClientId")

def get_cognito_token(username, password):
    response = cognito_idp_client.admin_initiate_auth(
        AuthFlow="ADMIN_NO_SRP_AUTH",
        AuthParameters={
            "USERNAME": username,
            "PASSWORD": password
        },
        ClientId=cognito_client_id,
        UserPoolId=cognito_user_pool_id
    )

    cognito_token = response['AuthenticationResult']['IdToken']
    return cognito_token
