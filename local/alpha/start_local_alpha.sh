#!/bin/bash

set -eo pipefail

#resource requirements
##SERVICE         MIN_MEMORY  MIN_CPU   MAX_MEMORY  MAX_CPU
#users-ch         256         0.05      N/A         N/A
#users-pm         256         0.05      N/A         N/A
#adapters         256         0.01      N/A         N/A
#payfiles-ch      256         0.05      N/A         N/A
#payfiles-pm      256         0.05      N/A         N/A
#transferwise     256         0.01      N/A         N/A
#bacs-adapter     256         0.05      N/A         N/A
#bacs-service     256         0.01      N/A         N/A
#alpha-frontend   10          0.01      50          0.05

#ports
##SERVICE             INTERNAL  EXTERNAL  HOST    INTERNAL URL          EXTERNAL URL
##users-ch            8092      8092      30012                         localhost:30012 / localhost/alpha/users/users-ch-app
##users-pm            8093      8093      30013                         localhost:30013 / localhost/alpha/users/users-pm-app
##adapters            8094      8094      30014                         localhost:30014 / localhost/adapters
##payfiles-ch         8095      8095      30015                         localhost:30015 / localhost/alpha/paymentfiles/paymentfiles-pm-app
##payfiles-pm         8096      8096      30016                         localhost:30016 / localhost/alpha/paymentfiles/paymentfiles-pm-app
##transferwise        8097      8097      30017                         localhost:30017 / localhost/alpha/transferwiseadapter/transferwise-adapter-app
##bacs-adapter        8098      8098      30018                         localhost:30018 / localhost/alpha/pspsadapter/bacs-adapter-app
##customers-ch        8099      8099      30019                         localhost:30019 / localhost/alpha/customers/customers-ch-app
##customers-pm        8100      8100      30020                         localhost:30020 / localhost/alpha/customers/customers-pm-app
##paymentbatches-ch   8101      8101      30021                         localhost:30021 / localhost/alpha/paymentbatches/paymentbatches-ch-app
##payments-ch         8102      8102      30022                         localhost:30022 / localhost/alpha/payments/payments-ch-app
##payments-pm         8103      8103      30023                         localhost:30023 / localhost/alpha/payments/payments-pm-app
##pspcore-ch          8104      8104      30024                         localhost:30024 / localhost/alpha/pspcore/pspcore-ch-app
##pspcore-pm          8105      8105      30025                         localhost:30025 / localhost/alpha/pspcore/pspcore-pm-app
##paymentbatches-proj 8106      8106      30026                         localhost:30026 / localhost/alpha/paymentbatches/paymentbatches-proj-app
##payments-proj       8107      8107      30027                         localhost:30027 / localhost/alpha/payments/payments-proj-app
##reports-proj        8108      8108      30028                         localhost:30028 / localhost/alpha/reports/reports-proj-app
##sepadc-method-ch    8109      8109      30029                         localhost:30029 / localhost/alpha/sepadcmethod/sepadc-method-ch-app
##sepadc-method-pm    8110      8110      30030                         localhost:30030 / localhost/alpha/sepadcmethod/sepadc-method-pm-app
##bacs-method-ch      8111      8111      30031                         localhost:30031 / localhost/alpha/bacsmethod/bacs-method-ch-app
##bacs-method-pm      8112      8112      30032                         localhost:30032 / localhost/alpha/bacsmethod/bacs-method-pm-app
##alpha-frontend      8086      8086      30006                         localhost:30006


kubectl config use-context docker-desktop

echo "Creating kafka credentials config maps"
cd ../../alpha-backend/application
for context in *; do
    if [ -d "${context}" ] && [ "${context}" != "helm" ] && [ "${context}" != "confluent" ] && [ "${context}" != "build" ]; then
        echo "Creating kafka credentials config maps for ${context}"
        cd ${context}
        for project in *; do
            if [ -d "${project}" ] && [[ "${project}" == *-app ]]; then
                givencontext=$(awk '/contextName/{print $NF}' "${project}/build.gradle" | sed 's/\"//g')
                if [ -z "$givencontext" ]; then givencontext=$context; fi
                echo "Creating kafka credentials config map for ${givencontext}-${project}"
                kubectl create secret generic -n localkube ${givencontext}-${project}-kafka-credentials \
                  --from-literal=kafka_key=local_key \
                  --from-literal=kafka_password=local_secret \
                  --from-literal=schema_registry_key=local_key \
                  --from-literal=schema_registry_password=local_secret \
                  -o yaml --dry-run | kubectl apply -f -
                 kubectl label secret -n localkube ${givencontext}-${project}-kafka-credentials app=alpha-backend --overwrite
            fi
        done
        cd ../
    fi
done
cd ../../local/alpha

helm upgrade --install --namespace=localkube payments-alpha-backend \
  ../../alpha-backend/application/build/helm/output/sg-payments-alpha \
  -f values-local.yaml \
  --set pspadapter.transferwiseadapterapp.transferWiseToken=${TRANSFERWISE_TOKEN} \
  --set pspadapter.transferwiseadapterapp.transferWiseProfileId=${TRANSFERWISE_PROFILE_ID:0}

helm upgrade --install --namespace=localkube payments-adapters \
  ../../alpha-backend/adapters/helm \
  -f values-local-adapters.yaml

helm upgrade --install --namespace=localkube payments-alpha-ui ../../alpha-frontend/helm -f values-local.yaml

echo "Waiting for alpha web-server"
until $(curl --output /dev/null --silent --head --fail http://localhost:30006); do
    echo -n  '.'
    sleep 5
done

# Get dashboard token
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep k8s-admin | awk '{print $1}')

start chrome "http://localhost:30005"
start chrome "http://localhost:30006"