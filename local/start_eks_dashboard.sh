#!/bin/sh

set -eo pipefail

AWS_REGION=eu-west-2
APP_NAME=payments
EKS_MASTERS_ROLE_ARN="arn:aws:iam::$AWS_ACCOUNT_ID:role/EKSMastersRole"

aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
aws configure set region $AWS_REGION
aws --region ${AWS_REGION} eks update-kubeconfig --name ${APP_NAME}-eks-cluster --role-arn ${EKS_MASTERS_ROLE_ARN}
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep k8s-admin | awk '{print $1}')
#start chrome "http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login"
kubectl proxy
