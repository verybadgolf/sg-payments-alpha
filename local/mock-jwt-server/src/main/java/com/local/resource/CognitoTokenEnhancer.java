package com.local.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.HashMap;
import java.util.Map;

public class CognitoTokenEnhancer implements TokenEnhancer {

    private final String issuerString;

    public CognitoTokenEnhancer(String issuer){
        issuerString = issuer;
    }


    private final Map<String, String[]> userRoles  = new HashMap<String, String[]>() {{
        put("admin_user", new String[] {"."});
        put("cust_user", new String[] {"."});
        put("ilf_user", new String[]{ "ilf"});
        put("ssc_user", new String[] {"ssc"});
    }};

    private final Map<String, String> userActions  = new HashMap<String, String>() {{
        put("admin_user", "paymentBatches:CreateBatch;users:Write;customers:Read;customers:ModifyCustomer;customers:ApproveCustomerModification;customers:ApproveProductModification;customers:CreateCustomer;customers:ModifyProduct;users:Read;payments:Read;payments:CreatePayment;payments:ApprovePayment;");
        put("cust_user", "paymentBatches:CreateBatch;users:Write;customers:Read;customers:ModifyCustomer;customers:ApproveCustomerModification;customers:ApproveProductModification;customers:CreateCustomer;customers:ModifyProduct;users:Read;payments:Read;payments:CreatePayment;payments:ApprovePayment;");
        put("ilf_user", "payments:Read");
        put("ssc_user", "payments:Read");
    }};


    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        Map<String, Object> additionalInfo = new HashMap<>();
        additionalInfo.put("iss", issuerString);
        String username = authentication.getOAuth2Request().getRequestParameters().get("username");
        additionalInfo.put("cognito:groups", userRoles.get(username));
        additionalInfo.put("cognito:username", username );
        additionalInfo.put("given_name", "Tester");
        additionalInfo.put("email", "test@example.com");
        additionalInfo.put("aud", "oauth2-resource");
        additionalInfo.put("token_use", "id");
        additionalInfo.put("custom:permitted_actions", userActions.get(username));
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(
                additionalInfo);
        return accessToken;
    }

}
