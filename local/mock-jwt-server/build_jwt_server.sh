#!/bin/sh

set -eo pipefail

./gradlew build
docker build -t sg-payments/mock-jwt-server .