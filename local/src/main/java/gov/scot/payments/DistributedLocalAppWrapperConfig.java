package gov.scot.payments;

import com.beust.jcommander.Parameter;
import gov.scot.payments.broker.EmbeddedBrokerConfig;
import gov.scot.payments.common.local.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.io.File;
import java.util.*;

@Data
@EqualsAndHashCode(callSuper = false)
public class DistributedLocalAppWrapperConfig extends LocalAppConfigBase {

    @Parameter(names = "-topics"
            , listConverter = EmbeddedBrokerConfig.SemiColonListConverter.class
            , hidden = true)
    private List<String> topics = EmbeddedBrokerConfig.TOPICS;

    @Parameter(names = "-inbound"
            , description = "Register an inbound gateway. This option can appear multiple times. The format is <NAME>(;(Card|Bacs|Chaps|FasterPayments)=<AMOUNT>(%))*")
    private List<String> inbound = new ArrayList<>();

    @Parameter(names = "-outbound"
            , description = "Register an outbound gateway. This option can appear multiple times. The format is <NAME>(;(Card|Bacs|Chaps|FasterPayments)=<AMOUNT>(%))*")
    private List<String> outbound = new ArrayList<>();

    private List<Integer> inboundPorts;
    private List<Integer> outboundPorts;

    public DistributedLocalAppWrapperConfig(){
        super(new LocalApplicationConfig(true));
    }

    public void setInboundPorts(List<Integer> inboundPorts){
        this.inboundPorts = inboundPorts;
    }
    public void setOutboundPorts(List<Integer> ports){
        this.outboundPorts = ports;
    }

    @Override
    public Set<String> getProfiles() {
        Set<String> profiles =  super.getProfiles();
        return profiles;
    }

    @Override
    public Map<String, Object> getProperties() {
        Map<String, Object> properties = super.getProperties();
        properties.put("payments.inbound.card.proxyMode","zookeeper");
        return properties;
    }

    @Override
    public Set<String> validate() {
        Set<String> violations = super.validate();
        try{
            inbound.forEach(LocalGatewayConfig::fromString);
            outbound.forEach(LocalGatewayConfig::fromString);
        } catch(Exception e){
            violations.add(e.getMessage());
        }
        return violations;
    }

    public DistributedLocalAppConfig toCoreConfig(String tmpDir
            , int port
            , int s3Port
            , LocalZookeeperConfig zookeeperConfig
            , LocalDatabaseConfig databaseConfig
            , LocalKafkaConfig kafkaConfig){
        LocalApplicationConfig app = new LocalApplicationConfig(tmpDir);
        app.setEnvironment(getApp().getEnvironment());
        app.setPidFile(getApp().getPidFile());
        app.setPortFile(getApp().getPortFile());
        app.setOpenBrowser(getApp().isOpenBrowser());
        app.setPort(port);
        DistributedLocalAppConfig config = new DistributedLocalAppConfig(app);
        config.setApp(app);
        config.setAws(getAws());
        config.setCognito(getCognito());
        config.setQuicksight(getQuicksight());
        LocalS3Config s3 = new LocalS3Config();
        s3.setAutoCreate(getS3().isAutoCreate());
        s3.setPath(getS3().getPath());
        s3.setPort(s3Port);
        s3.setS3(getS3().isS3());
        config.setS3(s3);
        config.setServiceToServiceClient(getServiceToServiceClient());
        config.setServiceToServiceSecret(getServiceToServiceSecret());
        config.setDatabase(databaseConfig);
        config.setKafka(kafkaConfig);
        config.setZookeeper(zookeeperConfig);
        return config;
    }

    public String[] getPrefixedTopics(){
        return topics.stream().map(t -> getApp().getEnvironment()+"-"+t).toArray(String[]::new);
    }

    public List<Tuple2<String,String[]>> getInboundGatewayConfigs(LocalDatabaseConfig databaseConfig, LocalZookeeperConfig zookeeperConfig) {
        List<Tuple2<String,String[]>> configs = new ArrayList<>();
        for(int i=0;i<inbound.size();i++){
            configs.add(getGatewayConfig(inboundPorts.get(i),LocalGatewayConfig.fromString(inbound.get(i)),databaseConfig,zookeeperConfig,null));
        }
        return configs;
    }

    public List<Tuple2<String,String[]>> getOutboundGatewayConfigs(LocalDatabaseConfig databaseConfig, LocalZookeeperConfig zookeeperConfig, LocalKafkaConfig kafkaConfig) {
        List<Tuple2<String,String[]>> configs = new ArrayList<>();
        for(int i=0;i<outbound.size();i++){
            configs.add(getGatewayConfig(outboundPorts.get(i),LocalGatewayConfig.fromString(outbound.get(i)),databaseConfig,zookeeperConfig,kafkaConfig));
        }
        return configs;
    }

    private Tuple2<String,String[]> getGatewayConfig(Integer port,LocalGatewayConfig c, LocalDatabaseConfig databaseConfig, LocalZookeeperConfig zookeeperConfig, LocalKafkaConfig kafkaConfig) {
        List<String> args = new ArrayList<>();
        LocalApplicationConfig app;
        if(kafkaConfig != null){
            app = new LocalApplicationConfig(new File(getApp().getTmpDir(),c.getName()).getAbsolutePath());
            args.addAll(Arrays.asList(kafkaConfig.toArgs()));
        } else {
            app = new LocalApplicationConfig(false);
        }
        args.add(LocalConfig.q(c.getName()));
        args.addAll(c.getChannelArguments());
        app.setEnvironment(getApp().getEnvironment());
        app.setPidFile(c.getName()+".pid");
        app.setPortFile(c.getName()+".port");
        app.setPort(port);
        args.addAll(Arrays.asList(zookeeperConfig.toArgs()));
        args.addAll(Arrays.asList(databaseConfig.toArgs()));
        args.addAll(Arrays.asList(app.toArgs()));
        args.addAll(Arrays.asList(getAws().toArgs()));
        args.addAll(Arrays.asList(getCognito().toArgs()));
        return Tuples.of(c.getName(),args.toArray(new String[0]));
    }
}
