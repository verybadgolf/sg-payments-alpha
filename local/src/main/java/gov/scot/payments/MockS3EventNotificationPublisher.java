package gov.scot.payments;

import com.amazonaws.services.s3.event.S3EventNotification;
import org.joda.time.DateTime;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;

public class MockS3EventNotificationPublisher implements AutoCloseable {

    private final WatchService watchService;
    private final Map<WatchKey,Path> watchKeys;
    private final Path bucket;
    private final Consumer<S3EventNotification> eventConsumer;
    private final ExecutorService executorService;
    private PollProcess pollThread;

    public MockS3EventNotificationPublisher(String path
            , Consumer<S3EventNotification> eventConsumer
            , ExecutorService executorService) throws IOException {
        this.eventConsumer = eventConsumer;
        this.executorService = executorService;
        this.watchService = FileSystems.getDefault().newWatchService();
        this.bucket = Paths.get(path);
        Map<WatchKey,Path> watchKeys = new HashMap<>();
        Files.walkFileTree(bucket, new SimpleFileVisitor<>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                    throws IOException
            {
                WatchKey key = dir.register(watchService,ENTRY_CREATE);
                watchKeys.put(key,dir);
                return FileVisitResult.CONTINUE;
            }
        });
        this.watchKeys = watchKeys;
    }

    public void start() {
        pollThread = new PollProcess();
        new Thread(pollThread).start();
    }

    @Override
    public void close() throws Exception {
        pollThread.stop();
        watchKeys.keySet().forEach(WatchKey::cancel);
        watchService.close();
    }

     class PollProcess implements  Runnable {

        private volatile boolean exit = false;

        @Override
        public void run() {
            while(!exit) {
                try {
                    WatchKey key = watchService.take();
                    for (WatchEvent<?> event : key.pollEvents()) {
                        WatchEvent.Kind<?> kind = event.kind();
                        WatchEvent<Path> ev = (WatchEvent<Path>) event;
                        Path absolute = watchKeys.get(key).resolve(ev.context());
                        Path relative = bucket.relativize(absolute);
                        if(relative.getNameCount() > 1){
                            try {
                                S3EventNotification notification = buildS3EventNotification(kind,bucket,relative);
                                executorService.submit(() -> eventConsumer.accept(notification));
                            } catch (IOException e) {}
                        }
                        boolean valid = key.reset();
                        if (!valid) {
                            break;
                        }
                    }
                } catch (Exception e) {
                    //return;
                }
            }
        }

         private S3EventNotification buildS3EventNotification(WatchEvent.Kind<?> kind, Path bucketPath, Path folderAndFile) throws IOException {
            String eventType = kind.equals(ENTRY_CREATE) ? "ObjectCreated:Post" : "ObjectCreated:Put";

             String bucketKey = bucketPath.getName(bucketPath.getNameCount()-1).toString().replace(File.separatorChar, '/');
             S3EventNotification.S3BucketEntity bucket = new S3EventNotification.S3BucketEntity(bucketKey
                     ,null
                     ,"");
             Path absolutePath = bucketPath.resolve(folderAndFile);
             String file = URLEncoder.encode(folderAndFile.getName(folderAndFile.getNameCount()-1).toString(),StandardCharsets.UTF_8);
             String folder = folderAndFile.subpath(0,folderAndFile.getNameCount()-1).toString().replace(File.separatorChar, '/');
             String key = folder+"/"+file;
             S3EventNotification.UserIdentityEntity user = new S3EventNotification.UserIdentityEntity("application");
             S3EventNotification.S3ObjectEntity object = new S3EventNotification.S3ObjectEntity(key
                     ,Files.size(absolutePath)
                     ,""
                     ,"1"
                     ,"");
             S3EventNotification.S3Entity s3Entity = new S3EventNotification.S3Entity(""
                     ,bucket
                     ,object
                     ,"1.0");
             S3EventNotification.S3EventNotificationRecord record = new S3EventNotification.S3EventNotificationRecord("eu-west-1"
                     ,eventType
                     ,"aws:s3"
                     , new DateTime(Files.readAttributes(absolutePath,BasicFileAttributes.class).creationTime().toMillis()).toString()
                     ,"2.1"
                     ,null
                     ,null
                     ,s3Entity
                     ,user);
             return new S3EventNotification(Collections.singletonList(record));
         }

         public void stop(){
            exit = true;
        }

    }

}
