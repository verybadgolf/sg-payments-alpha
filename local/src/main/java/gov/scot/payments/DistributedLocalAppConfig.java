package gov.scot.payments;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParametersDelegate;
import gov.scot.payments.common.local.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.*;

@Data
@EqualsAndHashCode(callSuper = false)
public class DistributedLocalAppConfig extends LocalAppConfigBase {

    @ParametersDelegate private LocalKafkaConfig kafka = new LocalKafkaConfig();
    @ParametersDelegate private LocalZookeeperConfig zookeeper = new LocalZookeeperConfig(false);
    @ParametersDelegate private LocalDatabaseConfig database = new LocalDatabaseConfig();

    public DistributedLocalAppConfig(LocalApplicationConfig app){
        super(app);
    }

    @Override
    public Set<String> getProfiles() {
        Set<String> profiles =  super.getProfiles();
        profiles.addAll(kafka.getProfiles());
        profiles.addAll(zookeeper.getProfiles());
        profiles.addAll(database.getProfiles());
        return profiles;
    }

    @Override
    public Map<String, Object> getProperties() {
        Map<String, Object> properties = super.getProperties();
        properties.putAll(kafka.getProperties());
        properties.putAll(zookeeper.getProperties());
        properties.putAll(database.getProperties());
        properties.put("payments.inbound.card.proxyMode","zookeeper");
        return properties;
    }

    @Override
    public Set<String> validate() {
        Set<String> violations = super.validate();
        violations.addAll(kafka.validate());
        violations.addAll(zookeeper.validate());
        violations.addAll(database.validate());
        return violations;
    }

    @Override
    public String toString() {
        return String.join(" ",toArgs());
    }

    @Override
    public String[] toArgs() {
        List<String> args = new ArrayList<>(Arrays.asList(super.toArgs()));
        args.addAll(Arrays.asList(kafka.toArgs()));
        args.addAll(Arrays.asList(zookeeper.toArgs()));
        args.addAll(Arrays.asList(database.toArgs()));
        return args.toArray(new String[0]);
    }
}
