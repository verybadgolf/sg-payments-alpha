package gov.scot.payments;

import gov.scot.payments.paymentfile.PaymentFileProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
@Profile("!s3")
public class MockSqsConfiguration {

    @Bean
    public ExecutorService sqsExecutorService() {
        return Executors.newSingleThreadExecutor();
    }

    @Bean(initMethod = "start")
    public MockS3EventNotificationPublisher mockS3EventNotificationPublisher(@Value("${s3.file.path}") String path
            , PaymentFileProcessor paymentFileProcessor
            , ExecutorService sqsExecutorService) throws IOException {
        return new MockS3EventNotificationPublisher(path, paymentFileProcessor::onS3Event,sqsExecutorService);
    }
}
