package gov.scot.payments;

import com.beust.jcommander.DynamicParameter;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParametersDelegate;
import gov.scot.payments.broker.EmbeddedBrokerConfig;
import gov.scot.payments.common.local.*;
import lombok.Data;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

import static gov.scot.payments.common.local.LocalConfig.q;

@Data
public abstract class LocalAppConfigBase implements LocalConfig{

    private static final List<String> CUSTOMERS = Arrays.asList("ilf","ssc","parking","fishing");
    private static final List<LocalServiceConfig> SERVICES = Arrays.asList(
            new LocalServiceConfig("ilf","ilf", LocalServiceConfig.FileFormat.ILF)
            ,new LocalServiceConfig("ssc","ssc", LocalServiceConfig.FileFormat.CPS)
            ,new LocalServiceConfig("parking_fine","parking", null)
            ,new LocalServiceConfig("fishing_permit","fishing", null));

    @Parameter(names = "-cognito.internalClient"
            ,description = "Only applicable if using the -cognito option. The cognito app client id of the internal communication app client")
    private String serviceToServiceClient;

    @Parameter(names = "-cognito.internalSecret"
    ,description = "Only applicable if using the -cognito option. The cognito client secret of the internal communication app client")
    private String serviceToServiceSecret;

    @Parameter(names = "-customers"
            ,listConverter = EmbeddedBrokerConfig.SemiColonListConverter.class
            ,description = "A ; separated list of the customers to register. Customer names must be unique and not contain spaces or ;")
    protected List<String> customers = CUSTOMERS;

    @Parameter(names = "-service"
    ,description = "Register a service. This option can appear multiple times. The format is <NAME>;<CUSTOMER_ID>;(CPS|ILF). The custoemr id must be contained in the -cusotmers list")
    protected List<String> services = SERVICES.stream().map(LocalServiceConfig::toString).collect(Collectors.toList());

    @ParametersDelegate private LocalApplicationConfig app;
    @ParametersDelegate private LocalCognitoConfig cognito = new LocalCognitoConfig();
    @ParametersDelegate private LocalAwsConfig aws = new LocalAwsConfig();
    @ParametersDelegate private LocalS3Config s3 = new LocalS3Config();
    @ParametersDelegate private LocalQuicksightConfig quicksight = new LocalQuicksightConfig();

    protected LocalAppConfigBase(LocalApplicationConfig app){
        this.app = app;
    }

    @Override
    public Set<String> getProfiles() {
        Set<String> profiles = new HashSet<>();
        profiles.addAll(app.getProfiles());
        profiles.addAll(cognito.getProfiles());
        profiles.addAll(aws.getProfiles());
        profiles.addAll(s3.getProfiles());
        profiles.addAll(quicksight.getProfiles());
        return profiles;
    }

    @Override
    public Map<String, Object> getProperties() {
        Map<String,Object> properties = new HashMap<>();
        if(cognito.isCognito()){
            properties.put("SERVICE_TO_SERVICE_COGNITO_APP_CLIENT_ID",serviceToServiceClient);
            properties.put("SERVICE_TO_SERVICE_COGNITO_APP_CLIENT_SECRET",serviceToServiceSecret);
        }
        properties.putAll(app.getProperties());
        properties.putAll(cognito.getProperties());
        properties.putAll(aws.getProperties());
        properties.putAll(s3.getProperties());
        properties.putAll(quicksight.getProperties());
        properties.put("API_GATEWAY_BASE_URI","http://localhost:"+app.getPort());
        customers.forEach(s -> properties.put("customer."+s+".name",s));
        services
                .stream()
                .map(LocalServiceConfig::fromString)
                .map(LocalServiceConfig::toProperty)
                .forEach(s -> properties.put(s.getT1(),s.getT2()));
        return properties;
    }

    @Override
    public Set<String> validate() {
        Set<String> violations = new HashSet<>();
        violations.addAll(app.validate());
        violations.addAll(cognito.validate());
        violations.addAll(aws.validate());
        violations.addAll(s3.validate());
        if(cognito.isCognito() && (serviceToServiceClient == null || serviceToServiceSecret == null)){
            violations.add("cognito.internalClient and cognito.internalSecret are required if using cognito");
        }
        if(!cognito.isCognito() && (serviceToServiceClient != null || serviceToServiceSecret != null)){
            violations.add("cognito.internalClient and cognito.internalSecret are only applicable if using cognito");
        }
        if(cognito.isCognito() && !aws.isAws()){
            violations.add("aws is required if using cognito");
        }
        if(s3.isS3() && !aws.isAws()){
            violations.add("aws is required if using s3");
        }
        if(quicksight.isQuicksight() && !aws.isAws()){
            violations.add("aws is required if using quicksight");
        }
        if(customers != null && customers.stream().anyMatch(StringUtils::containsWhitespace)){
            violations.add("customer names must not have spaces");
        }
        if(customers != null && new HashSet<>(customers).size() != customers.size()){
            violations.add("customer names must be unique");
        }
        if(services != null){
            if(s3.isS3() && !services.isEmpty()){
                violations.add("services can only be specified when using mock s3");
            }

            List<LocalServiceConfig> convertedServices = Collections.emptyList();
            try{
                convertedServices = services.stream().map(LocalServiceConfig::fromString).collect(Collectors.toList());
            } catch(Exception e){
                violations.add(e.getMessage());
            }

            if(convertedServices.stream().map(LocalServiceConfig::getName).anyMatch(StringUtils::containsWhitespace)){
                violations.add("service names must not have spaces");
            }
            if(convertedServices.stream().map(LocalServiceConfig::getName).collect(Collectors.toSet()).size() != services.size()){
                violations.add("service names must be unique");
            }
            for(LocalServiceConfig service : convertedServices){
                if(service.getFileFormat() != null){
                    File file = new File(s3.getPath(),service.getName());
                    if(s3.isAutoCreate()){
                        file.mkdirs();
                    }
                    if(!file.exists()){
                        violations.add(String.format("folder %s must exist",file.toString()));
                    }
                }
            }
        }
        return violations;
    }

    @Override
    public String[] toArgs() {
        List<String> args = new ArrayList<>();
        if(cognito.isCognito()){
            if(serviceToServiceClient != null){
                args.add("-cognito.internalClient");
                args.add(q(serviceToServiceClient));
            }
            if(serviceToServiceSecret != null){
                args.add("-cognito.internalSecret");
                args.add(q(serviceToServiceSecret));
            }
        }
        args.addAll(Arrays.asList(app.toArgs()));
        args.addAll(Arrays.asList(cognito.toArgs()));
        args.addAll(Arrays.asList(aws.toArgs()));
        args.addAll(Arrays.asList(s3.toArgs()));
        args.addAll(Arrays.asList(quicksight.toArgs()));
        if(customers != null && !customers.isEmpty()){
            args.add("-customers");
            args.addAll(customers.stream().map(LocalConfig::q).collect(Collectors.toList()));
        }
        if(services != null && !services.isEmpty()){
            services.stream()
                    .map(LocalServiceConfig::fromString)
                    .forEach(s -> {
                        args.add("-service");
                        args.add(s.toString());
            });
        }
        return args.toArray(new String[0]);
    }

    @Override
    public String toString() {
        return String.join(" ", toArgs());
    }
}
