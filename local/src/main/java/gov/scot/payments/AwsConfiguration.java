package gov.scot.payments;

import org.springframework.cloud.aws.autoconfigure.context.ContextCredentialsAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("aws")
@Import({ContextStackAutoConfiguration.class
        , ContextRegionProviderAutoConfiguration.class
        , ContextCredentialsAutoConfiguration.class
})
public class AwsConfiguration {
}
