package gov.scot.payments;

import gov.scot.payments.common.gateway.GatewayDetailsService;
import gov.scot.payments.common.gateway.inbound.cardpayment.CardPaymentGateway;
import gov.scot.payments.common.gateway.spring.AbstractGatewayConfiguration;
import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.gateway.inbound.cardpayment.GatewayCardPaymentService;
import gov.scot.payments.gateway.inbound.cardpayment.mock.MockInboundCardPaymentGateway;
import gov.scot.payments.gateway.inbound.cardpayment.spring.AbstractCardPaymentGatewayConfiguration;
import gov.scot.payments.gateway.outbound.PaymentInstructionBatcher;
import gov.scot.payments.gateway.outbound.PaymentInstructionBatchers;
import gov.scot.payments.gateway.outbound.mock.MockOutboundGatewayBase;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingSuccessEvent;
import gov.scot.payments.model.paymentinstruction.PaymentChannel;
import gov.scot.payments.model.paymentinstruction.event.PaymentSubmitFailedEvent;
import gov.scot.payments.model.paymentinstruction.event.PaymentSubmitSuccessEvent;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.converter.CompositeMessageConverterFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Configuration
@Profile("mock-stripe")
public class MockStripeConfiguration extends AbstractCardPaymentGatewayConfiguration {

    @Bean("mock-stripe")
    public StripeGatewayDetailsService stripeGatewayDetailsService(PaymentProcessorRepository paymentProcessorRepository){
        return new StripeGatewayDetailsService(paymentProcessorRepository,getGatewayName());
    }

    @Bean
    public StripeGatewayPaymentService stripeGatewayPaymentService(@Qualifier("mockStripeGateway") CardPaymentGateway gateway){
        return new StripeGatewayPaymentService(gateway);
    }

    @Bean
    public MockInboundCardPaymentGateway mockStripeGateway(@Value("${mock-stripe.submitExpression}") String submit
        , @Value("${mock-stripe.authExpression}") String auth){
        ExpressionParser mockGatewayExpressionParser = new SpelExpressionParser();
        Expression submitExpression = mockGatewayExpressionParser.parseExpression(submit);
        Expression authExpression = mockGatewayExpressionParser.parseExpression(auth);
        return new MockInboundCardPaymentGateway(getGatewayName(),authExpression,submitExpression);
    }

    @Override
    protected String getGatewayName() {
        return "mock-stripe";
    }

    @RequestMapping(value = "/mock-stripe/details")
    @ApiIgnore
    public class StripeGatewayDetailsService extends GatewayDetailsService {
        public StripeGatewayDetailsService(PaymentProcessorRepository repository, String name) {
            super(repository, name);
        }
    }

    @RequestMapping(value = "/mock-stripe")
    @ApiIgnore
    public class StripeGatewayPaymentService extends GatewayCardPaymentService {
        public StripeGatewayPaymentService(CardPaymentGateway gateway) {
            super(gateway);
        }
    }
}
