package gov.scot.payments;

import kafka.server.KafkaConfig;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.RetryOneTime;
import org.apache.kafka.streams.StreamsConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.cloud.zookeeper.ZookeeperProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaStreamsDefaultConfiguration;
import org.springframework.kafka.config.KafkaStreamsConfiguration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.retry.RetryPolicy;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

@Configuration
@Profile("embedded-broker")
public class EmbeddedKafkaConfiguration {

    @Bean
    public EmbeddedKafkaBroker embeddedKafkaBroker(@Value("${payments.environment}") String env
            , @Value("${payments.tmpDir}") String tmpDir
            , @Value("${kafka.embedded.topics}") String[] topics) throws IOException {
        String[] prefixedTopics = Arrays.stream(topics).map(t -> env+"-"+t).toArray(String[]::new);
        EmbeddedKafkaBroker broker = new EmbeddedKafkaBroker(1, true, 1,prefixedTopics);
        File logDir = new File(tmpDir,"kafka-logs");
        //logDir.deleteOnExit();
        broker.brokerProperty(KafkaConfig.LogDirProp(),logDir.getAbsolutePath());
        broker.kafkaPorts(0);
        return broker;
    }

    @Bean
    public ProducerFactory<?, ?> kafkaProducerFactory(KafkaProperties properties
            , EmbeddedKafkaBroker embeddedKafkaBroker) {
        String brokerAddresses = embeddedKafkaBroker.getBrokersAsString();
        properties.setBootstrapServers(Collections.singletonList(brokerAddresses));
        DefaultKafkaProducerFactory<?, ?> factory = new DefaultKafkaProducerFactory<>(
                properties.buildProducerProperties());
        String transactionIdPrefix = properties.getProducer()
                .getTransactionIdPrefix();
        if (transactionIdPrefix != null) {
            factory.setTransactionIdPrefix(transactionIdPrefix);
        }
        return factory;
    }


    @Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
    public KafkaStreamsConfiguration kStreamsConfigs(@Value("${payments.environment}") String env
            , KafkaProperties properties
            , EmbeddedKafkaBroker embeddedKafkaBroker) throws IOException {
        String brokerAddresses = embeddedKafkaBroker.getBrokersAsString();
        properties.setBootstrapServers(Collections.singletonList(brokerAddresses));
        Map<String, Object> props = properties.buildStreamsProperties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, env);
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, brokerAddresses);
        return new KafkaStreamsConfiguration(props);
    }

    @Bean(destroyMethod = "close")
    public CuratorFramework curatorFramework(ZookeeperProperties properties, EmbeddedKafkaBroker embeddedKafkaBroker) throws Exception {
        CuratorFrameworkFactory.Builder builder = CuratorFrameworkFactory.builder();
        builder.connectString(embeddedKafkaBroker.getZookeeperConnectionString());
        CuratorFramework curator = builder.retryPolicy(new RetryOneTime(1000)).build();
        curator.start();
        curator.blockUntilConnected(properties.getBlockUntilConnectedWait(), properties.getBlockUntilConnectedUnit());
        return curator;
    }

}
