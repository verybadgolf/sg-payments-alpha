package gov.scot.payments;

import com.beust.jcommander.Parameter;
import gov.scot.payments.common.local.LocalConfig;
import lombok.Data;

import java.util.*;

@Data
public class LocalQuicksightConfig implements LocalConfig {

    @Parameter(names = "-qs", description = "Specify to use Quicksight reporting")
    private boolean quicksight = false;

    @Parameter(names = "-qs.role.arn", description = "Only required if -qs is specified. The ARN of the role to use when accessing quicksight")
    private String arn;

    @Parameter(names = "-qs.role.name", description = "Only required if -qs is specified. The name of the role to use when accessing quicksight")
    private String name;

    @Parameter(names = "-qs.account", description = "Only required if -qs is specified. The Account ID to use when accessing quicksight (i.e. the owning account ID)")
    private String account;

    @Override
    public Set<String> getProfiles(){
        if(quicksight){
            return Set.of("quicksight");
        }
        else{
            return Collections.emptySet();
        }
    }

    @Override
    public Map<String, Object> getProperties(){
        if(quicksight){
            return Map.of("QUICK_SIGHT_ROLE_ARN",arn,"QUICK_SIGHT_ROLE_NAME",name,"AWS_ACCOUNT_ID",account);
        } else{
            return Collections.emptyMap();
        }
    }

    @Override
    public String[] toArgs() {
        List<String> args = new ArrayList<>();
        if(quicksight){
            args.add("-qs");
        }
        if(arn != null){
            args.add("-qs.role.arn");
            args.add(LocalConfig.q(arn));
        }
        if(name != null){
            args.add("-qs.role.name");
            args.add(LocalConfig.q(name));
        }
        if(account != null){
            args.add("-qs.account");
            args.add(LocalConfig.q(account));
        }
        return args.toArray(new String[0]);
    }

    @Override
    public Set<String> validate() {
        if(!quicksight){
            return Collections.emptySet();
        }
        Set<String> violations = new HashSet<>();
        if(arn == null){
            violations.add("qs.role.arn must be provided");
        }
        if(name == null){
            violations.add("qs.role.name must be provided");
        }
        if(account == null){
            violations.add("qs.account must be provided");
        }
        return violations;
    }

    @Override
    public String toString() {
        return String.join(" ", toArgs());
    }
}
