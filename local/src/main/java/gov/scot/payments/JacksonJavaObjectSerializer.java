package gov.scot.payments;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.common.ObjectMapperProvider;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.h2.api.JavaObjectSerializer;

public class JacksonJavaObjectSerializer implements JavaObjectSerializer {

    private final ObjectMapper mapper = ObjectMapperProvider.MAPPER;

    @Override
    public byte[] serialize(Object obj) throws Exception {
        return mapper.writeValueAsBytes(obj);
    }

    @Override
    public Object deserialize(byte[] bytes) throws Exception {
        return mapper.readTree(bytes);
    }

}
