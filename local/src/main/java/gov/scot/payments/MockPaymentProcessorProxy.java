package gov.scot.payments;

import gov.scot.payments.common.gateway.GatewayDetailsService;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import gov.scot.payments.model.user.Client;
import gov.scot.payments.paymentprocessor.PaymentProcessorProxy;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.cloud.client.serviceregistry.Registration;
import org.springframework.cloud.client.serviceregistry.ServiceRegistry;
import org.springframework.security.core.context.SecurityContextHolder;

import java.net.URI;
import java.util.Map;
import java.util.Optional;

import static org.springframework.cloud.zookeeper.support.StatusConstants.STATUS_OUT_OF_SERVICE;
import static org.springframework.cloud.zookeeper.support.StatusConstants.STATUS_UP;

public class MockPaymentProcessorProxy implements PaymentProcessorProxy, BeanFactoryAware {

    private final Map<String, GatewayDetailsService> gateways;
    private final ServiceRegistry serviceRegistry;
    private BeanFactory beanFactory;

    public MockPaymentProcessorProxy(ServiceRegistry serviceRegistry
            , Map<String, GatewayDetailsService> gateways) {
        this.gateways = gateways;
        this.serviceRegistry = serviceRegistry;
    }

    @Override
    public String setGatewayStatus(String name, URI uri, boolean enabled) {
        Registration registration = getRegistration(name);
        String status = enabled ? STATUS_UP : STATUS_OUT_OF_SERVICE;
        this.serviceRegistry.setStatus(registration, status);
        return (String)this.serviceRegistry.getStatus(registration);
    }

    @Override
    public String getStatus(String name, URI uri) {
        Registration registration = getRegistration(name);
        return (String)this.serviceRegistry.getStatus(registration);
    }

    @Override
    public PaymentProcessor updateDetails(URI uri, PaymentProcessor processor) {
        Client user = (Client)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return getGateway(processor.getName())
                .map(s -> s.updateDetails(processor,user))
                .orElseThrow(() -> new IllegalArgumentException("No gateway: "+processor.getName()));
    }

    @Override
    public PaymentProcessor getDetails(String name, URI uri) {
        return getGateway(name)
                .flatMap(s -> s.getDetails())
                .orElseThrow(() -> new IllegalArgumentException("No gateway: "+name));
    }

    private Optional<GatewayDetailsService> getGateway(String name) {
       return Optional.ofNullable(gateways.get(name));
    }

    private Registration getRegistration(String name) {
        return beanFactory.getBean(name+"-registration",Registration.class);
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }
}
