package gov.scot.payments;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.SubParameter;
import gov.scot.payments.common.local.LocalConfig;
import gov.scot.payments.model.paymentinstruction.PaymentChannel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static gov.scot.payments.common.local.LocalConfig.q;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LocalGatewayConfig{

    private static final String FLAT_FEE_TEMPLATE = "T(org.javamoney.moneta.Money).of(new java.math.BigDecimal('%s'),'GBP')";
    private static final String PCT_FEE_TEMPLATE = "T(org.javamoney.moneta.Money).of(numberStripped.multiply(new java.math.BigDecimal('%s')),'GBP')";
    private static final String ERROR_MSG = "Channels must be of the format <NAME>(;(Card|Bacs|Chaps|FasterPayments)=<AMOUNT>(%))*";

    private String name;

    private Map<String,String> channels;

    public static LocalGatewayConfig fromString(String s) {
        String[] components = s.split(";");
        String name;
        if(!s.contains(";")){
            name = s;
        } else{
            if(components.length == 0){
                throw new IllegalArgumentException(ERROR_MSG);
            }
            name = components[0];
        }
        Map<String,String> channels = new HashMap<>();
        for(int i=1;i<components.length;i++){
            String channel = components[i];
            String[] split = channel.split("=");
            if(split.length != 2){
                throw new IllegalArgumentException(ERROR_MSG);
            }
            PaymentChannel paymentChannel;
            try{
                paymentChannel = PaymentChannel.valueOf(split[0]);
            } catch(IllegalArgumentException e){
                throw new IllegalArgumentException(ERROR_MSG);
            }
            String expression;
            try{
                String expr = split[1];
                if(expr.endsWith("%")){
                    BigDecimal d = new BigDecimal(expr.substring(0, expr.length()-1));
                    expression = String.format(PCT_FEE_TEMPLATE, d.divide(new BigDecimal("100"), MathContext.DECIMAL32).toPlainString());
                } else{
                    BigDecimal d = new BigDecimal(expr);
                    expression = String.format(FLAT_FEE_TEMPLATE, expr);
                }
            } catch(NumberFormatException e){
                throw new IllegalArgumentException(ERROR_MSG);
            }
            channels.put(paymentChannel.name(),expression);
        }
        return new LocalGatewayConfig(name,channels);
    }

    public List<String> getChannelArguments(){
        return channels.entrySet()
                .stream()
                .flatMap( e -> List.of("-C",e.getKey()+"="+e.getValue()).stream())
                .collect(Collectors.toList());
    }

}
