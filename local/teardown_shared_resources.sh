#!/bin/sh

delete_helm_release() {
  if [[ $(helm ls -q) == *${1}* ]]
  then
    helm delete --purge ${1} || true
  else
    echo "Helm release ${1} not found, skipping"
  fi
}
set -eo pipefail

kubectl config use-context docker-desktop

delete_helm_release "mock-jwt-server"
delete_helm_release "postgres"
delete_helm_release "schemaregistry"
delete_helm_release "kafka"
delete_helm_release "zookeeper"
delete_helm_release "confluent-operator"

echo "Deleting Localstack"
kubectl delete -n localkube deployments -l app=localstack
kubectl delete -n localkube pods -l app=localstack --grace-period=0 --force
kubectl delete -n localkube service -l app=localstack

echo "Cleaning up kafka"
kubectl delete -n localkube statefulsets -l "physicalstatefulcluster.core.confluent.cloud/name"=kafka
kubectl delete -n localkube pods -l type=kakfa --grace-period=0 --force
kubectl delete -n localkube service -l type=kakfa
kubectl delete -n localkube configmap -l type=kakfa
kubectl patch physicalstatefulcluster kafka -n localkube -p '{"metadata":{"finalizers": []}}' --type=merge || true
kubectl delete physicalstatefulcluster -l type=kafka -n localkube || true

echo "Cleaning up zookeeper"
kubectl delete -n localkube statefulsets -l type=zookeeper
kubectl delete -n localkube pods -l type=zookeeper --grace-period=0 --force
kubectl delete -n localkube service -l type=zookeeper
kubectl delete -n localkube configmap -l type=zookeeper
kubectl patch physicalstatefulcluster zookeeper -n localkube -p '{"metadata":{"finalizers": []}}' --type=merge || true
kubectl delete physicalstatefulcluster -l type=zookeeper -n localkube || true

echo "Cleaning up persistent volumes"
kubectl patch pvc data-zookeeper-0 -n localkube -p '{"metadata":{"finalizers": []}}' --type=merge || true
kubectl patch pvc txnlog-zookeeper-0 -n localkube -p '{"metadata":{"finalizers": []}}' --type=merge || true
kubectl patch pvc data0-kafka-0 -n localkube -p '{"metadata":{"finalizers": []}}' --type=merge || true
kubectl delete -n localkube pvc -l type=zookeeper
kubectl delete -n localkube pvc -l type=kakfa

echo "Cleaning up custom resources"
kubectl patch crd kafkaclusters.cluster.confluent.com -n localkube -p '{"metadata":{"finalizers": []}}' --type=merge || true
kubectl patch crd zookeeperclusters.cluster.confluent.com -n localkube -p '{"metadata":{"finalizers": []}}' --type=merge || true
kubectl patch crd physicalstatefulclusters.operator.confluent.com -n localkube -p '{"metadata":{"finalizers": []}}' --type=merge || true
kubectl delete -n localkube crd kafkaclusters.cluster.confluent.com || true
kubectl delete -n localkube crd zookeeperclusters.cluster.confluent.com || true
kubectl delete -n localkube crd physicalstatefulclusters.operator.confluent.cloud || true