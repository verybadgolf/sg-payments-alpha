package gov.scot.payments.gateway.outbound;

import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.PaymentInstructionBatch;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KGroupedStream;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Transformer;

import java.util.UUID;
import java.util.function.Function;
import java.util.function.Supplier;

public class TransformerWindowingPaymentInstructionBatcher<K,PK> extends PaymentInstructionBatcher<K,PK> {

    private final Supplier<Transformer<K, PaymentInstruction, KeyValue<String, PaymentInstructionBatch>>> transformer;

    public TransformerWindowingPaymentInstructionBatcher(Function<PaymentInstruction,PK> branchPredicate
            , PK partitionKey
            , BatchGrouping<K> groupingFunction
            , Function<PaymentInstructionBatch, String> batchNameExtractor
            , Supplier<Transformer<K, PaymentInstruction, KeyValue<String, PaymentInstructionBatch>>> transformer) {
        super(branchPredicate,partitionKey, groupingFunction, batchNameExtractor);
        this.transformer = transformer;
    }

    @Override
    protected WindowedKStream<K> applyWindowing(KGroupedStream<K, PaymentInstruction> stream) {
        return null;
    }

    @Override
    public KStream<UUID, PartitionedPaymentInstructionBatch<PK>> batch(KStream<?, PaymentInstruction> stream){
        return stream
                .peek( (k,v) -> log.info("Batcher for {}, handling payment {}",partitionKey,v))
                .map( (k,v) -> {
                    K key = groupingFunction.apply(v);
                    return new KeyValue<>(key == null ? (K)"NULL_KEY" : key,v);
                })
                .transform(transformer::get)
                .mapValues(b -> new PartitionedPaymentInstructionBatch<>(partitionKey,b.toBuilder().name(batchNameExtractor.apply(b)).build()))
                .map( (k,b) -> new KeyValue<>(b.getId(),b))
                .peek( (k,v) -> log.info("Emitting batch {} of {} payments",v.getBatch(),v.size()));
    }

    public static <K,PK> TransformerWindowingPaymentInstructionBatcherBuilder<K,PK> builder(){
        return new TransformerWindowingPaymentInstructionBatcherBuilder<>();
    }

    public static class TransformerWindowingPaymentInstructionBatcherBuilder<K,PK>
            extends PaymentInstructionBatcherBuilder<K,PK, TransformerWindowingPaymentInstructionBatcher<K,PK>, TransformerWindowingPaymentInstructionBatcherBuilder<K,PK>> {

        private Supplier<Transformer<K, PaymentInstruction, KeyValue<String, PaymentInstructionBatch>>> transformer;

        TransformerWindowingPaymentInstructionBatcherBuilder() {
        }

        @Override
        public TransformerWindowingPaymentInstructionBatcher<K, PK> build() {
            return new TransformerWindowingPaymentInstructionBatcher<>(branchPredicate,partitionKey,groupingFunction,batchNameExtractor,transformer);
        }

        public TransformerWindowingPaymentInstructionBatcherBuilder<K,PK> transformer(Supplier<Transformer<K, PaymentInstruction, KeyValue<String, PaymentInstructionBatch>>> transformer) {
            this.transformer = transformer;
            return this;
        }
    }
}
