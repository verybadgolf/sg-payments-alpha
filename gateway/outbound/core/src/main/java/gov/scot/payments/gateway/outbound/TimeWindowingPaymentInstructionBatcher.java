package gov.scot.payments.gateway.outbound;

import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.PaymentInstructionBatch;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.WindowStore;

import java.util.function.Function;

public class TimeWindowingPaymentInstructionBatcher<K,PK> extends PaymentInstructionBatcher<K,PK> {

    private final BatchWindowing<K> window;

    public TimeWindowingPaymentInstructionBatcher(Function<PaymentInstruction,PK> branchPredicate
            , PK partitionKey
            , BatchGrouping<K> groupingFunction
            , Function<PaymentInstructionBatch, String> batchNameExtractor
            , BatchWindowing<K> window) {
        super(branchPredicate,partitionKey, groupingFunction, batchNameExtractor);
        this.window = window;
    }

    @Override
    protected WindowedKStream<K> applyWindowing(KGroupedStream<K, PaymentInstruction> stream) {
        TimeWindowedKStream<K, PaymentInstruction> windowed = stream.windowedBy(window.getWindow());
        return (i,a,m) -> windowed.aggregate(i, a,(Materialized<K, PaymentInstructionBatch, WindowStore<Bytes, byte[]>>)window.getMaterialized());
    }

    public static <K,PK> TimeWindowingPaymentInstructionBatcherBuilder<K,PK> builder(){
        return new TimeWindowingPaymentInstructionBatcherBuilder<>();
    }

    public static class TimeWindowingPaymentInstructionBatcherBuilder<K,PK>
            extends PaymentInstructionBatcherBuilder<K,PK,TimeWindowingPaymentInstructionBatcher<K,PK>,TimeWindowingPaymentInstructionBatcherBuilder<K,PK>> {

        private BatchWindowing<K> window;

        TimeWindowingPaymentInstructionBatcherBuilder() {
        }

        @Override
        public TimeWindowingPaymentInstructionBatcher<K, PK> build() {
            return new TimeWindowingPaymentInstructionBatcher<>(branchPredicate,partitionKey,groupingFunction,batchNameExtractor,window);
        }

        public TimeWindowingPaymentInstructionBatcherBuilder<K,PK> window(BatchWindowing<K> window) {
            this.window = window;
            return this;
        }
    }
}
