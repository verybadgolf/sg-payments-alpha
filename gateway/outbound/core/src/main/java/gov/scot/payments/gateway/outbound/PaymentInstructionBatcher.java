package gov.scot.payments.gateway.outbound;

import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.PaymentInstructionBatch;
import org.apache.kafka.streams.kstream.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;
import java.util.function.Function;

public abstract class PaymentInstructionBatcher<K,PK> implements org.apache.kafka.streams.kstream.Predicate<Object,PaymentInstruction> {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    protected abstract WindowedKStream<K> applyWindowing(KGroupedStream<K,PaymentInstruction> stream);
    protected final PK partitionKey;
    private final Function<PaymentInstruction,PK> branchPredicate;
    protected final BatchGrouping<K> groupingFunction;
    protected final Function<PaymentInstructionBatch,String> batchNameExtractor;

    protected PaymentInstructionBatcher(Function<PaymentInstruction,PK> branchPredicate
            , PK partitionKey
            , BatchGrouping<K> groupingFunction
            , Function<PaymentInstructionBatch, String> batchNameExtractor) {
        this.branchPredicate = branchPredicate;
        this.groupingFunction = groupingFunction;
        this.batchNameExtractor = batchNameExtractor;
        this.partitionKey = partitionKey;
    }

    public KStream<UUID, PartitionedPaymentInstructionBatch<PK>> batch(KStream<?, PaymentInstruction> stream){
        KGroupedStream<K,PaymentInstruction> grouped = applyGrouping(stream);
        WindowedKStream<K> windowed = applyWindowing(grouped);
        Suppressed<Windowed> suppressed = Suppressed.untilWindowCloses(Suppressed.BufferConfig.unbounded())/*.withName(environment+"-"+gatewayName+"-batch-topic")*/;
        return windowed.aggregate(PaymentInstructionBatch::new, this::aggregate, this::merge)
                .suppress(suppressed)
                .mapValues(b -> new PartitionedPaymentInstructionBatch<>(partitionKey,b.toBuilder().name(batchNameExtractor.apply(b)).build()))
                .toStream( (k,b) -> b == null ? null : b.getId())
                .filterNot( (k,v) -> v == null)
                .peek( (k,v) -> log.info("Emitting batch {} of {} payments",v.getBatch(),v.size()));
    }

    @Override
    public boolean test(Object key, PaymentInstruction value) {
        return partitionKey == null || partitionKey.equals(branchPredicate.apply(value));
    }

    protected KGroupedStream<K, PaymentInstruction> applyGrouping(KStream<?, PaymentInstruction> paymentInstructionStream) {
        return paymentInstructionStream
                .peek( (k,v) -> log.info("Batcher for {}, handling payment {}",partitionKey,v))
                .groupBy( (k,v) -> groupingFunction.apply(v),groupingFunction.getGrouped());
    }

    private PaymentInstructionBatch aggregate(K key, PaymentInstruction payment, PaymentInstructionBatch batch) {
        log.info("Adding payment {} to existing batch {}",payment,batch);
        return batch.addPayment(payment);
    }

    private PaymentInstructionBatch merge(K k, PaymentInstructionBatch v1, PaymentInstructionBatch v2) {
        log.debug("Merging batches {} and {}",v1,v2);
        return v1.merge(v2);
    }

    interface WindowedKStream<K> {
        KTable<Windowed<K>, PaymentInstructionBatch> aggregate(final Initializer<PaymentInstructionBatch> initializer,
                                               final Aggregator<? super K, PaymentInstruction, PaymentInstructionBatch> aggregator,
                                               final Merger<? super K, PaymentInstructionBatch> sessionMerger);
    }

    public static abstract class PaymentInstructionBatcherBuilder<K,PK,T extends PaymentInstructionBatcher<K,PK>,B extends PaymentInstructionBatcherBuilder> {
        protected PK partitionKey;
        protected Function<PaymentInstruction,PK> branchPredicate;
        protected BatchGrouping<K> groupingFunction;
        protected Function<PaymentInstructionBatch,String> batchNameExtractor;

        PaymentInstructionBatcherBuilder() {
        }

        public B branchPredicate(PK partitionKey, Function<PaymentInstruction,PK> branchPredicate) {
            this.partitionKey = partitionKey;
            this.branchPredicate = branchPredicate;
            return (B)this;
        }

        public B groupingFunction(BatchGrouping<K> groupingFunction) {
            this.groupingFunction = groupingFunction;
            return (B)this;
        }

        public B batchNameExtractor(Function<PaymentInstructionBatch,String> batchNameExtractor) {
            this.batchNameExtractor = batchNameExtractor;
            return (B)this;
        }

        public abstract T build();

    }


}
