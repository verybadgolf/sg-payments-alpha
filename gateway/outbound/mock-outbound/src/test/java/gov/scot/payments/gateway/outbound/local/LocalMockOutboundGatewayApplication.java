package gov.scot.payments.gateway.outbound.local;


import gov.scot.payments.MockWebSecurityConfigurationCore;
import gov.scot.payments.common.kafka.KafkaStreamsOverridingContext;
import gov.scot.payments.common.local.LocalConfig;
import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.gateway.outbound.mock.spring.MockOutboundGatewayConfiguration;
import gov.scot.payments.common.gateway.spring.CoreGatewayConfiguration;
import gov.scot.payments.gateway.outbound.spring.CoreOutboundGatewayBinding;
import gov.scot.payments.gateway.outbound.spring.OutboundGatewayInputBinding;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.boot.web.context.WebServerPortFileWriter;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

import java.util.Map;
import java.util.Set;

@SpringBootApplication(scanBasePackageClasses = LocalMockOutboundGatewayApplication.class
        , exclude = {UserDetailsServiceAutoConfiguration.class
})
@Import({MockWebSecurityConfigurationCore.class,MockOutboundGatewayConfiguration.class, CoreGatewayConfiguration.class})
@EnableSpringDataWebSupport
@EntityScan(basePackageClasses = PaymentProcessor.class)
@EnableJpaRepositories(basePackageClasses = PaymentProcessorRepository.class)
@EnableBinding({CoreOutboundGatewayBinding.class, OutboundGatewayInputBinding.class})
@Slf4j
public class LocalMockOutboundGatewayApplication extends SpringApplication{

    @Setter
    private LocalMockOutboundGatewayConfig config;

    public LocalMockOutboundGatewayApplication(){
        super(LocalMockOutboundGatewayApplication.class);
        setApplicationContextClass(KafkaStreamsOverridingContext.class);
    }

    public static void main(String[] args) throws Exception {
        LocalMockOutboundGatewayConfig config = LocalConfig.parseArgs(LocalMockOutboundGatewayConfig::new,args);
        LocalMockOutboundGatewayApplication application = new LocalMockOutboundGatewayApplication();
        application.setConfig(config);
        application.run(args);
    }

    @Override
    protected void configurePropertySources(ConfigurableEnvironment environment, String[] args) {
        MutablePropertySources sources = environment.getPropertySources();
        sources.addFirst(new MapPropertySource("commandLineProperties", this.config.getProperties()));
    }

    @Override
    public ConfigurableApplicationContext run(String... args) {
        if(config.getApp().getPidFile() != null){
            addListeners(new ApplicationPidFileWriter(config.getApp().getPidFile()));
        }
        if(config.getApp().getPortFile() != null){
            addListeners(new WebServerPortFileWriter(config.getApp().getPortFile()));
        }
        Set<String> profiles = config.getProfiles();
        setAdditionalProfiles(profiles.toArray(new String[0]));
        return super.run(args);
    }
}