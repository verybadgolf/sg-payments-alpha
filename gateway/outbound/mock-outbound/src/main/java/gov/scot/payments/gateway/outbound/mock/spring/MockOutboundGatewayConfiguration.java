package gov.scot.payments.gateway.outbound.mock.spring;

import gov.scot.payments.common.gateway.spring.AbstractGatewayConfiguration;
import gov.scot.payments.gateway.outbound.PaymentInstructionBatcher;
import gov.scot.payments.gateway.outbound.PaymentInstructionBatchers;
import gov.scot.payments.gateway.outbound.mock.MockOutboundGateway;
import gov.scot.payments.model.paymentinstruction.PaymentChannel;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.converter.CompositeMessageConverterFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.messaging.MessageChannel;

import java.time.Duration;
import java.util.Collection;
import java.util.concurrent.Executors;

@Configuration
public class MockOutboundGatewayConfiguration extends AbstractGatewayConfiguration {

    @Value("${payments.gateway.name}")
    String name;

    @Bean
    public MockOutboundGateway mockGateway(@Qualifier("payment-accepted-events") MessageChannel acceptedEvents
            , @Qualifier("payment-rejected-events") MessageChannel rejectedEvents
            , @Qualifier("payment-cleared-events") MessageChannel clearedEvents
            , @Qualifier("payment-failedToClear-events") MessageChannel failedToClearEvents
            , @Qualifier("payment-settled-events") MessageChannel settledEvents
            , @Value("${payments.gateway.submitExpression}") String submit
            , @Value("${payments.gateway.acceptExpression}") String accept
            , @Value("${payments.gateway.clearExpression}") String clear
            , Collection<PaymentInstructionBatcher> batchers){
        ExpressionParser mockGatewayExpressionParser = new SpelExpressionParser();
        Expression submitExpression = mockGatewayExpressionParser.parseExpression(submit);
        Expression acceptExpression = mockGatewayExpressionParser.parseExpression(accept);
        Expression clearExpression = mockGatewayExpressionParser.parseExpression(clear);

        return new MockOutboundGateway(acceptedEvents
                ,rejectedEvents
                ,clearedEvents
                ,failedToClearEvents
                ,settledEvents
                ,Executors.newScheduledThreadPool(1)
                ,name
                ,submitExpression
                ,acceptExpression
                ,clearExpression
                ,batchers.toArray(new PaymentInstructionBatcher[0]));
    }

    @Bean
    PaymentInstructionBatcher fpBatcher(@Value("${payments.gateway.fp.maxBatchSize:1}") int maxBatchSize,
                                        @Value("${payments.gateway.fp.maxWindowDuration:PT1M}") String duration,
                                        CompositeMessageConverterFactory compositeNonNativeSerde){
        return PaymentInstructionBatchers
                .<String,String>countWithTimeout(maxBatchSize, Duration.parse(duration))
                .batchNameExtractor(PaymentInstructionBatchers.paymentFileBatchNaming())
                .branchPredicate(PaymentChannel.FasterPayments.name(),PaymentInstructionBatchers.channelTypePredicate())
                .groupingFunction(PaymentInstructionBatchers.groupedByPaymentFile(compositeNonNativeSerde))
                .build();
    }

    @Bean
    PaymentInstructionBatcher bacsBatcher(@Value("${payments.gateway.bacs.maxBatchSize:1}") int maxBatchSize,
                                          @Value("${payments.gateway.bacs.maxWindowDuration:PT1M}") String duration,
                                          CompositeMessageConverterFactory compositeNonNativeSerde){
        return PaymentInstructionBatchers
                .<String,String>countWithTimeout(maxBatchSize, Duration.parse(duration))
                .batchNameExtractor(PaymentInstructionBatchers.paymentFileBatchNaming())
                .branchPredicate(PaymentChannel.Bacs.name(), PaymentInstructionBatchers.channelTypePredicate())
                .groupingFunction(PaymentInstructionBatchers.groupedByPaymentFile(compositeNonNativeSerde))
                .build();
    }

    @Bean
    PaymentInstructionBatcher chapsBatcher(@Value("${payments.gateway.chaps.maxBatchSize:1}") int maxBatchSize,
                                           @Value("${payments.gateway.chaps.maxWindowDuration:PT1M}") String duration,
                                           CompositeMessageConverterFactory compositeNonNativeSerde){
        return PaymentInstructionBatchers
                .<String,String>countWithTimeout(maxBatchSize, Duration.parse(duration))
                .batchNameExtractor(PaymentInstructionBatchers.paymentFileBatchNaming())
                .branchPredicate(PaymentChannel.Chaps.name(), PaymentInstructionBatchers.channelTypePredicate())
                .groupingFunction(PaymentInstructionBatchers.groupedByPaymentFile(compositeNonNativeSerde))
                .build();
    }

    @Override
    protected String getGatewayName() {
        return name;
    }
}
