package gov.scot.payments.gateway.outbound.bottomline.restclient;

import gov.scot.payments.gateway.outbound.bottomline.model.BottomLinePaymentInstruction;
import lombok.*;

import java.util.List;
import java.util.Map;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PACKAGE)
@Getter
public class BottomLineAddPaymentResponse {

    private BottomLinePaymentInstruction model;
    private Map<String, List<String>> warning;
}
