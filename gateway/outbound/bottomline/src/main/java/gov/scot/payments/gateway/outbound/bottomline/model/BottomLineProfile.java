package gov.scot.payments.gateway.outbound.bottomline.model;

import lombok.*;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter
@Getter
@Builder
public class BottomLineProfile {

    private BottomLineAuth auth;
    @NonNull private String id;
    @NonNull @Builder.Default private String description = "SL Testing";

}
