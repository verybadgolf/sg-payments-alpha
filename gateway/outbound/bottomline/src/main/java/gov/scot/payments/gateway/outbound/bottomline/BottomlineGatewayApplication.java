package gov.scot.payments.gateway.outbound.bottomline;


import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.gateway.outbound.bottomline.spring.BottomlineGatewayConfiguration;
import gov.scot.payments.common.gateway.spring.CoreGatewayConfiguration;
import gov.scot.payments.gateway.outbound.spring.CoreOutboundGatewayBinding;
import gov.scot.payments.gateway.outbound.spring.OutboundGatewayInputBinding;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

@SpringBootApplication(scanBasePackageClasses = BottomlineGatewayApplication.class
        ,exclude = {UserDetailsServiceAutoConfiguration.class
})
@Import({BottomlineGatewayConfiguration.class, CoreGatewayConfiguration.class})
@EnableSpringDataWebSupport
@EntityScan(basePackageClasses = PaymentProcessor.class)
@EnableJpaRepositories(basePackageClasses = PaymentProcessorRepository.class)
@EnableBinding({CoreOutboundGatewayBinding.class, OutboundGatewayInputBinding.class})
public class BottomlineGatewayApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(BottomlineGatewayApplication.class);
        application.run(args);
    }
}