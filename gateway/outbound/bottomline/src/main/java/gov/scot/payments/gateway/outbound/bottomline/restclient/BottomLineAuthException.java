package gov.scot.payments.gateway.outbound.bottomline.restclient;

public class BottomLineAuthException extends Exception {

    public BottomLineAuthException(String message) {
        super(message);
    }

}
