package gov.scot.payments.gateway.outbound.bottomline.restclient;

public class BottomLineBatchValidationException extends BottomLineException {

    public BottomLineBatchValidationException(String message) {
        super(message);
    }

    public BottomLineBatchValidationException(String message, Throwable cause) {
        super(message, cause);
    }

}
