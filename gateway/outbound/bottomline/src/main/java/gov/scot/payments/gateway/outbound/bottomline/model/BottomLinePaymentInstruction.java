package gov.scot.payments.gateway.outbound.bottomline.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gov.scot.payments.gateway.outbound.bottomline.restclient.BottomLineAddPaymentException;
import gov.scot.payments.model.paymentinstruction.*;
import lombok.*;
import org.javamoney.moneta.Money;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;


@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@Builder(toBuilder = true)
@ToString
@EqualsAndHashCode
public class BottomLinePaymentInstruction {

    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");

    final private static String STANDARD_BANK_GIRO_TYPE_CODE = "99";
    final private static String STANDARD_DIRECT_DEBIT_TYPE_CODE = "17";

    @NonNull @Builder.Default private Boolean included = true;
    @NonNull @Builder.Default private String transactionType = "0";
    @NonNull private String sortCode;
    @NonNull private String accountNumber;
    private int id;
    private int batchId;
    private int index;
    @NonNull private String reference;
    @NonNull private BigDecimal amount;
    @JsonSerialize(using = BottomLinePaymentInstruction.DateSerializer.class)
    @JsonDeserialize(using = BottomLinePaymentInstruction.DateDeserializer.class)
    @NonNull private LocalDateTime paymentDate;
    @NonNull private String name;
    @NonNull private String text;

    @JsonIgnore private boolean accepted;
    @JsonIgnore private Throwable error;
    @JsonIgnore private PaymentInstruction paymentInstruction;

    //valid values are space, &, ., A-Z, 0-9 and 18 characters long
    private static String createValidValue(String input){
        if(input.length() > 18){
            input = input.substring(0, 17);
        }
        return input.toUpperCase().replaceAll("[^A-Z^0-9^.^&^\\-^ ]+", "");
    }

    public static BottomLinePaymentInstruction fromPaymentInstruction(PaymentInstruction instruction){
        var dateTime = LocalDateTime.of(instruction.getLatestTargetSettlementDate(), LocalTime.MAX);

        return BottomLinePaymentInstruction.builder()
                .sortCode(instruction.getRecipient().getSortCode().getValue())
                .accountNumber(instruction.getRecipient().getAccountNumber().getValue())
                .reference(createValidValue(instruction.getRecipient().getRef()))
                .amount(instruction.getTargetAmount().getNumberStripped())
                .paymentDate(dateTime)
                .name(createValidValue(instruction.getRecipient().getName().toUpperCase()))
                .text(createValidValue(instruction.getService()))
                .paymentInstruction(instruction)
                .build();

    }

    public BottomLinePaymentInstruction withBatch(int id) {
        return  toBuilder().batchId(id).build();
    }

    public BottomLinePaymentInstruction withAcceptance(PaymentInstruction paymentInstruction){
        return toBuilder().accepted(true).paymentInstruction(paymentInstruction).build();
    }

    public BottomLinePaymentInstruction withError(BottomLineAddPaymentException e) {
        return toBuilder().error(e).accepted(false).build();
    }

    public PaymentInstruction toPaymentInstruction(PaymentChannel channel,String processor) {
        if(accepted){
            return getSuccessPaymentInstruction(channel,processor);
        } else {
            return getErrorPaymentInstruction(channel,processor);
        }
    }

    private PaymentInstruction getSuccessPaymentInstruction(PaymentChannel channel,String processor){
        LocalDateTime now = LocalDateTime.now();
        return paymentInstruction.toBuilder()
                .addExecution(Execution.builder()
                        .executedAt(now)
                        .executedAmount(Money.of(amount,PaymentInstruction.GBP_CURRENCY))
                        .channel(channel)
                        .processor(processor)
                        .build())
                .status(PaymentInstructionStatus.Submitted)
                .timestamp(now)
                .build();
    }

    private PaymentInstruction getErrorPaymentInstruction(PaymentChannel channel,String processor){

        var errorMessage = String.format("Bottomline submission failed. Details: %s", error.getMessage());

        LocalDateTime now = LocalDateTime.now();
        return paymentInstruction.toBuilder()
                .status(PaymentInstructionStatus.SubmissionFailed)
                .addExecution(Execution.builder()
                        .executedAt(now)
                        .executedAmount(Money.zero(paymentInstruction.getTargetAmount().getCurrency()))
                        .channel(channel)
                        .processor(processor)
                        .status(ExecutionStatus.SubmissionFailed)
                        .statusMessage(errorMessage)
                        .build())
                .timestamp(now)
                .build();
    }

    public static class DateSerializer extends JsonSerializer<LocalDateTime> {

        @Override
        public void serialize(LocalDateTime value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeString(DATE_FORMATTER.format(value));
        }
    }

    public static class DateDeserializer extends JsonDeserializer<LocalDateTime> {
        @Override
        public LocalDateTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            String text = p.getText();
            return LocalDateTime.from(DATE_FORMATTER.parse(text));
        }
    }
}
