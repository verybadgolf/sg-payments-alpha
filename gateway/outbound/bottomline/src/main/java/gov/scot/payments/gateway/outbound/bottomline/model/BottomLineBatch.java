package gov.scot.payments.gateway.outbound.bottomline.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import gov.scot.payments.model.paymentinstruction.PaymentInstructionBatch;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@Builder(toBuilder = true)
public class BottomLineBatch {

    private int id;
    private int submission;
    @JsonProperty("statusDescription")
    @Builder.Default private BottomLineBatchStatus status = BottomLineBatchStatus.Entered;
    @JsonDeserialize(using = BottomLineDateDeserializer.class)
    private LocalDateTime enteredDate;
    private String application;
    private String applicationName;
    @NonNull @Builder.Default List<BottomLinePaymentInstruction> entries = Collections.emptyList();

    @NonNull @Builder.Default private String applicationDescription = "";
    private String description;

    private int creditCount;
    private BigDecimal creditTotal;



    public static BottomLineBatch fromBatch(PaymentInstructionBatch batch){

        return BottomLineBatch.builder()
                .description(batch.getName())
                .entries(batch.getPayments().stream().map(BottomLinePaymentInstruction::fromPaymentInstruction).collect(Collectors.toList()))
                .build();
    }

    @JsonIgnore
    public List<BottomLinePaymentInstruction> getAcceptedEntries() {
        return entries.stream().filter(BottomLinePaymentInstruction::isAccepted).collect(Collectors.toList());
    }

    @JsonIgnore
    public BottomLineBatchInfo getInfo() {
        return BottomLineBatchInfo.builder()
                .id(id)
                .submission(submission)
                .enteredDate(enteredDate)
                .status(status)
                .creditCount(creditCount)
                .creditTotal(creditTotal)
                .build();
    }

    @JsonIgnore
    public BottomLineBatchInfo getAcceptedInfo() {
        List<BottomLinePaymentInstruction> e = getAcceptedEntries();
        return BottomLineBatchInfo.builder()
                .id(id)
                .submission(submission)
                .enteredDate(enteredDate)
                .status(status)
                .creditCount(e.size())
                .creditTotal(e.stream().map(BottomLinePaymentInstruction::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add))
                .build();
    }
}
