package gov.scot.payments.gateway.outbound.bottomline.model;


import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Value;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Value
public class BottomLineErrorMessage {

    private final String errorValue;
    private final List<String> errorMessage;


    public static BottomLineErrorMessage fromResponseJson(String jsonString) {

        try {
            var om = new ObjectMapper();
            var jsonTree = om.readTree(jsonString);
            var errorMessageMap = jsonTree.get("error").fields();
            var entry = errorMessageMap.next();
            var errorValue = entry.getKey();
            var errorList = new ArrayList<String>();
            entry.getValue().elements().forEachRemaining(node -> errorList.add(node.toString()));
            return new BottomLineErrorMessage(errorValue, errorList);

        }catch (IOException e){

            return new BottomLineErrorMessage("INVALID", List.of("Could not read response string"));

        }
    }


}
