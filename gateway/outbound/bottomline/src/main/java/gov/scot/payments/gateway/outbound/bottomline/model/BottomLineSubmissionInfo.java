package gov.scot.payments.gateway.outbound.bottomline.model;

import lombok.Value;
import java.math.BigDecimal;

@Value
public class BottomLineSubmissionInfo {

    private BigDecimal amount;
}
