package gov.scot.payments.gateway.outbound.bottomline.restclient;

public class BottomLineUncommitBatchException extends BottomLineException {

    public BottomLineUncommitBatchException(String message) {
        super(message);
    }

    public BottomLineUncommitBatchException(String message, Throwable cause) {
        super(message, cause);
    }

}
