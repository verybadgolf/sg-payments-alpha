package gov.scot.payments.gateway.outbound.bottomline.local;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParametersDelegate;
import gov.scot.payments.common.local.*;
import lombok.Data;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Data
public class LocalBottomlineGatewayConfig implements LocalConfig{

    @Parameter(names = "-ptx.user",required = true)
    private String ptxUser;

    @Parameter(names = "-ptx.pwd",required = true)
    private String ptxPassword;

    @Parameter(names = "-ptx.url",required = true)
    private String ptxUrl;

    @Parameter(names = "-profile.bacs",required = true)
    private String bacsProfile;

    @Parameter(names = "-profile.fp",required = true)
    private String fpProfile;

    @ParametersDelegate private LocalKafkaConfig kafka = new LocalKafkaConfig();
    @ParametersDelegate private LocalZookeeperConfig zookeeper = new LocalZookeeperConfig();
    @ParametersDelegate private LocalDatabaseConfig database = new LocalDatabaseConfig();
    @ParametersDelegate private LocalApplicationConfig app = new LocalApplicationConfig(true);
    @ParametersDelegate private LocalCognitoConfig cognito = new LocalCognitoConfig();
    @ParametersDelegate private LocalAwsConfig aws = new LocalAwsConfig();

    @Override
    public Set<String> getProfiles() {
        Set<String> profiles = new HashSet<>();
        profiles.addAll(kafka.getProfiles());
        profiles.addAll(zookeeper.getProfiles());
        profiles.addAll(database.getProfiles());
        profiles.addAll(app.getProfiles());
        profiles.addAll(cognito.getProfiles());
        profiles.addAll(aws.getProfiles());
        return profiles;
    }

    @Override
    public Map<String, Object> getProperties() {
        Map<String,Object> properties = new HashMap<>();
        properties.put("PTX_USERNAME",ptxUser);
        properties.put("PTX_PASSWORD",ptxPassword);
        properties.put("PTX_ENDPOINT",ptxUrl);
        properties.put("BOTTOMLINE_BACS_PROFILE_ID",bacsProfile);
        properties.put("BOTTOMLINE_FPS_PROFILE_ID",fpProfile);
        properties.putAll(kafka.getProperties());
        properties.putAll(zookeeper.getProperties());
        properties.putAll(database.getProperties());
        properties.putAll(app.getProperties());
        properties.putAll(cognito.getProperties());
        properties.putAll(aws.getProperties());
        return properties;
    }

    @Override
    public String[] toArgs() {
        return new String[0];
    }

    @Override
    public Set<String> validate() {
        Set<String> violations = new HashSet<>();
        violations.addAll(kafka.validate());
        violations.addAll(zookeeper.validate());
        violations.addAll(database.validate());
        violations.addAll(app.validate());
        violations.addAll(cognito.validate());
        violations.addAll(aws.validate());
        if(cognito.isCognito() && !aws.isAws()){
            violations.add("aws is required if using cognito");
        }
        return violations;
    }
}
