package gov.scot.payments.gateway.outbound.bottomline.model;

import io.micrometer.core.instrument.util.IOUtils;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BottomLineErrorMessageTest {

    String jsonInstructionResponse = IOUtils.toString(this.getClass().getClassLoader().getResourceAsStream("errorResponse.json"));


    @Test
    void fromResponseJson() {

        BottomLineErrorMessage error = BottomLineErrorMessage.fromResponseJson(jsonInstructionResponse);
        assertEquals(error.getErrorValue(), "paymentDate");
        assertEquals(error.getErrorMessage(), List.of("\"Please enter valid value of Payment Date\""));
    }
}