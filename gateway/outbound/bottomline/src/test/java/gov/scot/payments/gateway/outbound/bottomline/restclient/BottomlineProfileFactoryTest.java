package gov.scot.payments.gateway.outbound.bottomline.restclient;

import gov.scot.payments.gateway.outbound.bottomline.model.BottomLineProfile;
import gov.scot.payments.model.paymentinstruction.PaymentChannel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BottomlineProfileFactoryTest {

    @Test
    void getProfileForChannel() throws BottomLineSubmitBatchException {

        var selector = BottomlineProfileFactory.builder().bacsProfile(BottomLineProfile.builder().id("001").build()).fpsProfile(BottomLineProfile.builder().id("002").build()).build();
        Assertions.assertEquals("001", selector.getProfile(PaymentChannel.Bacs).getId());
        Assertions.assertEquals("002", selector.getProfile(PaymentChannel.FasterPayments).getId());
        var thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> selector.getProfile(PaymentChannel.Chaps));
        assertEquals("Could not find a valid profile for payment method Chaps", thrown.getMessage());

    }
}