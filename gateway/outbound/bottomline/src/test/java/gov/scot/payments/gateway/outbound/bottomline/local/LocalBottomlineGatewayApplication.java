package gov.scot.payments.gateway.outbound.bottomline.local;

import gov.scot.payments.MockWebSecurityConfigurationCore;
import gov.scot.payments.common.local.LocalConfig;
import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.gateway.outbound.bottomline.spring.BottomlineGatewayConfiguration;
import gov.scot.payments.common.gateway.spring.CoreGatewayConfiguration;
import gov.scot.payments.gateway.outbound.spring.CoreOutboundGatewayBinding;
import gov.scot.payments.gateway.outbound.spring.OutboundGatewayInputBinding;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import lombok.Setter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

import java.util.Set;

@SpringBootApplication(scanBasePackageClasses = LocalBottomlineGatewayApplication.class
        , exclude = {UserDetailsServiceAutoConfiguration.class
})
@Import({MockWebSecurityConfigurationCore.class, BottomlineGatewayConfiguration.class, CoreGatewayConfiguration.class})
@EnableSpringDataWebSupport
@EntityScan(basePackageClasses = PaymentProcessor.class)
@EnableJpaRepositories(basePackageClasses = PaymentProcessorRepository.class)
@EnableBinding({CoreOutboundGatewayBinding.class, OutboundGatewayInputBinding.class})
public class LocalBottomlineGatewayApplication extends SpringApplication{

    @Setter
    private LocalBottomlineGatewayConfig config;

    public LocalBottomlineGatewayApplication(){
        super(LocalBottomlineGatewayApplication.class);
    }

    public static void main(String[] args) throws Exception {
        LocalBottomlineGatewayConfig config = LocalConfig.parseArgs(LocalBottomlineGatewayConfig::new,args);
        LocalBottomlineGatewayApplication application = new LocalBottomlineGatewayApplication();
        application.setConfig(config);
        application.run(args);
    }

    @Override
    protected void configurePropertySources(ConfigurableEnvironment environment, String[] args) {
        MutablePropertySources sources = environment.getPropertySources();
        sources.addFirst(new MapPropertySource("commandLineProperties", this.config.getProperties()));
    }

    @Override
    public ConfigurableApplicationContext run(String... args) {
        Set<String> profiles = config.getProfiles();
        setAdditionalProfiles(profiles.toArray(new String[0]));
        return super.run(args);
    }

}
