package gov.scot.payments.gateway.inbound.cardpayment.worldpay.model;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;

class WorldPayPaymentMethodTest {

    @Test
    void whenToStringCalledThenCardNumberAndCVCAreNotIncluded() {

        var name = "the name";
        var cardNumber = "1234 5678 9012 3456";
        var cvc = "246";
        var maskedCardNumber = "**** **** **** 3456";

        var worldPayPaymentMethod = WorldPayPaymentMethod.builder()
                .name(name)
                .expiryMonth(12)
                .expiryYear(1999)
                .cardType("VISA")
                .cardNumber(cardNumber)
                .cvc(cvc)
                .maskedCardNumber(maskedCardNumber)
                .cardIssuer("A Bank")
                .build();

        var toString = worldPayPaymentMethod.toString();

        assertThat(toString, containsString(name));
        assertThat(toString, not(containsString(cardNumber)));
        assertThat(toString, not(containsString(cvc)));
        assertThat(toString, containsString(maskedCardNumber));
    }
}
