package gov.scot.payments.gateway.inbound.cardpayment.worldpay.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.*;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Getter
@ToString
@EqualsAndHashCode
@Builder
@JsonInclude(Include.NON_NULL)
public class WorldPayPaymentMethod {

    @NonNull @Builder.Default private PaymentType type = PaymentType.Card;
    private String name;
    @NonNull private Integer expiryMonth;
    @NonNull private Integer expiryYear;
    private Integer issueNumber;
    private Integer startMonth;
    private Integer startYear;
    private String cardType;
    @ToString.Exclude private String cardNumber;
    @ToString.Exclude private String cvc;
    private String maskedCardNumber;
    private String cardSchemeType;
    private String cardSchemeName;
    private String cardProductTypeDescNonContactless;
    private String cardProductTypeDescContactless;
    private String cardIssuer;
    private String countryCode;
    private String cardClass;
    private String prepaid;

    public enum PaymentType {
        Card, ObfuscatedCard, APM
    }
}
