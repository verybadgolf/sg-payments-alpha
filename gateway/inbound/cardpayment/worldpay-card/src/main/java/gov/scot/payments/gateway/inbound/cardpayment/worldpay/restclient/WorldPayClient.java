package gov.scot.payments.gateway.inbound.cardpayment.worldpay.restclient;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.common.ObjectMapperProvider;
import gov.scot.payments.gateway.inbound.cardpayment.worldpay.model.WorldPayCardToken;
import gov.scot.payments.gateway.inbound.cardpayment.worldpay.model.WorldPayOrder;
import gov.scot.payments.gateway.inbound.cardpayment.worldpay.model.WorldPayPaymentMethod;
import gov.scot.payments.model.cardpayment.*;
import lombok.extern.slf4j.Slf4j;
import org.javamoney.moneta.Money;
import org.springframework.retry.RetryOperations;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.ExpressionRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Map;
import java.util.UUID;

import static gov.scot.payments.gateway.inbound.cardpayment.worldpay.WorldPayMetadata.WORLDPAY_ORDER_CODE_KEY;
import static gov.scot.payments.gateway.inbound.cardpayment.worldpay.model.WorldPayPaymentMethod.PaymentType.Card;
import static java.util.Map.entry;

@Slf4j
public class WorldPayClient {

    private final WorldPayAPI api;
    private final WorldPayAuthorization worldPayAuthorization;

    public WorldPayClient(RestOperations restOperations,
                          RetryOperations retryOperations,
                          String apiEndPoint,
                          String serviceKey,
                          String clientKey,
                          ObjectMapper objectMapper) {
        api = new WorldPayAPI(restOperations, retryOperations, apiEndPoint, objectMapper);
        worldPayAuthorization = new WorldPayAuthorization(serviceKey, clientKey);
    }

    public WorldPayClient(WorldPayAPI api, WorldPayAuthorization worldPayAuthorization) {
        this.api = api;
        this.worldPayAuthorization = worldPayAuthorization;
    }

    public synchronized WorldPayCardToken getToken(CardPaymentGatewayAuthRequest gatewayRequest) throws WorldPayCreateTokenException {

        WorldPayCardToken cardToken = WorldPayCardToken.builder()
                .reusable(false)
                .clientKey(worldPayAuthorization.getClientKey())
                .paymentMethod(
                        WorldPayPaymentMethod.builder()
                                .name(gatewayRequest.getName())
                                .expiryMonth(gatewayRequest.getExpiry().getMonthValue())
                                .expiryYear(gatewayRequest.getExpiry().getYear())
                                .cardNumber(gatewayRequest.getCardNumber())
                                .type(Card)
                                .cvc(gatewayRequest.getCvv())
                                .build()
                )
                .build();

        return api.token(cardToken, worldPayAuthorization);
    }

    public synchronized CardPaymentGatewayAuthResponse authorize(CardPaymentGatewayAuthRequest gatewayRequest, WorldPayCardToken cardToken) throws WorldPayOrderException {

        Money amount = gatewayRequest.getAmount();
        Integer amountInPennies = amount.scaleByPowerOfTen(amount.getCurrency().getDefaultFractionDigits()).getNumber().intValueExact();

        WorldPayOrder order = WorldPayOrder.builder()
                .token(cardToken.getToken())
                .amount(amountInPennies)
                .authorizeOnly(true)
                .currencyCode(gatewayRequest.getAmount().getCurrency().getCurrencyCode())
                .orderDescription("Authorization")
                .customerOrderCode(gatewayRequest.getPaymentRef().toString())
                .name(gatewayRequest.getName())
                .build();

        WorldPayOrder createdOrder = api.order(order, worldPayAuthorization);

        CardPaymentStatus status = createdOrder.getPaymentStatus().equals("AUTHORIZED") ? CardPaymentStatus.Submitted : CardPaymentStatus.Failed;

        CardPaymentGatewayAuthResponse.CardPaymentGatewayAuthResponseBuilder builder = CardPaymentGatewayAuthResponse.builder()
                .status(status)
                .message(createdOrder.getPaymentStatusReason())
                .ref(createdOrder.getCustomerOrderCode())
                .details(CardDetails.builder()
                        .issuer(createdOrder.getPaymentResponse().getCardIssuer())
                        .type(createdOrder.getPaymentResponse().getCardType())
                        .expiry(LocalDate.of(
                                createdOrder.getPaymentResponse().getExpiryYear(),
                                createdOrder.getPaymentResponse().getExpiryMonth(),
                                1))
                        .maskedCardNumber(createdOrder.getPaymentResponse().getMaskedCardNumber())
                        .name(createdOrder.getPaymentResponse().getName())
                        .build())
                .metadata(Map.ofEntries(
                        entry(WORLDPAY_ORDER_CODE_KEY, createdOrder.getOrderCode())));

        if (status == CardPaymentStatus.Submitted) {
            Money authorizedAmountInPennies = Money.of(createdOrder.getAuthorizedAmount(), createdOrder.getCurrencyCode());
            Money authorizedAmount = authorizedAmountInPennies.scaleByPowerOfTen(- authorizedAmountInPennies.getCurrency().getDefaultFractionDigits());
            builder = builder.amount(authorizedAmount);
        } else {
            builder = builder.message(createdOrder.getPaymentStatusReason());
        }

        return builder.build();
    }

    public CardPaymentGatewaySubmitResponse capture(CardPaymentGatewaySubmitRequest gatewayRequest) throws WorldPayCaptureException {

        WorldPayOrder capturedOrder = api.capture(gatewayRequest.getMetadata().get(WORLDPAY_ORDER_CODE_KEY), worldPayAuthorization);

        CardPaymentStatus status;

        switch (capturedOrder.getPaymentStatus()) {
            case "SUCCESS":
                status = CardPaymentStatus.Success;
                break;
            case "EXPIRED":
                status = CardPaymentStatus.Expired;
                break;
            default:
                status = CardPaymentStatus.Failed;
        }

        return CardPaymentGatewaySubmitResponse.builder()
                .status(status)
                .message(capturedOrder.getPaymentStatusReason())
                .ref(capturedOrder.getCustomerOrderCode())
                .metadata(Map.ofEntries(
                        entry(WORLDPAY_ORDER_CODE_KEY, capturedOrder.getOrderCode())))
                .build();
    }

    public CardPaymentGatewayCancelResponse cancel(CardPaymentGatewayCancelRequest gatewayRequest) throws WorldPayCancelException {

        String orderCode = gatewayRequest.getMetadata().get(WORLDPAY_ORDER_CODE_KEY);
        api.cancel(orderCode, worldPayAuthorization);

        return CardPaymentGatewayCancelResponse.builder()
                .status(CardPaymentStatus.Cancelled)
                .ref(gatewayRequest.getPaymentRef().toString())
                .metadata(gatewayRequest.getMetadata())
                .build();
    }

    public static void main(String[] args) {
        String clientKey = System.getenv("WORLDPAY_CLIENT_KEY");
        String serviceKey = System.getenv("WORLDPAY_SERVICE_KEY");
        String apiEndPoint = System.getenv("WORLDPAY_ENDPOINT");
        int backOffPeriod = 1000;
        int maxRetries = 3;

        // See https://developer.worldpay.com/jsonapi/docs/testing for test values
        String visaCreditCardNumber = "4444333322221111";
        String cvc = "123";
        String failedCvc = "000";
        String successCustomerName = "SUCCESS"; // Simulates successful payment
        String failedCustomerName = "FAILED";   // Simulates failed payment
        String errorCustomerName = "ERROR";     // Simulates error

        // Set up WorldPayClient

        RetryTemplate retryTemplate = new RetryTemplate();
        RestTemplate restTemplate = new RestTemplate();

        FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
        backOffPolicy.setBackOffPeriod(backOffPeriod);
        retryTemplate.setBackOffPolicy(backOffPolicy);
        var expressionRetryPolicy = new ExpressionRetryPolicy("#{statusCode.value() == 500}");
        expressionRetryPolicy.setMaxAttempts(maxRetries);
        retryTemplate.setRetryPolicy(expressionRetryPolicy);
        ObjectMapper objectMapper = new ObjectMapperProvider().get();

        WorldPayClient worldPayClient = new WorldPayClient(restTemplate, retryTemplate, apiEndPoint, serviceKey, clientKey, objectMapper);

        UUID paymentRef;
        LocalDate expiryDate = LocalDate.of(2025, 12, 1);
        WorldPayCardToken token;
        CardPaymentGatewayAuthRequest authRequest;
        CardPaymentGatewayAuthResponse authResponse;
        CardPaymentGatewaySubmitRequest submitRequest;
        CardPaymentGatewaySubmitResponse submitResponse;
        CardPaymentGatewayCancelRequest cancelRequest;

        log.info("\n"
                + "\n//////////////////////////////////////"
                + "\n// Get token, authorize and capture //"
                + "\n//////////////////////////////////////\n"
        );

        // Request token

        paymentRef = UUID.randomUUID();

        authRequest = CardPaymentGatewayAuthRequest.builder()
                .name(successCustomerName)
                .cardNumber(visaCreditCardNumber)
                .cvv(cvc)
                .expiry(expiryDate)
                .amount(Money.of(50.01, "GBP"))
                .paymentRef(paymentRef)
                .build();

        try {
            log.info("Getting token with SUCCESS user");
            token = worldPayClient.getToken(authRequest);
        } catch (WorldPayCreateTokenException e) {
            log.error("Getting token with SUCCESS user failed");
            return;
        }
        log.info("Received token " + token.getToken());

        // Use token to authorize payment

        try {
            log.info("Authorizing using token " + token.getToken());
            log.info("Amount is  " + authRequest.getAmount());
            authResponse = worldPayClient.authorize(authRequest, token);
        } catch (WorldPayOrderException e) {
            log.error("Authorizing using token " + token.getToken() + " failed");
            return;
        }
        log.info("Payment status is " + authResponse.getStatus() + ", with order code " + authResponse.getMetadata().get(WORLDPAY_ORDER_CODE_KEY) + " and amount " + authResponse.getAmount());

        // Capture the payment

        submitRequest = CardPaymentGatewaySubmitRequest.builder()
                .paymentRef(paymentRef)
                .ref(paymentRef.toString())
                .metadata(authResponse.getMetadata())
                .build();
        try {
            log.info("Capturing using order code " + submitRequest.getMetadata().get(WORLDPAY_ORDER_CODE_KEY));
            submitResponse = worldPayClient.capture(submitRequest);
        } catch (WorldPayCaptureException e) {
            log.error("Capturing using order code " + submitRequest.getMetadata().get(WORLDPAY_ORDER_CODE_KEY) + " failed");
            return;
        }
        log.info("Capture status is " + submitResponse.getStatus() + ", with order code " + submitResponse.getMetadata().get(WORLDPAY_ORDER_CODE_KEY));

        log.info("\n"
                + "\n/////////////////////////////////////"
                + "\n// Get token, authorize and cancel //"
                + "\n/////////////////////////////////////\n"
        );

        // Request token

        paymentRef = UUID.randomUUID();

        authRequest = CardPaymentGatewayAuthRequest.builder()
                .name(successCustomerName)
                .cardNumber(visaCreditCardNumber)
                .cvv(cvc)
                .expiry(expiryDate)
                .amount(Money.of(1, "GBP"))
                .paymentRef(paymentRef)
                .build();

        try {
            log.info("Getting token with SUCCESS user");
            token = worldPayClient.getToken(authRequest);
        } catch (WorldPayCreateTokenException e) {
            log.error("Getting token with SUCCESS user failed");
            return;
        }
        log.info("Received token " + token.getToken());

        // Use token to authorize payment

        try {
            log.info("Authorizing using token " + token.getToken());
            authResponse = worldPayClient.authorize(authRequest, token);
        } catch (WorldPayOrderException e) {
            log.error("Authorizing using token " + token.getToken() + " failed");
            return;
        }
        log.info("Payment status is " + authResponse.getStatus() + ", with order code " + authResponse.getMetadata().get(WORLDPAY_ORDER_CODE_KEY));

        // Cancel the payment

        cancelRequest = CardPaymentGatewayCancelRequest.builder()
                .paymentRef(paymentRef)
                .ref(paymentRef.toString())
                .metadata(authResponse.getMetadata())
                .build();

        try {
            log.info("Cancelling using order code " + cancelRequest.getMetadata().get(WORLDPAY_ORDER_CODE_KEY));
            worldPayClient.cancel(cancelRequest);
        } catch (WorldPayCancelException e) {
            log.error("Cancelling using order code " + cancelRequest.getMetadata().get(WORLDPAY_ORDER_CODE_KEY) + " failed");
            return;
        }
        log.info("Cancelled using order code " + cancelRequest.getMetadata().get(WORLDPAY_ORDER_CODE_KEY));

        log.info("\n"
                + "\n///////////////////////////////////"
                + "\n// Authorize a cancelled payment //"
                + "\n///////////////////////////////////\n"
        );

        submitRequest = CardPaymentGatewaySubmitRequest.builder()
                .paymentRef(paymentRef)
                .ref(paymentRef.toString())
                .metadata(authResponse.getMetadata())
                .build();
        try {
            log.info("Capturing using order code " + submitRequest.getMetadata().get(WORLDPAY_ORDER_CODE_KEY));
            worldPayClient.capture(submitRequest);
            log.error("Capturing a cancelled order using order code " + submitRequest.getMetadata().get(WORLDPAY_ORDER_CODE_KEY) + " should have failed.");
            return;
        } catch (WorldPayCaptureException e) {
            log.info("Capturing a cancelled order using order code " + submitRequest.getMetadata().get(WORLDPAY_ORDER_CODE_KEY) + " failed, as expected");
        }

        log.info("\n"
                + "\n///////////////////////////////////////////////////////"
                + "\n// Get token and authorize with FAILED customer name //"
                + "\n///////////////////////////////////////////////////////\n"
        );

        // Request token

        paymentRef = UUID.randomUUID();

        authRequest = CardPaymentGatewayAuthRequest.builder()
                .name(failedCustomerName)
                .cardNumber(visaCreditCardNumber)
                .cvv(cvc)
                .expiry(expiryDate)
                .amount(Money.of(1, "GBP"))
                .paymentRef(paymentRef)
                .build();

        try {
            log.info("Getting token with FAILED user");
            token = worldPayClient.getToken(authRequest);
        } catch (WorldPayCreateTokenException e) {
            log.error("Getting token with FAILED user failed");
            return;
        }
        log.info("Received token " + token.getToken());

        // Use token to authorize payment

        try {
            log.info("Authorizing using token " + token.getToken());
            authResponse = worldPayClient.authorize(authRequest, token);
        } catch (WorldPayOrderException e) {
            log.error("Authorizing using token " + token.getToken() + " failed");
            return;
        }
        log.info("Payment status is " + authResponse.getStatus() + " with message " + authResponse.getMessage() + ", and order code " + authResponse.getMetadata().get(WORLDPAY_ORDER_CODE_KEY));

        log.info("\n"
                + "\n//////////////////////////////////////////////////////"
                + "\n// Get token and authorize with ERROR customer name //"
                + "\n//////////////////////////////////////////////////////\n"
        );

        // Request token

        paymentRef = UUID.randomUUID();

        authRequest = CardPaymentGatewayAuthRequest.builder()
                .name(errorCustomerName)
                .cardNumber(visaCreditCardNumber)
                .cvv(cvc)
                .expiry(expiryDate)
                .amount(Money.of(1, "GBP"))
                .paymentRef(paymentRef)
                .build();

        try {
            log.info("Getting token with ERROR user");
            token = worldPayClient.getToken(authRequest);
        } catch (WorldPayCreateTokenException e) {
            log.error("Getting token with ERROR user failed");
            return;
        }
        log.info("Received token " + token.getToken());

        // Use token to authorize payment

        try {
            log.info("Authorizing using token " + token.getToken());
            authResponse = worldPayClient.authorize(authRequest, token);
        } catch (WorldPayOrderException e) {
            log.error("Authorizing using token " + token.getToken() + " failed");
            return;
        }
        log.info("Payment status is " + authResponse.getStatus() + " with message " + authResponse.getMessage() + ", and order code " + authResponse.getMetadata().get(WORLDPAY_ORDER_CODE_KEY));

        log.info("\n"
                + "\n//////////////////////////////////////////////////"
                + "\n// Get token and authorize with failed CVC code //"
                + "\n//////////////////////////////////////////////////\n"
        );

        // Request token

        paymentRef = UUID.randomUUID();

        authRequest = CardPaymentGatewayAuthRequest.builder()
                .name(errorCustomerName)
                .cardNumber(visaCreditCardNumber)
                .cvv(failedCvc)
                .expiry(expiryDate)
                .amount(Money.of(1, "GBP"))
                .paymentRef(paymentRef)
                .build();

        try {
            log.info("Getting token with failed CVC");
            token = worldPayClient.getToken(authRequest);
        } catch (WorldPayCreateTokenException e) {
            log.error("Getting token with failed CVC failed");
            return;
        }
        log.info("Received token " + token.getToken());

        // Use token to authorize payment

        try {
            log.info("Authorizing using token " + token.getToken());
            authResponse = worldPayClient.authorize(authRequest, token);
        } catch (WorldPayOrderException e) {
            log.error("Authorizing using token " + token.getToken() + " failed");
            return;
        }
        log.info("Payment status is " + authResponse.getStatus() + " with message " + authResponse.getMessage() + ", and order code " + authResponse.getMetadata().get(WORLDPAY_ORDER_CODE_KEY));


    }
}
