package gov.scot.payments.gateway.inbound.cardpayment.worldpay.spring;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "worldpay")
@Data
public class WorldPayProperties {

    private int maxRetries = 3;
    private int retryWait = 10000;
    private String apiEndpoint;
    private String serviceKey;
    private String clientKey;
}
