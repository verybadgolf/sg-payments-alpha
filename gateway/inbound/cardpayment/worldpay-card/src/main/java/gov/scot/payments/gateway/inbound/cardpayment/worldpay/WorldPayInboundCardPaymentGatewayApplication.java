package gov.scot.payments.gateway.inbound.cardpayment.worldpay;


import gov.scot.payments.common.gateway.spring.CoreGatewayConfiguration;
import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.gateway.inbound.cardpayment.worldpay.spring.WorldPayInboundCardPaymentGatewayConfiguration;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

@SpringBootApplication(scanBasePackageClasses = WorldPayInboundCardPaymentGatewayApplication.class
        ,exclude = {UserDetailsServiceAutoConfiguration.class
})
@Import({WorldPayInboundCardPaymentGatewayConfiguration.class, CoreGatewayConfiguration.class})
@EnableSpringDataWebSupport
@EntityScan(basePackageClasses = PaymentProcessor.class)
@EnableJpaRepositories(basePackageClasses = PaymentProcessorRepository.class)
public class WorldPayInboundCardPaymentGatewayApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(WorldPayInboundCardPaymentGatewayApplication.class);
        application.run(args);
    }
}