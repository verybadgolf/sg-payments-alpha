package gov.scot.payments.gateway.inbound.cardpayment.worldpay.restclient;

import lombok.Builder;
import lombok.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

@Value
@Builder
public class WorldPayAuthorization {

    private final String serviceKey;
    private final String clientKey;

    public WorldPayAuthorization(String serviceKey, String clientKey) {
        this.serviceKey = serviceKey;
        this.clientKey = clientKey;
    }

    HttpHeaders getServiceAuthHttpHeaders() {
        HttpHeaders headers = getNoAuthHttpHeaders();
        headers.add("Authorization", serviceKey);
        return headers;
    }

    HttpHeaders getNoAuthHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
}
