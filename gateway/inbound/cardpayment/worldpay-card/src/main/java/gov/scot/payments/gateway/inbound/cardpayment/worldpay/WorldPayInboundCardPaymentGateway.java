package gov.scot.payments.gateway.inbound.cardpayment.worldpay;

import gov.scot.payments.common.gateway.inbound.cardpayment.CardPaymentGateway;
import gov.scot.payments.gateway.inbound.cardpayment.worldpay.model.WorldPayCardToken;
import gov.scot.payments.gateway.inbound.cardpayment.worldpay.restclient.*;
import gov.scot.payments.model.cardpayment.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WorldPayInboundCardPaymentGateway implements CardPaymentGateway {

    private final String name;
    private final WorldPayClient worldPayClient;

    public WorldPayInboundCardPaymentGateway(String name, WorldPayClient worldPayClient) {
        this.name = name;
        this.worldPayClient = worldPayClient;
    }

    @Override
    public CardPaymentGatewayAuthResponse authorize(CardPaymentGatewayAuthRequest gatewayRequest) {

        WorldPayCardToken cardToken;

        try {
            cardToken = worldPayClient.getToken(gatewayRequest);
        } catch (WorldPayCreateTokenException e) {
            log.error("Error creating token for WorldPay for request {}", gatewayRequest.getPaymentRef());
            return handleAuthError(gatewayRequest, e);
        }

        CardPaymentGatewayAuthResponse response;
        try {
            response = worldPayClient.authorize(gatewayRequest, cardToken);
        } catch (WorldPayOrderException e) {
            log.error("Error authorizing WorldPay order for request {}", gatewayRequest.getPaymentRef());
            return handleAuthError(gatewayRequest, e);
        }

        return response;
    }

    @Override
    public CardPaymentGatewaySubmitResponse submit(CardPaymentGatewaySubmitRequest gatewayRequest) {

        try {
            return worldPayClient.capture(gatewayRequest);
        } catch (WorldPayCaptureException e) {
            log.error("Error authorizing WorldPay order for request {}", gatewayRequest.getPaymentRef());
            return handleSubmitError(gatewayRequest, e);
        }
    }

    @Override
    public CardPaymentGatewayCancelResponse cancel(CardPaymentGatewayCancelRequest gatewayRequest) {
        try {
            return worldPayClient.cancel(gatewayRequest);
        } catch (WorldPayCancelException e) {
            log.error("Error authorizing WorldPay order for request {}", gatewayRequest.getPaymentRef());
            return handleCancelError(gatewayRequest, e);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    private CardPaymentGatewayAuthResponse handleAuthError(CardPaymentGatewayAuthRequest gatewayRequest, Throwable e) {
        return CardPaymentGatewayAuthResponse.builder()
                .status(CardPaymentStatus.Error)
                .message(e.getMessage())
                .ref(gatewayRequest.getPaymentRef().toString())
                .build();
    }

    private CardPaymentGatewaySubmitResponse handleSubmitError(CardPaymentGatewaySubmitRequest gatewayRequest, Throwable e) {
        return CardPaymentGatewaySubmitResponse.builder()
                .status(CardPaymentStatus.Error)
                .message(e.getMessage())
                .ref(gatewayRequest.getPaymentRef().toString())
                .build();
    }

    private CardPaymentGatewayCancelResponse handleCancelError(CardPaymentGatewayCancelRequest gatewayRequest, Throwable e) {
        return CardPaymentGatewayCancelResponse.builder()
                .status(CardPaymentStatus.Error)
                .message(e.getMessage())
                .ref(gatewayRequest.getPaymentRef().toString())
                .build();
    }
}
