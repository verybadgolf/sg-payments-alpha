package gov.scot.payments.gateway.inbound.cardpayment.worldpay.spring;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.gateway.inbound.cardpayment.spring.AbstractCardPaymentGatewayConfiguration;
import gov.scot.payments.gateway.inbound.cardpayment.worldpay.WorldPayInboundCardPaymentGateway;
import gov.scot.payments.gateway.inbound.cardpayment.worldpay.restclient.WorldPayClient;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.ExpressionRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableConfigurationProperties(WorldPayProperties.class)
public class WorldPayInboundCardPaymentGatewayConfiguration extends AbstractCardPaymentGatewayConfiguration {

    private final static String name = "worldpay";

    @Bean
    WorldPayClient worldPayClient(WorldPayProperties properties, ObjectMapper objectMapper) {
        RetryTemplate retryTemplate = new RetryTemplate();
        RestTemplate restTemplate = new RestTemplate();
        FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
        backOffPolicy.setBackOffPeriod(properties.getRetryWait());
        retryTemplate.setBackOffPolicy(backOffPolicy);
        var expressionRetryPolicy = new ExpressionRetryPolicy("#{statusCode.value() == 500}");
        expressionRetryPolicy.setMaxAttempts(properties.getMaxRetries());
        retryTemplate.setRetryPolicy(expressionRetryPolicy);
        return new WorldPayClient(restTemplate, retryTemplate, properties.getApiEndpoint(), properties.getServiceKey(), properties.getClientKey(), objectMapper);
    }

    @Bean
    WorldPayInboundCardPaymentGateway worldPayInboundCardPaymentGateway(WorldPayClient worldPayClient) {
        return new WorldPayInboundCardPaymentGateway(name, worldPayClient);
    }

    @Override
    protected String getGatewayName() {
        return name;
    }
}
