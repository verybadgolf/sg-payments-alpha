package gov.scot.payments.gateway.inbound.cardpayment.worldpay.restclient;

public class WorldPayCancelException extends WorldPayException {

    public WorldPayCancelException(String message) {
        super(message);
    }

}
