package gov.scot.payments;

import gov.scot.payments.model.user.EntityOp;
import org.springframework.security.test.context.support.WithSecurityContext;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.METHOD })
@WithSecurityContext(factory = WithUserSecurityContextFactory.class)
public @interface WithUser {

    String userName() default "admin";

    String email() default "admin@admin.com";

    String entityType() default "*";

    String entityId() default "*";

    EntityOp operation() default EntityOp.Write;
}
