package gov.scot.payments;

import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.User;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

public class WithUserSecurityContextFactory implements WithSecurityContextFactory<WithUser> {

    @Override
    public SecurityContext createSecurityContext(WithUser annotation) {
        SecurityContext context = SecurityContextHolder.createEmptyContext();
        User user = User.builder()
                .email(annotation.email())
                .userName(annotation.userName())
                .withRole(new Role(annotation.operation(),annotation.entityType(),annotation.entityId()))
                .build();
        Authentication auth = new UsernamePasswordAuthenticationToken(user, "N/A", user.getAuthorities());
        context.setAuthentication(auth);
        return context;

    }
}
