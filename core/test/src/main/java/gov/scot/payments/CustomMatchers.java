package gov.scot.payments;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAmount;

public class CustomMatchers {

    public static final Matcher<LocalDateTime> isCloseTo(LocalDateTime time, TemporalAmount error) {
        return new TypeSafeMatcher<>() {

            @Override
            public void describeTo(Description description) {
                description.appendText("Time should be within \"").appendValue(error).appendText("\" of \"").appendValue(time).appendText("\"");
            }

            @Override
            protected boolean matchesSafely(LocalDateTime testTime) {
                return testTime.isAfter(time.minus(error)) && testTime.isBefore(time.plus(error));
            }
        };
    }

    public static final Matcher<LocalDateTime> isDBTimeCloseTo(LocalDateTime time) {
        return isCloseTo(time, Duration.ofMillis(1));
    }

    public static final Matcher<String> isDateTimeStringInRange(LocalDateTime before, LocalDateTime after) {
        return new TypeSafeMatcher<>() {

            @Override
            public void describeTo(Description description) {
                description.appendText("Time should be in range \"").appendValue(before).appendText("\" to \"").appendValue(after).appendText("\"");
            }

            @Override
            protected boolean matchesSafely(String item) {
                final LocalDateTime time = LocalDateTime.parse(item);
                return (time.isAfter(before) || time.isEqual(before)) && (time.isBefore(after) || time.isEqual(after));
            }
        };
    }

    public static final Matcher<LocalDateTime> isDateTimeInRange(LocalDateTime before, LocalDateTime after) {
        return new TypeSafeMatcher<>() {

            @Override
            public void describeTo(Description description) {
                description.appendText("Time should be in range \"").appendValue(before).appendText("\" to \"").appendValue(after).appendText("\"");
            }

            @Override
            protected boolean matchesSafely(LocalDateTime time) {
                return (time.isAfter(before) || time.isEqual(before)) && (time.isBefore(after) || time.isEqual(after));
            }
        };
    }
}
