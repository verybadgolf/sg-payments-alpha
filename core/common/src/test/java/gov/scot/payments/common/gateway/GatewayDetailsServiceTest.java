package gov.scot.payments.common.gateway;

import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import gov.scot.payments.model.user.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class GatewayDetailsServiceTest {

    private PaymentProcessorRepository repository;
    private GatewayDetailsService service;

    @BeforeEach
    public void setUp(){
        repository = mock(PaymentProcessorRepository.class);
        service = new GatewayDetailsService(repository,"test");
    }

    @Test
    public void testGet(){
        PaymentProcessor processor = PaymentProcessor.builder()
                .name("test")
                .createdBy("admin")
                .build();
        when(repository.findById("test")).thenReturn(Optional.of(processor));
        assertEquals(processor,service.getDetails().get());
    }

    @Test
    public void testUpdate(){
        PaymentProcessor processor = PaymentProcessor.builder()
                .name("test")
                .createdBy("admin")
                .build();
        doAnswer(i -> i.getArgument(0,PaymentProcessor.class)).when(repository).save(processor);
        assertEquals(processor,service.updateDetails(processor, User.admin()));
    }
}
