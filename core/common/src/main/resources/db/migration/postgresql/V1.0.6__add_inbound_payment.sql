CREATE TABLE INBOUND_PAYMENT (
    id UUID PRIMARY KEY,
    created_at TIMESTAMP NOT NULL,
    amount DECIMAL NOT NULL,
    amount_currency CHAR(3) NOT NULL,
    channel_type VARCHAR(20) NOT NULL,
    channel_id VARCHAR(255) NOT NULL,
    ref VARCHAR(255),
    status VARCHAR(20) NOT NULL,
    status_message TEXT,
    recipient VARCHAR(255) NOT NULL,
    cleared_amount DECIMAL,
    cleared_amount_currency CHAR(3),
    clearing_status VARCHAR(20),
    clearing_message TEXT,
    cleared_at TIMESTAMP,
    settled_amount DECIMAL,
    settled_amount_currency CHAR(3),
    settled_at TIMESTAMP,
    created_by VARCHAR(255) NOT NULL
);

CREATE INDEX inbound_payment_status ON INBOUND_PAYMENT(status);
CREATE INDEX inbound_payment_service ON INBOUND_PAYMENT(recipient);
CREATE INDEX inbound_payment_created_at ON INBOUND_PAYMENT(created_at DESC);

CREATE TABLE TEMPORAL_INBOUND_PAYMENT (
    id UUID NOT NULL,
    recipient VARCHAR(255) NOT NULL,
    version_id UUID PRIMARY KEY,
    timestamp TIMESTAMP NOT NULL,
    status VARCHAR(20) NOT NULL,
    channel_id VARCHAR(255) NOT NULL,
    details JSONB NOT NULL
);

CREATE INDEX inbound_temporal_payment_id ON TEMPORAL_INBOUND_PAYMENT(id);
CREATE INDEX inbound_temporal_payment_status ON TEMPORAL_INBOUND_PAYMENT(status);
CREATE INDEX inbound_temporal_payment_timestamp ON TEMPORAL_INBOUND_PAYMENT(timestamp DESC);
CREATE INDEX inbound_temporal_payment_service ON TEMPORAL_INBOUND_PAYMENT(recipient);
