package gov.scot.payments.common.gateway.inbound.cardpayment;

import gov.scot.payments.model.cardpayment.*;

public interface CardPaymentGateway {
    CardPaymentGatewayAuthResponse authorize(CardPaymentGatewayAuthRequest gatewayRequest);
    CardPaymentGatewaySubmitResponse submit(CardPaymentGatewaySubmitRequest gatewayRequest);
    CardPaymentGatewayCancelResponse cancel(CardPaymentGatewayCancelRequest gatewayRequest);

    String getName();
}
