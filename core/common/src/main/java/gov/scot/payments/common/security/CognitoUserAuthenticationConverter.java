package gov.scot.payments.common.security;

import gov.scot.payments.model.user.*;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class CognitoUserAuthenticationConverter implements UserAuthenticationConverter {

    private static final String ADMIN_GROUP = "administrator";
    private static final Pattern GROUP_PATTERN = Pattern.compile("(.+)_(payment|service|customer|processor|inbound-payments):(.+):(Read|Write)");

    private final String environment;

    public CognitoUserAuthenticationConverter(String environment) {
        this.environment = environment;
    }

    @Override
    public Map<String, ?> convertUserAuthentication(Authentication userAuthentication) {
        return null;
    }

    @Override
    public Authentication extractAuthentication(Map<String, ?> map) {
        Optional<Client> principal = Optional.empty();
        if("id".equals(map.get("token_use"))){
            User user = createUser(map);
            principal = Optional.of(user);
        } else if("access".equals(map.get("token_use")) && !map.containsKey("username")){
            ApiClient apiClient = createApiClient(map);
            principal = Optional.of(apiClient);
        }
        return principal
                .map(u -> new UsernamePasswordAuthenticationToken(u, "N/A", u.getAuthorities()))
                .orElse(null);
    }

    private ApiClient createApiClient(Map<String, ?> map) {
        String clientId = (String)map.get("client_id");
        if(clientId == null){
            throw new BadCredentialsException("No email or client_id claim found in token");
        }
        String scope = (String)map.get("scope");
        List<Scope> scopes = scope == null ? Collections.emptyList() : Stream.of(scope.split(" "))
                .map(this::createScope)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        return ApiClient.builder()
                .userName(clientId)
                .scopes(scopes)
                .build();
    }

    private User createUser(Map<String, ?> map) {
        List<String> groups = (List<String>) map.get("cognito:groups");
        List<Role> roles = groups == null ? Collections.emptyList() : groups.stream()
                .map(this::createRole)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        String email = (String)map.get("email");
        String userName = (String)map.get("cognito:username");
        if(email == null || userName == null){
            throw new BadCredentialsException("No email or cognito:username claim found in token");
        }
        return User.builder()
                .email(email)
                .userName(userName)
                .roles(roles)
                .build();
    }

    private Scope createScope(String scope) {
        return Scope.fromString(scope);
    }

    private Role createRole(String group) {
        if(ADMIN_GROUP.equals(group)){
            return Role.WILDCARD_ROLE;
        }
        Matcher matcher = GROUP_PATTERN.matcher(group);
        if(!matcher.matches()){
            throw new BadCredentialsException("Cognito groups must be of format <ENVIRONMENT>_<ENTITY_TYPE>:<ENTITY_ID>:(Read|Write)");
        }
        String environment = matcher.group(1);
        if(!this.environment.equals(environment)){
            return null;
        }
        return new Role(EntityOp.valueOf(matcher.group(4)),matcher.group(2),matcher.group(3));
    }
}
