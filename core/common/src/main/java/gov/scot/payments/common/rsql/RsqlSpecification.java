package gov.scot.payments.common.rsql;

import cz.jirutka.rsql.parser.ast.ComparisonOperator;
import org.springframework.data.jpa.domain.Specification;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public  class RsqlSpecification<T> implements Specification<T> {

    private String property;
    private ComparisonOperator operator;
    private List<String> arguments;

    public RsqlSpecification(final String property, final ComparisonOperator operator, final List<String> arguments) {
        super();
        this.property = property;
        this.operator = operator;
        this.arguments = arguments;
    }

    @Override
    public Predicate toPredicate(final Root<T> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {
        RsqlSearchOperation oper = RsqlSearchOperation.getSimpleOperator(operator);
        if(oper != null) {
            Path path = getPath(root);
            final List<Object> args = castArguments(path);
            final Object argument = args.get(0);
            switch (oper) {
                case EQUAL: {
                    if (argument instanceof String) {
                        return builder.like(path, argument.toString().replace('*', '%'));
                    } else if (argument == null) {
                        return builder.isNull(path);
                    } else {
                        return builder.equal(path, argument);
                    }
                }
                case NOT_EQUAL: {
                    if (argument instanceof String) {
                        return builder.notLike(path, argument.toString().replace('*', '%'));
                    } else if (argument == null) {
                        return builder.isNotNull(path);
                    } else {
                        return builder.notEqual(path, argument);
                    }
                }
                case GREATER_THAN: {
                    return builder.greaterThan(path, (Comparable)argument);
                }
                case GREATER_THAN_OR_EQUAL: {
                    return builder.greaterThanOrEqualTo(path, (Comparable)argument);
                }
                case LESS_THAN: {
                    return builder.lessThan(path, (Comparable)argument);
                }
                case LESS_THAN_OR_EQUAL: {
                    return builder.lessThanOrEqualTo(path, (Comparable)argument);
                }
                case IN:
                    return path.in(args);
                case NOT_IN:
                    return builder.not(path.in(args));
            }
        }

        return null;
    }

    private <V> Path<V> getPath(Root<T> root) {
        String[] nodes = property.split("\\.");
        Path<V> path = root.get(nodes[0]);
        for(int i=1;i<nodes.length;i++){
            path = path.get(nodes[i]);
        }
        return path;
    }

    private List<Object> castArguments(final Path<?> root) {
        final List<Object> args = new ArrayList<>();
        final Class<? extends Object> type = root.getJavaType();

        for (final String argument : arguments) {
            if (type.equals(Integer.class)) {
                args.add(Integer.parseInt(argument));
            } else if (type.equals(Long.class)) {
                args.add(Long.parseLong(argument));
            } else if (type.equals(BigDecimal.class)) {
                args.add(new BigDecimal(argument));
            } else if (type.equals(LocalDate.class)) {
                args.add(LocalDate.parse(argument));
            } else if (type.equals(LocalDateTime.class)) {
                args.add(LocalDateTime.parse(argument));
            } else if (type.equals(CurrencyUnit.class)) {
                args.add(Monetary.getCurrency(argument.toUpperCase()));
            } else if (type.equals(UUID.class)) {
                args.add(UUID.fromString(argument));
            } else if (type.isEnum()) {
                args.add(Enum.valueOf((Class<? extends Enum>)type,argument));
            } else {
                args.add(argument);
            }
        }

        return args;
    }

}
