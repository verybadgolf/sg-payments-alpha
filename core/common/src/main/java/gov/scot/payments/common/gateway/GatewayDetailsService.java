package gov.scot.payments.common.gateway;

import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import gov.scot.payments.model.user.Client;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping(value = "/details")
@Slf4j
public class GatewayDetailsService {

    private final PaymentProcessorRepository repository;
    private final String gatewayName;

    public GatewayDetailsService(PaymentProcessorRepository repository
        , String gatewayName) {
        this.repository = repository;
        this.gatewayName = gatewayName;
    }

    @GetMapping
    @ResponseBody
    public Optional<PaymentProcessor> getDetails(){
        log.info("Fetching Details of {}",gatewayName);
        return repository.findById(gatewayName);
    }

    @PutMapping
    @ResponseBody
    @PreAuthorize("principal.hasWildcardAccess('processor',T(gov.scot.payments.model.user.EntityOp).Write)")
    public PaymentProcessor updateDetails(@RequestBody PaymentProcessor processor, @AuthenticationPrincipal Client currentUser){
        PaymentProcessor toUpdate = processor.toBuilder()
                .name(gatewayName)
                .createdBy(currentUser.getUserName())
                .build();
        log.info("Updating Details of {} to {}",gatewayName,processor);
        return repository.save(toUpdate);
    }

}
