package gov.scot.payments.common.local;

import com.beust.jcommander.Parameter;
import lombok.Data;

import java.util.*;

@Data
public class LocalAwsConfig implements LocalConfig{

    @Parameter(names = "-aws", description = "Specify if you wish to use any AWS resources")
    private boolean aws = false;

    @Parameter(names = "-aws.key"
            , description = "Only applicable if -aws is specified. The AWS Access Key to use when accessing AWS resources")
    private String key;

    @Parameter(names = "-aws.secret"
            , description = "Only applicable if -aws is specified. The AWS Secret Key to use when accessing AWS resources")
    private String secret;

    @Parameter(names = "-aws.region"
            , description = "Only applicable if -aws is specified. The regions in which to access AWS resources")
    private String region = "eu-west-1";

    @Parameter(names = "-aws.stack"
            , description = "Only applicable if -aws is specified. The name of the stack under which the AWS resources are running")
    private String stack = "payments-poc";

    @Override
    public Set<String> getProfiles(){
        if(aws){
            return Set.of("aws");
        }
        else{
            return Collections.emptySet();
        }
    }

    @Override
    public Map<String, Object> getProperties(){
        if(aws){
            Map<String,Object> properties = new HashMap<>();
            properties.put("AWS_ACCESS_KEY_ID",key);
            properties.put("AWS_SECRET_ACCESS_KEY",secret);
            properties.put("AWS_REGION",region);
            properties.put("AWS_STACK_NAME",stack);
            return properties;
        } else{
            return Collections.emptyMap();
        }
    }

    @Override
    public String[] toArgs() {
        List<String> args = new ArrayList<>();
        if(aws){
            args.add("-aws");
            if(key != null){
                args.add("-aws.key");
                args.add(LocalConfig.q(key));
            }
            if(secret != null){
                args.add("-aws.secret");
                args.add(LocalConfig.q(secret));
            }
            if(region != null){
                args.add("-aws.region");
                args.add(LocalConfig.q(region));
            }
            if(stack != null){
                args.add("-aws.stack");
                args.add(LocalConfig.q(stack));
            }
        }
        return args.toArray(new String[0]);
    }

    @Override
    public Set<String> validate() {
        if(!aws){
            return Collections.emptySet();
        }
        Set<String> violations = new HashSet<>();
        if(key == null){
            violations.add("aws.key must be provided");
        }
        if(secret == null){
            violations.add("aws.secret must be provided");
        }
        return violations;
    }


    @Override
    public String toString() {
        return String.join(" ", toArgs());
    }


}
