package gov.scot.payments.common.gateway.spring;

import gov.scot.payments.common.security.WebSecurityConfiguration;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;

import java.util.function.Consumer;

@Configuration
public class GatewayWebSecurityConfiguration extends WebSecurityConfiguration {

    @Bean
    public Consumer<ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry> gatewayEndpointCustomizer() {
        return r -> {
            super.endpointCustomizer().accept(r);
            r.antMatchers(HttpMethod.GET,"/actuator/service-registry**","/details**").permitAll();
        };
    }
}
