package gov.scot.payments.common.processor;

import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface PaymentProcessorRepository extends JpaRepository<PaymentProcessor,String> {
}
