package gov.scot.payments;

import gov.scot.payments.cardpayment.CardPaymentRepository;
import gov.scot.payments.cardpayment.spring.CardPaymentServiceBinding;
import gov.scot.payments.common.kafka.KafkaStreamsOverridingContext;
import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.customer.CustomerRepository;
import gov.scot.payments.customer.spring.CustomerPersistenceBindings;
import gov.scot.payments.customer.spring.CustomerServiceBindings;
import gov.scot.payments.customer.spring.CustomerServiceServiceBindings;
import gov.scot.payments.customer.spring.ServicePersistenceBindings;
import gov.scot.payments.inboundpayment.TemporalInboundPayment;
import gov.scot.payments.inboundpayment.TemporalInboundPaymentRepository;
import gov.scot.payments.inboundpayment.spring.InboundPaymentPersistenceBinding;
import gov.scot.payments.model.cardpayment.CardPayment;
import gov.scot.payments.model.customer.Service;
import gov.scot.payments.model.paymentfile.PaymentFile;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import gov.scot.payments.model.report.Report;
import gov.scot.payments.model.user.Client;
import gov.scot.payments.model.user.User;
import gov.scot.payments.notification.spring.NotificationProcessorInputBinding;
import gov.scot.payments.paymentfile.PaymentFileRepository;
import gov.scot.payments.paymentfile.spring.PaymentFilePersistenceProcessorInputBinding;
import gov.scot.payments.paymentfile.spring.PaymentFileProcessorInputBinding;
import gov.scot.payments.paymentfile.spring.PaymentFileProcessorOutputBinding;
import gov.scot.payments.paymentinstruction.TemporalPayment;
import gov.scot.payments.paymentinstruction.TemporalPaymentRepository;
import gov.scot.payments.paymentinstruction.spring.PaymentCreationBinding;
import gov.scot.payments.paymentinstruction.spring.PaymentPersistenceBinding;
import gov.scot.payments.report.ReportRepository;
import gov.scot.payments.router.spring.PaymentRouterBinding;
import gov.scot.payments.security.UserRepository;
import gov.scot.payments.spring.CoreConfiguration;
import gov.scot.payments.validation.spring.ValidationProcessorBinding;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.jdbc.AmazonRdsDatabaseAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.messaging.MessagingAutoConfiguration;
import org.springframework.cloud.aws.messaging.config.QueueMessageHandlerFactory;
import org.springframework.cloud.aws.messaging.config.annotation.EnableSqs;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.PayloadArgumentResolver;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableAutoConfiguration(exclude = {MessagingAutoConfiguration.class
        , AmazonRdsDatabaseAutoConfiguration.class
        , UserDetailsServiceAutoConfiguration.class
        , HypermediaAutoConfiguration.class})
@Import(CoreConfiguration.class)
@EnableSqs
@EnableBinding({ PaymentFilePersistenceProcessorInputBinding.class
        , PaymentFileProcessorInputBinding.class
        , PaymentFileProcessorOutputBinding.class
        , CustomerPersistenceBindings.class
        , CustomerServiceBindings.class
        , CustomerServiceServiceBindings.class
        , ServicePersistenceBindings.class
        , ValidationProcessorBinding.class
        , NotificationProcessorInputBinding.class
        , PaymentRouterBinding.class
        , PaymentPersistenceBinding.class
        , PaymentCreationBinding.class
        , InboundPaymentPersistenceBinding.class
        , CardPaymentServiceBinding.class})
@EnableJpaRepositories(basePackageClasses = {
        PaymentFileRepository.class
        , CustomerRepository.class
        , TemporalPaymentRepository.class
        , ReportRepository.class
        , UserRepository.class
        , PaymentProcessorRepository.class
        , TemporalInboundPaymentRepository.class
        , CardPaymentRepository.class})
@EntityScan(basePackageClasses = {
        PaymentFile.class
        , Service.class
        , TemporalPayment.class
        , Report.class
        , User.class
        , PaymentProcessor.class
        , TemporalInboundPayment.class
        , CardPayment.class})
@EnableSpringDataWebSupport
@EnableSwagger2
public class Application {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(Application.class);
        application.setApplicationContextClass(KafkaStreamsOverridingContext.class);
        application.run(args);
    }

    @Bean
    public QueueMessageHandlerFactory queueMessageHandlerFactory() {
        QueueMessageHandlerFactory factory = new QueueMessageHandlerFactory();
        MappingJackson2MessageConverter messageConverter = new MappingJackson2MessageConverter();

        //set strict content type match to false
        messageConverter.setStrictContentTypeMatch(false);
        factory.setArgumentResolvers(Collections.singletonList(new PayloadArgumentResolver(messageConverter)));
        return factory;
    }

    //to use an embedded kafka broker for testing:
    //  uncomment the below bean definitions
    //  comment the kafka properties in application-aws.properties
    //  move spring-kafka-test to implementation scope in build.gradle
    //  move org.apache.kafka:kafka_2.11 to implementation scope in gradle

    /*
    @Bean
    public EmbeddedKafkaBroker embeddedKafkaBroker(@Value("${payments.environment}") String env
            , @Value("${kafka.embedded.topics}") String[] topics){
        String[] prefixedTopics = Arrays.stream(topics).map(t -> env+"-"+t).toArray(String[]::new);
        EmbeddedKafkaBroker broker = new EmbeddedKafkaBroker(1, true, 1,prefixedTopics);
        broker.kafkaPorts(0);
        return broker;
    }

    @Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
    public KafkaStreamsConfiguration kStreamsConfigs(@Value("${payments.environment}") String env
            , KafkaProperties properties
            , EmbeddedKafkaBroker embeddedKafkaBroker) {
        String brokerAddresses = embeddedKafkaBroker.getBrokersAsString();
        properties.setBootstrapServers(Collections.singletonList(brokerAddresses));
        Map<String, Object> props = new HashMap<>();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, env);
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, brokerAddresses);
        return new KafkaStreamsConfiguration(props);
    }
    */


}