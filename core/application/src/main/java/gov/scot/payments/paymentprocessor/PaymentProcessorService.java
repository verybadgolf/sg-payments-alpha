package gov.scot.payments.paymentprocessor;

import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import gov.scot.payments.model.user.Client;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.cloud.zookeeper.support.StatusConstants.*;

@RequestMapping(value = "/processors")
@Slf4j
public class PaymentProcessorService {

    private final DiscoveryClient discoveryClient;
    private final String environment;
    private final PaymentProcessorProxy paymentProcessorProxy;
    private final PaymentProcessorRepository repository;

    public PaymentProcessorService(DiscoveryClient discoveryClient
            , PaymentProcessorProxy paymentProcessorProxy
            , String environment
            , PaymentProcessorRepository repository){
        this.discoveryClient = discoveryClient;
        this.paymentProcessorProxy = paymentProcessorProxy;
        this.environment = environment;
        this.repository = repository;
    }

    @GetMapping("/live/{name}")
    @ResponseBody
    public Optional<PaymentProcessor> getLiveProcessor(@PathVariable("name") String name){
        log.info("Fetching live processor details for {}",name);
        return getGateway(name).map(uri -> paymentProcessorProxy.getDetails(name,uri));
    }

    @GetMapping("/live")
    @ResponseBody
    public List<PaymentProcessor> getAllLiveProcessors(){
        log.info("Fetching all live processor details from remote services");
        return getAllGateways().entrySet()
                .stream()
                .map(e -> {
                    try{
                        return paymentProcessorProxy.getDetails(e.getKey(),e.getValue());
                    } catch(Exception ex){
                        log.warn("Error retrieving details from processor: {} at: {}",e.getKey(),e.getValue(),ex);
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @GetMapping
    @ResponseBody
    public List<PaymentProcessor> getAllProcessors(){
        log.info("Fetching all processors directly from DB");
        return repository.findAll();
    }

    @DeleteMapping("/live/{name}")
    @ResponseBody
    @PreAuthorize("principal.hasAccess('processor',#name,T(gov.scot.payments.model.user.EntityOp).Write)")
    public Optional<String> disableLiveProcessor(@PathVariable("name") String name){
        log.info("Disabling {}",name);
        return getGateway(name).map(uri -> paymentProcessorProxy.setGatewayStatus(name, uri,false));
    }

    @GetMapping("/live/{name}/status")
    @ResponseBody
    public Optional<String> getLiveProcessorStatus(@PathVariable("name") String name){
        log.debug("Discovering URI for processor: {}",name);
        List<ServiceInstance> serviceInstances = discoveryClient.getInstances(environment+".gateway."+name);
        if(serviceInstances == null || serviceInstances.isEmpty()){
            log.debug("No services registered for processor: {}",name);
            return Optional.empty();
        }
        log.info("Getting Status of {}",name);
        return serviceInstances.stream()
                .findFirst()
                .map(ServiceInstance::getUri)
                .map(uri -> paymentProcessorProxy.getStatus(name,uri));
    }

    @PostMapping("/live/{name}")
    @ResponseBody
    @PreAuthorize("principal.hasAccess('processor',#name,T(gov.scot.payments.model.user.EntityOp).Write)")
    public Optional<String> enableLiveProcessor(@PathVariable("name") String name){
        log.debug("Discovering URI for processor: {}",name);
        List<ServiceInstance> serviceInstances = discoveryClient.getInstances(environment+".gateway."+name);
        if(serviceInstances == null || serviceInstances.isEmpty()){
            log.debug("No services registered for processor: {}",name);
            return Optional.empty();
        }
        log.info("Enabling {}",name);
        return serviceInstances.stream()
                .findFirst()
                .map(ServiceInstance::getUri)
                .map(uri -> paymentProcessorProxy.setGatewayStatus(name,uri,true));
    }

    @PutMapping("/live")
    @ResponseBody
    @PreAuthorize("principal.hasAccess('processor',#processor.name,T(gov.scot.payments.model.user.EntityOp).Write)")
    public Optional<PaymentProcessor> updateLiveProcessor(@RequestBody PaymentProcessor processor, @ApiIgnore @AuthenticationPrincipal Client currentUser){
        PaymentProcessor toUpdate = processor.toBuilder().createdBy(currentUser.getUserName()).build();
        log.info("Updating Details of {} to {}",processor.getName(),processor);
        return getGateway(processor.getName()).map(uri -> paymentProcessorProxy.updateDetails(uri,toUpdate));
    }

    public Optional<URI> getGateway(String gatewayName){
        log.debug("Discovering URI for processor: {}",gatewayName);
        List<ServiceInstance> serviceInstances = discoveryClient.getInstances(environment+".gateway."+gatewayName);
        if(serviceInstances == null || serviceInstances.isEmpty()){
            log.debug("No services registered for processor: {}",gatewayName);
            return Optional.empty();
        }
        return serviceInstances.stream()
                .filter(i -> i.getMetadata() != null && ( i.getMetadata().get(INSTANCE_STATUS_KEY) == null || i.getMetadata().get(INSTANCE_STATUS_KEY).equals(STATUS_UP) ))
                .findFirst()
                .map(ServiceInstance::getUri);
    }

    private Map<String,URI> getAllGateways(){
        log.debug("Discovering all processors");
        String prefix = environment + ".gateway.";
        Map<String, URI> processors = discoveryClient.getServices()
                .stream()
                .filter(s -> s.startsWith(prefix))
                .map(s -> s.substring(prefix.length()))
                .map(s -> Pair.of(s, getGateway(s)))
                .filter(s -> s.getRight().isPresent())
                .collect(Collectors.toMap(Pair::getLeft, v -> v.getRight().get()));
        log.debug("Discovered processors: {}",processors);
        return processors;
    }

}
