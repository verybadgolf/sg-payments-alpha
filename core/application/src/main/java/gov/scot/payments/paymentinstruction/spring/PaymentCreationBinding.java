package gov.scot.payments.paymentinstruction.spring;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface PaymentCreationBinding {

    @Output("manual-payment-created-events")
    MessageChannel paymentCreatedEvents();
}
