package gov.scot.payments.customer.spring;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface CustomerServiceBindings {

    @Output("customer-created-events")
    MessageChannel customerCreatedEvents();

    @Output("customer-updated-events")
    MessageChannel customerUpdatedEvents();

    @Output("customer-deleted-events")
    MessageChannel customerDeletedEvents();


}
