package gov.scot.payments.customer;


import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.model.customer.Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.KafkaNull;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
public class ServicePersistenceProcessor {

    private final ServiceRepository repository;
    private final ObjectMapper objectMapper;

    public ServicePersistenceProcessor(ServiceRepository repository, ObjectMapper mapper){
        this.repository = repository;
        this.objectMapper = mapper;
    }

    //handler for upload queue
    @StreamListener("persist-service-updated-events")
    @Transactional
    public void onServiceUpdates(@Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) byte[] key,
                                 @Payload(required = false) Object service){
        if(service != KafkaNull.INSTANCE){
            try {
                log.info("Saving service {}",service);
                repository.save(objectMapper.readValue((String)service, Service.class));
            } catch (Exception e) {
                log.info("Error saving service {}",service,e);
                throw new RuntimeException(e);
            }
        } else {
            String id = new String(key);
            log.info("Deleting service {}",id);
            repository.deleteById(id);
        }
    }





}
