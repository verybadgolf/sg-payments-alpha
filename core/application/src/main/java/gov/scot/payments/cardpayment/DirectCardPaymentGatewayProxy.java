package gov.scot.payments.cardpayment;

import gov.scot.payments.common.gateway.inbound.cardpayment.CardPaymentGateway;
import gov.scot.payments.model.cardpayment.*;

import java.util.Map;
import java.util.Optional;

public class DirectCardPaymentGatewayProxy implements CardPaymentGatewayProxy {

    private final Map<String, CardPaymentGateway> delegates;

    public DirectCardPaymentGatewayProxy(Map<String, CardPaymentGateway> delegates) {
        this.delegates = delegates;
    }

    @Override
    public CardPaymentGatewayAuthResponse authorize(String name, CardPaymentGatewayAuthRequest gatewayRequest) {
        CardPaymentGateway gateway = lookupGateway(name);
        return gateway.authorize(gatewayRequest);
    }

    @Override
    public CardPaymentGatewaySubmitResponse submit(String name, CardPaymentGatewaySubmitRequest gatewayRequest) {
        CardPaymentGateway gateway = lookupGateway(name);
        return gateway.submit(gatewayRequest);
    }


    @Override
    public CardPaymentGatewayCancelResponse cancel(String name, CardPaymentGatewayCancelRequest gatewayRequest) {
        CardPaymentGateway gateway = lookupGateway(name);
        return gateway.cancel(gatewayRequest);
    }

    private CardPaymentGateway lookupGateway(String name) {
        Optional<CardPaymentGateway> gatewayOpt = Optional.ofNullable(delegates.get(name));
        return gatewayOpt.orElseThrow(() -> new IllegalArgumentException("No gateway named: " + name));
    }
}
