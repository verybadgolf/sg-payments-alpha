package gov.scot.payments.customer;

import gov.scot.payments.model.customer.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, String> {

    @Query("select c from Customer c where c.id in ?#{principal.getEntityIdsWithAccess('customer',T(gov.scot.payments.model.user.EntityOp).Read)}")
    List<Customer> findAllWithAcl();

}
