package gov.scot.payments.paymentfile;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.event.S3EventNotification;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.sns.message.SnsMessageManager;
import com.amazonaws.services.sns.message.SnsNotification;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.customer.Service;
import gov.scot.payments.model.paymentfile.PaymentFile;
import gov.scot.payments.model.paymentfile.PaymentFileStatus;
import gov.scot.payments.model.paymentfile.event.FileTranslationFailedEvent;
import gov.scot.payments.model.paymentfile.event.FileTranslationSuccessEvent;
import gov.scot.payments.model.paymentfile.event.FileUploadedEvent;
import gov.scot.payments.model.paymentinstruction.event.PaymentInstructionCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Predicate;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.support.GenericMessage;
import reactor.util.function.Tuple3;
import reactor.util.function.Tuples;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
public class PaymentFileProcessor {

    private final AmazonS3 s3Client;
    private final Function<String, Optional<PaymentFileTranslator>> fileTranslatorFactory;
    private final Function<String, Optional<Service>> serviceLookupByFolder;
    private final Function<String, Optional<Service>> serviceLookupById;
    private final ObjectMapper objectMapper;
    private final MessageChannel fileUploadedEvents;

    public PaymentFileProcessor(AmazonS3 s3Client
            , Function<String, Optional<PaymentFileTranslator>> fileTranslatorFactory
            , Function<String, Optional<Service>> serviceLookupByFolder
            , Function<String, Optional<Service>> serviceLookupById
            , MessageChannel fileUploadedEvents){
        this.fileTranslatorFactory = fileTranslatorFactory;
        this.serviceLookupByFolder = serviceLookupByFolder;
        this.serviceLookupById = serviceLookupById;
        this.s3Client = s3Client;
        this.fileUploadedEvents = fileUploadedEvents;
        this.objectMapper = new ObjectMapper();
    }

    @SqsListener("${payments.processors.file.event.queue}")
    public void onSnsNotification(String snsNotificationStr){
        S3EventNotification s3Event = null;
        try (InputStream inputStream = new ByteArrayInputStream(snsNotificationStr.getBytes())) {
            SnsNotification snsNotification = (SnsNotification) new SnsMessageManager().parseMessage(inputStream);
        s3Event = S3EventNotification.parseJson(snsNotification.getMessage());
        } catch (IOException e) {
            log.error("Failed to retrieve S3 event from message : " + snsNotificationStr);
        }
        onS3Event(s3Event);
    }

    public void onS3Event(S3EventNotification s3Event){
        if(s3Event.getRecords() == null || s3Event.getRecords().isEmpty()){
            log.error("Skipping empty S3 event: ");
            return;
        }
        try{
            FileUploadedEvent event = convertS3Event(s3Event);
            if(event != null){
                fileUploadedEvents.send(new GenericMessage<>(event));
            }
        } catch (Exception e){
            log.error("Could not parse s3 event: ",e);
        }
    }

    FileUploadedEvent convertS3Event(S3EventNotification s3Event) throws JsonProcessingException {
        log.info("Handling S3 event: {}",objectMapper.writeValueAsString(s3Event));
        for(S3EventNotification.S3EventNotificationRecord record : s3Event.getRecords()){
            Pair<String,String> folderAndName = parseFolder(record);
            if(folderAndName == null){
                log.warn("Skipping event as file is not in a sub directory: {}",record );
                continue;
            }
            Optional<Service> service = serviceLookupByFolder.apply(folderAndName.getLeft());
            if(service.isEmpty()){
                log.warn("Skipping file event as no registered service for that location: {}",folderAndName.getLeft());
                continue;
            }
            switch (record.getEventName()){
                case "ObjectCreated:Post":
                case "ObjectCreated:Put":
                case "ObjectCreated:CompleteMultipartUpload":
                    PaymentFile file = PaymentFile.builder()
                            .name(folderAndName.getRight())
                            .service(service.get().getId())
                            .createdAt(LocalDateTime.ofInstant(Instant.ofEpochMilli(record.getEventTime().toInstant().getMillis()), ZoneId.systemDefault()))
                            .type(service.get().getFileFormat())
                            .url(buildS3Url(record))
                            .createdBy(record.getUserIdentity().getPrincipalId())
                            .build();
                    FileUploadedEvent event = new FileUploadedEvent(file);
                    log.info("Handled Post/Put event from s3 and generated file-upload-event: {}", event);
                    return event;
                case "ObjectRemoved:Delete":
                case "ObjectRemoved:DeleteMarkerCreated":
                    //TODO: implement delete event handling
                    break;
                default:
                    log.info("Ignoring S3 event.record: {}",objectMapper.writeValueAsString(record));
            }
        }
        return null;
    }

    @StreamListener("process-file-uploaded-events")
    @SendTo({"file-translation-failed-events","file-translation-success-events","payment-created-events"})
    public KStream<?,? extends Event>[] handleFileUploadEvents(KStream<?,FileUploadedEvent> fileUploadEvents){
        Predicate<Object,Event> translationFailed = (k,v) -> FileTranslationFailedEvent.class.isAssignableFrom(v.getClass());
        Predicate<Object,Event> translationSucceeded = (k,v) -> FileTranslationSuccessEvent.class.isAssignableFrom(v.getClass());
        Predicate<Object,Event> paymentCreated = (k,v) -> PaymentInstructionCreatedEvent.class.isAssignableFrom(v.getClass());

        return fileUploadEvents
                .mapValues(FileUploadedEvent::getPayload)
                .mapValues( v -> Tuples.of(v,fileTranslatorFactory.apply(v.getType()),serviceLookupById.apply(v.getService())))
                .flatMapValues(v -> createEvents(v.getT1(),v.getT2().orElse(null),v.getT3().orElse(null)))
                .branch(translationFailed,translationSucceeded,paymentCreated);
    }

    List<? extends Event> createEvents(PaymentFile paymentFile, PaymentFileTranslator translator, Service service) {
        log.info("Handling file-upload-event for: {}",paymentFile);
        if(translator == null){
            log.info("No translator available for file type {}, ignoring file: {}",paymentFile.getType(),paymentFile.getUrl());
            return Collections.singletonList(new FileTranslationFailedEvent(paymentFile.toBuilder()
                    .id(UUID.randomUUID())
                    .timestamp(LocalDateTime.now())
                    .status(PaymentFileStatus.Failed)
                    .statusMessage("No translator found for this file format")
                    .build()));
        } else if(service == null){
            log.info("No service available for file, ignoring: {}",paymentFile.getUrl());
            return Collections.singletonList(new FileTranslationFailedEvent(paymentFile.toBuilder()
                    .id(UUID.randomUUID())
                    .timestamp(LocalDateTime.now())
                    .status(PaymentFileStatus.Failed)
                    .statusMessage("No service found for this file")
                    .build()));
        } else {
            try(InputStream stream = s3Client.getObject(createGetRequest(paymentFile.getUrl())).getObjectContent()) {
                List<Event> instructionEvents = translator.generatePaymentInstructionsFromFile(paymentFile,stream)
                        .stream()
                        .map(pi -> pi.toBuilder()
                                .createdBy(paymentFile.getCreatedBy())
                                .kvRef(service.getMetadata())
                                .build())
                        .map(PaymentInstructionCreatedEvent::new)
                        .collect(Collectors.toList());
                log.info("Successfully parsed file: {} in to {} payment instructions",paymentFile.getUrl(),instructionEvents.size());
                instructionEvents.add( new FileTranslationSuccessEvent(paymentFile.toBuilder()
                        .id(UUID.randomUUID())
                        .timestamp(LocalDateTime.now())
                        .status(PaymentFileStatus.Translated)
                        .build()));
                return instructionEvents;
            } catch (Exception e) {
                log.warn("Error parsing file: {}",paymentFile.getUrl(),e);
                return Collections.singletonList(new FileTranslationFailedEvent(paymentFile.toBuilder()
                        .id(UUID.randomUUID())
                        .timestamp(LocalDateTime.now())
                        .status(PaymentFileStatus.Failed)
                        .statusMessage(e.getMessage())
                        .build()));
            }
        }
    }

    private URI buildS3Url(S3EventNotification.S3EventNotificationRecord record){
        String path = Path.of(record.getS3().getBucket().getName(),record.getS3().getObject().getKey()).toString().replace(File.separatorChar,'/');
        return URI.create("s3://"+path+"#"+record.getS3().getObject().getVersionId());
    }

    private Pair<String,String> parseFolder(S3EventNotification.S3EventNotificationRecord record) {
        String key = record.getS3().getObject().getUrlDecodedKey();
        Path path = Paths.get(key);
        int nameCount = path.getNameCount();
        if(nameCount < 2){
            return null;
        }
        String file = path.subpath(1, nameCount).toString();
        String versionId = record.getS3().getObject().getVersionId();
        if(StringUtils.isNotEmpty(versionId)){
            file = file+":"+ versionId;
        }
        return Pair.of(path.subpath(0,1).toString(), file);
    }

    private GetObjectRequest createGetRequest(URI url) {
        String bucket = url.getHost();
        String key = URLDecoder.decode(url.getPath().substring(1), StandardCharsets.UTF_8);
        String versionId = url.getFragment();
        return new GetObjectRequest(bucket,key,versionId);
    }

}
