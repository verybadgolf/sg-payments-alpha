package gov.scot.payments.cardpayment;

import gov.scot.payments.model.cardpayment.CardPayment;
import gov.scot.payments.model.cardpayment.CardPaymentGatewayAuthRequest;
import lombok.*;

import java.time.LocalDate;
import java.time.YearMonth;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter
@Getter
@ToString
public class AuthorizeCardPaymentRequest {

    @ToString.Exclude private String name;
    @ToString.Exclude private String cardNumber;
    @ToString.Exclude private String cvv;
    @ToString.Exclude private Integer month;
    @ToString.Exclude private Integer year;

    public CardPaymentGatewayAuthRequest toGatewayRequest(CardPayment payment) {

        if(year < 2000) year += 2000;

        LocalDate expiry = YearMonth.of(year, month).atEndOfMonth();

        return CardPaymentGatewayAuthRequest.builder()
                .amount(payment.getAmount())
                .cardNumber(cardNumber == null ? cardNumber : cardNumber.replaceAll("\\s",""))
                .cvv(cvv)
                .expiry(expiry)
                .paymentRef(payment.getId())
                .name(name)
                .build();
    }
}
