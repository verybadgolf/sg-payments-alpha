package gov.scot.payments.inboundpayment;

import gov.scot.payments.model.inboundpayment.InboundPayment;
import gov.scot.payments.model.inboundpayment.event.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionOperations;

import java.util.Optional;

@Slf4j
public class InboundPaymentPersistenceProcessor {

    private final TemporalInboundPaymentRepository temporalInboundPaymentRepository;
    private final NonTemporalInboundPaymentRepository nonTemporalInboundPaymentRepository;
    private final TransactionOperations transactionTemplate;

    public InboundPaymentPersistenceProcessor(TemporalInboundPaymentRepository temporalInboundPaymentRepository
            , NonTemporalInboundPaymentRepository nonTemporalInboundPaymentRepository
            , TransactionOperations transactionTemplate){
        this.temporalInboundPaymentRepository = temporalInboundPaymentRepository;
        this.nonTemporalInboundPaymentRepository = nonTemporalInboundPaymentRepository;
        this.transactionTemplate = transactionTemplate;
    }

    @StreamListener
    public void handleInboundPaymentEvents(
            @Input("persist-inbound-payment-received-events") KStream<?, InboundPaymentReceivedEvent> inboundPaymentReceivedEvents
            ,@Input("persist-inbound-payment-attempt-failed-events") KStream<?, InboundPaymentAttemptFailedEvent> inboundPaymentAttemptFailedEvents
            ,@Input("persist-inbound-payment-cleared-events") KStream<?, InboundPaymentClearedEvent> inboundPaymentClearedEvents
            ,@Input("persist-inbound-payment-failed-to-clear-events") KStream<?, InboundPaymentFailedToClearEvent> inboundPaymentFailedToClearEvents
            ,@Input("persist-inbound-payment-settled-events") KStream<?, InboundPaymentSettledEvent> inboundPaymentSettledEvents) {

        inboundPaymentReceivedEvents.foreach( (k,v) -> persist(v.getPayload()));
        inboundPaymentAttemptFailedEvents.foreach( (k,v) -> persist(v.getPayload()));
        inboundPaymentClearedEvents.foreach( (k,v) -> persist(v.getPayload()));
        inboundPaymentFailedToClearEvents.foreach( (k,v) -> persist(v.getPayload()));
        inboundPaymentSettledEvents.foreach( (k,v) -> persist(v.getPayload()));
    }

    void persist(InboundPayment inboundPayment) {
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                persistTemporal(inboundPayment);
                persistNonTemporal(inboundPayment);
                status.flush();
            }
        });
    }

    void persistNonTemporal(InboundPayment inboundPayment) {
        FlattenedInboundPayment toPersist = FlattenedInboundPayment.from(inboundPayment);
        Optional<FlattenedInboundPayment> existingNonTemporalPayment = nonTemporalInboundPaymentRepository.findById(inboundPayment.getId());
        if(existingNonTemporalPayment.isPresent()){
            FlattenedInboundPayment existingVersion = existingNonTemporalPayment.get();
            if(existingVersion.isAfter(toPersist)){
                log.info("Existing non-temporal version {} of {} is after received version {}, skipping persistence",existingVersion.getMaxTimestamp(),existingVersion,toPersist.getMaxTimestamp());
            } else{
                existingVersion.merge(toPersist);
                log.info("Persisting updated non-temporal version of {}", existingVersion);
                nonTemporalInboundPaymentRepository.save(existingVersion);
            }
        } else{
            log.info("Persisting initial non-temporal version of {}", toPersist);
            nonTemporalInboundPaymentRepository.save(toPersist);
        }
    }

    private void persistTemporal(InboundPayment inboundPayment) {
        TemporalInboundPayment temporalInboundPayment = TemporalInboundPayment.from(inboundPayment);
        log.info("Persisting temporal version of {}", temporalInboundPayment);
        temporalInboundPaymentRepository.save(temporalInboundPayment);
    }

}
