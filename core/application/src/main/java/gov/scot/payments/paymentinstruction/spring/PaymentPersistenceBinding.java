package gov.scot.payments.paymentinstruction.spring;

import gov.scot.payments.model.paymentfile.event.PaymentRoutingFailedEvent;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingSuccessEvent;
import gov.scot.payments.model.paymentfile.event.PaymentValidationFailureEvent;
import gov.scot.payments.model.paymentfile.event.PaymentValidationSuccessEvent;
import gov.scot.payments.model.paymentinstruction.event.*;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;

public interface PaymentPersistenceBinding {

    @Input("persist-payment-created-events")
    KStream<?, PaymentInstructionCreatedEvent> paymentCreatedEvents();

    @Input("persist-payment-validation-success-events")
    KStream<?, PaymentValidationSuccessEvent> successfulValidationEvents();

    @Input("persist-payment-validation-failed-events")
    KStream<?, PaymentValidationFailureEvent> failedValidationEvents();

    @Input("persist-payment-routing-success-events")
    KStream<?, PaymentRoutingSuccessEvent> successfulRoutingEvents();

    @Input("persist-payment-routing-failed-events")
    KStream<?, PaymentRoutingFailedEvent> failedRoutingEvents();

    @Input("persist-payment-submission-success-events")
    KStream<?, PaymentSubmitSuccessEvent> successfulSubmissionEvents();

    @Input("persist-payment-submission-failed-events")
    KStream<?, PaymentSubmitFailedEvent> failedSubmissionEvents();

    @Input("persist-payment-accepted-events")
    KStream<?, PaymentAcceptedEvent> acceptedEvents();

    @Input("persist-payment-rejection-events")
    KStream<?, PaymentRejectedEvent> rejectedEvents();

    @Input("persist-payment-cleared-events")
    KStream<?, PaymentClearedEvent> clearedEvents();

    @Input("persist-payment-failedToClear-events")
    KStream<?, PaymentFailedToClearEvent> failedToClearEvents();

    @Input("persist-payment-settled-events")
    KStream<?, PaymentSettledEvent> settledEvents();

}
