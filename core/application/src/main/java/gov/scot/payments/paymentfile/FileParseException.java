package gov.scot.payments.paymentfile;

public class FileParseException extends Exception {

    public FileParseException(){
        super();
    }

    public FileParseException(String message) {
        super(message);
    }

    public FileParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileParseException(Throwable cause) {
        super(cause);
    }
}
