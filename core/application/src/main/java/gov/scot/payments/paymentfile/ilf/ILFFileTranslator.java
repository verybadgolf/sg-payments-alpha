package gov.scot.payments.paymentfile.ilf;

import gov.scot.payments.model.paymentfile.PaymentFile;
import gov.scot.payments.paymentfile.FileParseException;
import gov.scot.payments.paymentfile.FileTranslateException;
import gov.scot.payments.paymentfile.PaymentFileTranslator;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ILFFileTranslator implements PaymentFileTranslator {

    private ILFFileParser fileParser;

    public ILFFileTranslator(ILFFileParser fileParser){
        this.fileParser = fileParser;
    }

    @Override
    public List<PaymentInstruction> generatePaymentInstructionsFromFile(PaymentFile paymentFile, InputStream paymentFileStream) throws FileParseException, FileTranslateException {

        try {
            List<ILFBACSPayment> payments = fileParser.generatePaymentList(paymentFileStream);
            return payments.stream().map(p -> p.toPaymentInstruction(paymentFile)).collect(Collectors.toList());
        } catch (IOException e){
            throw new FileTranslateException("Error encountered reading ILF CSV input stream",e);
        }
    }

    @Override
    public List<String> getSupportedFileTypes() {
        return Collections.singletonList("ILF");
    }

}
