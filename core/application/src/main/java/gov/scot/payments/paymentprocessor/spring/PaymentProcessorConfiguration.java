package gov.scot.payments.paymentprocessor.spring;

import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.paymentprocessor.DirectPaymentProcessorProxy;
import gov.scot.payments.paymentprocessor.PaymentProcessorProxy;
import gov.scot.payments.paymentprocessor.PaymentProcessorService;
import gov.scot.payments.paymentprocessor.RemotePaymentProcessorProxy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.serviceregistry.ServiceRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.implicit.ImplicitResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import java.util.function.Function;
import java.util.function.Supplier;

@Configuration
public class PaymentProcessorConfiguration {

    @Bean
    @Profile("!embedded-gateway & !direct-processor-proxy")
    public PaymentProcessorProxy remotePaymentProcessorProxy(@Value("${payments.processor.max-retries:3}") int maxRetries
            , @Value("${payments.processor.retry-wait:1000}") int retryWait
            , Supplier<RestOperations> serviceToServiceRestOperations){
        RetryTemplate retryTemplate = new RetryTemplate();
        FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
        backOffPolicy.setBackOffPeriod(retryWait);
        retryTemplate.setBackOffPolicy(backOffPolicy);
        retryTemplate.setRetryPolicy(new SimpleRetryPolicy(maxRetries));
        return new RemotePaymentProcessorProxy( serviceToServiceRestOperations, retryTemplate);
    }


    @Bean
    @Profile("!embedded-gateway & direct-processor-proxy")
    public PaymentProcessorProxy directPaymentProcessorProxy(PaymentProcessorRepository repository
            , ServiceRegistry serviceDiscovery
            , DiscoveryClient discoveryClient
            , @Value("${payments.environment}") String environment){
        return new DirectPaymentProcessorProxy(repository, serviceDiscovery, discoveryClient, environment);
    }
}
