package gov.scot.payments.validation;

import gov.scot.payments.model.customer.PaymentValidationRule;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.Validation;
import gov.scot.payments.model.paymentinstruction.ValidationStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.expression.ExpressionParser;

import java.util.Comparator;
import java.util.List;

@Slf4j
public class PaymentInstructionValidator {

    private final ExpressionParser expressionParser;

    public PaymentInstructionValidator(ExpressionParser parser){
        this.expressionParser = parser;
    }

    public Validation validate(PaymentInstruction paymentInstruction, List<PaymentValidationRule> validationRules){

            return validationRules.stream()
                    .sorted(Comparator.comparingInt(PaymentValidationRule::getPrecedence))
                    .map(vr -> applyValidationRule(paymentInstruction,vr))
                    .reduce(Validation::merge)
                    .orElseGet(Validation::success);
    }

    private Validation applyValidationRule(PaymentInstruction paymentInstruction, PaymentValidationRule rule) {
        Validation validation;
        try {
            var expression = expressionParser.parseExpression(rule.getRule());
            if (expression.getValue(paymentInstruction, Boolean.class)) {
                validation = Validation.success();
            } else{
                var invalidMessageStr = rule.getMessageTemplate();
                validation = Validation.builder()
                        .status(ValidationStatus.Invalid)
                        .message(invalidMessageStr)
                        .build();
            }
        } catch (Exception e){
            validation = Validation.builder()
                    .status(ValidationStatus.Invalid)
                    .message(String.format("An unexpected error occurred validating rule '%s', Details: %s",rule.getRule(),e.getMessage()))
                    .build();
        }
        return validation;
    }

}
