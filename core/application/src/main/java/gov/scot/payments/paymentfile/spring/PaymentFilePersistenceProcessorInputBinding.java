package gov.scot.payments.paymentfile.spring;

import gov.scot.payments.model.paymentfile.event.FileTranslationFailedEvent;
import gov.scot.payments.model.paymentfile.event.FileTranslationSuccessEvent;
import gov.scot.payments.model.paymentfile.event.FileUploadedEvent;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;

public interface PaymentFilePersistenceProcessorInputBinding {

    @Input("persist-file-uploaded-events")
    KStream<?, FileUploadedEvent> fileUploadEvents();

    @Input("persist-file-translation-success-events")
    KStream<?, FileTranslationSuccessEvent> persistFileTranslationSuccessEvents();

    @Input("persist-file-translation-failed-events")
    KStream<?, FileTranslationFailedEvent> persistFileTranslationFailedEvents();

}
