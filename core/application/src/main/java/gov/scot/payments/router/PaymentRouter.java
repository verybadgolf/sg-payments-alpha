package gov.scot.payments.router;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingFailedEvent;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingSuccessEvent;
import gov.scot.payments.model.paymentinstruction.*;
import gov.scot.payments.model.paymentfile.event.PaymentValidationSuccessEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Predicate;
import org.javamoney.moneta.Money;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.kafka.core.KafkaOperations;
import org.springframework.messaging.handler.annotation.SendTo;
import reactor.util.function.Tuple3;
import reactor.util.function.Tuples;

import javax.money.CurrencyUnit;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static gov.scot.payments.model.paymentinstruction.PaymentInstruction.GBP_CURRENCY;

@Slf4j
public class PaymentRouter {
    private final Predicate<Object, Event> isSuccess = (k, v) -> PaymentRoutingSuccessEvent.class.isAssignableFrom(v.getClass());
    public static final Money NULL_PLACEHOLDER = Money.of(new BigDecimal("-1"),GBP_CURRENCY);

    private final KafkaOperations<byte[],byte[]> kafkaTemplate;
    private final String environment;
    private final ExpressionParser expressionParser;
    private final ObjectMapper objectMapper;
    private final Supplier<Collection<PaymentProcessor>> paymentProcessorRegistry;

    public PaymentRouter(Supplier<Collection<PaymentProcessor>> paymentProcessorRegistry
            , KafkaOperations<byte[],byte[]> kafkaTemplate
            , ObjectMapper objectMapper
            , String environment
            , ExpressionParser expressionParser){
        this.paymentProcessorRegistry = paymentProcessorRegistry;
        this.kafkaTemplate = kafkaTemplate;
        this.environment = environment;
        this.objectMapper = objectMapper;
        this.expressionParser = expressionParser;
    }

    @StreamListener
    @SendTo("payment-route-failed-events")
    public KStream<?,? extends Event> routePaymentEvents(@Input("route-payment-validation-success-events") KStream<?, PaymentValidationSuccessEvent> eventStream){
        return eventStream.mapValues(v -> mapEvent(v.getPayload()))
                .peek((k,v) -> sendSuccessEvent(v))
                .filterNot(isSuccess);
    }

    public List<Tuple3<String, PaymentProcessorChannel, Money>> getValidInboundChannels(PaymentChannel channel, Money amount) {
        return paymentProcessorRegistry.get().stream()
                .flatMap(p -> p.getChannels().stream().filter(c -> c.getDirection() == PaymentChannelDirection.Inbound).map(c -> Pair.of(p.getName(),c)))
                //only consider channels of the specified type
                .filter(c -> c.getRight().getPaymentChannel() == channel)
                //only consider channels tha can meet the limit and currency
                .filter(c -> c.getRight().getPaymentChannel().isAbleToPay(amount,amount.getCurrency()))
                //calculate the cost of each channel
                .map(c -> Tuples.of(c.getLeft(),c.getRight(), calculateInboundChannelCost(amount,c.getRight())))
                .filter(t -> t.getT3() != NULL_PLACEHOLDER)
                //order by cost
                .sorted(Comparator.comparing(Tuple3::getT3))
                .collect(Collectors.toList());
    }


    void sendSuccessEvent(Event<PaymentInstruction> event) {
        if(isSuccess.test(null,event)){
            try{
                String queue = event.getPayload().getRouting().getQueue();
                log.info("Sending routed message for payment {} to topic {}",event.getPayload().getId(),queue);
                kafkaTemplate.send(new ProducerRecord<>(queue,objectMapper.writeValueAsBytes(event)));
            } catch(Exception e){
                throw new RuntimeException(e);
            }
        }
    }

    Event<PaymentInstruction> mapEvent(PaymentInstruction instruction){
        LocalDateTime now = LocalDateTime.now();
        List<Tuple3<String, PaymentProcessorChannel, Money>> channels = getValidOutboundChannels(instruction, now);

        Event<PaymentInstruction> toReturn;
        if(channels.isEmpty()){
            log.warn("Can not route payment {}",instruction.getId());
            toReturn = new PaymentRoutingFailedEvent(instruction.toBuilder()
                    .timestamp(now)
                    .status(PaymentInstructionStatus.FailedToRoute)
                    .routing(Routing.builder()
                            .routedAt(now)
                            .status(RoutingStatus.Failure)
                            .statusMessage(String.format("No suitable processors for channel type %s available to make payment of %s in %s before latest target date %s",instruction.getTargetChannelType(),instruction.getTargetAmount(),instruction.getTargetSettlementCurrency(),instruction.getLatestTargetSettlementDate()))
                            .build())
                    .build());
        } else{
            Tuple3<String, PaymentProcessorChannel,Money> processor = channels.get(0);
            String processorName = processor.getT1();
            PaymentChannel paymentChannel = processor.getT2().getPaymentChannel();
            Money expectedCost = processor.getT3();
            log.info("Routing payment {} to processor: {} using channel {}. Expected cost is {}",instruction.getId(),processorName, paymentChannel, expectedCost);
            toReturn = new PaymentRoutingSuccessEvent(instruction.toBuilder()
                    .timestamp(now)
                    .status(PaymentInstructionStatus.Routed)
                    .routing(Routing.builder()
                            .routedAt(now)
                            .processor(processorName)
                            .channel(paymentChannel)
                            .queue(generateTopicName(processorName))
                            .expectedCost(expectedCost)
                            .build())
                    .build());
        }
        return toReturn;
    }

    private List<Tuple3<String, PaymentProcessorChannel, Money>> getValidOutboundChannels(PaymentInstruction instruction, LocalDateTime now) {
        LocalDate latestPayDate = instruction.getLatestTargetSettlementDate();
        LocalDate earliestPayDate = instruction.getEarliestTargetSettlementDate() == null ? instruction.getLatestTargetSettlementDate() : instruction.getEarliestTargetSettlementDate();
        Money amount = instruction.getTargetAmount();
        CurrencyUnit targetCurrency = instruction.getTargetSettlementCurrency();
        PaymentChannelType targetType = instruction.getTargetChannelType();
        //this could be more efficient
        return paymentProcessorRegistry.get().stream()
                    .flatMap(p -> p.getChannels().stream().filter(c -> c.getDirection() == PaymentChannelDirection.Outbound).map(c -> Pair.of(p.getName(),c)))
                    //only consider channels of the specified type
                    .filter(c -> c.getRight().getPaymentChannel().getType() == targetType)
                    //only consider channels tha can meet the limit and currency
                    .filter(c -> c.getRight().getPaymentChannel().isAbleToPay(amount,targetCurrency))
                    //only consider channels that can meet the target settlement date
                    .filter(c -> !latestPayDate.isBefore(calculateEarliestSettlementDate(c.getRight().getPaymentChannel(),now)))
                    //only consider channels that can meet the target settlement date
                    .filter(c -> {
                        LocalDate latestPossibleSettlementDate = calculateLatestSettlementDate(c.getRight().getPaymentChannel(), now);
                        return latestPossibleSettlementDate == null || !earliestPayDate.isAfter(latestPossibleSettlementDate);
                    })
                    //calculate the cost of each channel
                    .map(c -> Tuples.of(c.getLeft(),c.getRight(), calculateOutboundChannelCost(instruction,c.getRight())))
                    .filter(t -> t.getT3() != NULL_PLACEHOLDER)
                    //order by cost
                    .sorted(Comparator.comparing(Tuple3::getT3))
                    .collect(Collectors.toList());
    }

    Money calculateOutboundChannelCost(PaymentInstruction instruction, PaymentProcessorChannel channel) {
        String expression = channel.getCostExpression();
        try{
            StandardEvaluationContext context = new StandardEvaluationContext(instruction);
            return expressionParser.parseExpression(expression).getValue(context,Money.class);
        } catch(Exception e){
            log.warn("Error when calculating cost for payment {} with channel: {}",instruction,channel,e);
            return NULL_PLACEHOLDER;
        }
    }

    Money calculateInboundChannelCost(Money amount, PaymentProcessorChannel channel) {
        String expression = channel.getCostExpression();
        try{
            StandardEvaluationContext context = new StandardEvaluationContext(amount);
            return expressionParser.parseExpression(expression).getValue(context,Money.class);
        } catch(Exception e){
            log.warn("Error when calculating cost for payment of {} with channel: {}",amount,channel,e);
            return NULL_PLACEHOLDER;
        }
    }

    LocalDate calculateLatestSettlementDate(PaymentChannel paymentChannel, LocalDateTime now) {
        return paymentChannel.getLatestSettlementDate(now);
    }

    LocalDate calculateEarliestSettlementDate(PaymentChannel paymentChannel, LocalDateTime now) {
        return paymentChannel.getEarliestSettlementDate(now);
    }

    private String generateTopicName(String queue) {
        return environment+"-"+queue+"PaymentEvents";
    }
}
