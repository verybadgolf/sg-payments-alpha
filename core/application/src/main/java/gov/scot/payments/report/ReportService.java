package gov.scot.payments.report;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.quicksight.AmazonQuickSight;
import com.amazonaws.services.quicksight.model.*;
import gov.scot.payments.model.report.Report;
import gov.scot.payments.model.user.Client;
import gov.scot.payments.model.user.User;
import gov.scot.payments.security.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Optional;

@Slf4j
@RequestMapping(value = "/reports")
public class ReportService {

    private final ReportRepository repository;
    private final AmazonQuickSightClientFactory amazonQuickSightClientFactory;
    private final String awsAccountId;
    private final String quickSightRoleArn;
    private final String quickSightRoleName;
    private final UserRepository userRepository;

    public ReportService(ReportRepository repository
            , AmazonQuickSightClientFactory amazonQuickSightClientFactory
            , UserRepository userRepository
            , String awsAccountId
            , String quickSightRoleArn
            , String quickSightRoleName){
        this.repository = repository;
        this.amazonQuickSightClientFactory = amazonQuickSightClientFactory;
        this.awsAccountId = awsAccountId;
        this.quickSightRoleArn = quickSightRoleArn;
        this.quickSightRoleName = quickSightRoleName;
        this.userRepository = userRepository;
    }

    @GetMapping
    @ResponseBody
    public List<Report> getAllReports(){
        return repository.findAll();
    }

    @PostMapping
    @ResponseBody
    @PreAuthorize("principal.hasWildcardAccess('report',T(gov.scot.payments.model.user.EntityOp).Write)")
    public Report registerReport(@RequestParam("id") String id,@RequestParam("name") String name, @RequestParam("dashboardId") String dashboardId, @ApiIgnore @AuthenticationPrincipal Client currentUser){
        Report report = Report.builder()
                .id(id)
                .dashboardId(dashboardId)
                .name(name)
                .createdBy(currentUser.getUserName())
                .build();
        return repository.save(report);
    }

    @GetMapping("/{id}/embedUrl")
    @ResponseBody
    public Optional<String> getEmbedUrl(@PathVariable("id") String id, @ApiIgnore @AuthenticationPrincipal User currentUser){
        Optional<Report> report = repository.findById(id);
        if (report.isEmpty()) {
            log.warn("No report with id {} found",id);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No  report "+id+" found");
        }

        userRepository.save(currentUser);
        if (!quickSightUserExists(currentUser)) {
            createQuickSightUser(currentUser);
        }
        return report.map(r -> getQuicksightUrl(r.getDashboardId(), currentUser));
    }

    private String getQuicksightUrl(String dashboardId, User currentUser) {

        AmazonQuickSight amazonQuickSight = amazonQuickSightClientFactory.embedUrlClient(currentUser);

        GetDashboardEmbedUrlRequest request = new GetDashboardEmbedUrlRequest()
                .withDashboardId(dashboardId)
                .withAwsAccountId(awsAccountId)
                .withIdentityType(IdentityType.IAM)
                .withResetDisabled(true)
                .withSessionLifetimeInMinutes(100l)
                .withUndoRedoDisabled(false);
        GetDashboardEmbedUrlResult dashboardEmbedUrlResult = amazonQuickSight.getDashboardEmbedUrl(request);
        if(dashboardEmbedUrlResult.getStatus() != 200){
            log.info("Throwing exception - got status " + dashboardEmbedUrlResult.getStatus());
            throw new ResponseStatusException(HttpStatus.resolve(dashboardEmbedUrlResult.getStatus()),"Error calling quicksight API: "+dashboardEmbedUrlResult.toString());
        }
        return dashboardEmbedUrlResult.getEmbedUrl();
    }

    private boolean quickSightUserExists(User user) {

        AmazonQuickSight amazonQuickSight = amazonQuickSightClientFactory.listUsersClient();

        ListUsersRequest listUsersRequest = new ListUsersRequest()
                .withAwsAccountId(awsAccountId)
                .withNamespace("default");

        try {
            ListUsersResult listUsersResult = amazonQuickSight.listUsers(listUsersRequest);
            if (listUsersResult.getStatus() != 200) {
                log.warn("List users non 200 response {}",listUsersResult.getStatus());
                throw new ResponseStatusException(HttpStatus.resolve(listUsersResult.getStatus()), "Error calling quicksight API to list users" + listUsersResult.toString());
            }
            return listUsersResult.getUserList().stream()
                    .anyMatch(u -> u.getUserName().equals(quickSightRoleName + '/' + user.getEmail()));
        } catch (AmazonServiceException ex) {
            log.warn("List users error {}", ex);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error calling quicksight API to list users",ex);
        }
    }

    private void createQuickSightUser(User user) {

        AmazonQuickSight amazonQuickSight = amazonQuickSightClientFactory.registerUserClient(user);

        log.info("Creating QuickSight user {}",user.getEmail());
        RegisterUserRequest registerUserRequest = new RegisterUserRequest()
                .withAwsAccountId(awsAccountId)
                .withNamespace("default")
                .withIdentityType(IdentityType.IAM)
                .withIamArn(quickSightRoleArn)
                .withUserRole(UserRole.READER)
                .withSessionName(user.getEmail())
                .withEmail(user.getEmail());

        try {
            RegisterUserResult registerUserResult = amazonQuickSight.registerUser(registerUserRequest);
            if (registerUserResult.getStatus() != HttpStatus.CREATED.value()) {
                log.warn("Register user {} non 200 response: {}",user.getEmail(),registerUserResult.getStatus());
                throw new ResponseStatusException(HttpStatus.resolve(registerUserResult.getStatus()), "Error calling quicksight API: " + registerUserResult.toString());
            }
        } catch (AmazonServiceException ex) {
            log.warn("Register user {} error {}",user.getEmail(), ex.getErrorMessage());
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error calling quicksight API",ex);
        }
    }
}
