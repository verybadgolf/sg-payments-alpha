package gov.scot.payments.spring;

import com.google.common.base.Predicates;
import gov.scot.payments.model.paymentinstruction.AccountNumber;
import gov.scot.payments.model.paymentinstruction.SortCode;
import lombok.Data;
import org.javamoney.moneta.Money;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import springfox.documentation.builders.AuthorizationCodeGrantBuilder;
import springfox.documentation.builders.OAuthBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.DocumentationCache;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.InMemorySwaggerResourcesProvider;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import javax.money.CurrencyUnit;
import java.util.*;

import static org.springframework.security.oauth2.common.util.OAuth2Utils.CLIENT_ID;

@Configuration
public class SwaggerConfiguration {

    @Bean
    public SwaggerResourceController swaggerResourceController(Environment environment,
                                                               DocumentationCache documentationCache){
        return new SwaggerResourceController(new InMemorySwaggerResourceProvider(environment,documentationCache));
    }

    @Bean
    public Docket api(ApiInfo apiInfo,SecurityScheme securityScheme
            , SecurityContext securityContext
            , @Value("${springfox.documentation.swagger.v2.basePath:}") String basePath
            , @Value("${springfox.documentation.swagger.v2.host:localhost:8080}") String host) {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                    .apis(RequestHandlerSelectors.any())
                    .paths(Predicates.not(Predicates.or(PathSelectors.ant("/cardpayments/*/form")
                            ,PathSelectors.ant("/cardpayments/*/authorize")
                            ,PathSelectors.ant("/cardpayments/*/submit")
                            ,PathSelectors.ant("/cardpayments/*/cancel")
                            ,PathSelectors.ant("/error")
                            ,PathSelectors.ant("/actuator**")
                            ,PathSelectors.ant("/payments/query/download")
                            ,PathSelectors.ant("/inboundpayments/query/download")
                            ,PathSelectors.ant("/reports/*/embedUrl"))))
                .build()
                .host(host)
                //.pathMapping(basePath)
                .pathProvider(new RelativePathProvider(basePath))
                .apiInfo(apiInfo)
                .produces(Set.of(MediaType.APPLICATION_JSON_VALUE))
                .securitySchemes(Collections.singletonList(securityScheme))
                .securityContexts(Collections.singletonList(securityContext))
                .directModelSubstitute(AccountNumber.class, String.class)
                .directModelSubstitute(SortCode.class, String.class)
                .directModelSubstitute(Money.class, Amount.class)
                .directModelSubstitute(CurrencyUnit.class, String.class)
                .genericModelSubstitutes(Optional.class);
    }

    @Bean
    @Profile("cognito")
    SecurityScheme cognitoSecurityScheme(@Value("${cloud.aws.cognito.domain}") String domain) {
        GrantType clientCredentials = new ClientCredentialsGrant(domain+"/oauth2/token");
        GrantType user = new AuthorizationCodeGrantBuilder()
                .tokenEndpoint(new TokenEndpoint(domain + "/oauth2/token", "oauthtoken"))
                .tokenRequestEndpoint(
                        new TokenRequestEndpoint(domain + "/oauth2/authorize", CLIENT_ID, CLIENT_ID))
                .build();

        return new OAuthBuilder()
                .name("auth")
                .grantTypes(List.of(clientCredentials))
                .build();
    }

    @Bean
    @Profile("!cognito")
    SecurityScheme mockSecurityScheme() {
        return new ApiKey("auth", "Authorization", "header");
    }

    @Bean
    SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(
                        Collections.singletonList(new SecurityReference("auth", new AuthorizationScope[0])))
                .forPaths(Predicates.not(PathSelectors.ant("/actuator/health**")))
                .build();
    }

    @Bean
    @Profile("cognito")
    SecurityConfiguration cognitoSecurity(/* @Value("${SERVICE_TO_SERVICE_COGNITO_APP_CLIENT_ID}") String appClientId
    ,@Value("${SERVICE_TO_SERVICE_COGNITO_APP_CLIENT_SECRET}") String appClientsecet */) {
        return SecurityConfigurationBuilder.builder()
                .scopeSeparator(" ")
                //.clientId(appClientId)
                //.clientSecret(appClientsecet)
                .useBasicAuthenticationWithAccessCodeGrant(true)
                .build();
    }

    @Bean
    @Profile("!cognito")
    SecurityConfiguration mockSecurity() {
        return SecurityConfigurationBuilder.builder()
                .scopeSeparator(" ")
                .useBasicAuthenticationWithAccessCodeGrant(true)
                .build();
    }

    @Bean
    ApiInfo apiInfo() {
        return new ApiInfo(
                "Payments Poc REST API",
                "Payments Poc REST API",
                "0.01-alpha",
                "N/A",
                new Contact("N/A", "N/A", "N/A"),
                "N/A", "N/A", Collections.emptyList());
    }

    @Data
    public class Amount {

        private CurrencyUnit currency;
        private String amount;
    }
}
