package gov.scot.payments.customer;

import gov.scot.payments.model.Entity;
import gov.scot.payments.model.customer.Service;
import gov.scot.payments.model.user.EntityOp;
import gov.scot.payments.model.user.Client;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.KafkaNull;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RequestMapping(value = "/services")
@Slf4j
public class CustomerServiceService {

    private final MessageChannel serviceUpdatedEvents;
    private final ServiceRepository serviceRepository;

    public CustomerServiceService(MessageChannel serviceUpdatedEvents, ServiceRepository serviceRepository){
        this.serviceUpdatedEvents = serviceUpdatedEvents;
        this.serviceRepository = serviceRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<Service> getAllServices(@ApiIgnore @AuthenticationPrincipal Client currentUser) {
        return currentUser.hasWildcardAccess("service", EntityOp.Read) ? serviceRepository.findAll() : serviceRepository.findAllWithAcl();
    }

    @RequestMapping(value = "/{serviceId}", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("principal.hasAccess('service',#serviceId,T(gov.scot.payments.model.user.EntityOp).Read)")
    public Optional<Service> getService(@PathVariable("serviceId") String serviceId) {
        return serviceRepository.findById(serviceId);
    }

    @RequestMapping(value = "/customer/{customerId}", method = RequestMethod.GET)
    @ResponseBody
    public List<Service> getServiceByCustomerId(@PathVariable("customerId") String customerId,@ApiIgnore @AuthenticationPrincipal Client currentUser) {
        return currentUser.hasWildcardAccess("service", EntityOp.Read) ? serviceRepository.findByCustomerId(customerId) : serviceRepository.findByCustomerIdWithAcl(customerId);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @PreAuthorize("principal.hasAccess('customer',#service.customerId,T(gov.scot.payments.model.user.EntityOp).Write)")
    public Service addService(@RequestBody Service service, @ApiIgnore @AuthenticationPrincipal Client currentUser) {
        serviceRepository.findById(service.getId()).ifPresent(p -> {throw new ResponseStatusException(HttpStatus.BAD_REQUEST,String.format("Service %s already exists",p.getId()));});
        serviceRepository.findByFolder(service.getFolder()).ifPresent(p -> {throw new ResponseStatusException(HttpStatus.BAD_REQUEST,String.format("Service %s already exists for folder %s",p.getId(),p.getFolder()));});
        Service toCreate = service.toBuilder().createdBy(currentUser.getUserName()).build();
        log.info("Sending service creation event for {}",toCreate);
        serviceUpdatedEvents.send(toCreate.createMessage());
        return toCreate;
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    @PreAuthorize("principal.hasAccess('service',#service.id,T(gov.scot.payments.model.user.EntityOp).Write)")
    public Service updateService(@RequestBody Service service,@ApiIgnore @AuthenticationPrincipal Client currentUser) {
        if(!serviceRepository.existsById(service.getId())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,String.format("Service %s does not exist",service.getId()));
        }
        Service toUpdate = service.toBuilder().createdBy(currentUser.getUserName()).build();
        log.info("Sending service updated event for {}",toUpdate);
        serviceUpdatedEvents.send(toUpdate.createMessage());
        return toUpdate;
    }

    @RequestMapping(value = "/{serviceId}", method = RequestMethod.DELETE)
    @PreAuthorize("principal.hasAccess('service',#serviceId,T(gov.scot.payments.model.user.EntityOp).Write)")
    public void deleteService(@PathVariable String serviceId) {
        log.info("Sending service deletion event: {}",serviceId);
        serviceUpdatedEvents.send(new GenericMessage<>(KafkaNull.INSTANCE,Collections.singletonMap(KafkaHeaders.MESSAGE_KEY, Entity.stringToKey(serviceId))));
    }

    public Optional<Service> getServiceByFolder(String folder) {
        return serviceRepository.findByFolder(folder);
    }
}
