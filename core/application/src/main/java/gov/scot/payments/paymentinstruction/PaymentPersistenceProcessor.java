package gov.scot.payments.paymentinstruction;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingFailedEvent;
import gov.scot.payments.model.paymentfile.event.PaymentValidationFailureEvent;
import gov.scot.payments.model.paymentfile.event.PaymentValidationSuccessEvent;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.event.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Optional;

@Slf4j
public class PaymentPersistenceProcessor {

    private final TemporalPaymentRepository temporalPaymentRepository;
    private final NonTemporalPaymentRepository nonTemporalPaymentRepository;
    private final TransactionOperations transactionTemplate;

    public PaymentPersistenceProcessor(TemporalPaymentRepository temporalPaymentRepository
            , NonTemporalPaymentRepository nonTemporalPaymentRepository
            , TransactionOperations transactionTemplate){
        this.temporalPaymentRepository = temporalPaymentRepository;
        this.nonTemporalPaymentRepository = nonTemporalPaymentRepository;
        this.transactionTemplate = transactionTemplate;
    }

    //TODO: this should really use a state machine to handle out-of-order updates
    // but for now we are relying on timestamp which is ok since we
    // are running all services in a single JVM and the state machine is monotonic
    @StreamListener
    public void handlePaymentEvents(
            @Input("persist-payment-created-events") KStream<?, PaymentInstructionCreatedEvent> paymentCreatedEvents
            ,@Input("persist-payment-validation-success-events") KStream<?, PaymentValidationSuccessEvent> successfulValidationEvents
            ,@Input("persist-payment-validation-failed-events") KStream<?, PaymentValidationFailureEvent> failedValidationEvents
            ,@Input("persist-payment-routing-success-events") KStream<?, PaymentRoutingFailedEvent> successfulRoutingEvents
            ,@Input("persist-payment-routing-failed-events") KStream<?, PaymentRoutingFailedEvent> failedRoutingEvents
            ,@Input("persist-payment-submission-success-events") KStream<?, PaymentSubmitSuccessEvent> successfulSubmissionEvents
            ,@Input("persist-payment-submission-failed-events") KStream<?, PaymentSubmitFailedEvent> failedSubmissionEvents
            ,@Input("persist-payment-accepted-events") KStream<?, PaymentAcceptedEvent> acceptedEvents
            ,@Input("persist-payment-rejection-events") KStream<?, PaymentRejectedEvent> rejectedEvents
            ,@Input("persist-payment-cleared-events") KStream<?, PaymentClearedEvent> clearedEvents
            ,@Input("persist-payment-failedToClear-events") KStream<?, PaymentFailedToClearEvent> failedToClearEvents
            ,@Input("persist-payment-settled-events") KStream<?, PaymentSettledEvent> settledEvents){

        paymentCreatedEvents.foreach( (k,v) -> persist(v.getPayload()));
        successfulValidationEvents.foreach( (k,v) -> persist(v.getPayload()));
        failedValidationEvents.foreach( (k,v) -> persist(v.getPayload()));
        successfulRoutingEvents.foreach( (k,v) -> persist(v.getPayload()));
        failedRoutingEvents.foreach( (k,v) -> persist(v.getPayload()));
        successfulSubmissionEvents.foreach( (k,v) -> persist(v.getPayload()));
        failedSubmissionEvents.foreach( (k,v) -> persist(v.getPayload()));
        acceptedEvents.foreach( (k,v) -> persist(v.getPayload()));
        rejectedEvents.foreach( (k,v) -> persist(v.getPayload()));
        clearedEvents.foreach( (k,v) -> persist(v.getPayload()));
        failedToClearEvents.foreach( (k,v) -> persist(v.getPayload()));
        settledEvents.foreach( (k,v) -> persist(v.getPayload()));
    }

    void persist(PaymentInstruction payment) {
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                persistTemporal(payment);
                persistNonTemporal(payment);
                status.flush();
            }
        });
    }

    void persistNonTemporal(PaymentInstruction payment) {
        FlattenedPayment toPersist = FlattenedPayment.from(payment);
        Optional<FlattenedPayment> existingNonTemporalPayment = nonTemporalPaymentRepository.findById(payment.getId());
        if(existingNonTemporalPayment.isPresent()){
            FlattenedPayment existingVersion = existingNonTemporalPayment.get();
            if(existingVersion.isAfter(toPersist)){
                log.info("Existing non-temporal version {} of {} is after received version {}, skipping persistence",existingVersion.getMaxTimestamp(),existingVersion,toPersist.getMaxTimestamp());
            } else{
                existingVersion.merge(toPersist);
                log.info("Persisting updated non-temporal version of {}",existingVersion);
                nonTemporalPaymentRepository.save(existingVersion);
            }
        } else{
            log.info("Persisting initial non-temporal version of {}",toPersist);
            nonTemporalPaymentRepository.save(toPersist);
        }
    }

    private void persistTemporal(PaymentInstruction payment) {
        TemporalPayment temporalPayment = TemporalPayment.from(payment);
        log.info("Persisting temporal version of {}",temporalPayment);
        temporalPaymentRepository.save(temporalPayment);
    }

}
