package gov.scot.payments.paymentinstruction;

import gov.scot.payments.model.customer.Service;
import gov.scot.payments.model.paymentinstruction.AccountNumber;
import gov.scot.payments.model.paymentinstruction.InvalidPaymentFieldException;
import gov.scot.payments.model.paymentinstruction.PaymentInstructionStatus;
import gov.scot.payments.model.paymentinstruction.SortCode;
import gov.scot.payments.model.user.EntityOp;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Client;
import gov.scot.payments.model.user.User;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.messaging.MessageChannel;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class PaymentQueryServiceTest {

    private PaymentQueryService service;
    private TemporalPaymentRepository temporalPaymentRepository;
    private NonTemporalPaymentRepository nonTemporalPaymentRepository;
    private MessageChannel messageChannel;
    private Function<String, Optional<Service>> serviceLookup;

    @BeforeEach
    public void setUp(){
        serviceLookup = mock(Function.class);
        when(serviceLookup.apply(anyString())).thenReturn(Optional.empty());
        temporalPaymentRepository = mock(TemporalPaymentRepository.class);
        nonTemporalPaymentRepository = mock(NonTemporalPaymentRepository.class);
        messageChannel = mock(MessageChannel.class);
        service = new PaymentQueryService(temporalPaymentRepository,nonTemporalPaymentRepository,messageChannel,serviceLookup);
    }

    @Test
    public void testGetById(){
        service.getPaymentById(UUID.randomUUID(), User.admin());
        verify(nonTemporalPaymentRepository,times(1)).findByPaymentId(any());
        verifyNoMoreInteractions(nonTemporalPaymentRepository,temporalPaymentRepository);
    }

    @Test
    public void testGetByIdWithAcl(){
        service.getPaymentById(UUID.randomUUID(), User.builder()
                .userName("")
                .email("")
                .withRole(new Role(EntityOp.Read,"service","123"))
                .build());
        verify(nonTemporalPaymentRepository,times(1)).findByPaymentIdWithAcl(any());
        verifyNoMoreInteractions(nonTemporalPaymentRepository,temporalPaymentRepository);
    }

    @Test
    public void testGetHistoryById(){
        service.getPaymentHistoryById(UUID.randomUUID(), User.admin());
        verify(temporalPaymentRepository,times(1)).findAllByPaymentId(any());
        verifyNoMoreInteractions(nonTemporalPaymentRepository,temporalPaymentRepository);
    }

    @Test
    public void testGetHistoryByIdWithAcl(){
        service.getPaymentHistoryById(UUID.randomUUID(), User.builder()
                .userName("")
                .email("")
                .withRole(new Role(EntityOp.Read,"service","123"))
                .build());
        verify(temporalPaymentRepository,times(1)).findAllByPaymentIdWithAcl(any());
        verifyNoMoreInteractions(nonTemporalPaymentRepository,temporalPaymentRepository);
    }

    @Test
    public void testGetStatuses(){
        List<PaymentInstructionStatus> statuses = service.getPaymentStatuses();
        assertEquals(statuses, Arrays.asList(PaymentInstructionStatus.values()));
        verifyNoMoreInteractions(nonTemporalPaymentRepository,temporalPaymentRepository);
    }

    @Test
    public void testGetAll(){
        when(nonTemporalPaymentRepository.findAll(any(Pageable.class))).thenReturn(new PageImpl<>(Collections.emptyList()));
        service.getAllLatestPayments(mock(Pageable.class),mock(PagedResourcesAssembler.class), User.admin());
        verify(nonTemporalPaymentRepository,times(1)).findAll(any(Pageable.class));
        verifyNoMoreInteractions(nonTemporalPaymentRepository,temporalPaymentRepository);
    }

    @Test
    public void testGetAllWithAcl(){
        when(nonTemporalPaymentRepository.findAllWithAcl(any(Pageable.class))).thenReturn(new PageImpl<>(Collections.emptyList()));
        service.getAllLatestPayments(mock(Pageable.class),mock(PagedResourcesAssembler.class), User.builder()
                .userName("")
                .email("")
                .withRole(new Role(EntityOp.Read,"service","123"))
                .build());
        verify(nonTemporalPaymentRepository,times(1)).findAllWithAcl(any(Pageable.class));
        verifyNoMoreInteractions(nonTemporalPaymentRepository,temporalPaymentRepository);
    }

    @Test
    public void testQuery(){
        when(nonTemporalPaymentRepository.findAll(any(Specification.class),any(Pageable.class))).thenReturn(new PageImpl<>(Collections.emptyList()));
        service.queryLatestPayments("service==ilf",mock(Pageable.class),mock(PagedResourcesAssembler.class), User.admin());
        verify(nonTemporalPaymentRepository,times(1)).findAll(any(Specification.class),any(Pageable.class));
        verifyNoMoreInteractions(nonTemporalPaymentRepository,temporalPaymentRepository);
    }

    @Test
    public void testParseQueryInvalid(){
        assertThrows(ResponseStatusException.class,() -> service.queryLatestPayments("service=ilf",mock(Pageable.class),mock(PagedResourcesAssembler.class), User.admin()));
    }

    @Test
    public void testCreate() throws InvalidPaymentFieldException {
        Service service = Service.builder()
                .folder("abc")
                .fileFormat("ILF")
                .id("123")
                .createdBy("user")
                .customerId("customer1")
                .paymentValidationRules(Collections.emptyList())
                .build();
        when(serviceLookup.apply("123")).thenReturn(Optional.of(service));
        PaymentCreateRequest request = PaymentCreateRequest.builder()
                .amount(Money.of(new BigDecimal("1"),"GBP"))
                .sortCode(SortCode.fromString("101010"))
                .accountNumber(AccountNumber.fromString("12345678"))
                .service("123")
                .payee("dave")
                .build();
        User user = User.builder()
                .email("user")
                .userName("user")
                .withRole(Role.builder()
                        .entityId("123")
                        .entityType("payment")
                        .operation(EntityOp.Write)
                        .build())
                .build();
        this.service.createPayment(request, user);
        verify(messageChannel,times(1)).send(any());
    }
}
