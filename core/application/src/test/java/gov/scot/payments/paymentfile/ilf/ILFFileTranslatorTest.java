package gov.scot.payments.paymentfile.ilf;

import gov.scot.payments.paymentfile.FileParseException;
import gov.scot.payments.paymentfile.FileTranslateException;
import gov.scot.payments.model.paymentfile.PaymentFile;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class ILFFileTranslatorTest {

    @Test
    void generatePaymentInstructionsFromFileTest() throws IOException, FileTranslateException, FileParseException, URISyntaxException {
        var paymentFile = PaymentFile.builder()
                .service("service")
                .name("file1")
                .type("ILF")
                .url(new URI("s3://path/to/file"))
                .build();

        var parser = new ILFFileParser();
        var translator = new ILFFileTranslator(parser);
        try (var fileReader = getClass().getClassLoader().getResourceAsStream("ilf_examples/ILF_test.csv")) {

            List<PaymentInstruction> instructions = translator.generatePaymentInstructionsFromFile(paymentFile,fileReader);
            assertEquals(3, instructions.size());
            assertNotNull(instructions.get(0));
            var recipient = instructions.get(0).getRecipient();
            assertEquals("820001",recipient.getSortCode().getValue());
            assertEquals("Test 01",recipient.getName());
            assertEquals("12345678",recipient.getAccountNumber().getValue());
            Money money = Money.of(new BigDecimal("1234.56"), "GBP");
            assertEquals(money,instructions.get(0).getTargetAmount());
            var recipient3 = instructions.get(2).getRecipient();
            assertEquals("820003",recipient3.getSortCode().getValue());
            assertEquals("Test 03",recipient3.getName());
            assertEquals("12345680",recipient3.getAccountNumber().getValue());
            money = Money.of(new BigDecimal("1048.23"), "GBP");
            assertEquals(money,instructions.get(2).getTargetAmount());
        }
    }

}