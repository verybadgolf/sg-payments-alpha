package gov.scot.payments.paymentfile.ilf;

import gov.scot.payments.model.paymentfile.PaymentFile;
import gov.scot.payments.model.paymentinstruction.AccountNumber;
import gov.scot.payments.model.paymentinstruction.SortCode;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertEquals;


class ILFBACSPaymentTest {

    private CSVRecord generateCSVRecordFromString(String row) throws IOException {

        CSVFormat ILF_CSV_FORMAT = CSVFormat.RFC4180
                .withTrim()
                .withNullString("")
                .withIgnoreEmptyLines(true);

        return CSVParser.parse(row, ILF_CSV_FORMAT).getRecords().get(0);

    }

    @Test
    void fromCSVRecordValidTest() throws IOException, InvalidCSVFieldException {
        String csvRow = "Test 164,ILF 0000,800808,11232272,511.22";
        CSVRecord record = generateCSVRecordFromString(csvRow);
        ILFBACSPayment payment = ILFBACSPayment.fromCSVRecord(record);
        assertEquals(new BigDecimal("511.22"), payment.getAmount());
        assertEquals("800808", payment.getSortCode().getValue());
        assertEquals("11232272", payment.getAccountNumber().getValue());
        assertEquals("ILF 0000", payment.getReference());
        assertEquals("Test 164", payment.getRecipient());

    }

    @Test
    void fromCSVRecordInvalidSortCodeTest() throws IOException {
        String csvRow = "Test 164,,808,11232272,-234.21,0";
        CSVRecord record = generateCSVRecordFromString(csvRow);
        var exception = Assertions.assertThrows(InvalidCSVFieldException.class, () -> ILFBACSPayment.fromCSVRecord(record));
        assertEquals("[Row 1 Field Name:'Sort Code'] Parsing error: Invalid sort-code number 808 - must be 6 digits", exception.getMessage());
        assertEquals("Sort Code", exception.getFieldName());
    }


    @Test
    void fromCSVRecordMissingReferenceTest() throws IOException {
        String csvRow = "Test 164,,800808,11232272,511.22";
        CSVRecord record = generateCSVRecordFromString(csvRow);
        var exception = Assertions.assertThrows(InvalidCSVFieldException.class, () -> ILFBACSPayment.fromCSVRecord(record));
        assertEquals("Reference", exception.getFieldName());
        assertEquals("[Row 1 Field Name:'Reference'] Parsing error: Field not found", exception.getMessage());
    }

    @Test
    void fromCSVRecordMissingAmountTest() throws IOException {
        String csvRow = "Test 164,,800808,11232272,,0";
        CSVRecord record = generateCSVRecordFromString(csvRow);
        var exception = Assertions.assertThrows(InvalidCSVFieldException.class, () -> ILFBACSPayment.fromCSVRecord(record));
        assertEquals("[Row 1 Field Name:'Amount'] Parsing error: Field not found", exception.getMessage());
        assertEquals("Amount", exception.getFieldName());
    }

    @Test
    void fromCSVRecordNegativeAmountTest() throws IOException {
        String csvRow = "Test 164,ILF_REF,800808,11232272,-234.21,0";
        CSVRecord record = generateCSVRecordFromString(csvRow);
        var exception = Assertions.assertThrows(InvalidCSVFieldException.class, () -> ILFBACSPayment.fromCSVRecord(record));
        assertEquals("[Row 1 Field Name:'Amount'] Parsing error: Value -234.21 not a valid positive number", exception.getMessage());
        assertEquals("Amount", exception.getFieldName());
    }

    @Test
    void toPaymentInstruction() throws URISyntaxException {

        var payment = ILFBACSPayment.builder()
                .reference("TEST_REF")
                .recipient("jon")
                .accountNumber(new AccountNumber("00124589"))
                .sortCode(new SortCode("801208"))
                .amount(new BigDecimal("1234.21"))
                .build();
        var paymentFile = PaymentFile.builder()
                .service("service")
                .name("file1")
                .type("ILF")
                .url(new URI("s3://path/to/file"))
                .build();

        var instruction = payment.toPaymentInstruction(paymentFile);

        assertEquals("TEST_REF", instruction.getRecipient().getRef());
        assertEquals("jon", instruction.getRecipient().getName());
        assertEquals("00124589", instruction.getRecipient().getAccountNumber().getValue());
        assertEquals("801208", instruction.getRecipient().getSortCode().getValue());
        assertEquals("GBP 1234.21", instruction.getTargetAmount().toString());
        assertEquals("service", instruction.getService());
        assertEquals("file1", instruction.getPaymentFile());
        assertEquals("service", instruction.getService());
        assertEquals(paymentFile.getTimestamp(), instruction.getTimestamp());
    }
}