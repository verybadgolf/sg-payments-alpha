package gov.scot.payments.paymentfile.ilf;

import gov.scot.payments.paymentfile.FileParseException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ILFFileParserTest {

    @Test
    void generatePaymentTest() throws IOException, FileParseException {

        var path = getClass().getClassLoader().getResource("ilf_examples/ILF_test.csv").getFile();

        try (var fileReader = new FileInputStream(path)) {
            ILFFileParser parser = new ILFFileParser();
            List<ILFBACSPayment> paymentList = parser.generatePaymentList(fileReader);
            assertEquals(3, paymentList.size());
            assertEquals("Test 01", paymentList.get(0).getRecipient());
            assertEquals("ILF 0001", paymentList.get(0).getReference());
            assertEquals("12345678", paymentList.get(0).getAccountNumber().getValue());
            assertEquals("820001", paymentList.get(0).getSortCode().getValue());
            assertEquals(new BigDecimal("1234.56"), paymentList.get(0).getAmount());
        }
    }

    @Test
    void generatePaymentTestWithEmptyRows() throws IOException, FileParseException {

        var path = getClass().getClassLoader().getResource("ilf_examples/ILF_test_with_empty.csv").getFile();

        try (var fileReader = new FileInputStream(path)) {
            ILFFileParser parser = new ILFFileParser();
            List<ILFBACSPayment> paymentList = parser.generatePaymentList(fileReader);
            assertEquals(8, paymentList.size());
            assertEquals("Test 122", paymentList.get(7).getRecipient());
        }
    }

    @Test
    void generatePaymentTestWithNotEnoughColumns() throws IOException {

        String csvRow = "Test 164,ILF 0000,012322724,511.22";
        try (var targetStream = new ByteArrayInputStream(csvRow.getBytes())) {
            ILFFileParser parser = new ILFFileParser();
            FileParseException e = Assertions.assertThrows(FileParseException.class, () ->parser.generatePaymentList(targetStream));
            assertEquals("Row 1 has 4 columns. Must be between 5-6" , e.getMessage());
        }
    }

    @Test
    void generatePaymentTestWithTooManyColumns() throws IOException {

        String csvRow = "Test 164,ILF 0000,012322724,511.22,,,,,";
        try (var targetStream = new ByteArrayInputStream(csvRow.getBytes())) {
            ILFFileParser parser = new ILFFileParser();
            FileParseException e = Assertions.assertThrows(FileParseException.class, () ->parser.generatePaymentList(targetStream));
            assertEquals("Row 1 has 9 columns. Must be between 5-6" , e.getMessage());
        }
    }

    @Test
    void generatePaymentTestWithErrorRow() throws IOException {

        var path = getClass().getClassLoader().getResource("ilf_examples/ILF_test_with_error_row.csv").getFile();
        try (var fileReader = new FileInputStream(path)) {
            ILFFileParser parser = new ILFFileParser();
            FileParseException e = Assertions.assertThrows(FileParseException.class, () ->parser.generatePaymentList(fileReader));
            assertEquals("[Row 4 Field Name:'Sort Code'] Parsing error: Invalid sort-code number 81537 - must be 6 digits" , e.getMessage());
        }
    }

}