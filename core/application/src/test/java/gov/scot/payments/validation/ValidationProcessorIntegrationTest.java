package gov.scot.payments.validation;

import gov.scot.payments.AbstractKafkaTest;
import gov.scot.payments.common.EntityKeyValueStore;
import gov.scot.payments.model.customer.PaymentValidationRule;
import gov.scot.payments.model.customer.Service;
import gov.scot.payments.model.paymentfile.event.PaymentValidationFailureEvent;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.Recipient;
import gov.scot.payments.model.paymentinstruction.Validation;
import gov.scot.payments.model.paymentinstruction.ValidationStatus;
import gov.scot.payments.model.paymentinstruction.event.PaymentInstructionCreatedEvent;
import gov.scot.payments.validation.spring.ValidationConfiguration;
import gov.scot.payments.validation.spring.ValidationProcessorBinding;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.cloud.aws.autoconfigure.context.ContextCredentialsAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextResourceLoaderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.jdbc.AmazonRdsDatabaseAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.messaging.MessagingAutoConfiguration;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.test.annotation.DirtiesContext;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;


@EnableAutoConfiguration(exclude = {MessagingAutoConfiguration.class
        , ContextStackAutoConfiguration.class
        , ContextResourceLoaderAutoConfiguration.class
        , ContextRegionProviderAutoConfiguration.class
        , ContextCredentialsAutoConfiguration.class
        , AmazonRdsDatabaseAutoConfiguration.class
})
@EmbeddedKafka(partitions = 1
        ,topics = {"test-paymentCreatedEvents","test-paymentValidationSuccessEvents","test-paymentValidationFailedEvents"})
@SpringBootTest(classes = ValidationProcessorIntegrationTest.TestConfiguration.class, properties = {
        "debug=true",
        "payments.environment=test",
        "spring.cloud.stream.bindings.validate-payment-created-events-send.binder=kafka",
        "spring.cloud.stream.bindings.validate-payment-created-events-send.destination=test-paymentCreatedEvents"
})
@DirtiesContext
@Tag("kafka")
public class ValidationProcessorIntegrationTest extends AbstractKafkaTest {

    @Autowired
    @Qualifier("validate-payment-created-events-send")
    MessageChannel paymentCreatedEvents;

    @SpyBean
    PaymentInstructionValidator validator;

    @MockBean
    EntityKeyValueStore<Service> entityKeyValueStore;

    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();


    }

    private Service createServiceWithRule(String ruleStr) {
        PaymentValidationRule rule = createPaymentValidationRule(ruleStr);

        return Service.builder()
                .id("service_id")
                .fileFormat("CSV")
                .createdBy("user")
                .folder("folder")
                .paymentValidationRules(List.of(rule))
                .customerId("customer_1_1")
                .createdAt(LocalDateTime.parse("2020-01-01T10:10:10")).build();
    }

    private PaymentValidationRule createPaymentValidationRule(String ruleStr) {
        return PaymentValidationRule.builder()
                    .name("test")
                    .rule(ruleStr)
                    .createdBy("user")
                    .messageTemplate("This failed testRule")
                    .createdAt(LocalDateTime.parse("2020-01-01T10:10:10"))
                    .build();
    }

    @Test
    public void testValidationSuccess() throws Exception {

        PaymentInstruction payment = PaymentInstruction.builder()
                .recipient(Recipient.builder().build())
                .targetAmount(Money.of(new BigDecimal("200"), "GBP"))
                .paymentFile("payment_file_test")
                .service("service1")
                .build();

        var ruleStr = "targetAmount.getNumber() < 1000.0";
        Service serviceObj = createServiceWithRule(ruleStr);
        when(entityKeyValueStore.get(Mockito.anyString())).thenReturn(Optional.of(serviceObj));

        var event = new PaymentInstructionCreatedEvent(payment);
        paymentCreatedEvents.send(new GenericMessage<>(event));
        sleep();
        verifyEvents("test-paymentValidationSuccessEvents", events -> assertEquals(1,events.size()));
        verifyEvents("test-paymentValidationFailedEvents",events -> assertEquals(0,events.size()));

    }

    @Test
    public void testValidationFailure() throws Exception {

        PaymentInstruction payment = PaymentInstruction.builder()
                .recipient(Recipient.builder().build())
                .targetAmount(Money.of(new BigDecimal("200"), "GBP"))
                .paymentFile("payment_file_test")
                .service("service1")
                .build();

        var ruleStr = "targetAmount.getNumber() > 1000.0";
        Service serviceObj = createServiceWithRule(ruleStr);
        when(entityKeyValueStore.get(Mockito.anyString())).thenReturn(Optional.of(serviceObj));

        doReturn(Validation.builder()
                .status(ValidationStatus.Invalid)
                .build()).when(validator).validate(Mockito.any(), Mockito.anyList());
        var event = new PaymentValidationFailureEvent(payment);

        paymentCreatedEvents.send(new GenericMessage<>(event));
        sleep();
        verifyEvents("test-paymentValidationSuccessEvents", events -> assertEquals(0,events.size()));
        verifyEvents("test-paymentValidationFailedEvents",events -> assertEquals(1,events.size()));

    }

    @Import({ValidationConfiguration.class})
    @EnableBinding({ ValidationProcessorBinding.class
            , ValidationTestBinding.class})
    public static class TestConfiguration extends AbstractKafkaTest.TestConfiguration {


    }

    public interface ValidationTestBinding {

        @Output("validate-payment-created-events-send")
        MessageChannel validatePaymentsChannel();

    }


}
