package gov.scot.payments.customer;


import gov.scot.payments.AbstractKafkaTest;
import gov.scot.payments.common.OptionalResponseControllerAdvice;
import gov.scot.payments.model.customer.event.CustomerCreationEvent;
import gov.scot.payments.model.customer.event.CustomerDeletionEvent;
import gov.scot.payments.model.customer.event.CustomerUpdateEvent;
import gov.scot.payments.customer.spring.CustomerPersistenceBindings;
import gov.scot.payments.model.customer.Customer;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.aws.autoconfigure.context.ContextCredentialsAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextResourceLoaderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.jdbc.AmazonRdsDatabaseAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.messaging.MessagingAutoConfiguration;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.test.annotation.DirtiesContext;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@EnableAutoConfiguration(exclude = {MessagingAutoConfiguration.class
        , ContextStackAutoConfiguration.class
        , ContextResourceLoaderAutoConfiguration.class
        , ContextRegionProviderAutoConfiguration.class
        , ContextCredentialsAutoConfiguration.class
        , AmazonRdsDatabaseAutoConfiguration.class
})
@EmbeddedKafka(partitions = 1
        ,topics = {"test-customerCreatedEvents","test-customerUpdatedEvents","test-customerDeletedEvents"})
@SpringBootTest(classes = CustomerPersistenceIntegrationTest.TestConfiguration.class, properties = {
        "debug=true",
        "payments.environment=test",
        "spring.cloud.stream.bindings.validate-customer-created-events-send.binder=kafka",
        "spring.cloud.stream.bindings.validate-customer-created-events-send.destination=test-customerCreatedEvents",
        "spring.cloud.stream.bindings.validate-customer-updated-events-send.binder=kafka",
        "spring.cloud.stream.bindings.validate-customer-updated-events-send.destination=test-customerUpdatedEvents",
        "spring.cloud.stream.bindings.validate-customer-deleted-events-send.binder=kafka",
        "spring.cloud.stream.bindings.validate-customer-deleted-events-send.destination=test-customerDeletedEvents"
})
@DirtiesContext
//@DBRider
@Tag("kafka")
public class CustomerPersistenceIntegrationTest extends AbstractKafkaTest{


    @Autowired
    @Qualifier("validate-customer-created-events-send")
    MessageChannel customerCreatedEvent;
    @Autowired
    @Qualifier("validate-customer-updated-events-send")
    MessageChannel customerUpdatedEvent;
    @Autowired
    @Qualifier("validate-customer-deleted-events-send")
    MessageChannel customerDeletedEvent;

    @MockBean
    CustomerRepository repository;


    @Test
    public void testSaveCustomerOnEvent() throws Exception {

        var customerObj = Customer.builder()
                .id("customer_6_1")
                .name("test_2")
                .createdBy("testuser")
                .createdAt(LocalDateTime.parse("2020-01-01T10:10:10"))
                .build();

        customerCreatedEvent.send(new GenericMessage<>(new CustomerCreationEvent(customerObj)));
        sleep();

        ArgumentCaptor<Customer> argumentCaptor = ArgumentCaptor.forClass(Customer.class);
        verify(repository, times(1)).save(argumentCaptor.capture());
        assertTrue(argumentCaptor.getValue().equals(customerObj));
    }

    @Test
    public void testUpdateCustomerOnEvent() throws Exception {

        var customerAdd = Customer.builder()
                .id("customer_6_1")
                .name("test_2")
                .createdBy("testuser")
                .createdAt(LocalDateTime.parse("2020-01-01T10:10:10"))
                .build();


        customerUpdatedEvent.send(new GenericMessage<>(new CustomerUpdateEvent(customerAdd)));
        sleep();

        ArgumentCaptor<Customer> argumentCaptor = ArgumentCaptor.forClass(Customer.class);
        verify(repository, times(1)).save(argumentCaptor.capture());
        assertTrue(argumentCaptor.getValue().equals(customerAdd));
    }


    @Test
    public void testDeleteCustomerOnEvent() throws Exception {

        customerDeletedEvent.send(new GenericMessage<>(new CustomerDeletionEvent("customer_1_1")));
        sleep();
        verify(repository, times(1)).deleteById("customer_1_1");
    }


    @EnableJpaRepositories(basePackageClasses = CustomerRepository.class)
    @EntityScan(basePackageClasses = Customer.class)
    @Import({OptionalResponseControllerAdvice.class })
    @EnableBinding({CustomerPersistenceBindings.class, CustomerPersistTestBinding.class})
    public static class TestConfiguration extends AbstractKafkaTest.TestConfiguration {

        @Bean
        public CustomerPersistenceProcessor getPersistenceProcessor(CustomerRepository customerRepo){
            return new CustomerPersistenceProcessor(customerRepo);
        }

    }

    public interface CustomerPersistTestBinding {

        @Output("validate-customer-created-events-send")
        MessageChannel validateCustomerCreatedChannel();
        @Output("validate-customer-updated-events-send")
        MessageChannel validateCustomerDeletedChannel();
        @Output("validate-customer-deleted-events-send")
        MessageChannel validateCustomerUpdatedChannel();

    }




}
