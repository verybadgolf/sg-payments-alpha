package gov.scot.payments.cardpayment;

import gov.scot.payments.model.cardpayment.CardPayment;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AuthorizeCardPaymentRequestTest {

    @Test
    void toGatewayRequest() {

        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service1")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy("test")
                .expectedCost(Money.of(1,"GBP"))
                .build();

        var cardDetails = AuthorizeCardPaymentRequest.builder()
                .name("James Bond")
                .cardNumber("1234567812345678")
                .cvv("123")
                .year(23)
                .month(01)
                .build();

        var details = cardDetails.toGatewayRequest(payment);
        var date = LocalDate.of(2023, 01, 31);
        assertEquals(date, details.getExpiry());

    }
}