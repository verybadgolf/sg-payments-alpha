package gov.scot.payments.router;

import gov.scot.payments.AbstractKafkaTest;
import gov.scot.payments.common.OptionalResponseControllerAdvice;
import gov.scot.payments.common.kafka.KafkaNullConfiguration;
import gov.scot.payments.model.paymentinstruction.PaymentChannelType;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.Recipient;
import gov.scot.payments.model.paymentinstruction.PaymentChannel;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import gov.scot.payments.model.paymentinstruction.PaymentProcessorChannel;
import gov.scot.payments.router.spring.PaymentRouterBinding;
import gov.scot.payments.router.spring.PaymentRoutingConfiguration;
import gov.scot.payments.model.paymentfile.event.PaymentValidationSuccessEvent;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.aws.autoconfigure.context.ContextCredentialsAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextResourceLoaderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.jdbc.AmazonRdsDatabaseAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.messaging.MessagingAutoConfiguration;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@EnableAutoConfiguration(exclude = {MessagingAutoConfiguration.class
        , ContextStackAutoConfiguration.class
        , ContextResourceLoaderAutoConfiguration.class
        , ContextRegionProviderAutoConfiguration.class
        , ContextCredentialsAutoConfiguration.class
        , AmazonRdsDatabaseAutoConfiguration.class
})
@EmbeddedKafka(partitions = 1
        ,topics = {"test-paymentValidationSuccessEvents","test-paymentRoutingFailedEvents","test-123PaymentEvents","test-456PaymentEvents"})
@SpringBootTest(classes = PaymentRouterIntegrationTest.TestConfiguration.class
        , properties = {"payments.environment=test"
        ,"spring.cloud.stream.bindings.payment-validation-success-events.binder=kafka"})
@Tag("kafka")
public class PaymentRouterIntegrationTest extends AbstractKafkaTest {

    @MockBean
    Supplier<Collection<PaymentProcessor>> paymentProcessorRegistry;

    @Autowired
    @Qualifier("payment-validation-success-events")
    MessageChannel paymentValidationSuccessEvents;

    @Test
    public void testRouting(){

        PaymentProcessor processor1 = PaymentProcessor.builder()
                .name("123")
                .createdBy("admin")
                .addChannel(PaymentProcessorChannel.builder()
                        .costExpression("targetAmount.multiply(0.01)")
                        .paymentChannel(PaymentChannel.Bacs)
                        .build())
                .build();
        PaymentProcessor processor2 = PaymentProcessor.builder()
                .name("456")
                .createdBy("admin")
                .addChannel(PaymentProcessorChannel.builder()
                        .costExpression("targetAmount.multiply(0.02)")
                        .paymentChannel(PaymentChannel.FasterPayments)
                        .build())
                .build();
        when(paymentProcessorRegistry.get()).thenReturn(List.of(processor1,processor2));

        PaymentInstruction payment1 = PaymentInstruction.builder()
                .recipient(Recipient.builder().build())
                .targetChannelType(PaymentChannelType.Cheque)
                .latestTargetSettlementDate(LocalDate.now())
                .targetAmount(Money.of(new BigDecimal("50"),"GBP"))
                .service("service1")
                .build();
        PaymentInstruction payment2 = PaymentInstruction.builder()
                .recipient(Recipient.builder().build())
                .targetChannelType(PaymentChannelType.EFT)
                .latestTargetSettlementDate(LocalDate.now().plusDays(7))
                .targetAmount(Money.of(new BigDecimal("100"),"GBP"))
                .service("service1")
                .build();
        PaymentInstruction payment3 = PaymentInstruction.builder()
                .recipient(Recipient.builder().build())
                .targetChannelType(PaymentChannelType.EFT)
                .latestTargetSettlementDate(LocalDate.now())
                .targetAmount(Money.of(new BigDecimal("200"),"GBP"))
                .service("service1")
                .build();
        paymentValidationSuccessEvents.send(new GenericMessage<>(new PaymentValidationSuccessEvent(payment1)));
        paymentValidationSuccessEvents.send(new GenericMessage<>(new PaymentValidationSuccessEvent(payment2)));
        paymentValidationSuccessEvents.send(new GenericMessage<>(new PaymentValidationSuccessEvent(payment3)));
        sleep(Duration.ofSeconds(2));
        verifyEvents("test-paymentRoutingFailedEvents",l -> assertEquals(1,l.size()));
        verifyEvents("test-123PaymentEvents",l -> assertEquals(1,l.size()));
        verifyEvents("test-456PaymentEvents",l -> assertEquals(1,l.size()));
    }

    @Import({OptionalResponseControllerAdvice.class, KafkaNullConfiguration.class, PaymentRoutingConfiguration.class})
    @EnableBinding({ PaymentRouterBinding.class, TestBinding.class})
    public static class TestConfiguration extends AbstractKafkaTest.TestConfiguration {

    }

    public interface TestBinding {

        @Output("payment-validation-success-events")
        MessageChannel paymentValidationSuccessEvents();

    }
}
