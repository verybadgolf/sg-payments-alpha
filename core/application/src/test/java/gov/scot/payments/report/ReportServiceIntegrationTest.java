package gov.scot.payments.report;

import com.amazonaws.services.quicksight.AmazonQuickSight;
import com.amazonaws.services.quicksight.model.GetDashboardEmbedUrlResult;
import com.amazonaws.services.quicksight.model.ListUsersResult;
import com.amazonaws.services.quicksight.model.RegisterUserResult;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.spring.api.DBRider;
import gov.scot.payments.MockWebSecurityConfiguration;
import gov.scot.payments.WithUser;
import gov.scot.payments.common.OptionalResponseControllerAdvice;
import gov.scot.payments.model.report.Report;
import gov.scot.payments.model.user.EntityOp;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Client;
import gov.scot.payments.model.user.User;
import gov.scot.payments.report.spring.ReportConfiguration;
import gov.scot.payments.report.spring.ReportServiceConfiguration;
import gov.scot.payments.security.UserRepository;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = ReportServiceIntegrationTest.TestConfiguration.class
        , properties = {"AWS_ACCOUNT_ID=123"
            ,"QUICK_SIGHT_ROLE_NAME=QuickSightDashboardUserRole"
            ,"payments.report.abc=123"
            ,"payments.report.def=456"})
@AutoConfigureDataJpa
@AutoConfigureWebMvc
@AutoConfigureMockMvc
@DBRider
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@Tag("db")
@WithUser
public class ReportServiceIntegrationTest {

    @Autowired
    protected MockMvc mvc;

    @MockBean
    AmazonQuickSight amazonQuickSight;

    @MockBean
    AmazonQuickSightClientFactory quickSightClientFactory;

    @MockBean
    UserRepository userRepository;

    private static final String QUICK_SIGHT_ROLE_NAME = "QuickSightDashboardUserRole";

    @Test
    @DataSet("reports.yml")
    public void testRegister() throws Exception {
        User user = User.builder().userName("").email("").withRole(new Role(EntityOp.Read, "report", "*")).build();
        mvc.perform(post("/reports").param("name","ghi").param("id","abc").param("dashboardId","def")
        .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(status().isForbidden());

        user = User.builder().userName("").email("").withRole(new Role(EntityOp.Write, "report", "*")).build();
        mvc.perform(post("/reports").param("name","ghi").param("id","abc").param("dashboardId","def")
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(status().isOk());
        mvc.perform(get("/reports"))
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    @DataSet("reports.yml")
    public void testGetAll() throws Exception {
        mvc.perform(get("/reports"))
                .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    @DataSet("reports.yml")
    public void testEmbedDoesNotExist() throws Exception {
        User user = User.builder().userName("").email("email@address").withRole(new Role(EntityOp.Read, "report", "*")).build();
        com.amazonaws.services.quicksight.model.User quicksightUser = new com.amazonaws.services.quicksight.model.User()
                .withUserName(QUICK_SIGHT_ROLE_NAME + "/email@address");
        when(quickSightClientFactory.listUsersClient()).thenReturn(amazonQuickSight);
        when(amazonQuickSight.listUsers(any())).thenReturn(new ListUsersResult().withStatus(200).withUserList(List.of(quicksightUser)));

        mvc.perform(get("/reports/abc/embedUrl")
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(status().isNotFound());
    }

    @Test
    @DataSet("reports.yml")
    public void testEmbedError() throws Exception {
        User user = User.builder().userName("").email("email@address").withRole(new Role(EntityOp.Read, "report", "*")).build();
        com.amazonaws.services.quicksight.model.User quicksightUser = new com.amazonaws.services.quicksight.model.User()
                .withUserName(QUICK_SIGHT_ROLE_NAME + "/email@address");
        when(quickSightClientFactory.listUsersClient()).thenReturn(amazonQuickSight);
        when(amazonQuickSight.listUsers(any())).thenReturn(new ListUsersResult().withStatus(200).withUserList(List.of(quicksightUser)));
        when(quickSightClientFactory.embedUrlClient(any())).thenReturn(amazonQuickSight);
        when(amazonQuickSight.getDashboardEmbedUrl(any())).thenReturn(new GetDashboardEmbedUrlResult().withStatus(400));
        mvc.perform(get("/reports/123/embedUrl")
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DataSet("reports.yml")
    public void testEmbedSuccess() throws Exception {
        User user = User.builder().userName("").email("email@address").withRole(new Role(EntityOp.Read, "report", "*")).build();
        com.amazonaws.services.quicksight.model.User quicksightUser = new com.amazonaws.services.quicksight.model.User()
                .withUserName(QUICK_SIGHT_ROLE_NAME + "/email@address");
        when(quickSightClientFactory.listUsersClient()).thenReturn(amazonQuickSight);
        when(amazonQuickSight.listUsers(any())).thenReturn(new ListUsersResult().withStatus(200).withUserList(List.of(quicksightUser)));
        when(quickSightClientFactory.embedUrlClient(any())).thenReturn(amazonQuickSight);
        when(amazonQuickSight.getDashboardEmbedUrl(any())).thenReturn(new GetDashboardEmbedUrlResult().withStatus(200).withEmbedUrl("url"));
        mvc.perform(get("/reports/123/embedUrl")
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(content().json("\"url\""));
    }

    @Test
    @DataSet("reports.yml")
    public void testRegisterUserSuccess() throws Exception {
        User user = User.builder().userName("").email("email@address").withRole(new Role(EntityOp.Read, "report", "*")).build();
        com.amazonaws.services.quicksight.model.User quicksightUser = new com.amazonaws.services.quicksight.model.User()
                .withUserName(QUICK_SIGHT_ROLE_NAME + "/different.email@address");
        when(quickSightClientFactory.listUsersClient()).thenReturn(amazonQuickSight);
        when(amazonQuickSight.listUsers(any())).thenReturn(new ListUsersResult().withStatus(200).withUserList(List.of(quicksightUser)));
        when(quickSightClientFactory.registerUserClient(any())).thenReturn(amazonQuickSight);
        when(amazonQuickSight.registerUser(any())).thenReturn(new RegisterUserResult().withStatus(201));
        when(quickSightClientFactory.embedUrlClient(any())).thenReturn(amazonQuickSight);
        when(amazonQuickSight.getDashboardEmbedUrl(any())).thenReturn(new GetDashboardEmbedUrlResult().withStatus(200).withEmbedUrl("url"));
        mvc.perform(get("/reports/123/embedUrl")
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(content().json("\"url\""));
    }

    @Test
    @DataSet("reports.yml")
    public void testRegisterUserWithError() throws Exception {
        User user = User.builder().userName("").email("email@address").withRole(new Role(EntityOp.Read, "report", "*")).build();
        com.amazonaws.services.quicksight.model.User quicksightUser = new com.amazonaws.services.quicksight.model.User()
                .withUserName(QUICK_SIGHT_ROLE_NAME + "/different.email@address");
        when(quickSightClientFactory.listUsersClient()).thenReturn(amazonQuickSight);
        when(amazonQuickSight.listUsers(any())).thenReturn(new ListUsersResult().withStatus(200).withUserList(List.of(quicksightUser)));
        when(quickSightClientFactory.registerUserClient(any())).thenReturn(amazonQuickSight);
        when(amazonQuickSight.registerUser(any())).thenReturn(new RegisterUserResult().withStatus(400));
        mvc.perform(get("/reports/123/embedUrl")
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(status().isBadRequest());
    }

    @Configuration
    @EnableJpaRepositories(basePackageClasses = ReportRepository.class)
    @EntityScan(basePackageClasses = Report.class)
    @Import({OptionalResponseControllerAdvice.class, ReportServiceConfiguration.class,ReportConfiguration.class, MockWebSecurityConfiguration.class})
    @EnableWebSecurity
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    public static class TestConfiguration{




    }
}
