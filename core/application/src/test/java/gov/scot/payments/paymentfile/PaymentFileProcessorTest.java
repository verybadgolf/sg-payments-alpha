package gov.scot.payments.paymentfile;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.event.S3EventNotification;
import com.amazonaws.services.s3.model.S3Object;
import com.fasterxml.jackson.core.JsonProcessingException;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.paymentfile.PaymentFile;
import gov.scot.payments.model.paymentfile.PaymentFileStatus;
import gov.scot.payments.model.customer.Service;
import gov.scot.payments.model.paymentfile.event.FileTranslationFailedEvent;
import gov.scot.payments.model.paymentfile.event.FileTranslationSuccessEvent;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.event.PaymentInstructionCreatedEvent;
import gov.scot.payments.model.paymentinstruction.Recipient;
import org.javamoney.moneta.Money;
import org.joda.time.DateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.messaging.MessageChannel;

import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class PaymentFileProcessorTest {

    private PaymentFileProcessor processor;
    private AmazonS3 s3Client;
    private Function<String, Optional<PaymentFileTranslator>> fileTranslatorFactory;
    private Function<String, Optional<Service>> serviceLookup;
    private MessageChannel messageChannel;

    @BeforeEach
    public void setUp(){
        s3Client = mock(AmazonS3.class);
        fileTranslatorFactory = mock(Function.class);
        serviceLookup = mock(Function.class);
        when(fileTranslatorFactory.apply(anyString())).thenReturn(Optional.empty());
        when(serviceLookup.apply(anyString())).thenReturn(Optional.empty());
        messageChannel = mock(MessageChannel.class);
        processor = new PaymentFileProcessor(s3Client,fileTranslatorFactory,serviceLookup,serviceLookup,messageChannel);
    }

    @Test
    public void testOnS3EventIgnore() throws JsonProcessingException {
        assertNull(processor.convertS3Event(generateNotification("ObjectRemoved:Delete","123","abc/def.csv")));
        assertNull(processor.convertS3Event(generateNotification("ObjectRemoved:DeleteMarkerCreated","123","abc/def.csv")));
        verify(serviceLookup,times(2)).apply(anyString());
    }

    @Test
    public void testOnS3EventNoServiceForBucket() throws JsonProcessingException{
        assertNull(processor.convertS3Event(generateNotification("ObjectCreated:Post","456","abc/def.csv")));
        verify(serviceLookup,times(1)).apply(anyString());
    }

    @Test
    public void testOnS3EventValid() throws JsonProcessingException{
        S3EventNotification s3Event = generateNotification("ObjectCreated:Post", "123", "/abc/def.csv");
        Service service = Service.builder()
                .folder("abc")
                .fileFormat("ILF")
                .id("service1")
                .createdBy("user")
                .customerId("customer1")
                .paymentValidationRules(Collections.emptyList())
                .build();
        PaymentFile event = PaymentFile.builder()
                .createdAt(LocalDateTime.ofInstant(Instant.ofEpochMilli(s3Event.getRecords().get(0).getEventTime().toInstant().getMillis()), ZoneId.systemDefault()))
                .url(URI.create("s3://123/abc/def.csv#1"))
                .service("service1")
                .name("def.csv:1")
                .type("ILF")
                .build();
        when(serviceLookup.apply(anyString())).thenReturn(Optional.of(service));
        assertThat(processor.convertS3Event(s3Event).getPayload()).isEqualToIgnoringGivenFields(event,"id","timestamp");
        verify(serviceLookup,times(1)).apply(anyString());
    }

    @Test
    public void testCreateEventsNoTranslator(){
        Service service = Service.builder()
                .folder("abc")
                .fileFormat("ILF")
                .id("service1")
                .createdBy("user")
                .customerId("customer1")
                .paymentValidationRules(Collections.emptyList())
                .build();
        PaymentFile paymentFile = PaymentFile.builder()
                .url(URI.create("s3://123/abc/def.csv"))
                .service("service1")
                .name("abc.csv")
                .type("ILF")
                .build();
        List<? extends Event> events = processor.createEvents(paymentFile,null,service);
        assertEquals(1,events.size());
        verify(s3Client,never()).getObject(any());
        FileTranslationFailedEvent failedEvent = (FileTranslationFailedEvent)events.get(0);
        assertEquals(PaymentFileStatus.Failed,failedEvent.getPayload().getStatus());
        assertNotNull(failedEvent.getPayload().getStatusMessage());
    }

    @Test
    public void testCreateEventsNoService(){
        PaymentFile paymentFile = PaymentFile.builder()
                .url(URI.create("s3://123/abc/def.csv"))
                .service("service1")
                .name("abc.csv")
                .type("ILF")
                .build();
        PaymentFileTranslator translator = mock(PaymentFileTranslator.class);
        List<? extends Event> events = processor.createEvents(paymentFile,translator,null);
        assertEquals(1,events.size());
        verify(s3Client,never()).getObject(any());
        FileTranslationFailedEvent failedEvent = (FileTranslationFailedEvent)events.get(0);
        assertEquals(PaymentFileStatus.Failed,failedEvent.getPayload().getStatus());
        assertNotNull(failedEvent.getPayload().getStatusMessage());
    }

    @Test
    public void testCreateEventsTranslationIOError(){
        PaymentFile paymentFile = PaymentFile.builder()
                .url(URI.create("s3://123/abc/def.csv"))
                .service("service1")
                .name("abc.csv")
                .type("ILF")
                .build();
        S3Object s3Object = spy(new S3Object());
        when(s3Object.getObjectContent()).thenThrow(new RuntimeException("IO Error"));
        when(s3Client.getObject(any())).thenReturn(s3Object);
        PaymentFileTranslator translator = mock(PaymentFileTranslator.class);
        Service service = Service.builder()
                .folder("abc")
                .fileFormat("ILF")
                .id("service1")
                .createdBy("user")
                .customerId("customer1")
                .paymentValidationRules(Collections.emptyList())
                .build();
        List<? extends Event> events = processor.createEvents(paymentFile,translator,service);
        verify(s3Client,times(1)).getObject(any());
        assertEquals(1,events.size());
        FileTranslationFailedEvent failedEvent = (FileTranslationFailedEvent)events.get(0);
        assertEquals(PaymentFileStatus.Failed,failedEvent.getPayload().getStatus());
        assertEquals("IO Error",failedEvent.getPayload().getStatusMessage());
        assertEquals(paymentFile.getName(),failedEvent.getPayload().getName());
    }

    @Test
    public void testCreateEventsTranslationFailure() throws FileTranslateException, FileParseException {
        PaymentFile paymentFile = PaymentFile.builder()
                .url(URI.create("s3://123/abc/def.csv"))
                .service("service1")
                .name("abc.csv")
                .type("ILF")
                .build();
        S3Object s3Object = new S3Object();
        InputStream stream = InputStream.nullInputStream();
        s3Object.setObjectContent(stream);
        when(s3Client.getObject(any())).thenReturn(s3Object);
        PaymentFileTranslator translator = mock(PaymentFileTranslator.class);
        when(translator.generatePaymentInstructionsFromFile(any(),any())).thenThrow(new FileParseException("Translation Failed"));
        Service service = Service.builder()
                .folder("abc")
                .fileFormat("ILF")
                .id("service1")
                .createdBy("user")
                .customerId("customer1")
                .paymentValidationRules(Collections.emptyList())
                .build();
        List<? extends Event> events = processor.createEvents(paymentFile,translator,service);
        verify(s3Client,times(1)).getObject(any());
        verify(translator,times(1)).generatePaymentInstructionsFromFile(eq(paymentFile),any());
        assertEquals(1,events.size());
        FileTranslationFailedEvent failedEvent = (FileTranslationFailedEvent)events.get(0);
        assertEquals(PaymentFileStatus.Failed,failedEvent.getPayload().getStatus());
        assertEquals("Translation Failed",failedEvent.getPayload().getStatusMessage());
        assertEquals(paymentFile.getName(),failedEvent.getPayload().getName());
    }

    @Test
    public void testCreateEventsTranslationSuccess() throws FileTranslateException, FileParseException {
        PaymentFile paymentFile = PaymentFile.builder()
                .url(URI.create("s3://123/abc/def.csv"))
                .service("service1")
                .name("abc.csv")
                .type("ILF")
                .build();
        S3Object s3Object = new S3Object();
        InputStream stream = InputStream.nullInputStream();
        s3Object.setObjectContent(stream);
        when(s3Client.getObject(any())).thenReturn(s3Object);
        PaymentFileTranslator translator = mock(PaymentFileTranslator.class);
        PaymentInstruction payment1 = PaymentInstruction.builder()
                .recipient(Recipient.builder().build())
                .targetAmount(Money.of(new BigDecimal("100"),"GBP"))
                .service("service1")
                .build();
        PaymentInstruction payment2 = PaymentInstruction.builder()
                .recipient(Recipient.builder().build())
                .targetAmount(Money.of(new BigDecimal("200"),"GBP"))
                .service("service1")
                .build();
        when(translator.generatePaymentInstructionsFromFile(any(),any())).thenReturn(List.of(payment1,payment2));
        Service service = Service.builder()
                .folder("abc")
                .fileFormat("ILF")
                .id("service1")
                .createdBy("user")
                .customerId("customer1")
                .paymentValidationRules(Collections.emptyList())
                .build();
        List<? extends Event> events = processor.createEvents(paymentFile,translator,service);
        verify(s3Client,times(1)).getObject(any());
        verify(translator,times(1)).generatePaymentInstructionsFromFile(eq(paymentFile),any());
        assertEquals(3,events.size());
        FileTranslationSuccessEvent successEvent = (FileTranslationSuccessEvent)events.get(2);
        assertEquals(PaymentFileStatus.Translated,successEvent.getPayload().getStatus());
        assertEquals(paymentFile.getName(),successEvent.getPayload().getName());
        PaymentInstructionCreatedEvent paymentEvent1 = (PaymentInstructionCreatedEvent)events.get(0);
        assertThat(paymentEvent1.getPayload()).isEqualToIgnoringGivenFields(payment1,"id","timestamp");
        PaymentInstructionCreatedEvent paymentEvent2 = (PaymentInstructionCreatedEvent)events.get(1);
        assertThat(paymentEvent2.getPayload()).isEqualToIgnoringGivenFields(payment2,"id","timestamp");
    }


    public static S3EventNotification generateNotification(String type, String s3Bucket, String folderAndFile){
        Path folderAndFilePath = Path.of(folderAndFile);
        String file = URLEncoder.encode(folderAndFilePath.getName(folderAndFilePath.getNameCount()-1).toString(),StandardCharsets.UTF_8);
        String folder = folderAndFilePath.subpath(0,folderAndFilePath.getNameCount()-1).toString().replace(File.separatorChar, '/');
        String key = folder+"/"+file;
        S3EventNotification.UserIdentityEntity user = new S3EventNotification.UserIdentityEntity("application");
        S3EventNotification.S3BucketEntity bucket = new S3EventNotification.S3BucketEntity(s3Bucket
                ,null
                ,"");
        S3EventNotification.S3ObjectEntity object = new S3EventNotification.S3ObjectEntity(key
                , 0L
                ,""
                ,"1"
                ,"");
        S3EventNotification.S3Entity s3Entity = new S3EventNotification.S3Entity(""
                ,bucket
                ,object
                ,"");
        S3EventNotification.S3EventNotificationRecord record = new S3EventNotification.S3EventNotificationRecord("eu-west-1"
                ,type
                ,""
                , DateTime.now().toString()
                ,""
                ,null
                ,null
                ,s3Entity
                ,user);
        return new S3EventNotification(Collections.singletonList(record));
    }

}
