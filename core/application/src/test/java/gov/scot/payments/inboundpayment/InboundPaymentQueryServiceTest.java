package gov.scot.payments.inboundpayment;

import gov.scot.payments.model.inboundpayment.PaymentStatus;
import gov.scot.payments.model.user.EntityOp;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Client;
import gov.scot.payments.model.user.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class InboundPaymentQueryServiceTest {

    private InboundPaymentQueryService service;
    private TemporalInboundPaymentRepository temporalInboundPaymentRepository;
    private NonTemporalInboundPaymentRepository nonTemporalInboundPaymentRepository;

    @BeforeEach
    public void setUp(){
        temporalInboundPaymentRepository = mock(TemporalInboundPaymentRepository.class);
        nonTemporalInboundPaymentRepository = mock(NonTemporalInboundPaymentRepository.class);
        service = new InboundPaymentQueryService(temporalInboundPaymentRepository, nonTemporalInboundPaymentRepository);
    }

    @Test
    public void testGetById(){
        service.getPaymentById(UUID.randomUUID(), User.admin());
        verify(nonTemporalInboundPaymentRepository,times(1)).findByPaymentId(any());
        verifyNoMoreInteractions(nonTemporalInboundPaymentRepository, temporalInboundPaymentRepository);
    }

    @Test
    public void testGetByIdWithAcl(){
        service.getPaymentById(UUID.randomUUID(), User.builder()
                .userName("")
                .email("")
                .withRole(new Role(EntityOp.Read,"service","123"))
                .build());
        verify(nonTemporalInboundPaymentRepository,times(1)).findByPaymentIdWithAcl(any());
        verifyNoMoreInteractions(nonTemporalInboundPaymentRepository, temporalInboundPaymentRepository);
    }

    @Test
    public void testGetHistoryById(){
        service.getPaymentHistoryById(UUID.randomUUID(), User.admin());
        verify(temporalInboundPaymentRepository,times(1)).findAllByInboundPaymentId(any());
        verifyNoMoreInteractions(nonTemporalInboundPaymentRepository, temporalInboundPaymentRepository);
    }

    @Test
    public void testGetHistoryByIdWithAcl(){
        service.getPaymentHistoryById(UUID.randomUUID(), User.builder()
                .userName("")
                .email("")
                .withRole(new Role(EntityOp.Read,"service","123"))
                .build());
        verify(temporalInboundPaymentRepository,times(1)).findAllByInboundPaymentIdWithAcl(any());
        verifyNoMoreInteractions(nonTemporalInboundPaymentRepository, temporalInboundPaymentRepository);
    }

    @Test
    public void testGetStatuses(){
        List<PaymentStatus> statuses = service.getPaymentStatuses();
        assertEquals(statuses, Arrays.asList(PaymentStatus.values()));
        verifyNoMoreInteractions(nonTemporalInboundPaymentRepository, temporalInboundPaymentRepository);
    }

    @Test
    public void testGetAll(){
        when(nonTemporalInboundPaymentRepository.findAll(any(Pageable.class))).thenReturn(new PageImpl<>(Collections.emptyList()));
        service.getAllLatestPayments(mock(Pageable.class),mock(PagedResourcesAssembler.class), User.admin());
        verify(nonTemporalInboundPaymentRepository,times(1)).findAll(any(Pageable.class));
        verifyNoMoreInteractions(nonTemporalInboundPaymentRepository, temporalInboundPaymentRepository);
    }

    @Test
    public void testGetAllWithAcl(){
        when(nonTemporalInboundPaymentRepository.findAllWithAcl(any(Pageable.class))).thenReturn(new PageImpl<>(Collections.emptyList()));
        service.getAllLatestPayments(mock(Pageable.class),mock(PagedResourcesAssembler.class), User.builder()
                .userName("")
                .email("")
                .withRole(new Role(EntityOp.Read,"service","123"))
                .build());
        verify(nonTemporalInboundPaymentRepository,times(1)).findAllWithAcl(any(Pageable.class));
        verifyNoMoreInteractions(nonTemporalInboundPaymentRepository, temporalInboundPaymentRepository);
    }

    @Test
    public void testQuery(){
        when(nonTemporalInboundPaymentRepository.findAll(any(Specification.class),any(Pageable.class))).thenReturn(new PageImpl<>(Collections.emptyList()));
        service.queryLatestPayments("service==ilf",mock(Pageable.class),mock(PagedResourcesAssembler.class), User.admin());
        verify(nonTemporalInboundPaymentRepository,times(1)).findAll(any(Specification.class),any(Pageable.class));
        verifyNoMoreInteractions(nonTemporalInboundPaymentRepository, temporalInboundPaymentRepository);
    }

    @Test
    public void testParseQueryInvalid(){
        assertThrows(ResponseStatusException.class,() -> service.queryLatestPayments("service=ilf",mock(Pageable.class),mock(PagedResourcesAssembler.class), User.admin()));
    }

}
