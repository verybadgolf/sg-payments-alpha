package gov.scot.payments.cardpayment;

import gov.scot.payments.common.gateway.inbound.cardpayment.CardPaymentGateway;
import gov.scot.payments.model.cardpayment.*;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.web.client.RestOperations;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriBuilderFactory;

import java.net.URI;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class RemoteLookupCardPaymentGatewayProxyTest {

    private CardPaymentGatewayProxy proxy;
    private RestOperations restOperations;
    private String basePath;
    private UriBuilderFactory uriBuilderFactory;

    @BeforeEach
    public void setUp(){
        restOperations = mock(RestOperations.class);
        when(restOperations.postForEntity(any(URI.class),any(),any())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        uriBuilderFactory = new DefaultUriBuilderFactory();
        basePath = "http://localhost/test";
        Map<String, URI> map = Collections.singletonMap("worldpay", uriBuilderFactory.uriString(basePath+"/{name}").build("worldpay"));

        proxy = new RemoteLookupPaymentGatewayProxy(n -> Optional.ofNullable(map.get(n)),() -> restOperations,uriBuilderFactory);
    }

    @Test
    public void testAuthNotPresent(){
        var request = CardPaymentGatewayAuthRequest.builder()
                .amount(Money.of(1,"GBP"))
                .paymentRef(UUID.randomUUID())
                .build();
        assertThrows(IllegalArgumentException.class,() -> proxy.authorize("stripe",request));
    }

    @Test
    public void testSubmitNotPresent(){
        var request = CardPaymentGatewaySubmitRequest.builder()
                .ref("123")
                .paymentRef(UUID.randomUUID())
                .build();
        assertThrows(IllegalArgumentException.class,() -> proxy.submit("stripe",request));
    }

    @Test
    public void testCancelNotPresent(){
        var request = CardPaymentGatewayCancelRequest.builder()
                .paymentRef(UUID.randomUUID())
                .ref("123")
                .build();
        assertThrows(IllegalArgumentException.class,() -> proxy.cancel("stripe",request));
    }

    @Test
    public void testAuthPresent(){
        var request = CardPaymentGatewayAuthRequest.builder()
                .amount(Money.of(1,"GBP"))
                .paymentRef(UUID.randomUUID())
                .build();
        proxy.authorize("worldpay",request);
        URI path = URI.create(basePath+"/worldpay/cardPayment/authorize");
        verify(restOperations,times(1)).postForEntity(path,request, CardPaymentGatewayAuthResponse.class);
    }

    @Test
    public void testCancelPresent(){
        var request = CardPaymentGatewayCancelRequest.builder()
                .paymentRef(UUID.randomUUID())
                .ref("123")
                .build();
        proxy.cancel("worldpay",request);
        URI path = URI.create(basePath+"/worldpay/cardPayment/cancel");
        verify(restOperations,times(1)).postForEntity(path,request, CardPaymentGatewayCancelResponse.class);
    }

    @Test
    public void testSubmitPresent(){
        var request = CardPaymentGatewaySubmitRequest.builder()
                .ref("123")
                .paymentRef(UUID.randomUUID())
                .build();
        proxy.submit("worldpay",request);
        URI path = URI.create(basePath+"/worldpay/cardPayment/submit");
        verify(restOperations,times(1)).postForEntity(path,request, CardPaymentGatewaySubmitResponse.class);
    }

}
