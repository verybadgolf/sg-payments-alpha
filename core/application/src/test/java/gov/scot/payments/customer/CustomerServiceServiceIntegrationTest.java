package gov.scot.payments.customer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.spring.api.DBRider;
import gov.scot.payments.AbstractKafkaTest;
import gov.scot.payments.MockWebSecurityConfiguration;
import gov.scot.payments.common.OptionalResponseControllerAdvice;
import gov.scot.payments.common.kafka.KafkaNullConfiguration;
import gov.scot.payments.customer.spring.CustomerServiceServiceBindings;
import gov.scot.payments.model.customer.PaymentValidationRule;
import gov.scot.payments.model.customer.Service;
import gov.scot.payments.WithUser;
import gov.scot.payments.model.paymentinstruction.AccountNumber;
import gov.scot.payments.model.paymentinstruction.SortCode;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.aws.autoconfigure.context.ContextCredentialsAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextResourceLoaderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.jdbc.AmazonRdsDatabaseAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.messaging.MessagingAutoConfiguration;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.MediaType;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.messaging.MessageChannel;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureDataJpa
@AutoConfigureWebMvc
@AutoConfigureMockMvc
@DBRider
@EnableAutoConfiguration(exclude = {MessagingAutoConfiguration.class
        , ContextStackAutoConfiguration.class
        , ContextResourceLoaderAutoConfiguration.class
        , ContextRegionProviderAutoConfiguration.class
        , ContextCredentialsAutoConfiguration.class
        , AmazonRdsDatabaseAutoConfiguration.class
})
@EmbeddedKafka(partitions = 1
        ,topics = {"test-serviceChangedEvents"})
@SpringBootTest(classes = CustomerServiceServiceIntegrationTest.TestConfiguration.class, properties = {
        "debug=true",
        "payments.environment=test"
})
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@Tag("kafka")
@Tag("db")
@WithUser
public class CustomerServiceServiceIntegrationTest extends AbstractKafkaTest{

    @Autowired
    private MockMvc mvc;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    @DataSet("services.yml")
    public void testAddService() throws Exception {

        var rule = PaymentValidationRule.builder()
                .name("test")
                .rule("testRule")
                .createdBy("user")
                .messageTemplate("This failed testRule")
                .createdAt(LocalDateTime.parse("2020-01-01T10:10:10"))
                .build();

        var serviceObj = Service.builder()
                .id("service_id")
                .fileFormat("CSV")
                .folder("folder")
                .createdBy("user")
                .paymentValidationRules(List.of(rule))
                .customerId("customer_1_1")
                .inboundAccount(new AccountNumber("00000000"))
                .inboundSortCode(new SortCode("000000"))
                .outboundAccount(new AccountNumber("00000000"))
                .outboundSortCode(new SortCode("000000"))
                .addMetadata("code","123")
                .createdAt(LocalDateTime.parse("2020-01-01T10:10:10")).build();

        var json = objectMapper.writeValueAsString(serviceObj);
        mvc.perform(post("/services")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        sleep();

        verifyEvents("test-serviceChangedEvents", StringDeserializer.class,StringDeserializer.class,Duration.ofSeconds(2), list -> assertEquals(1, list.size()));

    }

    @Test
    @DataSet("services.yml")
    @WithUser(entityType = "customer", entityId = "customer_1_2")
    public void testAddServiceWithAcl() throws Exception {
        var rule = PaymentValidationRule.builder()
                .name("test")
                .rule("testRule")
                .createdBy("user")
                .messageTemplate("This failed testRule")
                .createdAt(LocalDateTime.parse("2020-01-01T10:10:10"))
                .build();

        var serviceObj = Service.builder()
                .id("service_id")
                .fileFormat("CSV")
                .folder("folder")
                .createdBy("user")
                .paymentValidationRules(List.of(rule))
                .customerId("customer_1_1")
                .createdAt(LocalDateTime.parse("2020-01-01T10:10:10")).build();

        var json = objectMapper.writeValueAsString(serviceObj);
        mvc.perform(post("/services")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @DataSet("services.yml")
    public void testGetServices() throws Exception {

        mvc.perform(get("/services"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(4)));
    }

    @Test
    @DataSet("services.yml")
    @WithUser(entityType = "service", entityId = "ilf_customer")
    public void testGetServicesWithAcl() throws Exception {
        mvc.perform(get("/services"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    @DataSet("services.yml")
    public void testFindServiceById() throws Exception {

        String response = "{\n" +
                "  \"createdAt\" : \"2019-01-01T10:23:54\",\n" +
                "  \"folder\" : \"ilf_folder\",\n" +
                "  \"createdBy\" : \"user\",\n" +
                "  \"fileFormat\" : \"CSV\",\n" +
                "  \"id\" : \"ilf_customer\",\n" +
                "  \"customerId\" : \"customer_2_1\",\n" +
                "  \"outboundSortCode\" : \"000000\",\n" +
                "  \"outboundAccount\" : \"00000000\",\n" +
                "  \"inboundSortCode\" : \"000000\",\n" +
                "  \"inboundAccount\" : \"00000000\",\n" +
                "  \"metadata\" : {\n" +
                "    \"code\" : \"123\"\n" +
                "  },\n" +
                "  \"paymentValidationRules\" : [ {\n" +
                "    \"createdBy\" : \"2019-01-01 10:23:54\",\n" +
                "    \"messageTemplate\" : \"This failed rule\",\n" +
                "    \"name\" : \"test\",\n" +
                "    \"precedence\" : \"0\",\n" +
                "    \"rule\" : \"targetAmount > 0\",\n" +
                "    \"createdAt\" : \"2019-01-01T10:23:54\"\n" +
                "  } ]\n" +
                "}";

        mvc.perform(get("/services/ilf_customer"))
                .andExpect(status().isOk())
                .andExpect(content().json(response));
    }

    @Test
    @DataSet("services.yml")
    @WithUser(entityType = "service", entityId = "ilf_customer1")
    public void testFindServiceByIdWithAcl() throws Exception {
        mvc.perform(get("/services/ilf_customer"))
                .andExpect(status().isForbidden());
    }

    @Test
    @DataSet("services.yml")
    public void testFindServiceByCustomerId() throws Exception {

        mvc.perform(get("/services/customer/customer_2_1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));
        mvc.perform(get("/services/customer/customer_1_1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)));
    }

    @Test
    @DataSet("services.yml")
    @WithUser(entityType = "service", entityId = "ilf_customer_3")
    public void testFindServiceByCustomerIdWithAcl() throws Exception {
        mvc.perform(get("/services/customer/customer_1_1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    @DataSet("services.yml")
    public void testPutService() throws Exception {
        var serviceObj = Service.builder()
                .id("ilf_customer_3")
                .fileFormat("csv")
                .folder("folder_ilf_customer")
                .customerId("test_customer")
                .createdBy("user")
                .paymentValidationRules(Collections.emptyList())
                .createdAt(LocalDateTime.parse("2025-01-01T10:10:10")).build();

        var json = objectMapper.writeValueAsString(serviceObj);

        mvc.perform(put("/services")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        verifyEvents("test-serviceChangedEvents", StringDeserializer.class,StringDeserializer.class,Duration.ofSeconds(2), list -> assertEquals(1, list.size()));
    }

    @Test
    @DataSet("services.yml")
    @WithUser(entityType = "service", entityId = "ilf_customer_2")
    public void testPutServiceWithAcl() throws Exception {
        var serviceObj = Service.builder()
                .id("ilf_customer_3")
                .fileFormat("csv")
                .folder("folder_ilf_customer")
                .customerId("test_customer")
                .createdBy("user")
                .paymentValidationRules(Collections.emptyList())
                .createdAt(LocalDateTime.parse("2025-01-01T10:10:10")).build();

        var json = objectMapper.writeValueAsString(serviceObj);

        mvc.perform(put("/services")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @DataSet("services.yml")
    public void testDeleteService() throws Exception {

        mvc.perform(get("/services/ilf_customer_2"))
                .andExpect(status().isOk());

        mvc.perform(delete("/services/ilf_customer_2")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        sleep();

        verifyEvents("test-serviceChangedEvents", StringDeserializer.class,StringDeserializer.class,Duration.ofSeconds(2), list -> assertEquals(1, list.size()));

    }

    @Test
    @DataSet("services.yml")
    @WithUser(entityType = "service", entityId = "ilf_customer_1")
    public void testDeleteServiceWithAcl() throws Exception {
        mvc.perform(delete("/services/ilf_customer_2")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isForbidden());

    }

    @Configuration
    @EnableJpaRepositories(basePackageClasses = {ServiceRepository.class} )
    @EntityScan(basePackageClasses = {Service.class} )
    @Import({OptionalResponseControllerAdvice.class, KafkaNullConfiguration.class, MockWebSecurityConfiguration.class})
    @EnableBinding({CustomerServiceServiceBindings.class})
    @EnableWebSecurity
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    public static class TestConfiguration extends AbstractKafkaTest.TestConfiguration {

        @Bean
        public CustomerServiceService customerServiceTest(@Qualifier("service-updated-events") MessageChannel serviceUpdates, ServiceRepository serviceRepository){
            return new CustomerServiceService(serviceUpdates, serviceRepository);
        }

    }


}
