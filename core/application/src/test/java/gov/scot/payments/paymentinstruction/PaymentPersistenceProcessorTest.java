package gov.scot.payments.paymentinstruction;

import gov.scot.payments.model.paymentinstruction.AccountNumber;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.Recipient;
import gov.scot.payments.model.paymentinstruction.SortCode;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

import static gov.scot.payments.model.paymentinstruction.PaymentInstruction.GBP_CURRENCY;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class PaymentPersistenceProcessorTest {

    private PaymentPersistenceProcessor processor;
    private TemporalPaymentRepository temporalPaymentRepository;
    private NonTemporalPaymentRepository nonTemporalPaymentRepository;
    private TransactionOperations transactionTemplate;

    @BeforeEach
    public void setUp(){
        temporalPaymentRepository = mock(TemporalPaymentRepository.class);
        nonTemporalPaymentRepository = mock(NonTemporalPaymentRepository.class);
        transactionTemplate = mock(TransactionTemplate.class);
        doAnswer(i -> {
            i.getArgument(0, TransactionCallback.class).doInTransaction(mock(TransactionStatus.class));
            return null;
        })
                .when(transactionTemplate)
                .execute(any());
        processor = new PaymentPersistenceProcessor(temporalPaymentRepository,nonTemporalPaymentRepository,transactionTemplate);
    }

    @Test
    public void testPersistNew(){
        when(nonTemporalPaymentRepository.findById(any())).thenReturn(Optional.empty());
        var payment = PaymentInstruction.builder()
                .targetAmount(Money.of(new BigDecimal("10"),"GBP"))
                .timestamp(LocalDateTime.now())
                .service("")
                .recipient(Recipient.builder().build())
                .paymentFile("")
                .targetSettlementCurrency(GBP_CURRENCY)
                .latestTargetSettlementDate(LocalDate.parse("2020-01-01"))
                .build();
        processor.persist(payment);
        verify(temporalPaymentRepository,times(1)).save(any());
        verify(nonTemporalPaymentRepository,times(1)).save(any());
        verify(nonTemporalPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(temporalPaymentRepository,nonTemporalPaymentRepository);
    }

    @Test
    public void testPersistExistsOlder(){
        var existing = PaymentInstruction.builder()
                .timestamp(LocalDateTime.now().minusSeconds(10))
                .targetAmount(Money.of(new BigDecimal("10"),"GBP"))
                .service("")
                .recipient(Recipient.builder().build())
                .paymentFile("")
                .targetSettlementCurrency(GBP_CURRENCY)
                .latestTargetSettlementDate(LocalDate.parse("2020-01-01"))
                .build();
        var payment = PaymentInstruction.builder()
                .timestamp(LocalDateTime.now())
                .targetAmount(Money.of(new BigDecimal("100"),"GBP"))
                .service("")
                .recipient(Recipient.builder().build())
                .paymentFile("")
                .targetSettlementCurrency(GBP_CURRENCY)
                .latestTargetSettlementDate(LocalDate.parse("2020-01-01"))
                .build();
        when(nonTemporalPaymentRepository.findById(any())).thenReturn(Optional.of(FlattenedPayment.from(existing)));
        processor.persist(payment);
        verify(temporalPaymentRepository,times(1)).save(any());
        verify(nonTemporalPaymentRepository,times(1)).save(any());
        verify(nonTemporalPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(temporalPaymentRepository,nonTemporalPaymentRepository);
    }

    @Test
    public void testPersistExistsNewer(){
        var existing = PaymentInstruction.builder()
                .timestamp(LocalDateTime.now())
                .targetAmount(Money.of(new BigDecimal("10"),"GBP"))
                .service("")
                .recipient(Recipient.builder().build())
                .paymentFile("")
                .targetSettlementCurrency(GBP_CURRENCY)
                .latestTargetSettlementDate(LocalDate.parse("2020-01-01"))
                .build();
        var payment = PaymentInstruction.builder()
                .timestamp(LocalDateTime.now().minusSeconds(10))
                .targetAmount(Money.of(new BigDecimal("100"),"GBP"))
                .service("")
                .recipient(Recipient.builder().build())
                .paymentFile("")
                .targetSettlementCurrency(GBP_CURRENCY)
                .latestTargetSettlementDate(LocalDate.parse("2020-01-01"))
                .build();
        when(nonTemporalPaymentRepository.findById(any())).thenReturn(Optional.of(FlattenedPayment.from(existing)));
        processor.persist(payment);
        verify(temporalPaymentRepository,times(1)).save(any());
        verify(nonTemporalPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(temporalPaymentRepository,nonTemporalPaymentRepository);
    }
}
