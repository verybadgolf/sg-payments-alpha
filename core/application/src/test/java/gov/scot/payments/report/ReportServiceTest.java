package gov.scot.payments.report;

import com.amazonaws.regions.Region;
import com.amazonaws.services.quicksight.AmazonQuickSight;
import com.amazonaws.services.quicksight.model.*;
import gov.scot.payments.model.report.Report;
import gov.scot.payments.model.user.Client;
import gov.scot.payments.model.user.User;
import gov.scot.payments.security.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.cloud.aws.core.region.RegionProvider;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class ReportServiceTest {

    private ReportService service;
    private ReportRepository repository;
    private UserRepository userRepository;
    private AmazonQuickSightClientFactory quickSightClientFactory;
    private AmazonQuickSight directoryQuicksight;
    private AmazonQuickSight embedUrlQuicksight;
    private RegionProvider regionProvider;
    private Region region;
    private ListUsersResult listUsersResult;
    private RegisterUserResult registerUserResult;
    private GetDashboardEmbedUrlResult getDashboardEmbedUrlResult;

    private static final String AWS_ACCOUNT_ID = "123";
    private static final String QUICKSIGHT_ROLE_ARN = "arn:aws:iam::123:policy/quickSightRole";
    private static final String QUICKSIGHT_ROLE_NAME = "quickSightRole";
    private static final String REGION_NAME = "eu-west-1";

    @BeforeEach
    public void setUp(){
        repository = mock(ReportRepository.class);
        quickSightClientFactory = mock(AmazonQuickSightClientFactory.class);
        directoryQuicksight = mock(AmazonQuickSight.class);
        embedUrlQuicksight = mock(AmazonQuickSight.class);
        regionProvider = mock(RegionProvider.class);
        region = mock(Region.class);
        listUsersResult = mock(ListUsersResult.class);
        registerUserResult = mock(RegisterUserResult.class);
        getDashboardEmbedUrlResult = mock(GetDashboardEmbedUrlResult.class);
        userRepository = mock(UserRepository.class);
        service = new ReportService(repository,quickSightClientFactory,userRepository,AWS_ACCOUNT_ID, QUICKSIGHT_ROLE_ARN, QUICKSIGHT_ROLE_NAME);
    }

    @Test
    public void testRegister(){
        doAnswer(i -> i.getArgument(0)).when(repository).save(any());
        Report report = service.registerReport("123","789","456", User.admin());
        assertEquals("789",report.getName());
        assertEquals("456",report.getDashboardId());
        verify(repository,times(1)).save(any());
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void testList(){
        Report report = Report.builder()
                .dashboardId("456")
                .id("123")
                .name("789")
                .createdBy("application")
                .build();
        when(repository.findAll()).thenReturn(Collections.singletonList(report));
        List<Report> reports = service.getAllReports();
        assertEquals(1,reports.size());
        report = reports.get(0);
        assertEquals("789",report.getName());
        assertEquals("456",report.getDashboardId());
        verify(repository,times(1)).findAll();
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void testEmbedNoReport(){
        quickSightUserExists(User.standard());
        when(repository.findById(anyString())).thenReturn(Optional.empty());
        assertThrows(ResponseStatusException.class, () -> service.getEmbedUrl("", User.standard()));
    }

    @Test
    public void testEmbedError(){
        quickSightUserExists(User.standard());
        Report report = Report.builder()
                .dashboardId("456")
                .id("123")
                .name("789")
                .createdBy("application")
                .build();
        when(repository.findById(anyString())).thenReturn(Optional.of(report));
        when(quickSightClientFactory.embedUrlClient(any())).thenReturn(embedUrlQuicksight);
        when(embedUrlQuicksight.getDashboardEmbedUrl(any())).thenThrow(new RuntimeException());
        assertThrows(RuntimeException.class,() -> service.getEmbedUrl("123", User.standard()));
    }

    @Test
    public void testEmbedNon200Response(){
        quickSightUserExists(User.standard());
        Report report = Report.builder()
                .dashboardId("456")
                .id("123")
                .name("789")
                .createdBy("application")
                .build();
        when(repository.findById(anyString())).thenReturn(Optional.of(report));
        when(quickSightClientFactory.embedUrlClient(any())).thenReturn(embedUrlQuicksight);
        when(embedUrlQuicksight.getDashboardEmbedUrl(any())).thenReturn(new GetDashboardEmbedUrlResult().withStatus(400));
        when(regionProvider.getRegion()).thenReturn(region);
        when(region.getName()).thenReturn(REGION_NAME);
        assertThrows(ResponseStatusException.class,() -> service.getEmbedUrl("123", User.standard()));
    }

    @Test
    public void testEmbedSuccess(){
        quickSightUserExists(User.standard());
        Report report = Report.builder()
                .dashboardId("456")
                .id("123")
                .name("789")
                .createdBy("application")
                .build();
        when(repository.findById(anyString())).thenReturn(Optional.of(report));
        when(quickSightClientFactory.embedUrlClient(any())).thenReturn(embedUrlQuicksight);
        when(embedUrlQuicksight.getDashboardEmbedUrl(any())).thenReturn(getDashboardEmbedUrlResult);
        when(getDashboardEmbedUrlResult.getStatus()).thenReturn(200);
        when(getDashboardEmbedUrlResult.getEmbedUrl()).thenReturn("url");
        when(regionProvider.getRegion()).thenReturn(region);
        when(region.getName()).thenReturn(REGION_NAME);
        Optional<String> response = service.getEmbedUrl("123", User.standard());
        assertEquals("url",response.get());
    }

    @Test
    public void testQuickSightUserRegisteredWhenNotExists() {
        Report report = Report.builder()
                .dashboardId("456")
                .id("123")
                .name("789")
                .createdBy("application")
                .build();
        User user = User.standard();
        RegisterUserRequest registerUserRequest = new RegisterUserRequest()
                .withAwsAccountId(AWS_ACCOUNT_ID)
                .withNamespace("default")
                .withIdentityType(IdentityType.IAM)
                .withIamArn(QUICKSIGHT_ROLE_ARN)
                .withUserRole(UserRole.READER)
                .withSessionName(user.getEmail())
                .withEmail(user.getEmail());

        when(regionProvider.getRegion()).thenReturn(region);
        when(region.getName()).thenReturn(REGION_NAME);
        when(quickSightClientFactory.listUsersClient()).thenReturn(directoryQuicksight);
        when(directoryQuicksight.listUsers(any())).thenReturn(listUsersResult);
        when(listUsersResult.getStatus()).thenReturn(200);
        com.amazonaws.services.quicksight.model.User quicksightUser = mock(com.amazonaws.services.quicksight.model.User.class);
        when(listUsersResult.getUserList()).thenReturn(List.of(quicksightUser));
        when(quicksightUser.getUserName()).thenReturn(QUICKSIGHT_ROLE_ARN + "/notmatched@email.address");

        when(quickSightClientFactory.registerUserClient(any())).thenReturn(directoryQuicksight);
        when(directoryQuicksight.registerUser(registerUserRequest)).thenReturn(registerUserResult);
        when(registerUserResult.getStatus()).thenReturn(201);

        when(repository.findById(anyString())).thenReturn(Optional.of(report));
        when(quickSightClientFactory.embedUrlClient(any())).thenReturn(embedUrlQuicksight);
        when(embedUrlQuicksight.getDashboardEmbedUrl(any())).thenReturn(getDashboardEmbedUrlResult);
        when(getDashboardEmbedUrlResult.getStatus()).thenReturn(200);
        when(getDashboardEmbedUrlResult.getEmbedUrl()).thenReturn("url");

        service.getEmbedUrl("123", user);

        verify(directoryQuicksight, times(1)).registerUser(registerUserRequest);
    }

    @Test
    public void testRegisterQuickSightUserWithErrorResponse() {
        User user = User.standard();
        RegisterUserRequest registerUserRequest = new RegisterUserRequest()
                .withAwsAccountId(AWS_ACCOUNT_ID)
                .withNamespace("default")
                .withIdentityType(IdentityType.IAM)
                .withIamArn(QUICKSIGHT_ROLE_ARN)
                .withUserRole(UserRole.READER)
                .withSessionName(user.getEmail())
                .withEmail(user.getEmail());

        when(regionProvider.getRegion()).thenReturn(region);
        when(region.getName()).thenReturn(REGION_NAME);
        when(quickSightClientFactory.listUsersClient()).thenReturn(directoryQuicksight);
        when(directoryQuicksight.listUsers(any())).thenReturn(listUsersResult);
        when(listUsersResult.getStatus()).thenReturn(200);
        com.amazonaws.services.quicksight.model.User quicksightUser = mock(com.amazonaws.services.quicksight.model.User.class);
        when(listUsersResult.getUserList()).thenReturn(List.of(quicksightUser));
        when(quicksightUser.getEmail()).thenReturn("notmatched@email.address");

        when(quickSightClientFactory.registerUserClient(any())).thenReturn(directoryQuicksight);
        when(directoryQuicksight.registerUser(registerUserRequest)).thenReturn(registerUserResult);
        when(registerUserResult.getStatus()).thenReturn(400);

        assertThrows(ResponseStatusException.class,() -> service.getEmbedUrl("123", User.standard()));
    }

    private void quickSightUserExists(User user) {
        when(quickSightClientFactory.listUsersClient()).thenReturn(directoryQuicksight);
        when(directoryQuicksight.listUsers(any())).thenReturn(listUsersResult);
        when(listUsersResult.getStatus()).thenReturn(200);
        com.amazonaws.services.quicksight.model.User quicksightUser = mock(com.amazonaws.services.quicksight.model.User.class);
        when(listUsersResult.getUserList()).thenReturn(List.of(quicksightUser));
        when(quicksightUser.getUserName()).thenReturn(QUICKSIGHT_ROLE_NAME + '/' + user.getEmail());
    }
}
