package gov.scot.payments.model.paymentinstruction;

import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDateTime;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode
@Embeddable
public class Validation {

    private String message;
    @NonNull @Builder.Default private final LocalDateTime validatedAt = LocalDateTime.now();
    @NonNull @Builder.Default @Enumerated(EnumType.STRING) private final ValidationStatus status = ValidationStatus.Valid;

    public static Validation success() {
        return Validation.builder().build();
    }

    public Validation merge(Validation other) {
        if(other.status == ValidationStatus.Valid){
            if(status == ValidationStatus.Valid){
                return toBuilder()
                        .validatedAt(validatedAt.isAfter(other.validatedAt) ? validatedAt : other.validatedAt)
                        .build();
            } else{
                return this;
            }
        } else{
            return toBuilder()
                    .status(ValidationStatus.Invalid)
                    .validatedAt(validatedAt.isAfter(other.validatedAt) ? validatedAt : other.validatedAt)
                    .message(message == null ? other.message : message + ", " + other.message)
                    .build();
        }
    }
}
