package gov.scot.payments.model.cardpayment;

import lombok.*;

import java.util.HashMap;
import java.util.Map;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
public class CardPaymentGatewayCancelResponse {

    @NonNull @Builder.Default private CardPaymentStatus status = CardPaymentStatus.Cancelled;
    private String message;
    private String ref;
    @NonNull @Builder.Default private Map<String,String> metadata = new HashMap<>();

}
