package gov.scot.payments.model.core;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.CurrencyType;
import org.hibernate.type.StringType;
import org.hibernate.usertype.UserType;
import org.javamoney.moneta.Money;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Objects;

public class MoneyType implements UserType {
    @Override
    public int[] sqlTypes() {
        return new int[] {
                BigDecimalType.INSTANCE.sqlType(),
                StringType.INSTANCE.sqlType(),
        };
    }

    @Override
    public Class returnedClass() {
        return Money.class;
    }

    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        return Objects.equals(x,y);
    }

    @Override
    public int hashCode(Object x) throws HibernateException {
        return x.hashCode();
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner) throws HibernateException, SQLException {
        assert names.length == 2;
        BigDecimal amount = rs.getBigDecimal(names[0]) ;// already handles null check
        String currency = rs.getString(names[1]); // already handles null check
        return amount == null || currency == null
                ? null
                : Money.of( amount, currency );
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session) throws HibernateException, SQLException {
        if ( value == null ) {
            st.setNull(index, BigDecimalType.INSTANCE.sqlType());
            st.setNull(index+1, StringType.INSTANCE.sqlType());
        }
        else {
            final Money money = (Money) value;
            st.setBigDecimal(index, money.getNumberStripped());
            st.setString(index+1, money.getCurrency().getCurrencyCode());
        }
    }

    @Override
    public Object deepCopy(Object value) throws HibernateException {
        if (Objects.isNull(value))
            return null;
        Money val = (Money)value;
        return Money.of(val.getNumber(),val.getCurrency());
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable) value;
    }

    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return cached;
    }

    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return original;
    }
}
