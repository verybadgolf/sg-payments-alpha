package gov.scot.payments.model.cardpayment;

import lombok.*;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
public class CardAddress {

    private String postCode;
    private String city;
    private String country;
    private String address1;
    private String address2;
}
