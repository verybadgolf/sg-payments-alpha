package gov.scot.payments.model.paymentinstruction;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import lombok.*;
import org.springframework.util.StringUtils;

import javax.persistence.AttributeConverter;
import java.io.IOException;
import java.util.regex.Pattern;

@Value
public class AccountNumber {

    private static int ACCOUNT_NUMBER_LENGTH = 8;

    private static final Pattern regexNumeric = Pattern.compile("^[0-9]*$");

    private String value;

    public static AccountNumber fromString(String accountNumberStr) throws InvalidPaymentFieldException {
        if(accountNumberStr.length() != ACCOUNT_NUMBER_LENGTH){
            throw new InvalidPaymentFieldException("Invalid account number "+ accountNumberStr + " - must be 8 digits");
        }
        boolean isNumeric = regexNumeric.matcher(accountNumberStr).matches();
        if(!isNumeric){
            throw new InvalidPaymentFieldException("Invalid account number "+ accountNumberStr + " - must be numeric");
        }
        return new AccountNumber(accountNumberStr);
    }

    @Override
    public String toString() {
        return value;
    }

    public static class AccountNumberSerializer extends JsonSerializer<AccountNumber> {


        @Override
        public void serialize(AccountNumber value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeString(value.toString());
        }
    }

    public static class AccountNumberDeserializer extends JsonDeserializer<AccountNumber> {
        @Override
        public AccountNumber deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            try {
                return AccountNumber.fromString(p.getText());
            } catch (InvalidPaymentFieldException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static class JpaAccountNumberConverter implements AttributeConverter<AccountNumber, String> {

        @Override
        public String convertToDatabaseColumn(AccountNumber attribute) {
            return attribute == null ? null : attribute.toString();
        }

        @Override
        public AccountNumber convertToEntityAttribute(String dbData) {
            return StringUtils.hasLength(dbData) ? new AccountNumber(dbData) : null;
        }

    }
}
