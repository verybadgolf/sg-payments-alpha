package gov.scot.payments.model.paymentinstruction.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.model.NotificationEvent;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import lombok.NonNull;
import lombok.Value;

@Value
public class PaymentSubmitFailedEvent implements NotificationEvent<PaymentInstruction> {

    @JsonCreator
    public PaymentSubmitFailedEvent(@JsonProperty("payload") PaymentInstruction payload) {
        this.payload = payload;
    }

    @NonNull
    private final PaymentInstruction payload;

    @Override
    public String getNotificationMessage() {
        return String.format("Error occurred submitting payment %s",payload);
    }
}
