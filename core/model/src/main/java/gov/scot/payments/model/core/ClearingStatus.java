package gov.scot.payments.model.core;

public enum ClearingStatus {

    FailedToClear, Cleared
}
