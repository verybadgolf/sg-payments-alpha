package gov.scot.payments.model.cardpayment;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import gov.scot.payments.model.core.MoneyDeserializer;
import gov.scot.payments.model.core.MoneySerializer;
import gov.scot.payments.model.customer.Service;
import gov.scot.payments.model.inboundpayment.ChannelType;
import gov.scot.payments.model.inboundpayment.InboundPayment;
import lombok.*;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.javamoney.moneta.Money;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode
@TypeDef(name = "jsonb",typeClass = JsonBinaryType.class)
public class CardPayment {

    @NonNull @Id @Builder.Default
    private final UUID id = UUID.randomUUID();
    @JsonDeserialize(using = MoneyDeserializer.class)
    @JsonSerialize(using = MoneySerializer.class)
    @Type(type = "gov.scot.payments.model.core.MoneyType")
    @Columns(columns = {@Column(name = "amount"),@Column(name = "amount_currency")})
    @NonNull private Money amount;
    @NonNull @Builder.Default @Enumerated(EnumType.STRING) private final CardPaymentStatus status = CardPaymentStatus.Created;
    private String statusMessage;
    @NonNull private String service;
    @NonNull private String reference;
    @NonNull @Builder.Default private final LocalDateTime timestamp = LocalDateTime.now();
    @NonNull private String processorId;
    @NonNull private String returnUrl;
    @Type(type = "gov.scot.payments.model.core.MoneyType")
    @Columns(columns = {@Column(name = "expected_cost"),@Column(name = "expected_cost_currency")})
    @JsonDeserialize(using = MoneyDeserializer.class)
    @JsonSerialize(using = MoneySerializer.class)
    @NonNull private Money expectedCost;
    @NonNull @Builder.Default private String createdBy = "application";
    private UUID paymentRef;

    @Type(type = "jsonb")
    private CardPaymentGatewayAuthResponse auth;
    @Type(type = "jsonb")
    private CardPaymentGatewaySubmitResponse submission;

    public InboundPayment toInboundPayment(Service service) {
        return InboundPayment.builder()
                .amount(amount)
                .channelType(ChannelType.Card_Online)
                .channelId(processorId)
                .ref(reference)
                .status(status.toInboundPaymentStatus())
                .statusMessage(statusMessage)
                .timestamp(timestamp)
                .service(this.service)
                .createdBy(createdBy)
                .details(this)
                .kvRef(service.getMetadata())
                .build();
    }

    public CardPayment withAuthResponse(CardPaymentGatewayAuthResponse response) {
        return toBuilder()
                .auth(response)
                .status(response.getStatus())
                .statusMessage(response.getMessage())
                .build();
    }

    public CardPayment withSubmitResponse(CardPaymentGatewaySubmitResponse response) {
        return toBuilder()
                .submission(response)
                .status(response.getStatus())
                .statusMessage(response.getMessage())
                .build();
    }

    public CardPayment withCancelResponse(CardPaymentGatewayCancelResponse response) {
        return toBuilder()
                .status(response.getStatus())
                .statusMessage(response.getMessage())
                .build();
    }

    public CardPaymentGatewaySubmitRequest toSubmissionRequest() {
        return CardPaymentGatewaySubmitRequest.builder()
                .ref(auth.getRef())
                .paymentRef(id)
                .metadata(auth.getMetadata())
                .build();
    }

    public CardPaymentGatewayCancelRequest toCancelRequest() {
        return CardPaymentGatewayCancelRequest.builder()
                .ref(auth.getRef())
                .paymentRef(id)
                .metadata(auth.getMetadata())
                .build();
    }


}
