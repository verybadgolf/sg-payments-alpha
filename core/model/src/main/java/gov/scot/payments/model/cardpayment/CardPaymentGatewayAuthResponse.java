package gov.scot.payments.model.cardpayment;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gov.scot.payments.model.core.MoneyDeserializer;
import gov.scot.payments.model.core.MoneySerializer;
import lombok.*;
import org.javamoney.moneta.Money;

import java.util.HashMap;
import java.util.Map;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode
public class CardPaymentGatewayAuthResponse {

    @NonNull @Builder.Default private CardPaymentStatus status = CardPaymentStatus.Submitted;
    private String message;
    @NonNull private String ref;
    private CardDetails details;
    @JsonDeserialize(using = MoneyDeserializer.class)
    @JsonSerialize(using = MoneySerializer.class)
    private Money amount;
    @NonNull @Builder.Default private Map<String,String> metadata = new HashMap<>();

}
