package gov.scot.payments.model.paymentinstruction;

public class InvalidPaymentFieldException extends Exception {

    public InvalidPaymentFieldException(String errorMessage){
        super(errorMessage);
    }

}
