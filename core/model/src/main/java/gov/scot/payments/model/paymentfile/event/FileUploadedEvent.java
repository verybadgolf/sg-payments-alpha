package gov.scot.payments.model.paymentfile.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.paymentfile.PaymentFile;
import lombok.NonNull;
import lombok.Value;

@Value
public class FileUploadedEvent implements Event<PaymentFile> {

    @JsonCreator
    public FileUploadedEvent(@JsonProperty("payload") PaymentFile payload) {
        this.payload = payload;
    }

    @NonNull
    private final PaymentFile payload;
}
