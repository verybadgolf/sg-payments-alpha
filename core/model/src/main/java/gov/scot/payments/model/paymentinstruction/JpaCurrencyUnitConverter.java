package gov.scot.payments.model.paymentinstruction;

import org.springframework.util.StringUtils;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.persistence.AttributeConverter;

public class JpaCurrencyUnitConverter implements AttributeConverter<CurrencyUnit, String> {

    @Override
    public String convertToDatabaseColumn(CurrencyUnit attribute) {
        return attribute == null ? null : attribute.getCurrencyCode().toUpperCase();
    }

    @Override
    public CurrencyUnit convertToEntityAttribute(String dbData) {
        return StringUtils.hasLength(dbData) ? Monetary.getCurrency(dbData) : null;
    }

}
