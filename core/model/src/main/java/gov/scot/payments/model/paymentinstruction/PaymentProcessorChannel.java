package gov.scot.payments.model.paymentinstruction;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode
@Embeddable
public class PaymentProcessorChannel {

    @NonNull private PaymentChannel paymentChannel;
    @NonNull private String costExpression;
    @NonNull @Builder.Default @Enumerated(EnumType.STRING) private PaymentChannelDirection direction = PaymentChannelDirection.Outbound;

}
