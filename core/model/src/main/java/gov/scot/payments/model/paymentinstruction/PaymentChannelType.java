package gov.scot.payments.model.paymentinstruction;

public enum PaymentChannelType {

    EFT, Cheque, PostalOrder, Card
}
