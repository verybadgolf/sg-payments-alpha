package gov.scot.payments.model.cardpayment;

import lombok.*;

import java.util.HashMap;
import java.util.Map;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode
public class CardPaymentGatewaySubmitResponse {

    @NonNull @Builder.Default private CardPaymentStatus status = CardPaymentStatus.Success;
    private String message;
    @NonNull private String ref;
    @NonNull @Builder.Default private Map<String,String> metadata = new HashMap<>();

}
