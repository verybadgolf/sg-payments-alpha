package gov.scot.payments.model.customer;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gov.scot.payments.model.paymentinstruction.AccountNumber;
import gov.scot.payments.model.paymentinstruction.SortCode;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Builder(toBuilder = true)
@Entity
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode
public class Service implements gov.scot.payments.model.Entity {

    @NonNull @Builder.Default private LocalDateTime createdAt = LocalDateTime.now();
    private String folder;
    private String createdBy;
    private String fileFormat;
    @NonNull @Id private String id;
    @NonNull private String customerId;
    @JsonDeserialize(using = SortCode.SortCodeDeserializer.class)
    @JsonSerialize(using = SortCode.SortCodeSerializer.class)
    @Convert(converter = SortCode.JpaSortCodeConverter.class)
    private SortCode outboundSortCode;
    @JsonDeserialize(using = AccountNumber.AccountNumberDeserializer.class)
    @JsonSerialize(using = AccountNumber.AccountNumberSerializer.class)
    @Convert(converter = AccountNumber.JpaAccountNumberConverter.class)
    private AccountNumber outboundAccount;
    @JsonDeserialize(using = SortCode.SortCodeDeserializer.class)
    @JsonSerialize(using = SortCode.SortCodeSerializer.class)
    @Convert(converter = SortCode.JpaSortCodeConverter.class)
    private SortCode inboundSortCode;
    @JsonDeserialize(using = AccountNumber.AccountNumberDeserializer.class)
    @JsonSerialize(using = AccountNumber.AccountNumberSerializer.class)
    @Convert(converter = AccountNumber.JpaAccountNumberConverter.class)
    private AccountNumber inboundAccount;
    @MapKeyColumn(name="key")
    @Column(name="value")
    @CollectionTable(name="service_metadata", joinColumns=@JoinColumn(name="service_id"))
    @ElementCollection(fetch = FetchType.EAGER) @Singular("addMetadata") private Map<String,String> metadata;

    @ElementCollection(fetch = FetchType.EAGER) @NonNull @Singular("addRule") private List<PaymentValidationRule> paymentValidationRules;

    @Override
    public byte[] getKey() {
        return gov.scot.payments.model.Entity.stringToKey(id);
    }
}
