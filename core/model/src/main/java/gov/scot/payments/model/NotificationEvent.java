package gov.scot.payments.model;

public interface NotificationEvent<T> extends Event<T> {

    String getNotificationMessage();

}
