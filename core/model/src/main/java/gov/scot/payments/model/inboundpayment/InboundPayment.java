package gov.scot.payments.model.inboundpayment;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gov.scot.payments.model.Entity;
import gov.scot.payments.model.core.Clearing;
import gov.scot.payments.model.core.MoneyDeserializer;
import gov.scot.payments.model.core.MoneySerializer;
import gov.scot.payments.model.core.Settlement;
import lombok.*;
import org.javamoney.moneta.Money;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode
public class InboundPayment implements Entity {

    @NonNull @Builder.Default private final UUID id = UUID.randomUUID();
    @JsonDeserialize(using = MoneyDeserializer.class)
    @JsonSerialize(using = MoneySerializer.class)
    @NonNull private Money amount;
    @NonNull private ChannelType channelType;
    @NonNull private String channelId;
    private Object details;
    @NonNull private String ref;
    @NonNull @Builder.Default private final PaymentStatus status = PaymentStatus.New;
    private String statusMessage;
    @NonNull @Builder.Default private final LocalDateTime timestamp = LocalDateTime.now();
    @NonNull private String service;
    private Clearing clearing;
    private Settlement settlement;
    @NonNull @Builder.Default private String createdBy = "application";
    @Singular("addRef") @NonNull private Map<String,String> kvRef;

    @Override
    public byte[] getKey() {
        return Entity.stringToKey(id.toString());
    }
}
