package gov.scot.payments.model.customer;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.time.LocalDateTime;
import java.util.List;

@Builder(toBuilder = true)
@Entity
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode
public class Customer {

    @NonNull @Builder.Default private final LocalDateTime createdAt = LocalDateTime.now();
    private String createdBy;
    @NonNull @Id private String id;
    @NonNull private String name;
    @Transient List<Service> services;


}
