package gov.scot.payments.model.paymentinstruction;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


class AccountNumberTest {

    @Test
    void fromStringTest() throws InvalidPaymentFieldException {
        AccountNumber accountNumber = AccountNumber.fromString("00023458");
        Assertions.assertEquals("00023458", accountNumber.getValue());
    }

    @Test
    void fromStringInvalidLengthTest() {
        var exception = Assertions.assertThrows(InvalidPaymentFieldException.class, () -> AccountNumber.fromString("1002345"));
        Assertions.assertEquals("Invalid account number 1002345 - must be 8 digits", exception.getMessage());
    }

    @Test
    void fromStringInvalidNumberTest()  {
        var exception = Assertions.assertThrows(InvalidPaymentFieldException.class, () -> AccountNumber.fromString("110b2345"));
        Assertions.assertEquals("Invalid account number 110b2345 - must be numeric",exception.getMessage());
    }
}